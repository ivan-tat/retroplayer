#!/bin/bash -e
# Debian packages used: qemu-system-x86.

. ./build.conf

qemu-system-$QEMU_SYSTEM -enable-kvm -m $QEMU_RAM_INSTALL \
-drive file="$BOOT_FILE",index=0,media=disk,format=raw,$BOOT_QEMU_FLAGS \
-drive file="$SWAP_FILE",index=1,media=disk,format=raw,$SWAP_QEMU_FLAGS \
-cdrom "$CDROM_FILE" \
-boot order=d
