program playosci;

uses
    hexdigts,
    startup,
    dos,
    ems,
    string_,
    stdio,
    stdlib,
    i86,
    conio,
    debug,
    dynarray,
    hwowner,
    pic,
    dma,
    vbios,
    c6845,
    vga,
    sbctl,
    musmod,
    mixchn,
    musmodps,
    fillvars,
    musplay;

(*$I defines.pas*)

(*$L playosci.obj*)
procedure playosci_main; far; external;

begin
    playosci_main;
end.
