/* smalls3m.h -- declarations for smalls3m.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _SMALLS3M_H_INCLUDED
#define _SMALLS3M_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

#if LINKER_TPC != 1

void __noreturn _start_c (void);
int main (int argc, const char **argv);

#endif  /* LINKER_TPC != 1 */

void smalls3m_main (void);

/*** Initialization ***/

void init_smalls3m (void);
void done_smalls3m (void);

#endif  /* !_SMALLS3M_H_INCLUDED */
