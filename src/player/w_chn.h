/* w_chn.h -- declarations for w_chn.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _PLAYER_W_CHN_H_INCLUDED
#define _PLAYER_W_CHN_H_INCLUDED 1

#include <stdbool.h>
#include "defines.h"
#include "player/screen.h"

bool win_channels_init (SCRWIN *self);
void win_channels_set_channels (SCRWIN *self, MIXCHNLIST *value);

#endif  /* !_PLAYER_W_CHN_H_INCLUDED */
