/* w_pat.h -- declarations for w_pat.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _PLAYER_W_PAT_H_INCLUDED
#define _PLAYER_W_PAT_H_INCLUDED 1

#include <stdbool.h>
#include "defines.h"
#include "main/mixchn.h"
#include "main/musmod.h"
#include "main/musmodps.h"
#include "main/musplay.h"
#include "player/screen.h"

bool win_pattern_init (SCRWIN *self);
void win_pattern_set_player (SCRWIN *self, MUSPLAYER *value);
void win_pattern_set_track (SCRWIN *self, MUSMOD *value);
void win_pattern_set_play_state (SCRWIN *self, PLAYSTATE *value);
void win_pattern_set_channels (SCRWIN *self, MIXCHNLIST *value);
void win_pattern_set_start_channel (SCRWIN *self, int value);

#endif  /* !_PLAYER_W_PAT_H_INCLUDED */
