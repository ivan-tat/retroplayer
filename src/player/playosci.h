/* playosci.h -- declarations for playosci.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _PLAYOSCI_H_INCLUDED
#define _PLAYOSCI_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

#if LINKER_TPC != 1

void __noreturn _start_c (void);
int main (int argc, const char **argv);

#endif  /* LINKER_TPC != 1 */

void playosci_main (void);

/*** Initialization ***/

void init_playosci (void);
void done_playosci (void);

#endif  /* !_PLAYOSCI_H_INCLUDED */
