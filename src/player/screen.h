/* screen.h -- declarations for screen.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _PLAYER_SCREEN_H_INCLUDED
#define _PLAYER_SCREEN_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

/* Screen window */

#define scrWidth 80
#define scrHeight 25

#pragma pack(push, 1);
typedef struct screen_rect_t
{
    uint8_t x0, y0, x1, y1;
};
#pragma pack(pop);
typedef struct screen_rect_t SCRRECT;

typedef uint16_t WINFLAGS;

// state
#define WINFL_VISIBLE     (1<<0)
#define WINFL_FOCUSED     (1<<1)
// events
#define WINFL_FULLREDRAW  (1<<2)
#define WINFL_FOCUSREDRAW (1<<3)
#define WINFL_REDRAW      (WINFL_FULLREDRAW | WINFL_FOCUSREDRAW)

/* virtual methods table (vmt) */

typedef struct screen_window_t SCRWIN;

#pragma pack(push, 1);
typedef struct screen_window_vmt_t
{
    void (*on_resize) (SCRWIN *self);
    void (*draw) (SCRWIN *self);
    bool (*keypress) (SCRWIN *self, char key);
    void (*free)(SCRWIN *self);
};
#pragma pack(pop);
typedef struct screen_window_vmt_t SCRWINVMT;

/* structure */

#pragma pack(push, 1);
typedef struct screen_window_t
{
    char *__class_name;
    SCRWINVMT __vmt;
    WINFLAGS flags;
    SCRRECT rect;
    void *data;
};
#pragma pack(pop);

void           scrwin_init (SCRWIN *self, char *class_name);
void           scrwin_set_rect (SCRWIN *self, SCRRECT *rect);
void           scrwin_set_coords (SCRWIN *self, uint8_t x0, uint8_t y0, uint8_t x1, uint8_t y1);
void           scrwin_set_width (SCRWIN *self, uint8_t value);
uint8_t        scrwin_get_width (SCRWIN *self);
void           scrwin_set_height (SCRWIN *self, uint8_t value);
uint8_t        scrwin_get_height (SCRWIN *self);
#define        scrwin_on_resize(o) (o)->__vmt.on_resize (o)
void           scrwin_draw (SCRWIN *self);
bool           scrwin_keypress (SCRWIN *self, char key);
void           scrwin_show (SCRWIN *self);
void           scrwin_focus (SCRWIN *self);
void           scrwin_leave (SCRWIN *self);
void           scrwin_close (SCRWIN *self);
#define        scrwin_free(o) (o)->__vmt.free (o)

#endif  /* !_PLAYER_SCREEN_H_INCLUDED */
