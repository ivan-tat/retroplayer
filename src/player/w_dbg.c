/* w_dbg.c -- debug window methods.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stddef.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/conio.h"
#include "cc/stdio.h"
#include "common.h"
#include "main/musins.h"
#include "main/muspat.h"
#include "main/musmod.h"
#include "main/mixer.h"
#include "main/musmodps.h"
#include "main/fillvars.h"
#include "main/musplay.h"
#include "player/screen.h"
#include "player/plays3m.h"
#include "player/w_dbg.h"

/*
void win_debug_on_resize (SCRWIN *self);
*/
void win_debug_draw (SCRWIN *self);
/*
void win_debug_keypress (SCRWIN *self, char key);
*/
void win_debug_free (SCRWIN *self);

static const SCRWINVMT __win_debug_vmt =
{
    /*
    &win_debug_on_resize,
    */
    NULL,
    &win_debug_draw,
    /*
    &win_debug_keypress,
    */
    NULL,
    &win_debug_free
};

/* private data */

typedef struct win_debug_data_t
{
    MUSPLAYER *player;
    MUSMOD *track;
    PLAYSTATE *ps;
};

#define MIXBUF_Y    2
#define MIXBUF_X    3
#define MIXBUF_V    38 + 1

#define DMABUF_Y    2
#define DMABUF_X    49
#define DMABUF_V    74 + 1

#define MEMORY_Y    11
#define MEMORY_X    3
#define MEMORY_V    17 + 1

#define VERSION_X   3
#define VERSION_V   19

/* private methods */

/* public methods */

bool win_debug_init (SCRWIN *self)
{
    struct win_debug_data_t *data;

    scrwin_init (self, "debug window");
    _copy_vmt (self, __win_debug_vmt, SCRWINVMT);
    data = _new (struct win_debug_data_t);
    if (!data)
        return false;
    self->data = (void *) data;
    memset (data, 0, sizeof (struct win_debug_data_t));
    return true;
}

void win_debug_set_player (SCRWIN *self, MUSPLAYER *value)
{
    struct win_debug_data_t *data;

    data = (struct win_debug_data_t *) self->data;
    data->player = value;
}

void win_debug_set_track (SCRWIN *self, MUSMOD *value)
{
    struct win_debug_data_t *data;

    data = (struct win_debug_data_t *) self->data;
    data->track = value;
}

void win_debug_set_play_state (SCRWIN *self, PLAYSTATE *value)
{
    struct win_debug_data_t *data;

    data = (struct win_debug_data_t *) self->data;
    data->ps = value;
}

/*
void win_debug_on_resize (SCRWIN *self)
{
}
*/

void win_debug_draw (SCRWIN *self)
{
    struct win_debug_data_t *data;
    MUSPLAYER *player;
    MUSMOD *track;
    PLAYSTATE *ps;
    PCMSMPLIST *samples;
    MUSPATLIST *patterns;
    MIXER *mixer;
    MIXBUF *mixbuf;
    SNDDMABUF *sndbuf;
    DMABUF *dmabuf;
    unsigned out_channels;
    unsigned out_rate;
    unsigned out_samples_per_channel;
    unsigned out_tick_spc;
    unsigned out_tick_spc_counter;
    void    *out_dma_buf_unaligned;
    void    *out_dma_buf;
    unsigned out_frame_size;
    unsigned out_frames_count;
    unsigned out_frame_last;
    unsigned out_frame_active;
    unsigned out_fps;

    data = (struct win_debug_data_t *) self->data;
    player = data->player;
    track = data->track;
    ps = player_get_play_state (player);
    samples = &track->samples;
    patterns = &track->patterns;

    textbackground (_black);

    if (self->flags & WINFL_FULLREDRAW)
    {
        uint8_t y = scrwin_get_height (self) - 1;

        textcolor (_lightgray);
        clrscr ();

        textcolor (_white);

        gotoxy (MIXBUF_X, MIXBUF_Y); printf ("%s", "Mixing buffer:");
        gotoxy (DMABUF_X, DMABUF_Y); printf ("%s", "DMA buffer:");
        gotoxy (MEMORY_X, MEMORY_Y); printf ("%s", "Memory usage:");
        gotoxy (VERSION_X, y); printf ("%s", "Player version:");

        textcolor (_lightgray);

        gotoxy (MIXBUF_X, MIXBUF_Y + 2); printf ("%s", "Channels:");
        gotoxy (MIXBUF_X, MIXBUF_Y + 3); printf ("%s", "Rate:");
        gotoxy (MIXBUF_X, MIXBUF_Y + 4); printf ("%s", "Samples per channel:");
        gotoxy (MIXBUF_X, MIXBUF_Y + 5); printf ("%s", "Samples:");
        gotoxy (MIXBUF_X, MIXBUF_Y + 6); printf ("%s", "Tick samples per channel:");
        gotoxy (MIXBUF_X, MIXBUF_Y + 7); printf ("%s", "Tick samples per channel left: ");

        gotoxy (DMABUF_X, DMABUF_Y + 2); printf ("%s", "Allocated:");
        gotoxy (DMABUF_X, DMABUF_Y + 3); printf ("%s", "Aligned:");
        gotoxy (DMABUF_X, DMABUF_Y + 4); printf ("%s", "Frame size:");
        gotoxy (DMABUF_X, DMABUF_Y + 5); printf ("%s", "Frames count:");
        gotoxy (DMABUF_X, DMABUF_Y + 6); printf ("%s", "Last frame:");
        gotoxy (DMABUF_X, DMABUF_Y + 7); printf ("%s", "Current frame:");
        gotoxy (DMABUF_X, DMABUF_Y + 8); printf ("%s", "Frames per second:");

        gotoxy (MEMORY_X, MEMORY_Y + 2); printf ("%s", "Samples:        KiB EM");
        gotoxy (MEMORY_X, MEMORY_Y + 3); printf ("%s", "Patterns:       KiB EM");
        gotoxy (MEMORY_X, MEMORY_Y + 4); printf ("%s", "Free:           KiB EM");
        gotoxy (MEMORY_X, MEMORY_Y + 5); printf ("%s", "Free:           KiB DOS");

        textcolor (_yellow);

        if (player_is_EM_in_use (player))
        {
            gotoxy (MEMORY_V - 5, MEMORY_Y + 2); printf ("%5lu", (uint32_t) emsGetHandleSize (samples->handle) << 4);
            gotoxy (MEMORY_V - 5, MEMORY_Y + 3); printf ("%5u", ((uint32_t) emsGetHandleSize (patterns->handle) << 4));
            gotoxy (MEMORY_V - 5, MEMORY_Y + 4); printf ("%5lu", (uint32_t) getFreeEMMMemory () / 1024);
        }
        else
        {
            gotoxy (MEMORY_V - 4, MEMORY_Y + 2); printf ("%s", "none");
            gotoxy (MEMORY_V - 4, MEMORY_Y + 3); printf ("%s", "none");
            gotoxy (MEMORY_V - 4, MEMORY_Y + 4); printf ("%s", "none");
        }

        gotoxy (MEMORY_V - 5, MEMORY_Y + 5); printf ("%5hu",
            (uint16_t) ((uint32_t) getFreeDOSMemory () / 1024));
        gotoxy (VERSION_V, y); printf ("%s", PLAYER_VERSION);
    }

    mixer = player_get_mixer (player);
    mixbuf = mixer->mixbuf;
    sndbuf = player_get_sound_buffer (player);
    dmabuf = sndbuf->buf;
    out_channels = mixbuf->channels;
    out_rate = ps->rate;
    out_samples_per_channel = mixbuf->samples_per_channel;
    out_tick_spc = ps->tick_spc;
    out_tick_spc_counter = ps->tick_spc_counter;
    out_dma_buf_unaligned = dmabuf->unaligned;
    out_dma_buf = dmabuf->data;
    out_frame_size = sndbuf->frameSize;
    out_frames_count = sndbuf->framesCount;
    out_frame_last = sndbuf->frameLast;
    out_frame_active = sndbuf->frameActive;
    out_fps = (long) player_get_output_rate (player) * get_sample_format_width (& (sndbuf->format)) / sndbuf->frameSize;

    textcolor (_yellow);

    gotoxy (MIXBUF_V - 5, MIXBUF_Y + 2); printf ("%5u", out_channels);
    gotoxy (MIXBUF_V - 5, MIXBUF_Y + 3); printf ("%5u", out_rate);
    gotoxy (MIXBUF_V - 5, MIXBUF_Y + 4); printf ("%5u", out_samples_per_channel);
    gotoxy (MIXBUF_V - 5, MIXBUF_Y + 5); printf ("%5u", out_channels * out_samples_per_channel);
    gotoxy (MIXBUF_V - 5, MIXBUF_Y + 6); printf ("%5u", out_tick_spc);
    gotoxy (MIXBUF_V - 5, MIXBUF_Y + 7); printf ("%5u", out_tick_spc_counter);
    gotoxy (DMABUF_V - 9, DMABUF_Y + 2); printf ("%04X:%04X", FP_SEG (out_dma_buf_unaligned), FP_OFF (out_dma_buf_unaligned));
    gotoxy (DMABUF_V - 9, DMABUF_Y + 3); printf ("%04X:%04X", FP_SEG (out_dma_buf), FP_OFF (out_dma_buf));
    gotoxy (DMABUF_V - 5, DMABUF_Y + 4); printf ("%5u", out_frame_size);
    gotoxy (DMABUF_V - 5, DMABUF_Y + 5); printf ("%5u", out_frames_count);
    gotoxy (DMABUF_V - 5, DMABUF_Y + 6); printf ("%5u", out_frame_last);
    gotoxy (DMABUF_V - 5, DMABUF_Y + 7); printf ("%5u", out_frame_active);
    gotoxy (DMABUF_V - 5, DMABUF_Y + 8); printf ("%5u", out_fps);
}

/*
void win_debug_keypress (SCRWIN *self, char key)
{
}
*/

/* free */

void win_debug_free (SCRWIN *self)
{
    if (self->data)
        _delete (self->data);
}
