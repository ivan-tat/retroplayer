/* plays3m.h -- declarations for plays3m.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _PLAYS3M_H_INCLUDED
#define _PLAYS3M_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "main/musmod.h"
#include "main/mixchn.h"
#include "main/musmodps.h"

#if LINKER_TPC != 1

void __noreturn _start_c (void);
int main (int argc, const char **argv);

#endif /* LINKER_TPC != 1 */

uint32_t getFreeDOSMemory(void);
uint32_t getFreeEMMMemory(void);

/* Information windows */

void get_note_name(char *__dest, uint8_t note);

void plays3m_main (void);

/*** Initialization ***/

void init_plays3m (void);
void done_plays3m (void);

#endif  /* !_PLAYS3M_H_INCLUDED */
