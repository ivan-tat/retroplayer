/* plays3m.c -- full featured tracked music player.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#include "cc/pascal.h"
#include "cc/startup.h"
#include "cc/i86.h"
#include "cc/conio.h"
#include "cc/stdio.h"
#include "cc/stdlib.h"
#include "cc/string.h"
#include "cc/malloc.h"
#include "cc/errno.h"
#include "cc/unistd.h"
#include "cc/hw/vbios.h"
#include "dos/ems.h"
#include "common.h"
#include "hw/hwowner.h"
#include "hw/pic.h"
#include "hw/dma.h"
#include "hw/sb/sbctl.h"
#include "hw/vga.h"
#include "main/musins.h"
#include "main/muspat.h"
#include "main/musmod.h"
#include "main/mixchn.h"
#include "main/musmodps.h"
#include "main/effects.h"
#include "main/mixer.h"
#include "main/fillvars.h"
#include "main/musplay.h"
#include "player/screen.h"
#include "player/w_inf.h"
#include "player/w_hlp.h"
#include "player/w_chn.h"
#include "player/w_pat.h"
#include "player/w_ins.h"
#include "player/w_smp.h"
#include "player/w_dbg.h"
#include "player/plays3m.h"

/* Command line's options */

#define DEVSEL_AUTO     1
#define DEVSEL_ENV      2
#define DEVSEL_MANUAL   3

static bool     opt_help;
static char     opt_filename[pascal_String_size];
static uint8_t  opt_devselect;
static uint16_t opt_mode_rate;
static bool     opt_mode_stereo;
static bool     opt_mode_16bits;
static bool     opt_mode_lq;
static bool     opt_dumpconf;
static uint8_t  opt_mvolume;
static bool     opt_st3order;
static uint8_t  opt_startpos;
static bool     opt_loop;
static bool     opt_em;
static uint8_t  opt_fps;

static MUSPLAYER *mp;
static MIXER *mixer;
static MIXERQUALITY mixer_quality;
static MUSMOD  *song_track;

uint32_t getFreeDOSMemory(void)
{
    return _memmax();
}

uint32_t getFreeEMMMemory(void)
{
    if (emsInstalled)
        return (uint32_t) emsGetFreePagesCount() * EM_PAGE_SIZE;
    else
        return 0;
}

static void __near memstats(void)
{
    printf ("Free DOS memory:........%6hu KiB" CRLF,
        (uint16_t) ((uint32_t) getFreeDOSMemory() / 1024));
    clreol();
    printf ("Free expanded memory:...%6lu KiB" CRLF,
        (uint32_t) getFreeEMMMemory() / 1024);
    clreol();
}

static void __near display_errormsg(void)
{
    printf("PLAYER STATUS - ");

    if (player_is_error (mp))
        printf("Error: %s." CRLF, player_get_error (mp));
    else
        printf("No error." CRLF);
}

static void __near display_help(void)
{
    printf(
        "USAGE" CRLF
        "  plays3m [<options>] <filename> [<options>]" CRLF
        CRLF
        "OPTIONS" CRLF
        "Sound card hardware:" CRLF
        "  -env    Use BLASTER environment variable to get sound card configuration." CRLF
        "  -cfg    Input sound card configuration manually." CRLF
        "          Default is to auto-detect." CRLF
        "  -c      Dump sound card configuration after detection." CRLF
        "  -f N    Set frames per second to N Hz for filling output buffer." CRLF
        "          Default is 70." CRLF
        "Output sound format:" CRLF
        "  -s N    Set output sample rate to N in Hz or KHz (4-48 or 4000-48000)." CRLF
        "  -m      Force output channels to 1 (mono mixing)." CRLF
        "          Default is 2 channels (stereo mixing)." CRLF
        "  -8      Force output sample bits to 8." CRLF
        "          Default is 16." CRLF
        "  -lq     Force low quality output mode." CRLF
        "          Default is normal quality." CRLF
        "Sound playback:" CRLF
        "  -v N    Set master volume to N (0-255)." CRLF
        "          Default is 0 (use volume as specified in file)." CRLF
        "  -o      Force to handle patterns order like ST3 does." CRLF
        "          Default is to play all patterns in specified order." CRLF
        "  -b N    Start to play at Nth position in order (0-255)." CRLF
        "          Default is 0." CRLF
        "System:" CRLF
        "  -noems  Disable EMS usage." CRLF
        "Others:" CRLF
        "  -h, -?  Show this help." CRLF
    );
    if (!opt_help)
        printf(CRLF "Error: no filename specified." CRLF);
}

static void __near display_playercfg(void)
{
    player_device_dump_conf (mp);
}

/* Information windows */

static const char _halftone_names[12][2] =
{
    "C-", "C#", "D-", "D#", "E-", "F-", "F#", "G-", "G#", "A-", "A#", "B-"
};

void get_note_name(char *__dest, uint8_t note)
{
    uint8_t halftone, octave;
    bool valid;

    valid = true;
    if (note == CHN_NOTE_NONE)
    {
        __dest[0] = '.';
        __dest[1] = '.';
        __dest[2] = '.';
        __dest[3] = 0;
    }
    else
    if (note == CHN_NOTE_OFF)
    {
        __dest[0] = '^';
        __dest[1] = '^';
        __dest[2] = '^';
        __dest[3] = 0;
    }
    else
    if (note <= CHN_NOTE_MAX)
    {
        halftone = note & 0x0f;
        octave = note >> 4;
        if ((halftone <= 11) && (octave <= 9))
        {
            __dest[0] = _halftone_names[halftone][0];
            __dest[1] = _halftone_names[halftone][1];
            __dest[2] = '0' + octave;
            __dest[3] = 0;
        }
        else
            valid = false;
    }
    else
        valid = false;

    if (!valid)
    {
        __dest[0] = '?';
        __dest[1] = '?';
        __dest[2] = '?';
        __dest[3] = 0;
    }
}

static SCRWIN *win_information = NULL;
static SCRWIN *win_help = NULL;
static SCRWIN *win_channels = NULL;
static SCRWIN *win_pattern = NULL;
static SCRWIN *win_instruments = NULL;
static SCRWIN *win_samples = NULL;
static SCRWIN *win_debug = NULL;

static SCRWIN *__near __window_create (bool (*init) (SCRWIN *self))
{
    SCRWIN *w = _new (SCRWIN);

    if (!w)
    {
        DEBUG_ERR_ ("Failed to allocate %s.", "information window");
        return NULL;
    }

    if (!init (w))
    {
        _delete (w);
        DEBUG_ERR_ ("Failed to initialize %s.", "information window");
        return NULL;
    }

    return w;
}

/* Window's event router */

#define WINLIST_SIZE 7

static SCRWIN *winlist_list[WINLIST_SIZE];
static uint8_t winlist_count;
static uint8_t winlist_selected;    /* current info on screen */

static void __near winlist_add_item (SCRWIN *win)
{
    winlist_list[winlist_count] = win;
    winlist_count++;
}

static bool __near winlist_init (void)
{
    DEBUG_BEGIN ();

    memset (winlist_list, 0, sizeof (winlist_list));
    winlist_count = 0;

    /* Help window */
    win_help = __window_create (&win_help_init);
    if (!win_help)
        return false;
    winlist_add_item (win_help);

    /* Channels window */
    win_channels = __window_create (&win_channels_init);
    if (!win_channels)
        return false;
    winlist_add_item (win_channels);

    /* Pattern window */
    win_pattern = __window_create (&win_pattern_init);
    if (!win_pattern)
        return false;
    winlist_add_item (win_pattern);

    /* Instruments window */
    win_instruments = __window_create (&win_instruments_init);
    if (!win_instruments)
        return false;
    winlist_add_item (win_instruments);

    /* Samples window */
    win_samples = __window_create (&win_samples_init);
    if (!win_samples)
        return false;
    winlist_add_item (win_samples);

    /* Debug window */
    win_debug = __window_create (&win_debug_init);
    if (!win_debug)
        return false;
    winlist_add_item (win_debug);

    /* Information window - must be last in list */
    win_information = __window_create (&win_information_init);
    if (!win_information)
        return false;
    winlist_add_item (win_information);

    DEBUG_SUCCESS ();
    return true;
}

static void __near winlist_on_resize (void)
{
    SCRWIN *win = win_information;
    SCRRECT r;
    int count, i;

    r.x0 = 1;
    r.y0 = 1;
    r.x1 = scrWidth;
    r.y1 = 5;
    scrwin_set_rect (win, &r);
    scrwin_on_resize (win);

    r.x0 = 1;
    r.y0 = 6;
    r.x1 = scrWidth;
    r.y1 = scrHeight;

    count = winlist_count - 1;
    for (i = 0; i < count; i++)
    {
        win = winlist_list[i];
        scrwin_set_rect (win, &r);
        scrwin_on_resize (win);
    }
}

static void __near winlist_select(uint16_t value)
{
    winlist_selected = value;
}

static void __near winlist_show_selected(void)
{
    SCRWIN *win = winlist_list[winlist_selected];

    if (!(win->flags & WINFL_VISIBLE))
        scrwin_show(win);
}

static void __near winlist_hide_selected(void)
{
    SCRWIN *win = winlist_list[winlist_selected];

    if (win->flags & WINFL_VISIBLE)
        scrwin_close(win);
}

static void __near winlist_show_all(void)
{
    scrwin_show (win_information);
    winlist_show_selected();
}

static void __near winlist_hide_all(void)
{
    int i;

    for (i = 0; i < winlist_count; i++)
    {
        SCRWIN *win = winlist_list[i];
        if (win->flags & WINFL_VISIBLE)
            scrwin_close(win);
    }
}

static void __near winlist_refresh_all(void)
{
    int i;

    for (i = 0; i < winlist_count; i++)
    {
        SCRWIN *win = winlist_list[i];
        if (win->flags & WINFL_VISIBLE)
            scrwin_draw(win);
    }
}

static bool __near winlist_keypress(char c)
{
    int i;

    for (i = 0; i < winlist_count; i++)
    {
        SCRWIN *win = winlist_list[i];
        if (win->flags & WINFL_VISIBLE)
            if (scrwin_keypress(win, c))
                return true;
    }

    return false;
}

static void __near winlist_free (void)
{
    int i;

    DEBUG_BEGIN ();
    for (i = 0; i < winlist_count; i++)
    {
        SCRWIN *win = winlist_list[i];

        if (win)
        {
            scrwin_free (win);
            _delete (winlist_list[i]);
        }
    }
    DEBUG_END ();
}

/* channels */

static bool savchn[MUSMOD_CHANNELS_MAX];

static void __near channels_save_all(void)
{
    PLAYSTATE *ps = player_get_play_state (mp);
    MIXCHNLIST *channels = ps->channels;
    uint8_t i, num_channels = mixchnl_get_count (channels);

    for (i = 0; i < num_channels; i++)
    {
        MIXCHN *chn = mixchnl_get (channels, i);

        savchn[i] = (chn->flags & MIXCHNFL_MIXING) ? true : false;
    }
}

static void __near channels_toggle_mixing(uint8_t index)
{
    PLAYSTATE *ps = player_get_play_state (mp);
    MIXCHNLIST *channels = ps->channels;
    MIXCHN *chn = mixchnl_get (channels, index);

    chn->flags ^= MIXCHNFL_MIXING;
}

static void __near channels_stop_all(void)
{
    PLAYSTATE *ps = player_get_play_state (mp);
    MIXCHNLIST *channels = ps->channels;
    uint8_t i, num_channels = mixchnl_get_count (channels);

    for (i = 0; i < num_channels; i++)
    {
        MIXCHN *chn = mixchnl_get (channels, i);

        chn->flags &= ~MIXCHNFL_PLAYING;
    }
}

static void __near cursor_hide(void)
{
    vbios_set_cursor_shape(32, 32);
}

static void __near cursor_show(void)
{
    vbios_set_cursor_shape(15, 16);
}

static void __near desktop_clear(void)
{
    window(1, 1, scrWidth, scrHeight);
    textbackground(_black);
    clrscr();
}

/*** Parsing command line's options ***/

#include "player/opts.inc"

static bool __near assign_opt_mvolume (uint8_t *var, uint8_t value)
{
    if (value > 0)
    {
        *var = value;
        return true;
    }
    else
        return false;
}

static bool __near assign_opt_mode_rate (uint16_t *var, uint16_t value)
{
    if ((value >= 4) && (value <= 48) || (value >= 4000) && (value <= 48000))
    {
        if (value < 100)
            *var = value * 1000;
        else
            *var = value;

        return true;
    }
    else
        return false;
}

static bool __near assign_opt_fps (uint8_t *var, uint8_t value)
{
    if ((value >= 2) && (value <= 200))
    {
        *var = value;
        return true;
    }
    else
        return false;
}

#define OPTS_MAX 14
static const struct optdef_t optsdef[OPTS_MAX] =
{
    {
        .opt = "h",
        .flags = OPT_BOOL | OPT_SET, .var = &opt_help, .name = "opt_help",
        .value._bool = true, ._set._bool = NULL
    },
    {
        .opt = "?",
        .flags = OPT_BOOL | OPT_SET, .var = &opt_help, .name = "opt_help",
        .value._bool = true, ._set._bool = NULL
    },
    {
        .opt = "env",
        .flags = OPT_U8 | OPT_SET, .var = &opt_devselect, .name = "opt_devselect",
        .value._u8 = DEVSEL_ENV, ._set._u8 = NULL
    },
    {
        .opt = "cfg",
        .flags = OPT_U8 | OPT_SET, .var = &opt_devselect, .name = "opt_devselect",
        .value._u8 = DEVSEL_MANUAL, ._set._u8 = NULL
    },
    {
        .opt = "c",
        .flags = OPT_BOOL | OPT_SET, .var = &opt_dumpconf, .name = "opt_dumpconf",
        .value._bool = true, ._set._u8 = NULL
    },
    {
        .opt = "noems",
        .flags = OPT_BOOL | OPT_SET, .var = &opt_em, .name = "opt_em",
        .value._bool = false, ._set._bool = NULL
    },
    {
        .opt = "f",
        .flags = OPT_U8 | OPT_GET, .var = &opt_fps, .name = "opt_fps",
        .value.unused = 0, ._set._u8 = &assign_opt_fps
    },
    {
        .opt = "s",
        .flags = OPT_U16 | OPT_GET, .var = &opt_mode_rate, .name = "opt_mode_rate",
        .value.unused = 0, ._set._u16 = &assign_opt_mode_rate
    },
    {
        .opt = "m",
        .flags = OPT_BOOL | OPT_SET, .var = &opt_mode_stereo, .name = "opt_mode_stereo",
        .value._bool = false, ._set._bool = NULL
    },
    {
        .opt = "8",
        .flags = OPT_BOOL | OPT_SET, .var = &opt_mode_16bits, .name = "opt_mode_16bits",
        .value._bool = false, ._set._bool = NULL
    },
    {
        .opt = "lq",
        .flags = OPT_BOOL | OPT_SET, .var = &opt_mode_lq, .name = "opt_mode_lq",
        .value._bool = true, ._set._bool = NULL
    },
    {
        .opt = "v",
        .flags = OPT_U8 | OPT_GET, .var = &opt_mvolume, .name = "opt_mvolume",
        .value.unused = 0, ._set._u8 = &assign_opt_mvolume
    },
    {
        .opt = "o",
        .flags = OPT_BOOL | OPT_SET, .var = &opt_st3order, .name = "opt_st3order",
        .value._bool = true, ._set._bool = NULL
    },
    {
        .opt = "b",
        .flags = OPT_U8 | OPT_GET, .var = &opt_startpos, .name = "opt_startpos",
        .value.unused = 0, ._set._u8 = NULL
    }
};

static bool __near parse_args (void)
{
    int i;

    for (i = 1; i < _argc; i++)
    {
        const char *s = _argv [i];

        DEBUG_dump_str ("option", s);

        if ((s[0] == '-') && (s[1] != '\0'))
        {
            if (!parse_opt (&i, _argc, _argv, optsdef, OPTS_MAX))
                return false;
        }
        else
        {
            strncpy (opt_filename, s, pascal_String_size);
            DEBUG_dump_str ("opt_filename", opt_filename);
        }
    }

    return true;
}

/*** Shell ***/

static void __near run_os_shell(void)
{
    char *comspec;
    bool result;

    vbios_set_mode(3);  // clear screen
    textbackground(_black);
    textcolor(_lightgray);
    printf("Starting DOS shell... (to return to player use 'exit' command)" CRLF);
    comspec = getenv("COMSPEC");
    result = execv (comspec, NULL);
    vbios_set_mode(3);  // restore text-mode
    cursor_hide();
    if (result)
    {
        while (kbhit())
            getch();
        printf(
            "Error %u (DOS error %u) while running command interpreter." CRLF
            "Press any key to continue...",
            errno, _doserrno
        );
        getch();
    }
}

/*** Main **/

#if LINKER_TPC != 1

void __noreturn _start_c (void)
{
    init_system ();
    init_console ();
    init_delay ();
    _cc_Exit (main (_argc, _argv));
}

int main (int argc, const char **argv)
{
    plays3m_main ();
    return 0;
}

#endif  /* LINKER_TPC != 1 */

void plays3m_main (void)
{
    PLAYSTATE *ps;
    MUSMOD *track;
    MIXCHNLIST *channels;
    SNDDMABUF *sndbuf;
    int pos;
    bool quit;
    char c;

#if LINKER_TPC == 1
    init_system ();
    init_console ();
    init_delay ();
#endif  /* LINKER_TPC == 1 */

    init_debug ();
    init_hwowner ();
    init_pic ();
    init_dma ();
    init_ems ();
    init_vga ();
    init_sbctl ();
    init_musplay ();
    init_plays3m ();

    if (!init_environ())
    {
        printf("Failed to setup DOS environment variables." CRLF);
        exit(1);
    }

    printf(
        "Simple music player for DOS, version %s." CRLF
        "Originally written by Andre Baresel, 1994, 1995." CRLF
        "Modified by Ivan Tatarinov <ivan-tat" "@" "ya" "." "ru>, 2016-2023." CRLF
        "This is free and unencumbered software released into the public domain." CRLF
        "For more information, please refer to <http://unlicense.org>." CRLF,
        PLAYER_VERSION
    );

    opt_help = false;
    opt_filename[0] = 0;
    opt_devselect = DEVSEL_AUTO;
    opt_mode_rate = 48000;
    opt_mode_stereo = true;
    opt_mode_16bits = true;
    opt_mode_lq = false;
    opt_dumpconf = false;
    opt_mvolume = 0;            // use volume from file
    opt_st3order = false;
    opt_startpos = 0;
    opt_loop = false;
    opt_em = true;
    opt_fps = 70;

    if (!parse_args ())
        exit (1);

    if (!strlen(opt_filename))
    {
        display_help();
        exit(1);
    }

    mp = player_new ();
    if (!mp)
    {
        printf ("%s", "Failed to create music player object." CRLF);
        exit (1);
    }

    if (!player_init (mp))
    {
        display_errormsg();
        exit(1);
    }

    player_set_EM_usage (mp, opt_em);

    if (DEBUG_FILE_S3M_LOAD)
    {
        printf ("Before loading:" CRLF);
        memstats ();
    }

    if (!player_load_s3m (mp, opt_filename, &song_track))
    {
        display_errormsg();
        exit(1);
    }

    if (!player_set_active_track (mp, song_track))
    {
        display_errormsg ();
        exit (1);
    }

    if (DEBUG_FILE_S3M_LOAD)
    {
        printf ("After loading:" CRLF);
        memstats ();
    }

    track = song_track;
    ps = player_get_play_state (mp);
    channels = ps->channels;

    printf ("Song \"%s\" loaded (%s)." CRLF,
        track->title,
        track->format);

    if (!player_init_device (mp, SNDDEVTYPE_SB, opt_devselect))
    {
        printf("No sound device found." CRLF);
        exit(1);
    }

    if (opt_dumpconf)
    {
        display_playercfg();
        printf("Press a key to continue...");
        getch();
        printf(CRLF);
    }

    if (opt_mvolume)
        player_set_master_volume (mp, opt_mvolume);

    if (!player_set_mode (mp, opt_mode_16bits, opt_mode_stereo, opt_mode_rate, opt_mode_lq))
    {
        display_errormsg();
        exit(1);
    }

    if (opt_st3order)
        ps->flags |= PLAYSTATEFL_SKIPENDMARK;
    else
        ps->flags &= ~PLAYSTATEFL_SKIPENDMARK;
    if (opt_loop)
        ps->flags |= PLAYSTATEFL_SONGLOOP;
    else
        ps->flags &= ~PLAYSTATEFL_SONGLOOP;
    playstate_setup_patterns_order (ps);
    ps->order_start = opt_startpos;
    player_set_sound_buffer_fps (mp, opt_fps);

    if (!player_init_mixer (mp))
    {
        display_errormsg ();
        exit (1);
    }

    sndbuf = player_get_sound_buffer (mp);

    channels_save_all();

    if (!winlist_init())
    {
        DEBUG_ERR_ ("Failed to initialize %s.", "windows list");
        exit(1);
    }

    winlist_on_resize ();

    win_information_set_player (win_information, mp);
    win_information_set_track (win_information, track);
    win_information_set_play_state (win_information, ps);
    win_pattern_set_player (win_pattern, mp);
    win_pattern_set_track (win_pattern, track);
    win_pattern_set_play_state (win_pattern, ps);
    win_pattern_set_start_channel (win_pattern, 0);
    win_instruments_set_track (win_instruments, track);
    win_instruments_set_page_start (win_instruments, 0);
    win_samples_set_track (win_samples, track);
    win_samples_set_page_start (win_samples, 0);
    win_debug_set_player (win_debug, mp);
    win_debug_set_track (win_debug, track);
    win_debug_set_play_state (win_debug, ps);

    if (!player_play_start (mp))
    {
        display_errormsg();
        exit(1);
    }

    /* FIXME: move these here for now (MIXCHNLIST is allocated in player_play_start() ) */
    win_channels_set_channels (win_channels, channels);
    win_pattern_set_channels (win_pattern, channels);
    win_instruments_set_channels (win_instruments, channels);
    win_samples_set_channels (win_samples, channels);

    cursor_hide();
    desktop_clear();
    winlist_select(0);
    winlist_show_all();

    mixer = player_get_mixer (mp);

    quit = false;
    do
    {
        winlist_refresh_all();

        if (kbhit())
        {
            c = getch();

            if (!winlist_keypress(c))
            {
                if (c == 27)
                {
                    quit = true;
                    c = 0;
                }

                if ((c >= 'x') && (c <= 'x' + 16))
                {
                    channels_toggle_mixing(c - 'x');
                    c = 0;
                }
                if ((c >= 16) && (c <= 19))
                {
                    channels_toggle_mixing(c - 4);
                    c = 0;
                }
                /* F1-F6 */
                if ((c >= 59) && (c <= 64))
                {
                    winlist_hide_selected();
                    winlist_select(c - 59);
                    winlist_show_selected();
                    c = 0;
                }
                if (toupper (c) == 'P')
                {
                    player_play_pause (mp);
                    getch();
                    player_play_continue (mp);
                    c = 0;
                }
                if (c == '+')
                {
                    pos = playstate_find_next_pattern (ps, ps->order, 1);
                    if (pos < 0)
                        ps->flags &= ~PLAYSTATEFL_PLAYING;
                    else
                        playstate_set_pos (ps, pos, 0, true);
                    c = 0;
                }
                if (c == '-')
                {
                    pos = playstate_find_next_pattern (ps, ps->order, -1);
                    if (pos < 0)
                        ps->flags &= ~PLAYSTATEFL_PLAYING;
                    else
                    {
                        playstate_set_pos (ps, pos, 0, false);
                        channels_stop_all ();
                    }
                    c = 0;
                }
                if (toupper (c) == 'L')
                {
                    ps->flags ^= PLAYSTATEFL_SONGLOOP;
                    c = 0;
                }
                if (toupper (c) == 'F')
                {
                    mixer_quality = mixer->quality + 1;
                    if (mixer_quality > MIXQ_MAX)
                        mixer_quality = 0;
                    mixer->quality = mixer_quality;
                    c = 0;
                }
                if (toupper (c) == 'D')
                {
                    winlist_hide_all();
                    run_os_shell();
                    desktop_clear();
                    winlist_show_all();
                }
            }
        }
    }
    while ((!(sndbuf->flags & SNDDMABUFFL_SLOW)) && (!quit) && (ps->flags & PLAYSTATEFL_PLAYING));

    textbackground(_black);
    textcolor(_lightgray);
    clrscr();
    cursor_show();

    if (sndbuf->flags & SNDDMABUFFL_SLOW)
        DEBUG_ERR ("PC is too slow");

    player_free (mp);
    player_delete (&mp);

    if (DEBUG)
    {
        printf ("After all:" CRLF);
        memstats ();
    }
}

/*** Initialization ***/

void init_plays3m (void)
{
    DEBUG_BEGIN ();
    mp = NULL;
    cc_atexit ((void *)done_plays3m);
    DEBUG_END ();
}

void done_plays3m (void)
{
    DEBUG_BEGIN ();
    winlist_free();
    if (mp)
    {
        player_free (mp);
        player_delete (&mp);
    }
    DEBUG_END ();
}
