/* plays3m.c -- full featured tracked music player.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#include "cc/pascal.h"
#include "cc/startup.h"
#include "cc/stdio.h"
#include "cc/stdlib.h"
#include "cc/string.h"
#include "cc/conio.h"
#include "cc/hw/bios.h"
#include "cc/hw/vbios.h"
#include "cc/hw/c6845.h"
#include "hw/hwowner.h"
#include "hw/pic.h"
#include "hw/dma.h"
#include "hw/sb/sbctl.h"
#include "hw/vga.h"
#include "main/musmod.h"
#include "main/mixchn.h"
#include "main/musmodps.h"
#include "main/fillvars.h"
#include "main/musplay.h"
#include "playosci.h"

#define def_rate 45454
#define def_stereo false
#define def_16bits false
#define def_lq false
#define def_skip_end_mark true
#define def_song_loop true

#define COL_BACKGROUND 1
#define COL_FOREGROUND 14

static uint16_t opt_rate = 0;
static bool opt_stereo = false;
static bool opt_16bits = false;
static bool opt_lq = false;
static char opt_filename[pascal_String_size] = { 0 };
static BIOS_data_area_t *bios_info;
static uint16_t vid_port;
static void *vid_buf;
static void *bufdata = NULL;
static uint8_t scr[2][320] = { 0 };

static MUSPLAYER *mp;
static MUSMOD *song_track;
static SNDDMABUF *sndbuf;

static void __near draw_channels_volume(void)
{
    PLAYSTATE *ps;
    MIXCHNLIST *channels;
    int16_t i;

    ps = player_get_play_state (mp);
    channels = ps->channels;
    for (i = 0; i < mixchnl_get_count (channels); i++)
    {
        MIXCHN *chn = mixchnl_get (channels, i);
        vga_bar (vid_buf,
            320 * 170 + i * 15 + 10, 10,
            (chn->flags & MIXCHNFL_PLAYING) ? chn->eff_volume : 0);
    }
}

static void __near get_current_sample1(int16_t *v)
{
    uint16_t pos;
    uint16_t value;
    void *p;

    pos = player_get_buffer_pos (mp);
    if (opt_16bits)
    {
        p = &((uint16_t *)bufdata)[pos >> 1];
        value = *(uint16_t *)p;
    }
    else
    {
        p = &((uint8_t *)bufdata)[pos];
        value = (*(uint8_t *)p) << 8;
    }
    if (opt_16bits) // FIXME: signed
        *v = value;
    else
        *v = value - 0x8000;
}

static void __near get_current_sample2(int16_t *v)
{
    uint16_t pos;
    uint16_t values[2];
    void *p;

    pos = player_get_buffer_pos (mp);
    if (opt_16bits)
    {
        p = &((uint16_t *)bufdata)[(pos >> 1) & 0xfffe];
        values[0] = ((uint16_t *)p)[0];
        values[1] = ((uint16_t *)p)[1];
    }
    else
    {
        p = &((uint8_t *)bufdata)[pos & 0xfffe];
        values[0] = ((uint8_t *)p)[0] << 8;
        values[1] = ((uint8_t *)p)[1] << 8;
    }
    if (opt_16bits) // FIXME: signed
    {
        v[0] = values[0];
        v[1] = values[1];
    }
    else
    {
        v[0] = values[0] - 0x8000;
        v[1] = values[1] - 0x8000;
    }
}

static void __near update_osci_mono(void)
{
    int16_t i, s;

    get_current_sample1 (&s);
    s = (s >> 9) + 64;
    for (i = 0; i < 318; i++)
    {
        vga_line (
            vid_buf,
            i,      (200 - 128) / 2 + scr[0][i],
            i + 1,  (200 - 128) / 2 + scr[0][i + 1],
            COL_BACKGROUND
        );
        scr[0][i] = s;
        get_current_sample1 (&s);
        s = (s >> 9) + 64;
        vga_line (
            vid_buf,
            i,      (200 - 128) / 2 + scr[0][i],
            i + 1,  (200 - 128) / 2 + s,
            COL_FOREGROUND
        );
    }
    scr[0][319] = s;
}

static void __near update_osci_stereo(void)
{
    int16_t i, s[2];

    get_current_sample2 (s);
    s[0] = (s[0] >> 9) + 64;
    s[1] = (s[1] >> 9) + 64;
    for (i = 0; i < 318; i++)
    {
        vga_line (
            vid_buf,
            i,      (200 - 128) / 2 + scr[0][i],
            i + 1,  (200 - 128) / 2 + scr[0][i + 1],
            COL_BACKGROUND
        );
        vga_line (
            vid_buf,
            i,      (200 - 128) / 2 + 64 + scr[1][i],
            i + 1,  (200 - 128) / 2 + 64 + scr[1][i + 1],
            COL_BACKGROUND
        );
        scr[0][i] = s[0];
        scr[1][i] = s[1];
        get_current_sample2 (s);
        s[0] = (s[0] >> 9) + 64;
        s[1] = (s[1] >> 9) + 64;
        vga_line (
            vid_buf,
            i,      (200 - 128) / 2 + scr[0][i],
            i + 1,  (200 - 128) / 2 + s[0],
            COL_FOREGROUND
        );
        vga_line (
            vid_buf,
            i,      (200 - 128) / 2 + 64 + scr[1][i],
            i + 1,  (200 - 128) / 2 + 64 + s[1],
            COL_FOREGROUND
        );
    }
    scr[0][319] = s[0];
    scr[1][319] = s[1];
}

#if LINKER_TPC != 1

void __noreturn _start_c (void)
{
    init_system ();
    init_console ();
    init_delay ();
    _cc_Exit (main (_argc, _argv));
}

int main (int argc, const char **argv)
{
    playosci_main ();
    return 0;
}

#endif  /* LINKER_TPC != 1 */

void playosci_main (void)
{
    PLAYSTATE *ps;

#if LINKER_TPC == 1
    init_system ();
    init_console ();
    init_delay ();
#endif  /* LINKER_TPC == 1 */

    init_debug ();
    init_hwowner ();
    init_pic ();
    init_dma ();
    init_ems ();
    init_vga ();
    init_sbctl ();
    init_musplay ();
    init_playosci ();

    if (!init_environ ())
    {
        printf ("Failed to setup DOS environment variables." CRLF);
        return;
    }
    textbackground (_black);
    textcolor (_lightgray);
    clrscr ();

    printf (
        "Simple music player with oscillator for DOS, version %s." CRLF
        "Originally written by Andre Baresel, 1994, 1995." CRLF
        "Modified by Ivan Tatarinov <ivan-tat" "@" "ya" "." "ru>, 2016-2023." CRLF
        "This is free and unencumbered software released into the public domain." CRLF
        "For more information, please refer to <http://unlicense.org>." CRLF,
        PLAYER_VERSION
    );

    opt_rate = def_rate;
    opt_stereo = def_stereo;
    opt_16bits = def_16bits;
    opt_lq = def_lq;

    bios_info = get_BIOS_data_area_ptr ();
    vid_port = bios_info->video_3D4_port;
    vid_buf = MK_FP (SegA000, 0);

    strncpy (opt_filename, _argv[1], pascal_String_size);

    if (!strlen(opt_filename))
    {
        printf ("No filename specified." CRLF);
        exit (1);
    }

    mp = player_new ();
    if (!mp)
    {
        printf ("%s", "Failed to create music player object." CRLF);
        exit (1);
    }

    if (!player_init (mp))
    {
        printf ("Failed to initialize player." CRLF);
        exit (1);
    }

    if (!player_load_s3m (mp, opt_filename, &song_track))
    {
        printf ("Failed to load file." CRLF);
        exit (1);
    }

    printf ("Loaded '%s' (%s)" CRLF,
        song_track->title,
        song_track->format
    );

    if (!player_set_active_track (mp, song_track))
    {
        printf ("Failed to set active track." CRLF);
        exit (1);
    }

    if (!player_init_device (mp, SNDDEVTYPE_SB, SNDDEVSETMET_ENV))
    {
        printf ("Failed to initialize sound device." CRLF);
        exit (1);
    }

    if (!player_set_mode (mp, opt_16bits, opt_stereo, opt_rate, opt_lq))
        exit (1);

    if (!player_init_mixer (mp))
        exit (1);

    sndbuf = player_get_sound_buffer (mp);
    ps = player_get_play_state (mp);

    if (def_skip_end_mark)
        ps->flags |= PLAYSTATEFL_SKIPENDMARK;
    else
        ps->flags &= ~PLAYSTATEFL_SKIPENDMARK;
    if (def_song_loop)
        ps->flags |= PLAYSTATEFL_SONGLOOP;
    else
        ps->flags &= ~PLAYSTATEFL_SONGLOOP;
    playstate_setup_patterns_order (ps);

    if (!player_play_start (mp))
        exit (1);

    printf (
        "DMA buffer frame size: %u" CRLF
        "Stop playing and exit with <ESC>" CRLF
        "Press any key to switch to oscillator..." CRLF,
        sndbuf->frameSize
    );
    getch ();

    vbios_set_mode (0x13);
    vga_clear_page_320x200x8 (vid_buf, COL_BACKGROUND);
    bufdata = sndbuf->buf->data;
    while (!kbhit ())
    {
        c6845_wait_vsync (vid_port, true);
        _enable ();
        draw_channels_volume ();
        if (opt_stereo)
            update_osci_stereo ();
        else
            update_osci_mono ();
    }
    while (kbhit ()) getch ();
    vbios_set_mode (3);
    player_free (mp);
    player_delete (&mp);
}

/*** Initialization ***/

void init_playosci (void)
{
    DEBUG_BEGIN ();
    mp = NULL;
    cc_atexit ((void *)done_playosci);
    DEBUG_END ();
}

void done_playosci (void)
{
    DEBUG_BEGIN ();
    if (mp)
    {
        player_free (mp);
        player_delete (&mp);
    }
    DEBUG_END ();
}
