/* opts.inc - options parsing routines.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#define OPT_TYPE_SHIFT  0
#define OPT_TYPE_MASK   (3 << OPT_TYPE_SHIFT)
#define OPT_BOOL        (0 << OPT_TYPE_SHIFT)
#define OPT_U8          (1 << OPT_TYPE_SHIFT)
#define OPT_U16         (2 << OPT_TYPE_SHIFT)
#define OPT_ACT_SHIFT   7
#define OPT_ACT_MASK    (1 << OPT_ACT_SHIFT)
#define OPT_SET         (0 << OPT_ACT_SHIFT)
#define OPT_GET         (1 << OPT_ACT_SHIFT)

#pragma pack(push, 1);
typedef struct optdef_t
{
    const char *opt;
    uint16_t flags;
    const char *name;
    void *var;
    union
    {
        bool _bool;
        uint8_t _u8;
        uint16_t _u16;
        uint16_t unused;    /* biggest data type */
    } value;
    union
    {
        bool __near (*_bool) (bool *var, bool value);
        bool __near (*_u8) (uint8_t *var, uint8_t value);
        bool __near (*_u16) (uint16_t *var, uint16_t value);
    } _set;
};
#pragma pack(pop);

static const struct optdef_t *__near find_opt (const char *opt, const struct optdef_t *opts, int max)
{
    int i;

    for (i = 0; i < max; i++)
        if (strcmp (opts[i].opt, opt) == 0)
            if (opt[strlen (opts[i].opt)] == '\0')
                return &opts[i];

    return NULL;
}

static bool __near read_value (long *var, const char *name, long min, long max, const char *s)
{
    char *endptr;
    long value;

    errno = 0;
    value = strtol (s, &endptr, 10);

    if (errno)
    {
        printf ("Invalid number \"%s\" given for option \"%s\"." CRLF, s, name);
        return false;
    }

    if ((value < min) || (value > max))
    {
        printf ("Value \"%s\" is out of range [%ld; %ld] for option \"%s\"." CRLF,
            s, (long) min, (long) max, name);
        return false;
    }

    *var = value;
    return true;
}

static bool __near set_opt (const struct optdef_t *opt)
{
    switch (opt->flags & OPT_TYPE_MASK)
    {
    case OPT_BOOL:
        if (opt->_set._bool)
            return opt->_set._bool ((bool *) opt->var, opt->value._bool);
        else
        {
            *(bool *) opt->var = opt->value._bool;
            return true;
        }

    case OPT_U8:
        if (opt->_set._u8)
            return opt->_set._u8 ((uint8_t *) opt->var, opt->value._u8);
        else
        {
            *(uint8_t *) opt->var = opt->value._u8;
            return true;
        }

    case OPT_U16:
        if (opt->_set._u16)
            return opt->_set._u16 ((uint16_t *) opt->var, opt->value._u16);
        else
        {
            *(uint16_t *) opt->var = opt->value._u16;
            return true;
        }

    default:
        DEBUG_INFO_ ("Unknown type %hu.", (uint16_t) opt->flags & OPT_TYPE_MASK);
        printf ("Internal failure." CRLF);
        return false;
    }
}

static bool __near get_opt (const struct optdef_t *opt, const char *s)
{
    union
    {
        bool _bool;
        uint8_t _u8;
        uint16_t _u16;
        long _long;
    } value;

    switch (opt->flags & OPT_TYPE_MASK)
    {
    case OPT_BOOL:
        if (!read_value (&value._long, opt->name, 0, 1, s))
            return false;

        if (opt->_set._bool)
            return opt->_set._bool ((bool *) opt->var, value._bool);
        else
        {
            *(bool *) opt->var = value._bool;
            return true;
        }

    case OPT_U8:
        if (!read_value (&value._long, opt->name, 0, 0xff, s))
            return false;

        if (opt->_set._u8)
            return opt->_set._u8 ((uint8_t *) opt->var, value._u8);
        else
        {
            *(uint8_t *) opt->var = value._u8;
            return true;
        }

    case OPT_U16:
        if (!read_value (&value._long, opt->name, 0, 0xffff, s))
            return false;

        if (opt->_set._u16)
            return opt->_set._u16 ((uint16_t *) opt->var, value._u16);
        else
        {
            *(uint16_t *) opt->var = value._u16;
            return true;
        }

    default:
        DEBUG_INFO_ ("Unknown type %hu.", (uint16_t) opt->flags & OPT_TYPE_MASK);
        printf ("Internal failure." CRLF);
        return false;
    }
}

static bool __near parse_opt (int *i, int argc, const char **argv, const struct optdef_t *opts, int max)
{
    const char *s = argv[*i];
    const struct optdef_t *o = find_opt (s + 1, opts, max);

    if (o)
    {
        switch (o->flags & OPT_ACT_MASK)
        {
        case OPT_SET:
            return set_opt (o);

        case OPT_GET:
            (*i)++;

            if (*i == _argc)
            {
                printf ("Missing argument for option \"%s\"." CRLF, s);
                return false;
            }

            return get_opt (o, _argv[*i]);

        default:
            DEBUG_INFO_ ("Unknown action type %hu.", (uint16_t) o->flags & OPT_ACT_MASK);
            printf ("Internal failure." CRLF);
            return false;
        }
    }
    else
    {
        printf ("Unknown option \"%s\"." CRLF, s);
        return false;
    }
}
