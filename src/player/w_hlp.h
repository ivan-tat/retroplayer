/* w_hlp.h -- declarations for w_hlp.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _PLAYER_W_HLP_H_INCLUDED
#define _PLAYER_W_HLP_H_INCLUDED 1

#include <stdbool.h>
#include "defines.h"
#include "player/screen.h"

bool win_help_init (SCRWIN *self);

#endif  /* !_PLAYER_W_HLP_H_INCLUDED */
