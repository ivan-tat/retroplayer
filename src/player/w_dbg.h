/* w_dbg.h -- declarations for w_dbg.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _PLAYER_W_DBG_H_INCLUDED
#define _PLAYER_W_DBG_H_INCLUDED 1

#include <stdbool.h>
#include "defines.h"
#include "player/screen.h"

bool win_debug_init (SCRWIN *self);
void win_debug_set_player (SCRWIN *self, MUSPLAYER *value);
void win_debug_set_track (SCRWIN *self, MUSMOD *value);
void win_debug_set_play_state (SCRWIN *self, PLAYSTATE *value);

#endif  /* !_PLAYER_W_DBG_H_INCLUDED */
