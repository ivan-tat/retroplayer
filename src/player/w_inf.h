/* w_inf.h -- declarations for w_inf.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _PLAYER_W_INF_H_INCLUDED
#define _PLAYER_W_INF_H_INCLUDED 1

#include <stdbool.h>
#include "defines.h"
#include "main/musmod.h"
#include "main/musmodps.h"
#include "main/musplay.h"
#include "player/screen.h"

bool win_information_init (SCRWIN *self);
void win_information_set_player (SCRWIN *self, MUSPLAYER *value);
void win_information_set_track (SCRWIN *self, MUSMOD *value);
void win_information_set_play_state (SCRWIN *self, PLAYSTATE *value);

#endif  /* !_PLAYER_W_INF_H_INCLUDED */
