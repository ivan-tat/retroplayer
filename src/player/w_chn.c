/* w_chn.c -- channels window methods.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stddef.h>
#include <stdint.h>
#include "defines.h"
#include "cc/conio.h"
#include "cc/stdio.h"
#include "cc/string.h"
#include "common.h"
#include "main/mixer.h"
#include "main/mixchn.h"
#include "main/effects.h"
#include "main/effinfo.h"
#include "player/screen.h"
#include "player/plays3m.h"
#include "player/w_chn.h"

void win_channels_on_resize (SCRWIN *self);
void win_channels_draw (SCRWIN *self);
/*
void win_channels_keypress (SCRWIN *self, char key);
*/
void win_channels_free (SCRWIN *self);

static const SCRWINVMT __win_channels_vmt =
{
    &win_channels_on_resize,
    &win_channels_draw,
    /*
    &win_channels_keypress,
    */
    NULL,
    &win_channels_free
};

/* private data */

typedef struct win_channels_data_t
{
    MIXCHNLIST *channels;
};

static const char CHANTYPES[3] =
{
    '-', 'P', 'A'
};

/* private methods */

/* public methods */

bool win_channels_init (SCRWIN *self)
{
    struct win_channels_data_t *data;

    scrwin_init (self, "channels list window");
    _copy_vmt (self, __win_channels_vmt, SCRWINVMT);
    data = _new (struct win_channels_data_t);
    if (!data)
        return false;
    self->data = (void *) data;
    memset (data, 0, sizeof (struct win_channels_data_t));
    return true;
}

void win_channels_set_channels (SCRWIN *self, MIXCHNLIST *value)
{
    struct win_channels_data_t *data;

    data = (struct win_channels_data_t *) self->data;
    data->channels = value;
}

void win_channels_on_resize (SCRWIN *self)
{
}

void win_channels_draw(SCRWIN *self)
{
    #define _BUF_SIZE 40

    if (self)
    {
        struct win_channels_data_t *data = (struct win_channels_data_t *) self->data;
        MIXCHNLIST *channels = data->channels;
        uint8_t i, count;

        textbackground(_black);

        if (self->flags & WINFL_FULLREDRAW)
        {
            textcolor(_white);
            clrscr();
            printf("Ch T Flags Pan Ins Note  Period  Step   Vol Effect");
        }

        textcolor(_lightgray);
        i = 0;
        count = 0;
        while (i < mixchnl_get_count (channels))
        {
            MIXCHN *chn = mixchnl_get (channels, i);
            MIXCHNTYPE type = chn->type;
            MIXCHNFLAGS flags = chn->flags;

            if  ((type == MIXCHNTYPE_PCM) &&  (mixchnl_get_count (channels) <= 16))
            {
                char flagsstr[6];
                char panstr[4];
                char notestr[4];
                char buf[_BUF_SIZE];
                count++;

                flagsstr[0] = '-';
                flagsstr[1] = '-';
                flagsstr[2] = '-';
                flagsstr[3] = '-';
                flagsstr[4] = '-';
                flagsstr[5] = 0;
                if (flags & MIXCHNFL_ENABLED)
                    flagsstr[0] = 'e';
                if (flags & MIXCHNFL_PLAYING)
                    flagsstr[1] = 'p';
                if (flags & MIXCHNFL_MIXING)
                    flagsstr[2] = 'm';
                if (chn->bSmpFlags & PLAYSMPFL_LOOP)
                    flagsstr[3] = 'l';
                if (chn->bEffFlags & EFFFLAG_CONTINUE)
                    flagsstr[4] = 'c';

                snprintf (panstr, 4, "%+3hhi", chn->pan - MIXCHNPAN_CENTER);

                if (chn->flags & MIXCHNFL_ENABLED)
                    get_note_name (notestr, chn->note);
                else
                {
                    notestr[0] = '.';
                    notestr[1] = '.';
                    notestr[2] = '.';
                    notestr[3] = 0;
                }

                if (chn->bCommand && (chn->bCommand <= MAXEFF))
                    chn_effGetName (chn, buf, _BUF_SIZE);
                else
                    buf[0] = 0;

                gotoxy(1, 2 + i);
                printf ("%2hhu %c %s %s %3hhu [%s] %5hu %4hX.%04hX %2hhu %s",
                    (uint8_t) i+1,
                    (char) CHANTYPES[type],
                    flagsstr,
                    panstr,
                    (uint8_t) chn->instrument_num,
                    notestr,
                    (uint16_t) chn->wSmpPeriod,
                    (uint16_t) (chn->dSmpStep >> 16),
                    (uint16_t) chn->dSmpStep,
                    (uint8_t) chn->eff_volume,
                    buf
                );
                clreol();
            }
            i++;
        }

        if (!count)
            printf("No channels to mix.");
    }
    #undef _BUF_SIZE
}

/*
void win_channels_keypress (SCRWIN *self, char key)
{
}
*/

/* free */

void win_channels_free (SCRWIN *self)
{
    if (self->data)
        _delete (self->data);
}
