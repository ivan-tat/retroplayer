/* w_smp.h -- declarations for w_smp.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _PLAYER_W_SMP_H_INCLUDED
#define _PLAYER_W_SMP_H_INCLUDED 1

#include <stdbool.h>
#include "defines.h"
#include "main/musmod.h"
#include "main/mixchn.h"
#include "player/screen.h"

bool win_samples_init (SCRWIN *self);
void win_samples_set_track (SCRWIN *self, MUSMOD *value);
void win_samples_set_channels (SCRWIN *self, MIXCHNLIST *value);
void win_samples_set_page_start (SCRWIN *self, int value);

#endif  /* !_PLAYER_W_SMP_H_INCLUDED */
