/* dstream.h -- declarations for dstream.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_DSTREAM_H_INCLUDED
#define _CC_DSTREAM_H_INCLUDED 1

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

/* Data stream */

typedef uint16_t datastream_flags_t;
typedef datastream_flags_t DATASTREAMFLAGS;

#define DSFLAG_DIRECT   0
#define DSFLAG_BUFFER   (1 << 0)
#define DSFLAG_SYNC     (1 << 1)
#define DSFLAG_STOP     (1 << 2)
#define DSFLAG_ERROR    (1 << 3)

typedef struct datastream_t DATASTREAM;
typedef struct datastream_t
{
    DATASTREAMFLAGS flags;
    bool (*m_flush) (DATASTREAM *self);
    /*size_t*/unsigned limit;       /* direct mode only */
    /*size_t*/unsigned buf_size;    /* buffer */
    char *buf;                      /* buffer */
    /*size_t*/unsigned pos;         /* buffer */
    /*size_t*/unsigned written;
    void *output;       /* parameter to user flush() method */
};

void datastream_init (DATASTREAM *self, DATASTREAMFLAGS flags);
bool datastream_write (DATASTREAM *self, const void *ptr, /*size_t*/unsigned len);
bool datastream_flush (DATASTREAM *self);

#endif  /* !_CC_DSTREAM_H_INCLUDED */
