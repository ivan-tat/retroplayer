/* dscon.h -- part of custom "dstream" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_DSCON_H_INCLUDED
#define _CC_DSCON_H_INCLUDED 1

#include <stdbool.h>
#include "defines.h"
#include "cc/dstream.h"
#include "cc/stdio.h"

void consolestream_init (DATASTREAM *self, /*size_t*/unsigned size, void *buf);

#endif /* !defined _CC_DSCON_H_INCLUDED */
