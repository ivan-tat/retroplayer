/* errno.c -- part of custom "errno" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdint.h>
#include "defines.h"
#include "cc/errno.h"

#if DEFINE_LOCAL_DATA == 1

int16_t cc_errno = EZERO;

#endif  /* DEFINE_LOCAL_DATA == 1 */
