(* i86.pas -- Pascal declarations for custom "i86" library.

   This file is for linking compiled object files with Pascal linker.
   It will be deleted in future when we rewrite the project in C.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. *)

unit i86;

interface

(*$I defines.pas*)

(*$ifdef DEFINE_LOCAL_DATA*)

var
    _cc_delay_base_ticks: array [0..7] of Byte;

(*$endif*)  (* DEFINE_LOCAL_DATA *)

(*$ifndef USE_INTRINSICS*)
procedure IS_FARPTR_BELOW;
procedure IS_FARPTR_NOT_ABOVE;
procedure IS_FARPTR_ABOVE;
procedure IS_FARPTR_NOT_BELOW;
procedure FARPTR_TO_LONG;
procedure FARPTR_FROM_LONG;
procedure FARPTR_ADD_LONG;
procedure FARPTR_DIST;
(*$endif*)  (* !USE_INTRINSICS *)

procedure cc_init_delay;
procedure cc_delay;
procedure cc_intr;
procedure _cc_DoINTR;

implementation

uses
    watcom,
    sysdbg;

(*$ifndef USE_INTRINSICS*)
(*$L i86\ptr.obj*)
procedure IS_FARPTR_BELOW; external;
procedure IS_FARPTR_NOT_ABOVE; external;
procedure IS_FARPTR_ABOVE; external;
procedure IS_FARPTR_NOT_BELOW; external;
(*$L i86\asm\farptr.obj*)
procedure FARPTR_TO_LONG; external;
procedure FARPTR_FROM_LONG; external;
procedure FARPTR_ADD_LONG; external;
procedure FARPTR_DIST; external;
(*$endif*)  (* !USE_INTRINSICS *)

(*$L i86\delay.obj*)
procedure cc_init_delay; external;
procedure cc_delay; external;

(*$L i86\intr.obj*)
procedure cc_intr; external;

(*$L i86\dointr.obj*)
procedure _cc_DoINTR; external;

end.
