/* ints.h -- declarations for ints.asm.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _STARTUP_INTS_H_INCLUDED
#define _STARTUP_INTS_H_INCLUDED

#include <stdint.h>
#include "defines.h"
#include "cc/startup.h"

/*
 * Division by zero signal handler.
 * Interrupt is called by hardware.
 */
extern void _cc_local_int0_asm (void);

/*
 * Ctrl-Break signal handler.
 * Interrupt is called by DOS.
 */
extern void _cc_local_int23_asm (void);

#endif  /* !_STARTUP_INTS_H_INCLUDED */
