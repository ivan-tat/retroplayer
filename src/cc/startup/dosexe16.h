/* dosexe16.h -- declarations for dosexe16.asm.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _STARTUP_DOSEXE16_H_INCLUDED
#define _STARTUP_DOSEXE16_H_INCLUDED 1

#include <stdint.h>

void _start_asm (void);
extern void _start_c (void);

#endif  /* !_STARTUP_DOSEXE16_H_INCLUDED */
