/* dstream.c -- simple data stream handling library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/string.h"
#include "cc/dstream.h"

/*** Data stream ***/

void datastream_init (DATASTREAM *self, DATASTREAMFLAGS flags)
{
    memset(self, 0, sizeof (DATASTREAM));
    self->flags = flags;
}

bool datastream_write (DATASTREAM *self, const void *ptr, /*size_t*/unsigned len)
{
    bool ok;

    if ((self->flags & DSFLAG_STOP) == 0)
    {
        ok = true;

        if (self->flags & DSFLAG_BUFFER)
        {
            /* buffered */
            const char *input = ptr;
            /*size_t*/unsigned maxsize = self->buf_size;

            do
            {
                /*size_t*/unsigned left = maxsize - self->pos;

                if (left > 0)
                {
                    /*size_t*/unsigned __n = len;

                    if (__n > left)
                        __n = left;
                    memcpy(&self->buf[self->pos], input, __n);
                    input += __n;
                    self->pos += __n;
                    len -= __n;
                }

                if (self->pos == maxsize)
                    ok = datastream_flush (self);

            } while (len && ((self->flags & DSFLAG_STOP) == 0));
        }
        else
            /* direct */
            if (self->output)
            {
                char *output = (char *) self->output + self->pos;

                if (self->limit)
                {
                    /*size_t*/unsigned left = self->limit - self->written;

                    if (len > left)
                        len = left;
                }
                memcpy (output, ptr, len);
                self->pos += len;
                self->written += len;
                if (self->limit)
                    if (self->written == self->limit)
                        self->flags |= DSFLAG_STOP;
            }
    }
    else
        ok = false;

    return ok;
}

bool datastream_flush (DATASTREAM *self)
{
    bool ok = true;

    if (self->pos)
    {
        /* we do use flush() in buffered mode only */
        if (self->flags & DSFLAG_BUFFER)
        {
            /*size_t*/unsigned size = self->pos;

            ok = self->m_flush (self);
            if (ok)
                self->written += size;
            else
                self->flags |= DSFLAG_STOP | DSFLAG_ERROR;
        }
        /* zero position */
        self->pos = 0;
    }

    return ok;
}
