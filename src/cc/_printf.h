/* _printf.h -- declarations for _printf.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC__PRINTF_H_INCLUDED
#define _CC__PRINTF_H_INCLUDED 1

#include <stdbool.h>
#include <stdarg.h>
#include "defines.h"
#include "cc/dstream.h"

bool _printf (DATASTREAM *stream, const char *format, va_list ap);

#endif  /* !_CC__PRINTF_H_INCLUDED */
