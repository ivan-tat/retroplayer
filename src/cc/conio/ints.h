/* ints.h -- declarations for ints.asm.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_CONIO_INTS_H_INCLUDED
#define _CC_CONIO_INTS_H_INCLUDED

#include "defines.h"

/*
 * Ctrl-Break keyboard signal handler.
 * Interrupt is called by BIOS.
 */
extern void _cc_local_int1b_asm (void);

#endif  /* !_CC_CONIO_INTS_H_INCLUDED */
