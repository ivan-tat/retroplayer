/* vprintf.c -- part of custom "stdio" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"

#include <stdarg.h>
#include "cc/dstream.h"
#include "cc/dscon.h"
#include "cc/_printf.h"
#include "cc/stdio.h"

#define BUFSIZE 128

int cc_vprintf(const char *format, va_list ap)
{
    char buf[BUFSIZE];
    DATASTREAM ds;
    consolestream_init (&ds, BUFSIZE, buf);
    ds.flags |= DSFLAG_SYNC;
    _printf (&ds, format, ap);
    return ds.written;
}
