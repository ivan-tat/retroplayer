/* fprintf.c -- part of custom "stdio" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdarg.h>
#include "defines.h"
#include "cc/dstream.h"
#include "cc/dsfile.h"
#include "cc/_printf.h"
#include "cc/stdio.h"

#define BUFSIZE 128

int cc_fprintf(FILE *stream, const char *format, ...)
{
    va_list ap;
    char buf[BUFSIZE];
    DATASTREAM ds;
    va_start(ap, format);
    filestream_init (&ds, BUFSIZE, buf, stream);
    ds.flags |= DSFLAG_SYNC;
    _printf (&ds, format, ap);
    va_end(ap);
    return ds.written;
}
