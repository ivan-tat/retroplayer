/* vsprintf.c -- part of custom "stdio" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdarg.h>
#include "defines.h"
#include "cc/dstream.h"
#include "cc/dsmem.h"
#include "cc/_printf.h"
#include "cc/stdio.h"

int cc_vsprintf(char *str, const char *format, va_list ap)
{
    DATASTREAM ds;
    memorystream_init (&ds, 0, str);
    _printf (&ds, format, ap);
    str[ds.written] = 0;
    return ds.written;
}
