/* dsmem.c -- part of custom "dstream" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include "defines.h"
#include "cc/dstream.h"
#include "cc/dsmem.h"

bool memorystream_flush (DATASTREAM *self);

void memorystream_init (DATASTREAM *self, /*size_t*/unsigned limit, void *ptr)
{
    datastream_init (self, DSFLAG_DIRECT);
    self->m_flush = &memorystream_flush;
    self->limit = limit;
    self->output = ptr;
}

bool memorystream_flush (DATASTREAM *self)
{
    return false; /* TODO: not implemented */
}
