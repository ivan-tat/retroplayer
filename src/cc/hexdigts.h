/* hexdigts.h -- declarations for hexdigts.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _HEXDIGTS_H_INCLUDED
#define _HEXDIGTS_H_INCLUDED

#include "defines.h"

extern const char HEXDIGITS[16];

#endif  /* !_HEXDIGTS_H_INCLUDED */
