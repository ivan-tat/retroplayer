/* i86.h -- declarations for custom "i86" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_I86_H_INCLUDED
#define _CC_I86_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

typedef void __near *NEARPTR;
typedef void __far *FARPTR;

/* intr structs */

struct CC_REGPACKB
{
    unsigned char al, ah;
    unsigned char bl, bh;
    unsigned char cl, ch;
    unsigned char dl, dh;
};
#define REGPACKB CC_REGPACKB

struct CC_REGPACKW
{
    unsigned short ax;
    unsigned short bx;
    unsigned short cx;
    unsigned short dx;
    unsigned short bp;
    unsigned short si;
    unsigned short di;
    unsigned short ds;
    unsigned short es;
    unsigned int flags;
};
#define REGPACKW CC_REGPACKW

union CC_REGPACK
{
    struct REGPACKB h;
    struct REGPACKW w;
};
#define REGPACK CC_REGPACK

/* bits defined for flags field defined in REGPACKW */

enum
{
    CC_INTR_CF = 0x0001, /* carry */
    CC_INTR_PF = 0x0004, /* parity */
    CC_INTR_AF = 0x0010, /* auxiliary carry */
    CC_INTR_ZF = 0x0040, /* zero */
    CC_INTR_SF = 0x0080, /* sign */
    CC_INTR_TF = 0x0100, /* trace */
    CC_INTR_IF = 0x0200, /* interrupt */
    CC_INTR_DF = 0x0400, /* direction */
    CC_INTR_OF = 0x0800  /* overflow */
};

#define INTR_CF CC_INTR_CF
#define INTR_PF CC_INTR_PF
#define INTR_AF CC_INTR_AF
#define INTR_ZF CC_INTR_ZF
#define INTR_SF CC_INTR_SF
#define INTR_TF CC_INTR_TF
#define INTR_IF CC_INTR_IF
#define INTR_DF CC_INTR_DF

/* Private */
extern uint64_t _cc_delay_base_ticks;

/* Public */
extern void cc_init_delay (void);
extern void cc_delay(unsigned int __milliseconds);

#ifdef __WATCOMC__

extern void __far __watcall cc_intr(int, union CC_REGPACK *);
#pragma aux cc_intr "*" parm [ ax ] [ bx cx ] modify [ ax bx cx dx es ];

void _cc_disable(void);
#pragma aux _cc_disable = \
    "cli" \
    parm nomemory \
    modify nomemory exact [];

void _cc_enable(void);
#pragma aux _cc_enable = \
    "sti" \
    parm nomemory \
    modify nomemory exact [];

#pragma intrinsic(_cc_disable, _cc_enable);

#else

extern void cc_intr(int, union CC_REGPACK *);

extern void _cc_disable(void);
extern void _cc_enable(void);

#endif  /* __WATCOMC__ */

/*** Aliases ***/

#define init_delay cc_init_delay
#define delay cc_delay
#define intr cc_intr

#define _disable _cc_disable
#define _enable _cc_enable

/* macros to break 'far' pointers into segment and offset components */

#ifndef FP_OFF
 #define FP_OFF(__p) ((unsigned)(__p))
#endif  /* !FP_OFF */

#ifndef FP_SEG
 #define FP_SEG(__p) ((unsigned)((unsigned long)(FARPTR)(__p) >> 16))
#endif  /* !FP_SEG */

/* make a far pointer from segment and offset */

#ifndef MK_FP
 #define MK_FP(__s,__o) (((unsigned short)(__s)):>((NEARPTR)(__o)))
#endif  /* !MK_FP */

/* far pointer comparisons */
#define _IS_FARPTR_BELOW(a, b)      ((FP_SEG (a) < FP_SEG (b)) || ((FP_SEG (a) == FP_SEG (b)) && (FP_OFF (a) <  FP_OFF (b))))
#define _IS_FARPTR_NOT_ABOVE(a, b)  ((FP_SEG (a) < FP_SEG (b)) || ((FP_SEG (a) == FP_SEG (b)) && (FP_OFF (a) <= FP_OFF (b))))
#define _IS_FARPTR_ABOVE(a, b)      ((FP_SEG (a) > FP_SEG (b)) || ((FP_SEG (a) == FP_SEG (b)) && (FP_OFF (a) >  FP_OFF (b))))
#define _IS_FARPTR_NOT_BELOW(a, b)  ((FP_SEG (a) > FP_SEG (b)) || ((FP_SEG (a) == FP_SEG (b)) && (FP_OFF (a) >= FP_OFF (b))))

/* far pointer conversions */
#define _FARPTR_TO_LONG(v)      ((uint32_t) FP_SEG (v) * 16 + (uint32_t) FP_OFF (v))
#define _FARPTR_FROM_LONG(v)    MK_FP ((v) / 16, (v) & 15)

/* far pointer arithmetics */
#define _FARPTR_ADD_LONG(a, b)  _FARPTR_FROM_LONG (_FARPTR_TO_LONG (a) + (b))
#define _FARPTR_DIST(a, b)      (_FARPTR_TO_LONG (a) - _FARPTR_TO_LONG (b))

#if USE_INTRINSICS != 0
# define IS_FARPTR_BELOW(a, b)      _IS_FARPTR_BELOW (a, b)
# define IS_FARPTR_NOT_ABOVE(a, b)  _IS_FARPTR_NOT_ABOVE (a, b)
# define IS_FARPTR_ABOVE(a, b)      _IS_FARPTR_ABOVE (a, b)
# define IS_FARPTR_NOT_BELOW(a, b)  _IS_FARPTR_NOT_BELOW (a, b)
# define FARPTR_TO_LONG(v)          _FARPTR_TO_LONG (v)
# define FARPTR_FROM_LONG(v)        _FARPTR_FROM_LONG (v)
# define FARPTR_ADD_LONG(a, b)      _FARPTR_ADD_LONG (a, b)
# define FARPTR_DIST(a, b)          _FARPTR_DIST (a, b)
#else   /* USE_INTRINSICS == 0 */
bool     IS_FARPTR_BELOW (const FARPTR a, const FARPTR b);
bool     IS_FARPTR_NOT_ABOVE (const FARPTR a, const FARPTR b);
bool     IS_FARPTR_ABOVE (const FARPTR a, const FARPTR b);
bool     IS_FARPTR_NOT_BELOW (const FARPTR a, const FARPTR b);
uint32_t FARPTR_TO_LONG (const FARPTR v);
FARPTR   FARPTR_FROM_LONG (uint32_t v);
FARPTR   FARPTR_ADD_LONG (const FARPTR a, int32_t b);
int32_t  FARPTR_DIST (const FARPTR a, const FARPTR b);
#endif  /* USE_INTRINSICS == 0 */

#endif  /* !_CC_I86_H_INCLUDED */
