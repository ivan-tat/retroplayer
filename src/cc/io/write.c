/* write.c -- part of custom "io" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stddef.h>
#include <stdint.h>
#include "defines.h"
#include "cc/dos.h"
#include "cc/io.h"

/*size_t*/int32_t cc_write (int fd, const void __far *buf, /*size_t*/unsigned count)
{
    uint16_t actual;

    return _cc_dos_write(fd, buf, count, &actual) ? -1 : actual;
}
