/* startup.h -- declarations for startup.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _STARTUP_H_INCLUDED
#define _STARTUP_H_INCLUDED

#include <stdint.h>
#include "defines.h"
#include "cc/dos.h"

/* File I/O */

// File handle
#define cc_UnusedHandle 0

// File mode
#define cc_fmClosed 0xD7B0
#define cc_fmInput  0xD7B1
#define cc_fmOutput 0xD7B2
#define cc_fmInOut  0xD7B3

typedef struct _cc_iobuf_t;
typedef struct _cc_iobuf_t _cc_iobuf;

typedef int cc_inoutres_t;
typedef cc_inoutres_t inoutres_t;

typedef cc_inoutres_t _cc_iobuf_proc_t (_cc_iobuf *f);

#define __IO_OPEN   0
#define __IO_IN_OUT 1
#define __IO_FLUSH  2
#define __IO_CLOSE  3

#pragma pack(push, 1);
typedef struct _cc_iobuf_t {
    uint16_t handle;
    uint16_t mode;
    uint16_t buf_size;
    //char private_data[26];
    uint16_t private;
    uint16_t buf_pos;
    uint16_t buf_end;
    void __far *buf_ptr;
    union {
        _cc_iobuf_proc_t *by_index[4];
        struct {
            _cc_iobuf_proc_t *open;
            _cc_iobuf_proc_t *in_out;
            _cc_iobuf_proc_t *flush;
            _cc_iobuf_proc_t *close;
        } by_name;
    } io;
    char user_data[16];
    char name[cc_PathStr_size];
};
#pragma pack(pop);

// cc_InOutRes variable
#define EINOUTRES_SUCCESS 0
#define EINOUTRES_READ 100
#define EINOUTRES_WRITE 101
#define EINOUTRES_NOT_ASSIGNED 102
#define EINOUTRES_NOT_OPENED 103
#define EINOUTRES_NOT_INPUT 104
#define EINOUTRES_NOT_OUTPUT 105
#define EINOUTRES_NOT_NUMBER 106

/* Global variables */

#define STDINBUF_SIZE 128
#define STDOUTBUF_SIZE 128

extern uint16_t     _cc_psp;
extern uint16_t     _cc_argc;
extern const char *__far *_cc_argv;
extern void __far  *cc_ErrorAddr;
extern int16_t      cc_ExitCode;
extern inoutres_t   cc_InOutRes;
extern uint8_t      cc_Test8086;
extern _cc_iobuf    cc_Input;
extern uint8_t      cc_InputBuf[STDINBUF_SIZE];
extern _cc_iobuf    cc_Output;
extern uint8_t      cc_OutputBuf[STDOUTBUF_SIZE];

/* Internal variables */
#define _CC_ATEXIT_MAX 32
extern uint8_t _cc_ExitCount;
extern void *_cc_ExitList[_CC_ATEXIT_MAX];

//void _cc_on_exit(void);   // internal

void __noreturn __far __cdecl _cc_ExitWithError (int16_t status, void __far *addr);
void __noreturn _cc_Exit (int16_t status);

/* System unit */

inoutres_t cc_IOResult (void);

extern void __far _cc_CheckInOutRes (void);

/* (+|-)InOutRes(+|-): prefix "+" means IO state check is done on enter, postfix "+" - on exit. */

/* Buffered file I/O */

void       cc_TextAssign (_cc_iobuf *f, void __far *buffer, uint16_t size, const char *name); /* -InOutRes- */
void       cc_TextSetTextBuf (_cc_iobuf *f, void __far *buffer, uint16_t size); /* -InOutRes- */
void       cc_TextReset (_cc_iobuf *f);                     /* -InOutRes- */
void       cc_TextRewrite (_cc_iobuf *f);                   /* -InOutRes- */
void       cc_TextAppend (_cc_iobuf *f);                    /* -InOutRes- */
inoutres_t cc_TextFlush (_cc_iobuf *f);                     /* -InOutRes+ */
inoutres_t cc_TextClose (_cc_iobuf *f);                     /* -InOutRes+ */
inoutres_t cc_TextEOL (_cc_iobuf *f);                       /* +InOutRes+ */
unsigned   cc_TextReadString (_cc_iobuf *f, char *dest, uint16_t max); /* +InOutRes? */
char       cc_TextReadChar (_cc_iobuf *f);                  /* +InOutRes+ */
int32_t    cc_TextReadInteger (_cc_iobuf *f);               /* -InOutRes+ */
inoutres_t cc_TextWriteString (_cc_iobuf *f, char *str, uint16_t padding); /* +InOutRes+ */
inoutres_t cc_TextWriteChar (_cc_iobuf *f, char _c, uint16_t padding); /* +InOutRes+ */
inoutres_t cc_TextWriteInteger (_cc_iobuf *f, uint32_t value, uint16_t padding); /* +InOutRes+ */
inoutres_t cc_TextWriteLn (_cc_iobuf *f);                   /* +InOutRes+ */
inoutres_t cc_TextSync (_cc_iobuf *f);                      /* +InOutRes+ */

/* System */

void __noreturn __far __cdecl _cc_local_int0 (void __far *addr, uint16_t flags);
void __noreturn __far __cdecl _cc_local_int23 (void __far *addr, uint16_t flags);

/* Application startup */

void cc_init_system (void);

/*** Aliases ***/

#define _psp _cc_psp
#define _argc _cc_argc
#define _argv _cc_argv
#define init_system cc_init_system
#define _checkinoutres _cc_CheckInOutRes

#endif  /* !_STARTUP_H_INCLUDED */
