/* dosret.h -- part of custom "dos" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_DOS_DOSRET_H_INCLUDED
#define _CC_DOS_DOSRET_H_INCLUDED 1

#include "defines.h"

unsigned __cc_set_errno_dos(unsigned code);

#endif  /* !_CC_DOS_DOSRET_H_INCLUDED */
