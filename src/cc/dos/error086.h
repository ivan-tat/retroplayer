/* error086.h -- part of custom "dos" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_DOS_ERROR086_H_INCLUDED
#define _CC_DOS_ERROR086_H_INCLUDED 1

#include "defines.h"
#include "cc/i86.h"

unsigned __cc_doserror (union CC_REGPACK *regs);
unsigned __cc_doserror2 (void);

#endif  /* !_CC_DOS_ERROR086_H_INCLUDED */
