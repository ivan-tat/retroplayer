/* ex.h -- part of custom "dos" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_DOS_EX_H_INCLUDED
#define _CC_DOS_EX_H_INCLUDED 1

#include <stdint.h>
#include "defines.h"
#include "cc/dos.h"

extern uint16_t __cdecl __far _cc_dos_exec_asm (const char __far *path, struct cc_dos_execparam_t __far *param);

#endif  /* !_CC_DOS_EX_H_INCLUDED */
