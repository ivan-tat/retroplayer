/* int24.h -- declarations for int24.asm.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_DOS_INT24_H_INCLUDED
#define _CC_DOS_INT24_H_INCLUDED

#include "defines.h"

/*
 * DOS critical error handler.
 * Interrupt is called by DOS.
 */
extern void _cc_local_int24_asm (void);

#endif  /* !_CC_DOS_INT24_H_INCLUDED */
