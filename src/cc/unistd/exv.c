/* exv.c -- part of custom "dos" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#if LINKER_TPC
# include "cc/pascal.h"
#endif /* LINKER_TPC */
#include "cc/startup.h" /* _cc_psp */
#include "cc/i86.h"
#include "cc/dos.h"
#include "cc/errno.h"
#include "cc/string.h"
#include "cc/sysdbg.h"
#include "cc/unistd.h"

/*
 * Description:
 *      This is not really an "execv()" but more like a "system()" method.
 *      It stops execution of current program (but background tasks are still
 *      working if any) and runs a specified one with arguments. After it
 *      finished the main program is continued.
 */
int cc_execv (const char *filename, char *const argv[])
{
    char cmdline[cc_ComStr_size];
    /*size_t*/unsigned len = 0;
    int status;

    if (argv)
    {
        int n = 0;
        char *arg;

        while ((arg = argv[n]) && (len < cc_ComStr_size - 2))
        {
            /*size_t*/unsigned part = strlen (arg);

            if (part)
            {
                bool space;

                if (len)
                {
                    space = true;
                    part++;
                }
                else
                    space = false;

                if (part > cc_ComStr_size - 2 - len)
                    part = cc_ComStr_size - 2 - len;

                if (space)
                {
                    cmdline [len] = ' ';
                    len++;
                    part--;
                }

                if (part)
                {
                    memcpy (cmdline + len, arg, part);
                    len += part;
                }
            }
            n++;
        }
    }
    cmdline [len] = 0;

    /* Pascal SwapVectors() must be the last call before executing. */
    cc_dos_swapvectors ();
#if LINKER_TPC == 1
    pascal_swapvectors ();
#endif  /* LINKER_TPC == 1 */
    status = _cc_dos_exec (
        ((struct cc_dospsp_t __far *) MK_FP (_cc_psp, 0))->env_seg, filename, cmdline);
#if LINKER_TPC == 1
    pascal_swapvectors ();
#endif  /* LINKER_TPC == 1 */
    cc_dos_swapvectors ();
    return status != 0 ? -1 : 0;
}
