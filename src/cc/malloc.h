/* malloc.h -- declarations for custom "malloc" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_MALLOC_H_INCLUDED
#define _CC_MALLOC_H_INCLUDED 1

#include <stdint.h>
#include "defines.h"

uint32_t _memmax(void);

#endif  /* !_CC_MALLOC_H_INCLUDED */
