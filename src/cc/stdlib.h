/* stdlib.h -- declarations for custom "stdlib" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_STDLIB_H_INCLUDED
#define _CC_STDLIB_H_INCLUDED 1

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include "defines.h"
#include "cc/startup.h"
#include "cc/errno.h"

/* _cc_psp is defined in startup.h */

/* atexit() */
#define CC_ATEXIT_MAX _CC_ATEXIT_MAX

/** Heap **/

#define CC_ECRIT_HEAP_OVERFLOW_ERROR 203
#define CC_ECRIT_INVALID_POINTER_OPERATION 204

#define CC_EHEAP_ABORT  0
#define CC_EHEAP_IGNORE 1
#define CC_EHEAP_RETRY  2

#pragma pack(push, 1);
typedef struct cc_heap_free_rec_t
{
    struct cc_heap_free_rec_t __far *next;
    uint32_t size;
};  /* 8 bytes */
#pragma pack(pop);

extern void __far *cc_heap_org;
extern void __far *cc_heap_ptr;
extern void __far *cc_heap_end;
extern struct cc_heap_free_rec_t cc_heap_free_rec;

#define cc_heap_free_list cc_heap_free_rec.next
#define cc_heap_free_zero cc_heap_free_rec.size

/* Returns CC_EHEAP_* constant */
extern char (*cc_heap_error) (uint32_t);

/* Environment variables */
extern char __far *__far *cc_environ;

int cc_atexit (void (*function)());

int  cc_atoi(const char *src);
long cc_atol(const char *src);

long cc_strtol(const char *nptr, char **endptr, int base);

#define CC_RAND_MAX 32767
int cc_rand (void);
void cc_srand (unsigned seed);

/* Heap */
uint32_t cc_memavail (void);
uint32_t cc_maxavail (void);
void __far *cc_malloc (uint32_t size);
void  cc_free (void __far *ptr);
#if SYSDEBUG
char __far *SYSDEBUG_dump_heap_ptr (char __far *s, void __far *ptr);
#else   /* !SYSDEBUG */
#define SYSDEBUG_dump_heap_ptr(s, ptr) ((char __far *) 0)
#endif  /* !SYSDEBUG */

void cc_exit(int status);

/* Environment variables */
char __far *cc_getenv (const char *name);
int16_t cc_unsetenv(const char *name);
int16_t cc_setenv(const char *name, const char *value, int16_t overwrite);

/*** Initialization ***/

bool cc_init_heap (uint16_t size_min_paras, uint16_t size_max_paras);
bool cc_init_environ (void);

/*** Aliases ***/

#define ATEXIT_MAX CC_ATEXIT_MAX

#define ECRIT_HEAP_OVERFLOW_ERROR CC_ECRIT_HEAP_OVERFLOW_ERROR
#define ECRIT_INVALID_POINTER_OPERATION CC_ECRIT_INVALID_POINTER_OPERATION

#define EHEAP_ABORT  CC_EHEAP_ABORT
#define EHEAP_IGNORE CC_EHEAP_IGNORE
#define EHEAP_RETRY  CC_EHEAP_RETRY

#define heap_org    cc_heap_org
#define heap_ptr    cc_heap_ptr
#define heap_end    cc_heap_end
#define heap_error  cc_heap_error
#define heap_free_list cc_heap_free_list

#define environ cc_environ

#define atexit cc_atexit

#define atoi cc_atoi
#define atol cc_atol

#define strtol cc_strtol

#define RAND_MAX CC_RAND_MAX
#define rand cc_rand
#define srand cc_srand

#define memavail cc_memavail
#define maxavail cc_maxavail
#define malloc cc_malloc
#define free   cc_free

#define exit cc_exit

#define getenv cc_getenv
#define unsetenv cc_unsetenv
#define setenv cc_setenv

#define init_heap cc_init_heap
#define init_environ cc_init_environ

#endif  /* !_CC_STDLIB_H_INCLUDED */
