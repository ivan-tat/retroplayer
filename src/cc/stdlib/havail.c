/* havail.c -- part of custom "stdlib" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/stdlib.h"
#include "cc/stdlib/heap.h"
#include "cc/stdlib/havail.h"

/*
 * Used by memavail() and maxavail().
 */
uint32_t _cc_heap_avail_func (uint32_t (*function) (uint32_t size, struct cc_heap_free_rec_t __far *rec, void __far *endp))
{
    return function (FARPTR_DIST (cc_heap_end, cc_heap_ptr),
        (struct cc_heap_free_rec_t __far *) cc_heap_free_list, cc_heap_ptr);
}
