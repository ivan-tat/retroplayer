/* ffree.c -- part of custom "stdlib" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#if LINKER_TPC
# include "cc/pascal.h"
#endif /* LINKER_TPC */
#include "cc/sysdbg.h"
#include "cc/i86.h"
#include "cc/stdlib.h"
#include "cc/stdlib/heap.h"

#if !LINKER_TPC

static void __near _freeblk (
    struct cc_heap_free_rec_t __far * __far *cur,
    struct cc_heap_free_rec_t __far * __far *next
)
{
    struct cc_heap_free_rec_t __far *a;

    a = *cur;
    *cur = *next;
    *next = FARPTR_ADD_LONG (*next, (*next)->size);
    if (a == *next)
    {
        if (a == cc_heap_ptr)
            cc_heap_ptr = *cur;
        else
        {
            (*cur)->next = a->next;
            (*cur)->size += a->size;
        }
    }
    else
        (*cur)->next = a;
}

/*
 * Returns "false" on success, "true" on error.
 */
static bool __near _free (void __far *ptr, uint32_t size)
{
    char s[3][20];  /* SYSDEBUG */

    struct cc_heap_free_rec_t __far *p, __far *cur, __far *prev;

    SYSDEBUG_INFO_ (
        "start=%s, size=%lu, end=%s",
        SYSDEBUG_dump_heap_ptr (s[0], ptr),
        (uint32_t) size,
        SYSDEBUG_dump_heap_ptr (s[1], FARPTR_ADD_LONG (ptr, size - 1))
    );
    if (!size) return false;
    p = ptr;
    // p is a block to be freed
    if (CC_BAD_FP_BITS_FOR_HEAP (p)
    ||  IS_FARPTR_BELOW (p, cc_heap_org)
    ||  IS_FARPTR_NOT_BELOW (p, cc_heap_ptr))
    {
        SYSDEBUG_ERR ("p is invalid");
        return true;
    }
    cur = &cc_heap_free_rec;
    while (true)
    {
        prev = cur;
        cur = cur->next;
        SYSDEBUG_INFO_ (
            "prev=%s, cur=%s, p=%s",
            SYSDEBUG_dump_heap_ptr (s[0], prev),
            SYSDEBUG_dump_heap_ptr (s[1], cur),
            SYSDEBUG_dump_heap_ptr (s[2], p)
        );
        if (p == cur)
        {
            SYSDEBUG_ERR ("ERROR: p==cur");
            return true;
        }
        if (IS_FARPTR_BELOW (p, cur))
        {
            SYSDEBUG_INFO ("OK: p < cur");
            SYSDEBUG_INFO ("_freeblk(&cur,&p)...");
            p->size = CC_ROUND_SIZE_FOR_HEAP (size);
            _freeblk (&cur, &p);
            SYSDEBUG_INFO ("_freeblk(&cur,&prev)...");
            _freeblk (&cur, &prev);
            SYSDEBUG_INFO ("ok");
            return false;
        }
    }
}

#endif /* !LINKER_TPC */

void cc_free (void __far *ptr)
{
    char s[2][20];  /* SYSDEBUG */
    void __far *data;
    uint32_t size;

    data = FARPTR_ADD_LONG (ptr, - (uint32_t) sizeof (uint32_t));
    size = *((uint32_t __far *) data);
    SYSDEBUG_INFO_ (
        "start=%s, size=%lu, end=%s",
        SYSDEBUG_dump_heap_ptr (s[0], data),
        (uint32_t) size,
        SYSDEBUG_dump_heap_ptr (s[1], FARPTR_ADD_LONG (data, size - 1))
    );
#if LINKER_TPC == 1
    pascal_freemem (data, size);
#else   /* LINKER_TPC != 1 */
    _free (data, size);
#endif  /* LINKER_TPC != 1 */
}
