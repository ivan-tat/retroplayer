/* _env.h -- part of custom "stdlib" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_STDLIB_ENV_H_INCLUDED
#define _CC_STDLIB_ENV_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

#pragma pack(push, 1);
typedef struct dosenvlist_t
{
    char __far *arr;
    uint16_t size;
};
#pragma pack(pop);

#pragma pack(push, 1);
typedef struct envstrlist_t
{
    char __far * __far *arr;
    uint16_t size;  /* count including terminating NULL */
};
#pragma pack(pop);

/* Global variables */

extern struct dosenvlist_t _dos_env;
extern struct envstrlist_t _env_list;

/* DOS environment */

uint16_t _dosenv_find_end (const struct dosenvlist_t *self, uint16_t *count);
char __far *_dosenv_find (const struct dosenvlist_t *self, const char *name, uint16_t nlen);

/* Internal environment variables list */

/* Returns zero (false) on success */
bool _env_name_check(const char *name, uint16_t *nlen);

/* Publics */

/*
// defined in "stdlib.h":
bool cc_init_environ (void);
*/
bool rebuild_environ (void);

#endif  /* !_CC_STDLIB_ENV_H_INCLUDED */
