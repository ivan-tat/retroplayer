/* havail.h -- part of custom "stdlib" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_STDLIB_HAVAIL_H_INCLUDED
#define _CC_STDLIB_HAVAIL_H_INCLUDED 1

#include <stdint.h>
#include "defines.h"

uint32_t _cc_heap_avail_func (uint32_t (*function) (uint32_t size, struct cc_heap_free_rec_t __far *rec, void __far *endp));

#endif  /* !_CC_STDLIB_HAVAIL_H_INCLUDED */
