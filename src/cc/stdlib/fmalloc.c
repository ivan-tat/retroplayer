/* fmalloc.c -- part of custom "stdlib" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#if LINKER_TPC
# include "cc/pascal.h"
#endif /* LINKER_TPC */
#include "cc/startup.h"
#include "cc/sysdbg.h"
#include "cc/i86.h"
#include "cc/stdlib.h"
#include "cc/stdlib/heap.h"

#if !LINKER_TPC

/* Sets "err" to "false" on success, or "true" on error. */
static void __far *__near _alloc (uint32_t size, bool *err)
{
    uint32_t orig_size, alloc_size;
    struct cc_heap_free_rec_t __far *p, __far *prev, __far *_new, __far *_next;

    if (!size)
    {
        *err = false;
        return NULL;
    }
    orig_size, alloc_size = size;

    while (true)
    {
        alloc_size = CC_ROUND_SIZE_FOR_HEAP (alloc_size);
        p = &cc_heap_free_rec;

        while (true)
        {
            prev = p;       // p[i-1].ptr
            p = p->next;    // p[i  ].ptr
            if (p == cc_heap_ptr) break;
            if (alloc_size <= p->size)
            {
                _next = p->next;    // p[i+1].ptr
                if (alloc_size != p->size)
                {
                    _new = FARPTR_ADD_LONG (p, alloc_size); // p[i  ].ptr + alloc_size
                    _new->next = _next;                 // p[i+1].ptr
                    _new->size = p->size - alloc_size;  // p[i  ].size - alloc_size
                    _next = _new;
                }
                prev->next = _next; // p[i-1].ptr = ...
                *err = false;
                return p;
            }
        }

        _new = FARPTR_ADD_LONG (p, alloc_size); // p[i  ].ptr + alloc_size
        if (IS_FARPTR_NOT_ABOVE (_new, cc_heap_end))
        {
            prev->next = _new;  // p[i-1].ptr = p[i  ].ptr + alloc_size
            cc_heap_ptr = _new;
            cc_heap_error (0);
            *err = false;
            return p;
        }
        switch (cc_heap_error (orig_size))
        {
        case CC_EHEAP_ABORT:
            *err = true;
            return NULL;    /* does not matter */
        case CC_EHEAP_IGNORE:
            *err = false;
            return NULL;
        default:
            // retry
            alloc_size = orig_size;
            break;  // goto main_loop;
        }
    }
}
#endif /* !LINKER_TPC */

void __far *cc_malloc (uint32_t size)
{
    char s[2][20];
    uint32_t n;
#if LINKER_TPC != 1
    char err;
#endif  /* LINKER_TPC != 1 */
    void __far *data;

    n = size + sizeof (uint32_t);
#if LINKER_TPC == 1
    pascal_getmem(&data, n);
#else   /* LINKER_TPC != 1 */
    data = _alloc (n, &err);
    if (err)
        _cc_ExitWithError (CC_ECRIT_HEAP_OVERFLOW_ERROR, NULL);
#endif  /* LINKER_TPC != 1 */
    SYSDEBUG_INFO_ (
        "start=%s, size=%lu, end=%s",
        SYSDEBUG_dump_heap_ptr (s[0], data),
        (uint32_t) n,
        SYSDEBUG_dump_heap_ptr (s[1], FARPTR_ADD_LONG (data, n - 1))
    );
    if (data)
    {
        *((uint32_t *)data) = n;
        data = FARPTR_ADD_LONG (data, sizeof (uint32_t));
    }
    return data;
}
