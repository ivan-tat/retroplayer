/* rand.c -- part of custom "stdlib" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#include "cc/stdlib.h"

/* Use "man 3 rand" or visit <https://man7.org/linux/man-pages/man3/rand.3.html> */

static unsigned long next = 1;

int cc_rand (void)
{
   next = next * 1103515245 + 12345;
   return (unsigned) (next >> 16) & CC_RAND_MAX;
}

void cc_srand (unsigned seed)
{
   next = seed;
}
