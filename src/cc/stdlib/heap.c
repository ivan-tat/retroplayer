/* heap.c -- part of custom "stdlib" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/sysdbg.h"
#include "cc/i86.h"
#include "cc/dos.h"
#include "cc/stdio.h"
#include "cc/stdlib.h"
#include "cc/stdlib/heap.h"
#include "cc/string.h"

#if DEFINE_LOCAL_DATA == 1

void __far *cc_heap_org = NULL;
void __far *cc_heap_ptr = NULL;
void __far *cc_heap_end = NULL;
struct cc_heap_free_rec_t cc_heap_free_rec = { NULL, 0 };
char (*cc_heap_error) (uint32_t size) = NULL;

#endif  /* DEFINE_LOCAL_DATA == 1 */

static char _heap_error_func (uint32_t size)
{
    return CC_EHEAP_ABORT;
}

/*** Initialization ***/

static void __near _init_heap (uint16_t seg_start, uint16_t size_paras)
{
    cc_heap_org = MK_FP (seg_start, 0);
    cc_heap_ptr = cc_heap_org;
    cc_heap_end = MK_FP (seg_start + size_paras, 0);
    ((struct cc_heap_free_rec_t __far *)cc_heap_org)->next = cc_heap_end;
    ((struct cc_heap_free_rec_t __far *)cc_heap_org)->size = (uint32_t)size_paras * 16UL;
    cc_heap_free_list = cc_heap_org;
    cc_heap_error = &_heap_error_func;
    SYSDEBUG_INFO_ (
        "heap_org=0x%lX (heap_ptr), heap_end=heap_org+%li, free_list=heap_org+%li",
        (uint32_t) FARPTR_TO_LONG (cc_heap_org),
        (int32_t) FARPTR_DIST (cc_heap_end, cc_heap_org),
        (int32_t) FARPTR_DIST (cc_heap_free_list, cc_heap_org)
    );
}

static void _heap_done (void)
{
    if (cc_heap_org)
        _cc_dos_freemem (FP_SEG (cc_heap_org));
}

/* Returns "true" on success. */
bool cc_init_heap (uint16_t size_min_paras, uint16_t size_max_paras)
{
    uint16_t size, seg;

    if (size_min_paras > size_max_paras || size_max_paras == 0)
    {
        cc_errno = CC_EINVAL;
        return false;
    }

    size = size_max_paras;
    if (_cc_dos_allocmem (size, &seg) != CC_EZERO)
    {
        size = seg;
        if (size < size_min_paras)
            return false;
        if (_cc_dos_allocmem (size, &seg) != CC_EZERO)
            return false;
    }

    _init_heap (seg, size);
    cc_atexit (&_heap_done);
    return true;
}

#if SYSDEBUG

char __far *SYSDEBUG_dump_heap_ptr (char __far *s, void __far *ptr)
{
    if (ptr == &cc_heap_free_rec)
        memcpy (s, "heap_free_rec", 13+1);
    else if (ptr == cc_heap_org)
        memcpy (s, "heap_org", 8+1);
    else if (ptr == cc_heap_end)
        memcpy (s, "heap_end", 8+1);
    else if (FARPTR_DIST (cc_heap_end, ptr) == 1)
        memcpy (s, "heap_end-1", 10+1);
    else
        sprintf (s, "heap_org+%li", (int32_t) FARPTR_DIST (ptr, cc_heap_org));
    return s;
}

#endif  /* SYSDEBUG */
