/* heap.h -- part of custom "stdlib" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_STDLIB_HEAP_H_INCLUDED
#define _CC_STDLIB_HEAP_H_INCLUDED 1

#include "defines.h"

#define CC_BAD_FP_BITS_FOR_HEAP(v) (FP_OFF (v) & ~8)
#define CC_ROUND_SIZE_FOR_HEAP(v) ((v + 7) & ~7UL)

/*** Aliases ***/

#define BAD_FP_BITS_FOR_HEAP CC_BAD_FP_BITS_FOR_HEAP
#define ROUND_SIZE_FOR_HEAP CC_ROUND_SIZE_FOR_HEAP

#endif  /* !_CC_STDLIB_HEAP_H_INCLUDED */
