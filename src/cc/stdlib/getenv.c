/* getenv.c -- part of custom "stdlib" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/dos.h"
#include "cc/stdlib.h"
#include "cc/stdlib/_env.h"

char __far *cc_getenv (const char *name)
{
    uint16_t nlen;
    struct dosenvlist_t *dosenv;
    char __far *value;

    if (_env_name_check(name, &nlen))
        return NULL;

    dosenv = &_dos_env;
    value = _dosenv_find(dosenv, name, nlen);
    if (value)
        value = &(value[nlen + 1]);
    return value;
}
