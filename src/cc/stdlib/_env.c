/* _env.c -- part of custom "stdlib" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/sysdbg.h"
#include "cc/i86.h"
#include "cc/string.h"
#include "cc/dos.h"
#include "cc/errno.h"
#include "cc/malloc.h"
#include "cc/stdlib.h"
#include "cc/stdlib/_env.h"

/* For the case if environment is not initialized */
static char __far *_env_list_empty = NULL;

#if DEFINE_LOCAL_DATA == 1

struct dosenvlist_t _dos_env;
struct envstrlist_t _env_list;
char __far *__far *cc_environ;

#endif  /* DEFINE_LOCAL_DATA == 1 */

/* DOS program segment prefix */

static void __near _psp_get_dosenv (uint16_t self, struct dosenvlist_t *env)
{
    struct cc_dospsp_t __far *p = MK_FP (self, 0);

    env->arr = MK_FP(p->env_seg, 0);
    env->size = ((struct cc_dosmcb_t __far *)(MK_FP (p->env_seg - 1, 0)))->size * 16;
}

/* DOS environment */

static bool __near _dosenv_next (const struct dosenvlist_t *self, uint16_t *i)
{
    char __far *p = self->arr;
    uint16_t len = strlen (&(p[*i]));

    if (len)
    {
        *i += len + 1;
        return true;
    }
    else
        return false;
}

uint16_t _dosenv_find_end (const struct dosenvlist_t *self, uint16_t *count)
{
    uint16_t i = 0;
    bool stop = false;

    *count = 0;
    do
    {
        if (_dosenv_next(self, &i))
            (*count)++;
        else
            stop = true;
    } while (!stop);
    return i;
}

char __far *_dosenv_find (const struct dosenvlist_t *self, const char *name, uint16_t nlen)
{
    uint16_t i = 0;

    do
    {
        char __far *s = &(self->arr[i]);
        if (!_dosenv_next(self, &i))
            return NULL;
        if ((memcmp(name, s, nlen) == 0) && (s[nlen] == '='))
            return s;
    } while (true); /* FIXME: weird? */
}

/* Returns zero (false) on success */
bool _env_name_check(const char *name, uint16_t *nlen)
{
    if (!name)
    {
        cc_errno = CC_EINVAL;
        return  true;
    }
    *nlen = strlen(name);
    if (!*nlen)
    {
        cc_errno = CC_EINVAL;
        return true;
    }
    if (strchr(name, '='))
    {
        cc_errno = CC_EINVAL;
        return true;
    }
    return false;
}

/* Internal environment variables list */

static void __near _clear_env (struct envstrlist_t *self)
{
    if (self)
    {
        self->arr = &_env_list_empty;
        self->size = 1;
    }
}

static bool __near _alloc_env (struct envstrlist_t *self, uint16_t count)
{
    uint16_t seg;

    if (SYSDEBUG_ENV) { SYSDEBUG_BEGIN (); }
    if (self)
    {
        if (count)
        {
            if (!_dos_allocmem(_dos_para(sizeof(char *) * count), &seg))
            {
                self->arr = MK_FP(seg, 0);
                self->size = count;
                if (SYSDEBUG_ENV) { SYSDEBUG_SUCCESS (); }
                return true;
            }
            else
            {
                cc_errno = CC_ENOMEM;
                if (SYSDEBUG_ENV) { SYSDEBUG_ERR ("_dos_allocmem(): failed."); }
                return false;
            }
        }
        else
        {
            cc_errno = CC_EINVAL;
            if (SYSDEBUG_ENV) { SYSDEBUG_ERR ("self is NULL."); }
            return false;
        }
    }
    else
    {
        cc_errno = CC_EINVAL;
        if (SYSDEBUG_ENV) { SYSDEBUG_ERR ("count is 0."); }
        return false;
    }
}

static void __near _fill_env (struct envstrlist_t *self, struct dosenvlist_t *dosenv)
{
    if (self && self->arr)
    {
        char __far * __far *p = self->arr;
        uint16_t count = self->size;
        uint16_t i = 0;

        do
        {
            p[0] = &(dosenv->arr[i]);
            p++;
            count--;
        } while ((count != 1) && _dosenv_next(dosenv, &i));
        *p = NULL;
    }
}

static void __near _free_env (struct envstrlist_t *self)
{
    if (self && self->arr && self->arr != &_env_list_empty)
    {
        _dos_freemem(FP_SEG(self->arr));
        _clear_env (self);
    }
}

static void __near _sync_env (void)
{
    cc_environ = _env_list.arr;
}

static bool __near _build_env (void)
{
    uint16_t count;

    if (SYSDEBUG_ENV) { SYSDEBUG_BEGIN (); }
    _dosenv_find_end(&_dos_env, &count);
    if (count)
    {
        count++;
        if (SYSDEBUG_ENV) { SYSDEBUG_INFO_ ("count=%d", count); }
        if (!_alloc_env (&_env_list, count))
        {
            if (SYSDEBUG_ENV) { SYSDEBUG_ERR ("Failed."); }
            return false;
        }
        _fill_env (&_env_list, &_dos_env);
    }
    else
        _clear_env (&_env_list);

    _sync_env ();

    if (SYSDEBUG_ENV) { SYSDEBUG_SUCCESS (); }
    return true;
}

/*** Initialization ***/

void cc_done_environ (void);

bool cc_init_environ (void)
{
    bool status;

    if (SYSDEBUG_ENV)
    {
        SYSDEBUG_BEGIN ();
        SYSDEBUG_INFO_ ("_memmax()=%ld", (uint32_t) _memmax ());
    }
    cc_atexit (&cc_done_environ);
    if (SYSDEBUG_ENV)
    {
        SYSDEBUG_INFO_ ("_psp=%04X, _masterpsp=%04X", _cc_psp, _cc_dos_getmasterpsp ());
    }
//~ #if LINKER_TPC == 1
    //~ _cc_psp = _cc_dos_getmasterpsp ();
//~ #endif  /* LINKER_TPC == 1 */
    _psp_get_dosenv (_cc_psp, &_dos_env);
    if (SYSDEBUG_ENV)
    {
        SYSDEBUG_INFO_ ("_dos_env: .arr=%04X:%04X, .size=%d",
            FP_SEG (_dos_env.arr), FP_OFF (_dos_env.arr), _dos_env.size);
    }
    _clear_env (&_env_list);
    status = _build_env ();
    if (SYSDEBUG_ENV)
    {
        if (status)
            SYSDEBUG_SUCCESS ();
        else
            SYSDEBUG_ERR ("Failed.");
    }
    return status;
}

bool rebuild_environ (void)
{
    bool status;

    if (SYSDEBUG_ENV) { SYSDEBUG_BEGIN (); }
    _free_env (&_env_list);
    status = _build_env ();
    if (SYSDEBUG_ENV)
    {
        if (status)
            SYSDEBUG_SUCCESS ();
        else
            SYSDEBUG_ERR ("Failed.");
    }
    return status;
}

void cc_done_environ (void)
{
    if (SYSDEBUG_ENV) { SYSDEBUG_BEGIN (); }
    _free_env (&_env_list);
    _sync_env ();
    if (SYSDEBUG_ENV) { SYSDEBUG_END (); }
}
