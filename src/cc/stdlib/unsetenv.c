/* unsetenv.c -- part of custom "stdlib" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/string.h"
#include "cc/dos.h"
#include "cc/errno.h"
#include "cc/stdlib.h"
#include "cc/stdlib/_env.h"

int16_t cc_unsetenv(const char *name)
{
    uint16_t nlen;
    struct dosenvlist_t *oenv, nenv;
    uint16_t envsize;
    uint16_t seg;
    char __far *s;

    if (_env_name_check(name, &nlen))
        return -1;

    oenv = &_dos_env;
    envsize = oenv->size;

    if (_dos_allocmem(_dos_para(envsize), &seg))
    {
        cc_errno = CC_ENOMEM;
        return -1;
    }
    nenv.arr = MK_FP(seg, 0);
    nenv.size = envsize;

    memcpy(nenv.arr, oenv->arr, envsize);
    s = _dosenv_find(&nenv, name, nlen);
    if (s)
    {
        uint16_t ssize = strlen(s) + 1;
        uint16_t diff = FARPTR_DIST (s, nenv.arr);
        char __far *endptr = &(s[ssize]);
        memcpy(s, endptr, envsize - (diff + ssize));
        memcpy(oenv->arr, nenv.arr, envsize);
    }

    _dos_freemem(FP_SEG(nenv.arr));

    if (rebuild_environ())
        return 0;
    else
    {
        cc_errno = CC_ENOMEM;
        return -1;
    }
}
