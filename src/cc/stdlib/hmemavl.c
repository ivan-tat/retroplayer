/* hmemavl.c -- part of custom "stdlib" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/stdlib.h"
#include "cc/stdlib/heap.h"
#include "cc/stdlib/havail.h"

/*
 * Returns total free heap size.
 */
static uint32_t _memavail (uint32_t size, struct cc_heap_free_rec_t __far *rec, void __far *endp)
{
    while (rec != endp)
    {
        size += rec->size;
        rec = rec->next;
    }
    return size;
}

uint32_t cc_memavail (void)
{
    return _cc_heap_avail_func (&_memavail);
}
