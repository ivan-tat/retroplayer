/* io.h -- declarations for custom "io" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_IO_H_INCLUDED
#define _CC_IO_H_INCLUDED 1

#include <stddef.h>
#include <stdint.h>
#include "defines.h"

/* Symbolic constants for the cc_lseek() function */

#define CC_SEEK_SET 0   // Seek relative to the start of file
#define CC_SEEK_CUR 1   // Seek relative to current position
#define CC_SEEK_END 2   // Seek relative to the end of the file

int cc_open (const char __far *filename, int flags, int mode);
int cc_close(int fd);
/*off_t*/int32_t cc_lseek (int fd, /*off_t*/int32_t offset, int whence);
/*size_t*/int32_t cc_read (int fd, void __far *buf, /*size_t*/unsigned count);
/*size_t*/int32_t cc_write (int fd, const void __far *buf, /*size_t*/unsigned count);

/*** Aliases ***/

#define SEEK_SET CC_SEEK_SET
#define SEEK_CUR CC_SEEK_CUR
#define SEEK_END CC_SEEK_END

#define open cc_open
#define close cc_close

#define lseek cc_lseek
#define read cc_read
#define write cc_write

#endif  /* !_CC_IO_H_INCLUDED */
