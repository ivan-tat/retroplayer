/* dsfile.c -- part of custom "dstream" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include "defines.h"
#include "cc/dstream.h"
#include "cc/dsfile.h"
#include "cc/stdio.h"

bool filestream_flush (DATASTREAM *self);

void filestream_init (DATASTREAM *self, /*size_t*/unsigned size, void *buf, void *stream)
{
    datastream_init (self, (size && buf) ? DSFLAG_BUFFER : DSFLAG_DIRECT);
    self->m_flush = &filestream_flush;
    self->buf_size = size;
    self->buf = buf;
    self->output = stream;
}

bool filestream_flush (DATASTREAM *self)
{
    if (self->output)
        return (fwrite (self->buf, self->pos, 1, (FILE *) self->output)) == 1;
    else
        return false;
}
