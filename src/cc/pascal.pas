(* pascal.pas -- support for Pascal linker.

   This file is for linking compiled object files with Pascal linker.
   It will be deleted in future when we rewrite the project in C.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. *)

unit
    pascal;

interface

uses
    dos;

(*$I defines.pas*)

(*** Unit "System" ***)

function  pascal_ParamCount: Word;
procedure pascal_ParamStr(var dest: String; i: Byte);
procedure pascal_Halt(status: Integer);
procedure pascal_GetMem(var p: pointer; size: word);
procedure pascal_FreeMem(p: pointer; size: word);

(*** Unit "Dos" ***)

procedure pascal_SwapVectors;
procedure pascal_Exec(Name: PathStr; CmdLine: String);

implementation

function pascal_ParamCount: Word;
begin
    pascal_ParamCount := System.ParamCount;
end;

procedure pascal_ParamStr(var dest: String; i: Byte);
begin
    dest := System.ParamStr(i);
end;

procedure pascal_Halt(status: Integer);
begin
    System.Halt(status);
end;

procedure pascal_GetMem(var p: pointer; size: word);
begin
    System.GetMem(p, size);
end;

procedure pascal_FreeMem(p: pointer; size: word);
begin
    System.FreeMem(p, size);
end;

procedure pascal_SwapVectors;
begin
    Dos.SwapVectors;
end;

procedure pascal_Exec(Name: PathStr; CmdLine: String);
begin
    Dos.Exec(Name, CmdLine);
end;

end.
