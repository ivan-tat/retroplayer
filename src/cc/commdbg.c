/* commdbg.c -- common library for debug messages.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdint.h>
#include "defines.h"
#include "cc/hexdigts.h"
#include "cc/string.h"
#include "cc/commdbg.h"

/*
 * Example:
 *      #define LEN 8
 *      char s [LEN + 1];
 *      _DEBUG_get_xnum (0xBAAD5EED, LEN, s);
 *      // s = "BAAD5EED";
 *
 *  Description:
 *      Destination must be of the size (len + 1) bytes to hold the result.
 */
void _DEBUG_get_xnum (uint32_t value, char len, char *dest)
{
    char *c = & (dest [len]), i;

    *c = '\0';

    for (c--, i = len; i; value >>= 4, c--, i--)
        *c = HEXDIGITS [value & 15];
}

/*
 * Example:
 *      #define MAX 16
 *      char s [MAX * 4 + 1];
 *      _DEBUG_get_xline (HEXDIGITS, 12, MAX, s);
 *      // s = "30 31 32 33|34 35 36 37|38 39 41 42|-- -- -- --|0123456789AB";
 *      _DEBUG_get_xline (HEXDIGITS, 6, MAX, s);
 *      // s = "30 31 32 33|34 35 -- --|-- -- -- --|-- -- -- --|012345";
 *
 *  Description:
 *      Destination must be of the size (max * 4 + 1) bytes to hold the result.
 */
void _DEBUG_get_xline (void *buf, uint8_t size, uint8_t max, char *dest)
{
    char *p;
    int i;

    for (p = (char *) buf, i = 0; i < max; i++)
    {
        if (i < size)
        {
            _DEBUG_get_xnum (*p, 2, dest);
            p++;
        }
        else
        {
            dest[0] = '-';
            dest[1] = '-';
        }
        dest[2] = ((i & 3) == 3) ? '|' : ' ';
        dest += 3;
    }
// FIXME: Watcom compilation is wrong
/*
    for (p = (char *) buf, i = size; i; p++, i--);
    {
        *dest = (*p < 32) ? '.' : *p;
        dest++;
    }
*/
    p = (char *) buf;
    i = size;
    while (i)
    {
        *dest = (*p < 32) ? '.' : *p;
        p++;
        dest++;
        i--;
    }

    *dest = '\0';
}
