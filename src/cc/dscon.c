/* dscon.c -- part of custom "dstream" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include "defines.h"
#include "cc/startup.h"
#include "cc/dstream.h"
#include "cc/dscon.h"
#include "cc/stdio.h"

bool consolestream_flush (DATASTREAM *self);

void consolestream_init (DATASTREAM *self, /*size_t*/unsigned size, void *buf)
{
    datastream_init (self, ((size > 1) && buf) ? DSFLAG_BUFFER : DSFLAG_DIRECT);
    self->m_flush = &consolestream_flush;
    self->buf_size = size - 1; /* (-1) for terminating zero */
    self->buf = buf;
}

bool consolestream_flush (DATASTREAM *self)
{
    self->buf[self->pos] = 0;
    cc_TextWriteString (&cc_Output, self->buf, 0);
    cc_TextSync (&cc_Output);
    return true;
}
