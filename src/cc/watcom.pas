(* watcom.pas -- Pascal declarations for Watcom C internal functions.

   This file is for linking compiled object files with Pascal linker.
   It will be deleted in future when we rewrite the project in C.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. *)

unit
    watcom;

interface

(*$I defines.pas*)

(* i4d.obj *)
procedure __I4D;
procedure __U4D;

(* i4m.obj *)
procedure __I4M;
procedure __U4M;

(* i8d086.obj *)
procedure __I8DQ;
procedure __I8DQE;
procedure __I8DR;
procedure __I8DRE;
procedure __U8DQ;
procedure __U8DQE;
procedure __U8DR;
procedure __U8DRE;

(* i8ls086.obj *)
procedure __I8LS;
procedure __U8LS;

(* i8m086.obj *)
procedure __I8M;
procedure __I8ME;
procedure __U8M;
procedure __U8ME;

(* i8rs086.obj *)
procedure __I8RS;

(* u8rs086.obj *)
procedure __U8RS;

implementation

(*$L i4d.obj*)
procedure __I4D; external;
procedure __U4D; external;

(*$L i4m.obj*)
procedure __I4M; external;
procedure __U4M; external;

(*$L i8d086.obj*)
procedure __I8DQ; external;
procedure __I8DQE; external;
procedure __I8DR; external;
procedure __I8DRE; external;
procedure __U8DQ; external;
procedure __U8DQE; external;
procedure __U8DR; external;
procedure __U8DRE; external;

(*$L i8ls086.obj*)
procedure __I8LS; external;
procedure __U8LS; external;

(*$L i8m086.obj*)
procedure __I8M; external;
procedure __I8ME; external;
procedure __U8M; external;
procedure __U8ME; external;

(*$L i8rs086.obj*)
procedure __I8RS; external;

(*$L u8rs086.obj*)
procedure __U8RS; external;

end.
