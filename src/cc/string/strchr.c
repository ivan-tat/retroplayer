/* strchr.c -- part of custom "string" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#include "cc/string.h"

char *cc_strchr(const char *s, int c)
{
    if (s)
    {
        const char *ptr = s;
        /*size_t*/unsigned count = ~0;

        while (*ptr && *ptr != c && count)
        {
            ptr++;
            count--;
        }

        if (*ptr == c)
            return (char *)ptr;
    }

    return NULL;
}
