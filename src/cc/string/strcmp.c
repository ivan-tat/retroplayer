/* strcmp.c -- part of custom "string" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include "defines.h"
#include "cc/string.h"

int cc_strcmp(const char *s1, const char *s2)
{
    int res = 0;

    if (s1 && s2)
    {
        const char *ptr1 = s1;
        const char *ptr2 = s2;
        /*size_t*/unsigned count = ~0;
        bool next;

        do
        {
            res = *ptr1 - *ptr2;
            next = *ptr1 && *ptr2;
            ptr1++;
            ptr2++;
            count--;
        } while ((!res) && next && count);
    }

    return res;
}
