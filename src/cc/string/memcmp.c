/* memcmp.c -- part of custom "string" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#include "cc/string.h"

int cc_memcmp (const void *s1, const void *s2, /*size_t*/unsigned n)
{
    if (n)
    {
        const char *ptr1 = s1;
        const char *ptr2 = s2;
        /*size_t*/unsigned count = n;
        int res;

        do
        {
            res = *ptr1 - *ptr2;
            ptr1++;
            ptr2++;
            count--;
        } while ((!res) && count);

        return res;
    }

    return 0;
}
