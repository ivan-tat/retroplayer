/* strlen.c -- part of custom "string" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#include "cc/string.h"

/*size_t*/unsigned cc_strlen (const char *s)
{
    if (s)
    {
        const char *ptr = s;
        /*size_t*/unsigned count = ~0;

        while (*ptr && count)
        {
            ptr++;
            count--;
        }

        return ~count;
    }

    return 0;
}
