/* strncpy.c -- part of custom "string" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include "defines.h"
#include "cc/string.h"

char *cc_strncpy (char *dest, const char *src, /*size_t*/unsigned n)
{
    if (dest && n)
    {
        char *ptr1 = dest;
        /*size_t*/unsigned count = n;

        if (src)
        {
            const char *ptr2 = src;
            bool next;

            do
            {
                *ptr1 = *ptr2;
                next = *ptr2 != 0;
                ptr1++;
                ptr2++;
                count--;
            } while (next && count);
        }

        while (count)
        {
            *ptr1 = 0;
            ptr1++;
            count--;
        }
    }

    return dest;
}
