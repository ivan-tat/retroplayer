/* memset.c -- part of custom "string" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#include "cc/i86.h"
#include "cc/string.h"

void *cc_memset (void __far *s, int c, /*size_t*/uint32_t n)
{
/*
    if (n)
    {
        char *ptr = s;
        /*size_t* /unsigned count = n;

        do
        {
            *ptr = c;
            ptr++;
            count--;
        } while (count);
    }
*/
    uint32_t o = FARPTR_TO_LONG (s);

    // 'n' is counter

    while (n)
    {
        uint8_t __far *p = FARPTR_FROM_LONG (o);
        uint16_t count;
        bool aligned;

        // counter register in DOS segment limit
        if ((uint32_t) FP_OFF (p) + n > (1UL<<16))
            count = (1UL<<16) - FP_OFF (p);
        else
            count = n;

        // trick: instead of 0 count use 1<<16

        o += count ? count : (1UL<<16);
        n -= count ? count : (1UL<<16);

        // 2 bytes alignment
        if (FP_OFF (p) & 1)
        {
            *p = c;
            p++;
            count--;
            aligned = true;
        }
        else
            aligned = false;

        if (((!count) && !aligned) || (count & ~1))
        {
            c += c << 8;
            do
            {
                *((uint16_t __far *)p) = c;
                p += 2;
                count -= 2;
            } while (count & ~1);
        }

        if (count & 1)
            *p = c;
    }

    return s;
}
