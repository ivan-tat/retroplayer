/* memcpy.c -- part of custom "string" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#include "cc/i86.h"
#include "cc/string.h"

void *cc_memcpy (void __far *dest, const void __far *src, /*size_t*/uint32_t n)
{
    if ((dest != src) && n)
    {
/*
        char *ptr1 = dest;
        const char *ptr2 = src;
        /*size_t* /unsigned count = n;

        do
        {
            *ptr1 = *ptr2;
            ptr1++;
            ptr2++;
            count--;
        } while (count);
*/
        uint32_t o1 = FARPTR_TO_LONG (dest);
        uint32_t o2 = FARPTR_TO_LONG ((void __far *) src);

        // 'n' is counter

        while (n)
        {
            uint8_t __far *p1 = FARPTR_FROM_LONG (o1);
            uint8_t __far *p2 = FARPTR_FROM_LONG (o2);
            uint16_t count;
            bool aligned;

            // counter register in DOS segment limit
            if ((uint32_t) FP_OFF (p1) + n > (1UL<<16))
                count = (1UL<<16) - FP_OFF (p1);
            else
                count = n;

            if ((uint32_t) FP_OFF (p2) + count > (1UL<<16))
                count = (1UL<<16) - FP_OFF (p2);

            // trick: instead of 0 count use 1<<16

            o1 += count ? count : (1UL<<16);
            o2 += count ? count : (1UL<<16);
            n -= count ? count : (1UL<<16);

            // 2 bytes alignment
            if (FP_OFF (p1) & 1)
            {
                *p1 = *p2;
                p1++;
                p2++;
                count--;
                aligned = true;
            }
            else
                aligned = false;

            if (((!count) && !aligned) || (count & ~1))
            {
                do
                {
                    *((uint16_t __far *)p1) = *((uint16_t __far *)p2);
                    p1 += 2;
                    p2 += 2;
                    count -= 2;
                } while (count & ~1);
            }

            if (count & 1)
                *p1 = *p2;
        }
    }

    return dest;
}
