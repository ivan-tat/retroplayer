/* conio.h -- declarations for custom "conio" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_CONIO_H_INCLUDED
#define _CC_CONIO_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/startup.h" /* _cc_iobuf */

/*** General I/O ***/

#ifdef __WATCOMC__

char cc_inp(unsigned int port);
#pragma aux cc_inp = \
    "in al,dx" \
    parm nomemory [dx] \
    value [al] \
    modify nomemory exact [];

void cc_outp(unsigned int port, char value);
#pragma aux cc_outp = \
    "out dx,al" \
    parm nomemory [dx] [al] \
    modify nomemory exact [];

#pragma intrinsic(cc_inp, cc_outp);

#else   /* !__WATCOMC__ */

extern char cc_inp(unsigned int port);
extern void cc_outp(unsigned int port, char value);

#endif  /* !__WATCOMC__ */

/*** Text-mode functions ***/

#define _TEXTBW40 0
#define _TEXTC40  1
#define _TEXTBW80 2
#define _TEXTC80  3
#define _TEXTMONO 7
#define _Font8x8  256

#define _black          0
#define _blue           1
#define _green          2
#define _cyan           3
#define _red            4
#define _magenta        5
#define _brown          6
#define _lightgray      7
#define _darkgray       8
#define _lightblue      9
#define _lightgreen     10
#define _lightcyan      11
#define _lightred       12
#define _lightmagenta   13
#define _yellow         14
#define _white          15
#define _blink          128

#pragma pack(push, 1);
typedef union text_rect_t {
    struct {
        uint8_t x, y;
    } rect;
    uint16_t value;
};
#pragma pack(pop);

/* Private */

extern bool     cc_gotbreak;
extern char     cc_lastscancode;
extern uint16_t cc_screenwidth;
extern uint16_t cc_screenheight;
extern uint8_t  cc_textattrorig;

/* Publics */

extern bool     cc_checkbreak;
extern bool     cc_checkeof;
extern bool     cc_checksnow;
extern bool     cc_directvideo;
extern uint16_t cc_lastmode;
extern uint8_t  cc_textattr;
extern union text_rect_t cc_windmin;
extern union text_rect_t cc_windmax;
extern uint16_t cc_SegB000;
extern uint16_t cc_SegB800;

void cc_clreol(void);
void cc_clrscr(void);
void cc_gotoxy(uint8_t x, uint8_t y);
void cc_window(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2);
void cc_textbackground(uint8_t color);
void cc_textcolor(uint8_t color);
void cc_lowvideo(void);
void cc_highvideo(void);
void cc_normvideo(void);
void cc_textmode(uint16_t mode);

/*** Keyboard functions ***/

extern bool cc_kbhit(void);
extern char cc_getch(void);

/*** Console initialization ***/

/* Private */

void _cc_console_set_mode(uint16_t mode);
void _cc_console_on_mode_change(void);
void _cc_console_on_start(void);

/* Publics */

void cc_init_console (void);

void cc_TextAssignCrt (_cc_iobuf *f, void __far *buffer, uint16_t size);

/*** Aliases ***/

/* General I/O */

#define inp cc_inp
#define outp cc_outp

/* Text-mode variables */

#define checkbreak      cc_checkbreak;
#define checkeof        cc_checkeof;
#define checksnow       cc_checksnow;
#define directvideo     cc_directvideo;
#define lastmode        cc_lastmode;
#define textattr        cc_textattr;
#define windmin         cc_windmin;
#define windmax         cc_windmax;
#define SegB000         cc_SegB000
#define SegB800         cc_SegB800

/* Text-mode functions */

#define textmode       cc_textmode
#define window         cc_window
#define clrscr         cc_clrscr
#define clreol         cc_clreol
#define gotoxy         cc_gotoxy
#define textbackground(c)       cc_textbackground(c)
#define textcolor(c)            cc_textcolor(c)
#define lowvideo(c)    cc_lowvideo(c)
#define highvideo(c)   cc_highvideo(c)
#define normvideo(c)   cc_normvideo(c)

/* Keyboard functions */

#define kbhit cc_kbhit
#define getch cc_getch

/*** Initialization ***/

#define init_console cc_init_console

#endif  /* !_CC_CONIO_H_INCLUDED */
