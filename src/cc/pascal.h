/* pascal.h -- support for pascal linker.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _PASCAL_H_INCLUDED
#define _PASCAL_H_INCLUDED 1

#include "defines.h"
#if LINKER_TPC
# include <stdint.h>
#endif /* LINKER_TPC */

#define pascal_String_size 256

/* Arguments handling */

#if LINKER_TPC

extern uint16_t __far __pascal pascal_paramcount (void);
extern void __far __pascal pascal_paramstr (char *dest, uint8_t i);
extern void __noreturn __far __pascal pascal_Halt (uint16_t status);
extern void __far __pascal pascal_getmem (void __far *__far *p, uint16_t size);
extern void __far __pascal pascal_freemem (void __far *p, uint16_t size);
extern void __far __pascal pascal_swapvectors (void);
extern void __far __pascal pascal_exec (char __far *name, char __far *cmdline);

#ifdef __WATCOMC__
# pragma aux pascal_paramcount modify [ bx cx dx si di es ];
# pragma aux pascal_paramstr modify [ ax bx cx dx si di es ];
# pragma aux pascal_getmem modify [ ax bx cx dx si di es ];
# pragma aux pascal_freemem modify [ ax bx cx dx si di es ];
# pragma aux pascal_swapvectors modify [ ax bx cx dx si di es ];
# pragma aux pascal_exec modify [ ax bx cx dx si di es ];
#endif /* defined __WATCOMC__ */

#endif /* LINKER_TPC */

#endif  /* !_PASCAL_H_INCLUDED */
