/* string.h -- declarations for custom "string" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_STRING_H_INCLUDED
#define _CC_STRING_H_INCLUDED 1

#include <stdint.h>
#include "defines.h"

#ifndef NULL
#define NULL ((void *)0)
#endif  /* !NULL */

#define toupper(c) (((c >= 'a') && (c <= 'z')) ? (c - 'a' + 'A') : c)
#define tolower(c) (((c >= 'A') && (c <= 'Z')) ? (c - 'A' + 'a') : c)

int    cc_memcmp (const void *s1, const void *s2, /*size_t*/unsigned n);
void  *cc_memcpy (void __far *dest, const void __far *src, /*size_t*/uint32_t n);
void  *cc_memset (void __far *s, int c, /*size_t*/uint32_t n);
char  *cc_strchr (const char *s, int c);
int    cc_strcmp (const char *s1, const char *s2);
int    cc_stricmp (const char *s1, const char *s2);
/*size_t*/unsigned cc_strlen (const char *s);
int    cc_strncmp (const char *s1, const char *s2, /*size_t*/unsigned n);
char  *cc_strncpy (char *dest, const char *src, /*size_t*/unsigned n);
int    cc_strnicmp (const char *s1, const char *s2, /*size_t*/unsigned n);

/*** Aliases ***/

#define memcmp   cc_memcmp
#define memcpy   cc_memcpy
#define memset   cc_memset
#define strchr   cc_strchr
#define strcmp   cc_strcmp
#define stricmp  cc_stricmp
#define strlen   cc_strlen
#define strncmp  cc_strncmp
#define strncpy  cc_strncpy
#define strnicmp cc_strnicmp

#endif  /* !_CC_STRING_H_INCLUDED */
