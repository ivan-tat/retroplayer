(* c6845.pas -- declarations for c6845.c.

   This file is for linking compiled object files with Pascal linker.
   It will be deleted in future when we rewrite the project in C.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. *)

unit c6845;

interface

(*$I defines.pas*)

procedure c6845_wait_sync;
procedure c6845_wait_vsync;
procedure c6845_set_text_cursor_position;

implementation

uses
    i86,
    conio;

(*$l c6845.obj*)

procedure c6845_wait_sync; external;
procedure c6845_wait_vsync; external;
procedure c6845_set_text_cursor_position; external;

end.
