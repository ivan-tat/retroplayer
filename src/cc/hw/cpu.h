/* cpu.h -- declarations for cpu.asm.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CPU_H_INCLUDED
#define _CPU_H_INCLUDED 1

#include <stdbool.h>
#include "defines.h"

extern bool isCPU_8086(void);
extern bool isCPU_i386(void);

#endif /* !_CPU_H_INCLUDED */
