/* c6845.c -- 6845 video controller chip library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h" /* _enable(), _disable() */
#include "cc/conio.h" /* inp(), outp() */
#include "cc/hw/c6845.h"

/*
 * "vid_port" - port address for 6845 video controller chip (0x3B4 or 0x3D4).
 */
void c6845_wait_sync (uint16_t vid_port, bool disable_irqs)
{
    uint16_t io;
    io = vid_port + 6;  /* port 0x3da: read status register */
    /* bit 0 - if set, display is in vertical or horizontal retrace */
    while (inp (io) & 1);
    if (disable_irqs) _disable ();
    while (!(inp (io) & 1));
}

/*
 * "vid_port" - port address for 6845 video controller chip (0x3B4 or 0x3D4).
 */
void c6845_wait_vsync (uint16_t vid_port, bool disable_irqs)
{
    uint16_t io;
    io = vid_port + 6;  /* port 0x3da: read status register */
    /* bit 3 - if set, display is in vertical retrace */
    while (!(inp (io) & 8));
    if (disable_irqs) _disable ();
    while (inp (io) & 8);
}

/*
 * "vid_port" - port address for 6845 video controller chip (0x3B4 or 0x3D4).
 */
void c6845_set_text_cursor_position (uint16_t vid_port, uint16_t offset)
{
    /* port 0x3d4: CRT controller register select (write) */
    /* port 0x3d5: CRT controller register read/write */
    outp (vid_port + 0, 0x0e);  /* 0x0e - cursor position (MSB) */
    outp (vid_port + 1, offset >> 8);
    outp (vid_port + 0, 0x0f);  /* 0x0f - cursor position (LSB) */
    outp (vid_port + 1, offset & 0xff);
}
