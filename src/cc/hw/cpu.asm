; cpu.asm -- CPU-specific functions.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

.386

public isCPU_8086
public isCPU_i386

DGROUP group _DATA

_DATA segment word public use16 'DATA'
_DATA ends

CPU_TEXT segment word public use16 'CODE'
assume cs:CPU_TEXT, ds:DGROUP, ss:DGROUP

isCPU_8086:
        xor     ax, ax
        pushf
        pop     bx
        and     bh,0Fh
        push    bx
        popf
        pushf
        pop     cx
        and     ch,0F0h
        cmp     ch,0F0h
        jz      short @exit ; ax = 0, fail
        inc     ax          ; ax = 1
        or      bh,0F0h
        push    bx
        popf
        pushf
        pop     cx
        and     ch,0F0h
        jz      short @exit ; ax = 1
        inc     ax          ; ax = 2
@exit:
        retf

; Check for Intel 80386 or higher CPU
isCPU_i386:
        mov     ax,7000h
        push    ax
        popf
        pushf
        pop     ax
        and     ax,7000h
        jz      @fail   ; ax = 0, fail
        xor     ax,ax
        inc     ax      ; ax = 1, success
@fail:
        retf

CPU_TEXT ends

end
