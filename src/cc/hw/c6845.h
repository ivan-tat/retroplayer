/* c6845.h -- declarations for c6845.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _C6845_H_INCLUDED
#define _C6845_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

void c6845_wait_sync (uint16_t vid_port, bool disable_irqs);
void c6845_wait_vsync (uint16_t vid_port, bool disable_irqs);
void c6845_set_text_cursor_position (uint16_t vid_port, uint16_t offset);

#endif  /* !_C6845_H_INCLUDED */
