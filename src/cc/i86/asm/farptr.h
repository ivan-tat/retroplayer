/* farptr.h -- part of custom "i86" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CC_I86_FARPTR_H_INCLUDED
#define _CC_I86_FARPTR_H_INCLUDED 1

#include "defines.h"
#include "cc/i86.h"

#if USE_INTRINSICS == 0

extern uint32_t FARPTR_TO_LONG (const FARPTR v);
extern FARPTR   FARPTR_FROM_LONG (uint32_t v);
extern FARPTR   FARPTR_ADD_LONG (const FARPTR a, int32_t b);
extern int32_t  FARPTR_DIST (const FARPTR a, const FARPTR b);

#ifdef __WATCOMC__
# pragma aux FARPTR_TO_LONG   parm [ax dx] value [ax dx] modify [bx];
# pragma aux FARPTR_FROM_LONG parm [ax dx] value [ax dx] modify [bx];
# pragma aux FARPTR_ADD_LONG  parm [ax dx] [bx cx] value [ax dx] modify [bx];
# pragma aux FARPTR_DIST      parm [ax dx] [bx cx] value [ax dx] modify [bx cx];
#else   /* !__WATCOMC__ */
# pragma error "not implemented"
#endif  /* !__WATCOMC__ */

#endif  /* USE_INTRINSICS == 0 */

#endif  /* !_CC_I86_FARPTR_H_INCLUDED */
