; farptr.asm -- part of custom "i86" library.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

.286

public FARPTR_TO_LONG
public FARPTR_FROM_LONG
public FARPTR_ADD_LONG
public FARPTR_DIST

DGROUP group _DATA

_DATA segment word public use16 'DATA'
_DATA ends

FARPTR_TEXT segment word public use16 'CODE'
assume cs:FARPTR_TEXT, ds:DGROUP, ss:DGROUP

; in:   dx:ax = FARPTR
; out:  dx:ax = uint32_t (0->0xFFFFF)
; used: bx
FARPTR_TO_LONG:
    rol     dx, 4       ; dx = ((seg * 16) & 0xFFFF) + ((seg * 16) >> 16)
    mov     bx, dx      ; bx = ((seg * 16) & 0xFFFF) + ((seg * 16) >> 16)
    and     dx, 0000Fh  ; dx = (seg * 16) >> 16
    and     bx, 0FFF0h  ; bx = (seg * 16) & 0xFFFF
    add     ax, bx      ;
    adc     dx, 0       ; result = ofs + seg * 16
    retf

; in:   dx:ax = uint32_t L (0->0xFFFFF)
; out:  dx:ax = FARPTR
; used: bx
FARPTR_FROM_LONG:
    and     dx, 0000Fh  ; L &= 0xFFFFF
    mov     bx, ax      ; bx = L & 0xFFFF
    and     bx, 0000Fh  ; bx = L & 0x000F
    ror     dx, 4       ; dx = (L / 16) & 0xF000
    shr     ax, 4       ; ax = (L / 16) & 0x0FFF
    add     dx, ax      ; dx = L / 16
    mov     ax, bx      ; result = MK_FP (L / 16, L & 0x000F)
    retf

; in:   dx:ax = FARPTR
; in:   cx:bx = int32_t
; out:  dx:ax = FARPTR
; used: bx
FARPTR_ADD_LONG:
    push    bx
    push    cs
    call    near ptr FARPTR_TO_LONG
    pop     bx
    add     ax, bx
    adc     dx, cx
    push    cs
    call    near ptr FARPTR_FROM_LONG
    retf

; in:   dx:ax = FARPTR a
; in:   cx:bx = FARPTR b
; out:  dx:ax = int32_t
; used: bx,cx
FARPTR_DIST:
    push    bx      ; LSW (b)
    push    cs
    call    near ptr FARPTR_TO_LONG
                    ; ax = LSW (FARPTR_TO_LONG (a))
                    ; cx = MSW (b)
                    ; dx = MSW (FARPTR_TO_LONG (a))
    mov     bx, ax  ; ax = LSW (FARPTR_TO_LONG (a))
                    ; bx = LSW (FARPTR_TO_LONG (a))
    xchg    cx, dx  ; cx = MSW (FARPTR_TO_LONG (a))
                    ; dx = MSW (b)
    pop     ax      ; ax = LSW (b)
                    ; dx:ax = b
                    ; cx:bx = FARPTR_TO_LONG (a)
    push    bx
    push    cs
    call    near ptr FARPTR_TO_LONG
    pop     bx      ; dx:ax = FARPTR_TO_LONG (b)
                    ; cx:bx = FARPTR_TO_LONG (a)
    sub     bx, ax
    sbb     cx, dx
    xchg    ax, bx
    xchg    dx, cx
    retf

FARPTR_TEXT ends

end
