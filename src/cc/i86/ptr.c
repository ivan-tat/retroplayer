/* ptr.c -- part of custom "i86" library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/i86/asm/farptr.h"

#if USE_INTRINSICS == 0

bool     IS_FARPTR_BELOW (FARPTR a, FARPTR b)      { return _IS_FARPTR_BELOW (a, b); }
bool     IS_FARPTR_NOT_ABOVE (FARPTR a, FARPTR b)  { return _IS_FARPTR_NOT_ABOVE (a, b); }
bool     IS_FARPTR_ABOVE (FARPTR a, FARPTR b)      { return _IS_FARPTR_ABOVE (a, b); }
bool     IS_FARPTR_NOT_BELOW (FARPTR a, FARPTR b)  { return _IS_FARPTR_NOT_BELOW (a, b); }
//uint32_t FARPTR_TO_LONG (FARPTR v)                 { return _FARPTR_TO_LONG (v); }
//FARPTR   FARPTR_FROM_LONG (uint32_t v)             { return _FARPTR_FROM_LONG (v); }
//FARPTR   FARPTR_ADD_LONG (FARPTR a, uint32_t b)    { return FARPTR_FROM_LONG (FARPTR_TO_LONG (a) + b); }
//uint32_t FARPTR_DIST (FARPTR a, FARPTR b)          { return FARPTR_TO_LONG (a) - FARPTR_TO_LONG (b); }

#endif  /* USE_INTRINSICS == 0 */
