/* ems.h -- declarations for ems.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _EMS_H_INCLUDED
#define _EMS_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

/* types */

#define EM_PAGE_SIZE (16 * 1024)

#define get_EM_page(offset)         ((offset) / EM_PAGE_SIZE)
#define get_EM_page_offset(offset)  ((offset) % EM_PAGE_SIZE)

typedef struct emsVersion_t
{
    uint8_t Lo, Hi;
};
typedef struct emsVersion_t EMSVER;

typedef uint16_t emsError_t;
typedef emsError_t EMSERR;

#define E_EMS_SUCCESS 0

typedef uint16_t emsHandle_t;
typedef emsHandle_t EMSHDL;

#define EMSBADHDL 0xffff

typedef uint8_t emsHandleName_t[8];
typedef emsHandleName_t EMSNAME;

typedef struct
{
   EMSHDL handle;
   uint16_t num;
} EMMAPPAGES [4];

typedef struct
{
   EMSHDL handle;
   EMMAPPAGES pages;
} EMMAPSTATE;

/* external variables */

extern bool     emsInstalled;
extern EMSERR   emsEC;
extern EMSVER   emsVersion;
extern uint16_t emsFrameSeg;    /* real memory segment for first page */
extern void    *emsFramePtr;    /* real memory pointer for first page */

/* public functions */

bool     emsIsInstalled (void);
bool     emsGetVersion (void);
uint16_t emsGetFrameSeg (void);
uint16_t emsGetFreePagesCount (void);
EMSHDL   emsAlloc (uint16_t pages);
bool     emsResize (EMSHDL handle, uint16_t pages);
bool     emsFree (EMSHDL handle);
bool     emsMap (EMSHDL handle, uint16_t logPage, uint8_t physPage);
bool     emsSaveMap (EMSHDL handle);
bool     emsRestoreMap (EMSHDL handle);
uint16_t emsGetHandleSize (EMSHDL handle);
bool     emsSetHandleName (EMSHDL handle, const EMSNAME *name);

bool map_EM_data (EMSHDL handle, uint32_t offset, uint32_t size);
void clear_EM_map_state (EMMAPSTATE *state);
bool init_EM_map_state (EMMAPSTATE *state, const EMSNAME *name);
bool save_EM_map_state (EMMAPSTATE *state);
bool restore_EM_map_state (EMMAPSTATE *state);
void free_EM_map_state (EMMAPSTATE *state);

/*** Initialization ***/

void init_ems (void);
void done_ems (void);

#endif  /* !_EMS_H_INCLUDED */
