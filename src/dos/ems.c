/* ems.c -- DOS Expanded Memory Service library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/stdlib.h"
#include "cc/stdio.h"
#include "cc/string.h"
#include "cc/dos.h"
#include "common.h"
#include "debug.h"
#include "dos/ems.h"

#if DEFINE_LOCAL_DATA == 1

bool     emsInstalled;
EMSERR   emsEC;
EMSVER   emsVersion;
uint16_t emsFrameSeg;    /* real memory segment for first page */
void    *emsFramePtr;    /* real memory pointer for first page */

#endif  /* DEFINE_LOCAL_DATA == 1 */

#define EMS_INT 0x67    /* EMS interrupt vector number */

static const DOSDRVNAME EMS_DRIVER_NAME = "EMMXXXX0";

#define MAX_ITEMS 6

#pragma pack(push, 1);
typedef struct handle_entry_t HDLENT;
typedef struct handle_entry_t
{
    HDLENT *next;
    EMSHDL items[MAX_ITEMS];
};  // size of entry is 16 bytes
#pragma pack(pop);

#pragma pack(push, 1);
typedef struct handles_list_t HANDLESLIST;
typedef struct handles_list_t
{
    HDLENT *root;
};
#pragma pack(pop);

static HANDLESLIST _handleslist;

/* Entry */

static void __near _entry_clear(HDLENT *self)
{
    if (self)
    {
        int i;

        self->next = NULL;
        for (i = 0; i < MAX_ITEMS; i++)
            self->items[i] = EMSBADHDL;
    }
}

static int __near _entry_find_item(HDLENT *self, EMSHDL item)
{
    int i;

    for (i = 0; i < MAX_ITEMS; i++)
        if (self->items[i] == item)
            return i;

    return -1;
}

static bool __near _entry_is_empty(HDLENT *self)
{
    int i;

    for (i = 0; i < MAX_ITEMS; i++)
        if (self->items[i] != EMSBADHDL)
            return false;

    return true;
}

/* List */

static void __near _list_clear(HANDLESLIST *self)
{
    if (self)
        self->root = NULL;
}

static HDLENT *__near _list_find_item(HANDLESLIST *self, EMSHDL item, int *index)
{
    if (self)
    {
        HDLENT *entry = self->root;

        while (entry)
        {
            int i = _entry_find_item(entry, item);

            if (i >= 0)
            {
                *index = i;
                return entry;
            }
            entry = entry->next;
        }
    }

    return NULL;
}

static bool __near _list_add_item(HANDLESLIST *self, EMSHDL item)
{
    HDLENT *entry;
    int i;

    if (self)
    {
        entry = _list_find_item(self, EMSBADHDL, &i);
        if (entry)
        {
            entry->items[i] = item;
            return true;
        }
        else
        {
            entry = _new(HDLENT);
            if (entry)
            {
                _entry_clear(entry);
                entry->next = self->root;
                entry->items[0] = item;
                self->root = entry;
                return true;
            }
        }
    }

    return false;
}

static bool __near _list_remove_item(HANDLESLIST *self, EMSHDL item)
{
    if (self)
    {
        HDLENT *prev = NULL;
        HDLENT *entry = self->root;

        while (entry)
        {
            int i = _entry_find_item (entry, item);

            if (i >= 0)
            {
                entry->items[i] = EMSBADHDL;
                if (_entry_is_empty(entry))
                {
                    if (prev)
                        prev->next = entry->next;
                    else
                        self->root = entry->next;

                    _delete(entry);
                }
                return true;
            }

            prev = entry;
            entry = entry->next;
        }
    }

    return false;
}

static bool __near _ems_free(EMSHDL handle);

static void __near _list_free(HANDLESLIST *self)
{
    if (self)
        while (self->root)
        {
            HDLENT *entry = self->root;
            int i;

            for (i = 0; i < MAX_ITEMS; i++)
            {
                EMSHDL item = entry->items[i];

                if (item != EMSBADHDL)
                    _ems_free(item);
            }
            self->root = entry->next;
            _delete(entry);
        }
}

bool emsIsInstalled (void)
{
    DOSDRVNAME *p;

    p = _dos_getvect (EMS_INT);
    p = MK_FP (FP_SEG (p), offsetof (struct dosdevhdr_t, name));
    return (memcmp (p, EMS_DRIVER_NAME, sizeof (DOSDRVNAME)) == 0);
}

bool emsGetVersion (void)
{
    union REGPACK regs;

    regs.w.ax = 0x4600;
    intr (EMS_INT, &regs);
    if (regs.h.ah)
    {
        emsEC = regs.h.ah;
        emsVersion.Lo = 0;
        emsVersion.Hi = 0;
        return false;
    }
    else
    {
        emsVersion.Lo = regs.h.al & 0x0f;
        emsVersion.Hi = regs.h.al >> 4;
        return true;
    }
}

uint16_t emsGetFrameSeg (void)
{
    union REGPACK regs;

    regs.w.ax = 0x4100;
    intr (EMS_INT, &regs);
    if (regs.h.ah)
    {
        emsEC = regs.h.ah;
        return -1;
    }
    else
        return regs.w.bx;
}

uint16_t emsGetFreePagesCount (void)
{
    union REGPACK regs;

    regs.w.ax = 0x4200;
    intr (EMS_INT, &regs);
    if (regs.h.ah)
    {
        emsEC = regs.h.ah;
        return 0;
    }
    else
        return regs.w.bx;
}

static EMSHDL __near _ems_alloc(uint16_t pages)
{
    union REGPACK regs;

    regs.w.ax = 0x4300;
    regs.w.bx = pages;
    intr (EMS_INT, &regs);
    if (regs.h.ah)
    {
        emsEC = regs.h.ah;
        return EMSBADHDL;
    }
    else
        return regs.w.dx;
}

EMSHDL emsAlloc (uint16_t pages)
{
    EMSHDL h;

    h = _ems_alloc(pages);
    if (h != EMSBADHDL)
        _list_add_item(&_handleslist, h);

    return h;
}

bool emsResize (EMSHDL handle, uint16_t pages)
{
    union REGPACK regs;

    if (emsVersion.Hi >= 4)
    {
        regs.w.ax = 0x5100;
        regs.w.bx = pages;
        regs.w.dx = handle;
        intr (EMS_INT, &regs);
        if (regs.h.ah)
        {
            emsEC = regs.h.ah;
            return false;
        }
        else
            return true;
    }
    else
    {
        emsEC = 0x84;
        return false;
    }
}

static bool __near _ems_free(EMSHDL handle)
{
    union REGPACK regs;

    regs.w.ax = 0x4500;
    regs.w.dx = handle;
    intr (EMS_INT, &regs);
    if (regs.h.ah)
    {
        emsEC = regs.h.ah;
        return false;
    }
    else
        return true;
}

bool emsFree (EMSHDL handle)
{
    if (_ems_free(handle))
    {
        _list_remove_item(&_handleslist, handle);
        return true;
    }

    return false;
}

bool emsMap (EMSHDL handle, uint16_t logPage, uint8_t physPage)
{
    union REGPACK regs;

    regs.h.ah = 0x44;
    regs.h.al = physPage;
    regs.w.bx = logPage;
    regs.w.dx = handle;
    intr (EMS_INT, &regs);
    if (regs.h.ah)
    {
        emsEC = regs.h.ah;
        return false;
    }
    else
        return true;
}

bool emsSaveMap (EMSHDL handle)
{
    union REGPACK regs;

    regs.w.ax = 0x4700;
    regs.w.dx = handle;
    intr (EMS_INT, &regs);
    if (regs.h.ah)
    {
        emsEC = regs.h.ah;
        return false;
    }
    else
        return true;
}

bool emsRestoreMap (EMSHDL handle)
{
    union REGPACK regs;

    regs.w.ax = 0x4800;
    regs.w.dx = handle;
    intr (EMS_INT, &regs);
    if (regs.h.ah)
    {
        emsEC = regs.h.ah;
        return false;
    }
    else
        return true;
}

uint16_t emsGetHandleSize (EMSHDL handle)
{
    union REGPACK regs;

    regs.w.ax = 0x4c00;
    regs.w.dx = handle;
    intr (EMS_INT, &regs);
    if (regs.h.ah)
    {
        emsEC = regs.h.ah;
        return 0;
    }
    else
        return regs.w.bx;
}

bool emsSetHandleName (EMSHDL handle, const EMSNAME *name)
{
    union REGPACK regs;

    if (emsVersion.Hi >= 4)
    {
        regs.w.ax = 0x5301;
        regs.w.dx = handle;
        regs.w.si = FP_OFF(name);
        regs.w.ds = FP_SEG(name);
        intr (EMS_INT, &regs);
        if (regs.h.ah)
        {
            emsEC = regs.h.ah;
            return false;
        }
        else
            return true;
    }
    else
    {
        emsEC = 0x84;
        return false;
    }
}

static EMMAPPAGES map_pages;

bool map_EM_data (EMSHDL handle, uint32_t offset, uint32_t size)
{
    uint16_t page, count, i;

    page = get_EM_page (offset);
    count = get_EM_page (size + get_EM_page_offset (offset) + EM_PAGE_SIZE - 1);

    if (!count)
        return false;   /* Error */
    else if (count > 4)
        count = 4;

    for (i = 0; i < count; i++)
    {
        bool do_map;

        _disable ();
        do_map = (map_pages[i].handle != handle) || (map_pages[i].num != page);
        _enable ();

        if (do_map)
            if (emsMap (handle, page, i))
            {
                _disable ();
                map_pages[i].handle = handle;
                map_pages[i].num = page;
                _enable ();
            }
            else
                return false;

        page++;
    }

    return true;
}

static void __near clear_EM_map_pages (EMMAPPAGES *pages)
{
    int i;

    for (i = 0; i < 4; i++)
    {
        (*pages)[i].handle = EMSBADHDL;
        (*pages)[i].num = 0;
    }
}

void clear_EM_map_state (EMMAPSTATE *state)
{
    state->handle = EMSBADHDL;
    clear_EM_map_pages (&state->pages);
}

bool init_EM_map_state (EMMAPSTATE *state, const EMSNAME *name)
{
    state->handle = emsAlloc (1);   // is 1 page enough?
    clear_EM_map_pages (&state->pages);

    if (emsEC == E_EMS_SUCCESS)
    {
        emsSetHandleName (state->handle, name);
        return true;
    }
    else
    {
        state->handle = EMSBADHDL;
        return false;
    }
}

bool save_EM_map_state (EMMAPSTATE *state)
{
    if ((state->handle != EMSBADHDL) && emsSaveMap (state->handle))
    {
        _disable ();
        memcpy (&state->pages, map_pages, sizeof (EMMAPPAGES));
        _enable ();
        return true;
    }
    else
        return false;
}

bool restore_EM_map_state (EMMAPSTATE *state)
{
    if ((state->handle != EMSBADHDL) && emsRestoreMap (state->handle))
    {
        _disable ();
        memcpy (map_pages, &state->pages, sizeof (EMMAPPAGES));
        _enable ();
        return true;
    }
    else
        return false;
}

void free_EM_map_state (EMMAPSTATE *state)
{
    if (state->handle != EMSBADHDL)
        emsFree (state->handle);
}

/*** Initialization ***/

void init_ems (void)
{
    DEBUG_BEGIN ();
    emsEC = 0;
    emsFrameSeg = 0;
    emsFramePtr = NULL;
    _list_clear(&_handleslist);
    emsInstalled = emsIsInstalled();
    if (emsInstalled)
    {
        if (emsGetVersion())
        {
            emsFrameSeg = emsGetFrameSeg();
            emsFramePtr = MK_FP(emsFrameSeg, 0);
            clear_EM_map_pages (map_pages);
            DEBUG_INFO_ ("Found EMS version %hu.%03hu.", emsVersion.Hi, emsVersion.Lo);
            DEBUG_INFO_ ("%lu KiB of EM is available.",
                (uint32_t) emsGetFreePagesCount () * EM_PAGE_SIZE / 1024);
        }
        else
        {
            emsInstalled = false;
            DEBUG_ERR ("Failed to get EMS version.");
        }
    }
    else
        DEBUG_INFO ("No EMS is available.");

    cc_atexit ((void *)done_ems);
    DEBUG_END ();
}

void done_ems (void)
{
    DEBUG_BEGIN ();
    _list_free(&_handleslist);
    DEBUG_END ();
}
