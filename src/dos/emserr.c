/* emserr.c -- error messages for DOS Expanded Memory Service library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#include "cc/string.h"  /* NULL */
#include "dos/ems.h"
#include "dos/emserr.h"

/* Error messages */

static const struct emsErrorDesc_t
{
   EMSERR code;
   char *msg;
} EMS_ERRORS[] =
{
   { 0x80, "Internal error in EMS" },
   { 0x81, "EMS hardware failure" },
   { 0x82, "EMS is busy" },
   { 0x83, "Invalid handle" },
   { 0x84, "Undefined function requested" },
   { 0x85, "No more handles available" },
   { 0x86, "Error in save or restore of mapping context" },
   { 0x87, "Allocation request was larger than total expanded memory" },
   { 0x88, "No enough free pages" },
   { 0x89, "Zero pages requested" },
   { 0x8a, "Access error: this logical page does not belong to this handle" },
   { 0x8b, "Wrong page number. Only physical pages 0-3 are defined" },
   { 0x8c, "Mapping context save area is full" },
   { 0x8d, "Can save context only once per handle" },
   { 0x8e, "Unable to restore context without a prior save" },
   { 0x8f, "Sub-function parameter not defined" },
   { 0x90, "Attribute type undefined" },
   { 0x91, "Non-volatility not supported" },
   { 0x92, "Source and dest. overlap in EMS RAM (warning, not error)" },
   { 0x93, "Destination area in handle too small" },
   { 0x94, "Conventional memory overlaps EMS RAM" },
   { 0x95, "Offset too large in block move" },
   { 0x96, "Block size too large (> 1M)" },
   { 0x97, "Source and dest. are in same handle and overlap" },
   { 0x98, "Source or dest. memory type invalid (in offset 4 of packet)" },
   { 0x9a, "Alternate map register set in not supported" },
   { 0x9b, "All alternate map/DMA register sets are all allocated" },
   { 0x9c, "All alternate map/DMA register sets are not supported" },
   { 0x9d, "Specified alternate map/DMA register set invalid or in use" },
   { 0x9e, "Dedicated DMA channels are not supported" },
   { 0x9f, "Specified DMA channel is not supported" },
   { 0xa0, "No handle matches specified name" },
   { 0xa1, "The specified name already exists" },
   { 0xa2, "Source offset+region length > 1M (attempted to wrap)" },
   { 0xa3, "Contents of specified data packet are corrupted or invalid" },
   { 0xa4, "Access to this function has been denied (bad access key)" },
   { 0, NULL }
};

const char *emsGetErrorMsg (void)
{
   int i = 0;

   while (EMS_ERRORS[i].code)
   {
      if (EMS_ERRORS[i].code == emsEC)
         return EMS_ERRORS[i].msg;

      i++;
   }

   return "Unknown error";
}
