/* emserr.h -- declarations for "emserr.c".

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _EMSERR_H_INCLUDED
#define _EMSERR_H_INCLUDED 1

#include "defines.h"

const char *emsGetErrorMsg (void);

#endif  /* !_EMSERR_H_INCLUDED */
