/* dynarray.h -- declarations for dynamic array handling library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _DYNARRAY_H_INCLUDED
#define _DYNARRAY_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"

/*** Dynamic array virtual methods ***/

typedef void dynarr_init_item_t (void *parent, void *item);
typedef void dynarr_free_item_t (void *parent, void *item);

/*** Dynamic array structure ***/

#pragma pack(push, 1);
typedef struct dynarr_t
{
    void *parent;
    uint16_t item_size;
    uint16_t size;
    void *list;
    dynarr_init_item_t *init_item;
    dynarr_free_item_t *free_item;
};
#pragma pack(pop);
typedef struct dynarr_t DYNARR;

void     dynarr_init (DYNARR *self, void *parent, uint16_t item_size,
            dynarr_init_item_t init_item,
            dynarr_free_item_t free_item);
void     dynarr_init_items (DYNARR *self, uint16_t index, uint16_t count);
void     dynarr_free_items (DYNARR *self, uint16_t index, uint16_t count);
void     dynarr_set_item (DYNARR *self, uint16_t index, void *item);
void    *dynarr_get_item (DYNARR *self, uint16_t index);
int32_t  dynarr_indexof (DYNARR *self, void *item);
bool     dynarr_set_size (DYNARR *self, uint16_t size);
uint16_t dynarr_get_size (DYNARR *self);
void     dynarr_free (DYNARR *self);

#endif  /* !_DYNARRAY_H_INCLUDED */
