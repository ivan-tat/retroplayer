/* debug.h -- declarations for debug.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _DEBUG_H_INCLUDED
#define _DEBUG_H_INCLUDED 1

#include <stdarg.h>
#include "defines.h"
#include "cc/commdbg.h"
#include "cc/stdio.h"

void _DEBUG_LOG (const int type, const char *file, int line, const char *method, const char *format, ...);
void _DEBUG_BEGIN (const char *file, int line, const char *method);
void _DEBUG_END (const char *file, int line, const char *method);
void _DEBUG_SUCCESS (const char *file, int line, const char *method);
void _DEBUG_dump_bool (const char *file, int line, const char *method, const char *name, const bool *value);
void _DEBUG_dump_s8 (const char *file, int line, const char *method, const char *name, const int8_t *value);
void _DEBUG_dump_u8 (const char *file, int line, const char *method, const char *name, const uint8_t *value);
void _DEBUG_dump_u16 (const char *file, int line, const char *method, const char *name, const uint16_t *value);
void _DEBUG_dump_u32 (const char *file, int line, const char *method, const char *name, const uint32_t *value);
void _DEBUG_dump_ptr (const char *file, int line, const char *method, const char *name, const void __far *value);
void _DEBUG_dump_str (const char *file, int line, const char *method, const char *name, const char *value);
void _DEBUG_dump_mem (void *buf, unsigned size, const char *padstr);

#if DEBUG == 1
 #define DEBUG_LOG(type, text)             _DEBUG_LOG (type, __FILE__, __LINE__, __func__, "%s", text)
 #define DEBUG_LOG_(type, format, ...)     _DEBUG_LOG (type, __FILE__, __LINE__, __func__, format, __VA_ARGS__)
 #define DEBUG_MSG(text)                   _DEBUG_LOG (DBGLOG_MSG, __FILE__, __LINE__, __func__, "%s", text)
 #define DEBUG_MSG_(format, ...)           _DEBUG_LOG (DBGLOG_MSG, __FILE__, __LINE__, __func__, format, __VA_ARGS__)
 #define DEBUG_INFO(text)                  _DEBUG_LOG (DBGLOG_INFO, __FILE__, __LINE__, __func__, "%s", text)
 #define DEBUG_INFO_(format, ...)          _DEBUG_LOG (DBGLOG_INFO, __FILE__, __LINE__, __func__, format, __VA_ARGS__)
 #define DEBUG_WARN(text)                  _DEBUG_LOG (DBGLOG_WARN, __FILE__, __LINE__, __func__, "%s", text)
 #define DEBUG_WARN_(format, ...)          _DEBUG_LOG (DBGLOG_WARN, __FILE__, __LINE__, __func__, format, __VA_ARGS__)
 #define DEBUG_BEGIN()                     _DEBUG_BEGIN (__FILE__, __LINE__, __func__)
 #define DEBUG_END()                       _DEBUG_END (__FILE__, __LINE__, __func__)
 #define DEBUG_SUCCESS()                   _DEBUG_SUCCESS (__FILE__, __LINE__, __func__)
 #define DEBUG_ERR(text)                   _DEBUG_LOG (DBGLOG_ERR, __FILE__, __LINE__, __func__, "%s", text)
 #define DEBUG_ERR_(format, ...)           _DEBUG_LOG (DBGLOG_ERR, __FILE__, __LINE__, __func__, format, __VA_ARGS__)
 #define DEBUG_dump_bool(name, value)      _DEBUG_dump_bool (__FILE__, __LINE__, __func__, name, value)
 #define DEBUG_dump_s8(name, value)        _DEBUG_dump_s8 (__FILE__, __LINE__, __func__, name, value)
 #define DEBUG_dump_u8(name, value)        _DEBUG_dump_u8 (__FILE__, __LINE__, __func__, name, value)
 #define DEBUG_dump_u16(name, value)       _DEBUG_dump_u16 (__FILE__, __LINE__, __func__, name, value)
 #define DEBUG_dump_u32(name, value)       _DEBUG_dump_u32 (__FILE__, __LINE__, __func__, name, value)
 #define DEBUG_dump_ptr(name, value)       _DEBUG_dump_ptr (__FILE__, __LINE__, __func__, name, value)
 #define DEBUG_dump_str(name, value)       _DEBUG_dump_str (__FILE__, __LINE__, __func__, name, value)
 #define DEBUG_dump_mem(buf, size, padstr) _DEBUG_dump_mem (buf, size, padstr)
#else   /* DEBUG != 1 */
 #define DEBUG_LOG(type, text)
 #define DEBUG_LOG_(type, format, ...)
 #define DEBUG_MSG(text)
 #define DEBUG_MSG_(format, ...)
 #define DEBUG_INFO(text)
 #define DEBUG_INFO_(format, ...)
 #define DEBUG_WARN(text)
 #define DEBUG_WARN_(format, ...)
 #define DEBUG_BEGIN()
 #define DEBUG_END()
 #define DEBUG_SUCCESS()
 #define DEBUG_ERR(text)
 #define DEBUG_ERR_(format, ...)
 #define DEBUG_dump_bool(name, value)
 #define DEBUG_dump_s8(name, value)
 #define DEBUG_dump_u8(name, value)
 #define DEBUG_dump_u16(name, value)
 #define DEBUG_dump_u32(name, value)
 #define DEBUG_dump_ptr(name, value)
 #define DEBUG_dump_str(name, value)
 #define DEBUG_dump_mem(buf, size, padstr)
#endif  /* DEBUG != 1 */

/*** Variables ***/

#if DEBUG_WRITE_LOG == 1

extern FILE *debuglogfile;

#endif  /* DEBUG_WRITE_LOG == 1 */

/*** Initialization ***/

void init_debug (void);
void done_debug (void);

#endif  /* !_DEBUG_H_INCLUDED */
