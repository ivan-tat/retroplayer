/* gentab.c - static table generator.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#define RELAX

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "main/musdefs.h"

#define PI 3.14159265358979323846
#define HALFTONES 12
#define TAB_SIZE 64

static void dump_tab (int *tab, int size, int columns)
{
    int rows = (size + columns - 1) / columns;
    int n = 0;
    bool last;

    do
    {
        int i = 0;

        do
        {
            last = n == (size - 1);

            printf ("%s%d%s",
                i ? "": "    ",
                (int) tab[n],
                (n == (size - 1)) ? "" : ", ");

            n++;
            i++;
        } while ((!last) && (i < columns));

        printf ("\n");

    } while (!last);
}

// static note periods table generator
static void gen_note_periods (void)
{
    int tab[HALFTONES], i;

    tab [0] = MID_C_PERIOD;
    for (i = 1; i < HALFTONES; i++)
        tab [i] = MID_C_PERIOD / pow (2, ((float) i / HALFTONES));

    dump_tab (tab, HALFTONES, HALFTONES);
}

// static square wave table generator
static void gen_square_wave (void)
{
    int tab[TAB_SIZE], i;

    for (i = 0; i < TAB_SIZE; i++)
        tab[i] = 127 - 255 * (i / 32);

    dump_tab (tab, TAB_SIZE, 8);
}

// static sinus wave table generator
static void gen_sinus_wave (void)
{
    int tab[TAB_SIZE], i;

    for (i = 0; i < TAB_SIZE; i++)
        tab[i] = 127 * sin (PI * 2 / TAB_SIZE * i);

    dump_tab (tab, TAB_SIZE, 8);
}

// static ramp wave table generator
static void gen_ramp_wave (void)
{
    int tab[TAB_SIZE], i;

    for (i = 0; i < TAB_SIZE; i++)
        tab[i] = i * 2 - 127;

    dump_tab (tab, TAB_SIZE, 8);
}

#define GEN_MAX 4
static struct
{
    const char *name;
    void (*proc) (void);
} generators [GEN_MAX] =
{
    { .name = "note-periods", .proc = &gen_note_periods },
    { .name = "square-wave", .proc = &gen_square_wave },
    { .name = "sinus-wave", .proc = &gen_sinus_wave },
    { .name = "ramp-wave", .proc = &gen_ramp_wave }
};

int main (int argc, const char **argv)
{
    int i;

    if (argc != 2)
    {
        printf ("Missing table name.\n");
        return 1;
    }

    for (i = 0; i < GEN_MAX; i++)
        if (strcmp (argv[1], generators[i].name) == 0)
        {
            generators[i].proc ();
            return 0;
        }

    printf ("Unknown table selected.\n");
    return 1;
}
