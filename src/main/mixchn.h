/* mixchn.h -- declarations for mixchn.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _MIXCHN_H_INCLUDED
#define _MIXCHN_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "dynarray.h"
#include "main/musdefs.h"
#include "main/musins.h"
#include "main/musmod.h"

/* Mixing channel */

typedef uint8_t mix_channel_type_t;
typedef mix_channel_type_t MIXCHNTYPE;

#define MIXCHNTYPE_NONE     0
#define MIXCHNTYPE_PCM      1
#define MIXCHNTYPE_ADLIB    2

typedef uint8_t MIXCHNFLAGS;

#define MIXCHNFL_ENABLED (1<<0) // channel is enabled (can play voice, do effects), otherwise ignored
#define MIXCHNFL_PLAYING (1<<1) // playing sample, otherwise passive
#define MIXCHNFL_MIXING  (1<<2) // mix output, otherwise muted

#define EFFFLAG_CONTINUE 0x01

typedef uint8_t mix_channel_pan_t;
typedef mix_channel_pan_t MIXCHNPAN;

#define MIXCHNPAN_LEFT      0
#define MIXCHNPAN_CENTER    32
#define MIXCHNPAN_RIGHT     64
#define MIXCHNPAN_MAX       64
#define MIXCHNPAN_BITS      6

#define MIXCHNVOL_MAX       64
#define MIXCHNVOL_BITS      6

/* Fixed point 32.16 */
#pragma pack(push, 1);
typedef struct sample_pos_t
{
    uint16_t frac;  // fractional part
    int32_t pos;    // integer part
} SMPPOS;
#pragma pack(pop);

#pragma pack(push, 1);
typedef union mix_volume_t {
    uint8_t b[2];   // [0] for mono or left channel when stereo (0->MIXCHNVOL_MAX)
                    // [1] for right channel when stereo (0->MIXCHNVOL_MAX)
    uint16_t w;     // for convenience
} MIXVOL;
#pragma pack(pop);

typedef struct mix_channel_t
{
    MIXCHNTYPE type;
    MIXCHNFLAGS flags;
    MIXCHNPAN pan;
    uint8_t instrument_num;
    uint8_t note;
    uint8_t note_volume;   // original volume
    uint8_t eff_volume;    // actual volume (may be changed by effect)
    MUSINS *instrument;
    PCMSMP *sample;
    // copy of sampledata (maybe it differs a bit):
    uint8_t  bSmpFlags;     // flags (looped sample)
    uint8_t  bSmpVol;       // final volume to mix into output buffer (0-MIXCHNVOL_MAX)
    MIXVOL   rSmpMixVol;    // final volume for each channel
    uint32_t dSmpStart;     // start offset of sample
    uint32_t dSmpLoopStart; // loop start of current sample (-1UL if no loop)
    uint32_t dSmpLoopEnd;   // normal or loop end of current sample
    SMPPOS   rSmpPos;       // current position in sample
    int32_t  dSmpStep;      // fixed point 16:16 value of frequency step
                            // (distance of one step depends on period we play
                            // currently, may be negative for backward loop)
    uint16_t wSmpPeriod;    // st3 period ... you know these amiga values (look at tech.doc of ST3)
                            // ( period does no influence playing a sample direct, but it's for sliding etc.)
    uint16_t wSmpPeriodLow; // B-7 or B-5 period for current instrument to check limits
    uint16_t wSmpPeriodHigh;// C-0 or C-3 period for current instrument to check limits
    // effect info :
    uint8_t  bCommand;      // effect number (for using a jmptable)
    uint8_t  bCommand2;     // internal 2nd command part - for multiple effects
    uint8_t  bParameter;    // just the command parameters
    // extra effect data :
    uint8_t  bEffFlags;     // multiple effects: flags (continue for Vibrato and Tremolo)
    uint8_t  bVibType;      // Vibrato: type (0->3, default: 0=sinuswave)
    uint8_t  bVibPos;       // Vibrato: position in table
    uint8_t  bVibDepth;     // Vibrato: depth
    uint8_t  bVibSpeed;     // Vibrato: speed
    uint8_t  bTrmType;      // Tremolo: type (0->3, default: 0=sinuswave)
    uint8_t  bTrmPos;       // Tremolo: position in table
    uint8_t  bTrmDepth;     // Tremolo: depth
    uint8_t  bTrmSpeed;     // Tremolo: speed
    uint8_t  bVibParam;     // Vibrato   +VolSlide: parameter
    uint8_t  bPortParam;    // Portamento+VolSlide: parameter
    uint16_t wSmpPeriodOld; // multiple effects: save that value
    uint8_t  bSmpVolOld;    // Tremolo: save that value
    uint16_t wSmpPeriodDest;// Portamento: period to slide to
    uint8_t  bArpPos;       // Arpeggio: which of thoses 3 notes we currently play
    uint8_t  bArpNotes[2];  // Arpeggio: +note - 3 notes we do arpeggio between
    uint32_t dArpSmpSteps[3];// Arpeggio: 3 step values we switch between (0 is start value <- we have to refesh after Arpeggio)
    uint8_t  bRetrigTicks;  // Retrigger: ticks left to retrigg note
    uint8_t  bSavNote;      // NoteDelay: new value
    uint8_t  bSavIns;       // NoteDelay: new value
    uint8_t  bSavVol;       // NoteDelay: new value
    uint8_t  bDelayTicks;   // NoteDelay: new value | NoteCut: ticks left to cut
    uint8_t  bTremorOn;         // Tremor: initial count
    uint8_t  bTremorOff;        // Tremor: initial count
    uint8_t  bTremorOnCounter;  // Tremor: current counter
    uint8_t  bTremorOffCounter; // Tremor: current counter
};

typedef struct mix_channel_t MIXCHN;

void           mixchn_init (MIXCHN *self);
void           mixchn_set_sample_period_limits (MIXCHN *self, uint16_t rate, bool amiga);
uint16_t       mixchn_check_sample_period (MIXCHN *self, uint32_t value);
void           mixchn_setup_sample_period (MIXCHN *self, uint32_t period, uint16_t mixrate);
void           mixchn_reset_wave_tables (MIXCHN *self);
void           mixchn_free (MIXCHN *self);

void     chn_setupInstrument (MIXCHN *chn, MUSMOD *track, uint8_t insNum);
uint16_t chn_calcNotePeriod (MIXCHN *chn, uint32_t rate, uint8_t note);
uint32_t chn_calcNoteStep (MIXCHN *chn, uint32_t rate, uint8_t note, uint16_t mixrate);
void     chn_setupNote (MIXCHN *chn, uint8_t note, uint16_t mixrate, bool keep);

/*** Mixing channels list ***/

/* Flags */

typedef uint16_t mixing_channels_list_flags_t;
typedef mixing_channels_list_flags_t MIXCHNLFLAGS;

/* Structure */

#pragma pack(push, 1);
typedef struct mixing_channels_list_t
{
    MIXCHNLFLAGS flags;
    DYNARR list;
};
#pragma pack(pop);

typedef struct mixing_channels_list_t MIXCHNLIST;

/* Methods */

void    mixchnl_init (MIXCHNLIST *self);
#define mixchnl_get(o, i)       dynarr_get_item (&(o)->list, i)
#define mixchnl_set_count(o, v) dynarr_set_size (&(o)->list, v)
#define mixchnl_get_count(o)    dynarr_get_size (&(o)->list)
void    mixchnl_free (MIXCHNLIST *self);

#endif  /* !_MIXCHN_H_INCLUDED */
