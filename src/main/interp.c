/* interptab.c -- functions to handle linear interpolation table.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#include "cc/string.h"
#include "cc/i86.h"
#include "cc/dos.h"
#if DEBUG == 1
# include "cc/stdio.h"
#endif  /* DEBUG == 1 */
#include "main/mixer.h"
#include "main/interp.h"

#if DEFINE_LOCAL_DATA == 1

interptab_t *interptab;

#endif  /* DEFINE_LOCAL_DATA == 1 */

void interptab_init (void)
{
    interptab = NULL;
}

bool interptab_alloc (void)
{
    uint16_t seg;

    if (!_dos_allocmem (_dos_para (sizeof (interptab_t)), &seg))
    {
        interptab = MK_FP (seg, 0);
        //memset (interptab, 0, sizeof (interptab_t));
        fill_16 (interptab, 0, sizeof (interptab_t) / 2);

        return true;
    }

    return false;
}

void interptab_calc (void)
{
    uint8_t i;
    int16_t sample, *p = (int16_t *) interptab;
    #if DEBUG == 1
    FILE *f;
    #endif  /* DEBUG == 1 */

    /* sample MSB (signed), coefficient 0 (1 - pos.frac) */
    for (i = 0; i < 16; i++)
        for (sample = 0; sample < 0x100; sample++)
        {
            *p = (int16_t) (16 - i) * (int16_t) ((int8_t) sample) * 16;
            p++;
        }

    /* sample LSB (unsigned), coefficient 0 (1 - pos.frac) */
    for (i = 0; i < 16; i++)
        for (sample = 0; sample < 0x100; sample++)
        {
            *p = (uint16_t) (16 - i) * (uint16_t) sample / 16;
            p++;
        }

    /* sample MSB (signed), coefficient 1 (pos.frac) */
    for (i = 0; i < 16; i++)
        for (sample = 0; sample < 0x100; sample++)
        {
            *p = (int16_t) i * (int16_t) ((int8_t) sample) * 16;
            p++;
        }

    /* sample LSB (unsigned), coefficient 1 (pos.frac) */
    for (i = 0; i < 16; i++)
        for (sample = 0; sample < 0x100; sample++)
        {
            *p = (uint16_t) i * (uint16_t) sample / 16;
            p++;
        }

    #if DEBUG == 1
    for (i = 0; i < 4; i++)
    {
        char *s = "_linear.x";
        s[8] = '0' + i;
        f = fopen (s, "wb+");
        if (f)
        {
            fwrite ((uint8_t *) interptab + i * sizeof (interptab_t) / 4, sizeof (interptab_t) / 4, 1, f);
            fclose (f);
        }
    }
    #endif  /* DEBUG == 1 */
}

void interptab_free (void)
{
    if (interptab)
    {
        _dos_freemem (FP_SEG (interptab));
        interptab = NULL;
    }
}
