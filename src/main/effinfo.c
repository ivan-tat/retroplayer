/* effinfo.c -- get current playing effect information.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#include "cc/stdio.h"
#include "main/effects.h"
#include "main/effinfo.h"

typedef void __near emGetName_t (MIXCHN *chn, char *__s, /*size_t*/unsigned __maxlen);

// Effect's methods table
typedef struct effMethodsTable_t
{
   emGetName_t *get_name;
} EFFMT;

// Effect's method name (effm)
#define NAME_GET_NAME(name) effm_##name##_get_name

// Effect's method definition
#define METHOD_GET_NAME(name) static void __near NAME_GET_NAME (name) (MIXCHN *chn, char *__s, /*size_t*/unsigned __maxlen)

#define DEFINE_METHOD_GET_NAME(name) METHOD_GET_NAME (name)

// Effect's description (effd)
#define EFFECT(name) effd_##name
#define DEFINE_EFFECT(name, get_name)                                        \
static const EFFMT __near EFFECT(name) =                                     \
{                                                                            \
   NAME_GET_NAME (get_name)                                                  \
}

// Sub-effects names list (effsnl)
#define SUB_EFFECTS_NAME_LIST(name) effsnl_##name
#define DEFINE_SUB_EFFECTS_NAME_LIST(name) static const char __near *SUB_EFFECTS_NAME_LIST (name)[]

// Effects descriptions list (effdl)
#define EFFECTS_LIST(name) effdl_##name
#define DEFINE_EFFECTS_LIST(name) static const EFFMT __near *EFFECTS_LIST (name)[]

//============================================================================
// none
//============================================================================

DEFINE_METHOD_GET_NAME (none);
DEFINE_EFFECT (none, none);

METHOD_GET_NAME (none)
{
   __s[0] = 0;
}

//============================================================================
// none_na
//============================================================================

DEFINE_METHOD_GET_NAME (none_na);
DEFINE_EFFECT (none_na, none_na);

METHOD_GET_NAME (none_na)
{
   snprintf (__s, __maxlen, "n/a");
}

//============================================================================
// setTempo
//============================================================================

DEFINE_METHOD_GET_NAME (setTempo);
DEFINE_EFFECT (setTempo, setTempo);

METHOD_GET_NAME (setTempo)
{
   snprintf (__s, __maxlen,
      "set tempo %hhu",
      (uint8_t) chn->bParameter
   );
}

//============================================================================
// setSpeed
//============================================================================

DEFINE_METHOD_GET_NAME (setSpeed);
DEFINE_EFFECT (setSpeed, setSpeed);

METHOD_GET_NAME (setSpeed)
{
   snprintf (__s, __maxlen,
      "set speed %hhu",
      (uint8_t) chn->bParameter
   );
}

//============================================================================
// jumpToOrder
//============================================================================

DEFINE_METHOD_GET_NAME (jumpToOrder);
DEFINE_EFFECT (jumpToOrder, jumpToOrder);

METHOD_GET_NAME (jumpToOrder)
{
   snprintf (__s, __maxlen,
      "jump to %03hhu",
      (uint8_t) chn->bParameter
   );
}

//============================================================================
// patBreak
//============================================================================

DEFINE_METHOD_GET_NAME (patBreak);
DEFINE_EFFECT (patBreak, patBreak);

METHOD_GET_NAME (patBreak)
{
   snprintf (__s, __maxlen,
      "break to %03hhu",
      (uint8_t) chn->bParameter
   );
}

//============================================================================
// setGVol
//============================================================================

DEFINE_METHOD_GET_NAME (setGVol);
DEFINE_EFFECT (setGVol, setGVol);

METHOD_GET_NAME (setGVol)
{
   snprintf (__s, __maxlen,
      "set gvol %03hhu",
      (uint8_t) chn->bParameter
   );
}

//============================================================================
// volSlide
//============================================================================

DEFINE_SUB_EFFECTS_NAME_LIST (volSlide) =
{
   (const char __near *) "vol.dn (norm)",
   (const char __near *) "vol.up (norm)",
   (const char __near *) "vol.dn (fine)",
   (const char __near *) "vol.up (fine)"
};

DEFINE_METHOD_GET_NAME (volSlide);
DEFINE_EFFECT (volSlide, volSlide);

METHOD_GET_NAME (volSlide)
{
   if (chn->bCommand2 <= EFFIDX_VOLSLIDE_MAX)
      snprintf (__s, __maxlen,
         "%s %02hhX",
         SUB_EFFECTS_NAME_LIST (volSlide)[chn->bCommand2],
         (uint8_t) chn->bParameter
      );
   else
      NAME_GET_NAME (none_na) (chn, __s, __maxlen);
}

//============================================================================
// pitchDown
//============================================================================

DEFINE_SUB_EFFECTS_NAME_LIST (pitchDown) =
{
   (const char __near *) "pit.dn (norm)",
   (const char __near *) "pit.dn (fine)",
   (const char __near *) "pit.dn (extr)"
};

DEFINE_METHOD_GET_NAME (pitchDown);
DEFINE_EFFECT (pitchDown, pitchDown);

METHOD_GET_NAME (pitchDown)
{
   if (chn->bCommand2 <= EFFIDX_PITCHDOWN_MAX)
      snprintf (__s, __maxlen,
         "%s %02hhX",
         SUB_EFFECTS_NAME_LIST (pitchDown)[chn->bCommand2],
         (uint8_t) chn->bParameter
      );
   else
      NAME_GET_NAME (none_na) (chn, __s, __maxlen);
}

//============================================================================
// pitchUp
//============================================================================

DEFINE_SUB_EFFECTS_NAME_LIST (pitchUp) =
{
   (const char __near *) "pit.up (norm)",
   (const char __near *) "pit.up (fine)",
   (const char __near *) "pit.up (extr)"
};

DEFINE_METHOD_GET_NAME (pitchUp);
DEFINE_EFFECT (pitchUp, pitchUp);

METHOD_GET_NAME (pitchUp)
{
   if (chn->bCommand2 <= EFFIDX_PITCHUP_MAX)
      snprintf (__s, __maxlen,
         "%s %02hhX",
         SUB_EFFECTS_NAME_LIST (pitchUp)[chn->bCommand2],
         (uint8_t) chn->bParameter
      );
   else
      NAME_GET_NAME (none_na) (chn, __s, __maxlen);
}

//============================================================================
// porta
//============================================================================

DEFINE_METHOD_GET_NAME (porta);
DEFINE_EFFECT (porta, porta);

METHOD_GET_NAME (porta)
{
   snprintf (__s, __maxlen,
      "porta %02hhX",
      (uint8_t) chn->bPortParam
   );
}

//============================================================================
// porta_vol
//============================================================================

DEFINE_SUB_EFFECTS_NAME_LIST (porta_vol) =
{
    (const char __near *) "port.+vol.dn (norm)",
    (const char __near *) "port.+vol.up (norm)",
    (const char __near *) "port.+vol.dn (fine)",
    (const char __near *) "port.+vol.up (fine)"
};

DEFINE_METHOD_GET_NAME (porta_vol);
DEFINE_EFFECT (porta_vol, porta_vol);

METHOD_GET_NAME (porta_vol)
{
   if (chn->bCommand2 <= EFFIDX_VOLSLIDE_MAX)
      snprintf (__s, __maxlen,
         "%s %02hhX",
         SUB_EFFECTS_NAME_LIST (porta_vol)[chn->bCommand2],
         (uint8_t) chn->bParameter
      );
   else
      NAME_GET_NAME (none_na) (chn, __s, __maxlen);
}

//============================================================================
// vibNorm
//============================================================================

DEFINE_METHOD_GET_NAME (vibNorm);
DEFINE_EFFECT (vibNorm, vibNorm);

METHOD_GET_NAME (vibNorm)
{
   snprintf (__s, __maxlen,
      "vib. (norm) %02hhX",
      (uint8_t) chn->bVibParam
   );
}

//============================================================================
// vibFine
//============================================================================

DEFINE_METHOD_GET_NAME (vibFine);
DEFINE_EFFECT (vibFine, vibFine);

METHOD_GET_NAME (vibFine)
{
   snprintf (__s, __maxlen,
      "vib. (fine) %02hhX",
      (uint8_t) chn->bVibParam
   );
}

//============================================================================
// vibNorm_vol
//============================================================================

DEFINE_SUB_EFFECTS_NAME_LIST (vibNorm_vol) =
{
   (const char __near *) "vib.+vol.dn (norm)",
   (const char __near *) "vib.+vol.up (norm)",
   (const char __near *) "vib.+vol.dn (fine)",
   (const char __near *) "vib.+vol.up (fine)"
};

DEFINE_METHOD_GET_NAME (vibNorm_vol);
DEFINE_EFFECT (vibNorm_vol, vibNorm_vol);

METHOD_GET_NAME (vibNorm_vol)
{
   if (chn->bCommand2 <= EFFIDX_VOLSLIDE_MAX)
      snprintf (__s, __maxlen,
         "%s %02hhX",
         SUB_EFFECTS_NAME_LIST (vibNorm_vol)[chn->bCommand2],
         (uint8_t) chn->bParameter
      );
   else
      NAME_GET_NAME (none_na) (chn, __s, __maxlen);
}

//============================================================================
// tremor
//============================================================================

DEFINE_METHOD_GET_NAME (tremor);
DEFINE_EFFECT (tremor, tremor);

METHOD_GET_NAME (tremor)
{
   snprintf (__s, __maxlen,
      "tremor %02hhX",
      (uint8_t) chn->bParameter
   );
}

//============================================================================
// arpeggio
//============================================================================

DEFINE_METHOD_GET_NAME (arpeggio);
DEFINE_EFFECT (arpeggio, arpeggio);

METHOD_GET_NAME (arpeggio)
{
   snprintf (__s, __maxlen,
      "arpeggio %02hhX",
      (uint8_t) chn->bParameter
   );
}

//============================================================================
// sampleOffset
//============================================================================

DEFINE_METHOD_GET_NAME (sampleOffset);
DEFINE_EFFECT (sampleOffset, sampleOffset);

METHOD_GET_NAME (sampleOffset)
{
   snprintf (__s, __maxlen,
      "sample ofs. %02hhX00",
      (uint8_t) chn->bParameter
   );
}

//============================================================================
// retrig
//============================================================================

DEFINE_SUB_EFFECTS_NAME_LIST (retrig) =
{
   (const char __near *) "retrig. (no sld)",
   (const char __near *) "retrig. (vol.dn)",
   (const char __near *) "retrig. (vol2/3)",
   (const char __near *) "retrig. (vol./2)",
   (const char __near *) "retrig. (vol.up)",
   (const char __near *) "retrig. (vol3/2)",
   (const char __near *) "retrig. (vol.*2)"
};

DEFINE_METHOD_GET_NAME (retrig);
DEFINE_EFFECT (retrig, retrig);

METHOD_GET_NAME (retrig)
{
   if (chn->bCommand2 <= EFFIDX_RETRIG_VOLSLIDE_MAX)
      snprintf (__s, __maxlen,
         "%s %02hhX",
         SUB_EFFECTS_NAME_LIST (retrig)[chn->bCommand2],
         (uint8_t) chn->bParameter
      );
   else
      NAME_GET_NAME (none_na) (chn, __s, __maxlen);
}

//============================================================================
// tremolo
//============================================================================

DEFINE_METHOD_GET_NAME (tremolo);
DEFINE_EFFECT (tremolo, tremolo);

METHOD_GET_NAME (tremolo)
{
   snprintf (__s, __maxlen,
      "tremolo %02hhX",
      (uint8_t) chn->bParameter
   );
}

//============================================================================
// special
//============================================================================

DEFINE_SUB_EFFECTS_NAME_LIST (special) =
{
   (const char __near *) NULL,
   (const char __near *) "set finetune",
   (const char __near *) "set vib. wave",
   (const char __near *) "set trm. wave",
   (const char __near *) "set panning",
   (const char __near *) "pat. loop",
   (const char __near *) "note cut",
   (const char __near *) "note delay",
   (const char __near *) "pat. delay"
};

DEFINE_METHOD_GET_NAME (special);
DEFINE_EFFECT (special, special);

METHOD_GET_NAME (special)
{
   if ((chn->bCommand2 > EFFIDX_SPECIAL_NONE)
   &&  (chn->bCommand2 <= EFFIDX_SPECIAL_MAX))
   {
      const char *name = SUB_EFFECTS_NAME_LIST (special)[chn->bCommand2];

      if (name)
      {
         snprintf (__s, __maxlen,
            "%s %hhX",
            name,
            (uint8_t) chn->bParameter & 0x0f
         );
         return;
      }
   }

   NAME_GET_NAME (none_na) (chn, __s, __maxlen);
}

//============================================================================

DEFINE_EFFECTS_LIST (main) =
{
   &EFFECT (none),
   &EFFECT (setSpeed),     // A
   &EFFECT (jumpToOrder),  // B
   &EFFECT (patBreak),     // C
   &EFFECT (volSlide),     // D
   &EFFECT (pitchDown),    // E
   &EFFECT (pitchUp),      // F
   &EFFECT (porta),        // G
   &EFFECT (vibNorm),      // H
   &EFFECT (tremor),       // I
   &EFFECT (arpeggio),     // J
   &EFFECT (vibNorm_vol),  // K: H + D
   &EFFECT (porta_vol),    // L: G + D
   &EFFECT (none_na),      // M
   &EFFECT (none_na),      // N
   &EFFECT (sampleOffset), // O
   &EFFECT (none_na),      // P
   &EFFECT (retrig),       // Q
   &EFFECT (tremolo),      // R
   &EFFECT (special),      // S
   &EFFECT (setTempo),     // T
   &EFFECT (vibFine),      // U
   &EFFECT (setGVol),      // V
   // W
   // X
   // Y
   // Z
};

void chn_effGetName (MIXCHN *chn, char *__s, unsigned __maxlen)
{
    #define _BUF_SIZE 40
    char s[_BUF_SIZE];
    uint8_t cmd, param;

    s[0] = 0;

    cmd = chn->bCommand;
    param = chn->bParameter;

    if ((cmd != CHN_CMD_NONE) && (cmd <= MAXEFF))
    {
        snprintf (s, 5, "%c%02hhX: ", (char) 'A' + cmd - 1, (uint8_t) param);
        EFFECTS_LIST (main)[cmd]->get_name (chn, s + 5, _BUF_SIZE - 5);
    }

    strncpy (__s, s, __maxlen);
    #undef _BUF_SIZE
}
