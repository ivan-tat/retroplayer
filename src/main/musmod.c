/* musmod.c -- musical module handling library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "common.h"
#include "cc/string.h"
#include "main/pcmsmp.h"
#include "main/musins.h"
#include "main/muspat.h"
#include "main/musmod.h"

void musmod_init (MUSMOD *self)
{
    if (self)
    {
        memset (self, 0, sizeof (MUSMOD));
        memset (self->channels, MUSMODCHNPAN_CENTER, sizeof (MUSMODCHN) * MUSMOD_CHANNELS_MAX);
        pcmsmpl_init (&self->samples);      // clear
        musinsl_init (&self->instruments);  // clear
        muspatl_init (&self->patterns);     // clear
        muspatorder_init (&self->order);    // clear
    }
}

void musmod_free (MUSMOD *self)
{
    if (self)
    {
        musinsl_free (&self->instruments);
        pcmsmpl_free (&self->samples);
        muspatl_free (&self->patterns);
        muspatorder_free (&self->order);

        musmod_init (self); // clear
    }
}
