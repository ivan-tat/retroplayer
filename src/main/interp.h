/* interp.h -- declarations for "interp.c".

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _INTERP_H_INCLUDED
#define _INTERP_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

typedef int16_t interptab_t[4][16][256];

extern interptab_t *interptab;

void interptab_init (void);
bool interptab_alloc (void);
void interptab_calc (void);
void interptab_free (void);

#endif  /* !_INTERP_H_INCLUDED */
