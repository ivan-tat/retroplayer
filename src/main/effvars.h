/* effvars.h -- declarations for effvars.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _EFFVARS_H_INCLUDED
#define _EFFVARS_H_INCLUDED 1

#include <stdint.h>
#include "defines.h"

extern const int8_t  __near sinuswave[64];
extern const int8_t  __near rampwave[64];
extern const uint8_t __near squarewave[64];
extern const void __near * __near wavetab[3];

#endif /* !_EFFVARS_H_INCLUDED */
