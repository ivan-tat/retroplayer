/* posttab.c -- functions to handle amplify table.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#if DEBUG == 1
# include "cc/stdio.h"
#endif  /* DEBUG == 1 */
#include "main/posttab.h"

#if DEFINE_LOCAL_DATA == 1

int32_t amptab[256*3];

#endif  /* DEFINE_LOCAL_DATA == 1 */

#pragma pack(push, 1)
typedef union sample_32_t
{
    uint8_t u8[4];
    int8_t s8[4];
    uint16_t u16[2];
    int16_t s16[2];
    uint32_t u32;
    int32_t s32;
};
#pragma pack(pop);

#define MID_VOL (1 << 7)

#define CLIP_MIN -0x8000L
#define CLIP_MAX 0x7fffL

void amptab_set_volume (int32_t __far *amptab, uint8_t volume)
{
    int16_t vol;
    int32_t __far *p, s, d;
    int16_t i;

    vol = volume;
    if (vol > MID_VOL)
        vol = MID_VOL;

    p = amptab;

    // i = sample & 0xff
    // s0[i] = (u32)((u8)i) * vol / MID_VOL
    s = 0;
    d = vol;
    for (i = 0; i < 0x100; i++)
    {
        *p = s / MID_VOL;
        p++;
    }

    // i = (sample & 0xff00) >> 8
    // s1[i] = ((u32)(((u8)i)) << 8) * vol / MID_VOL
    s = 0;
    //d = (vol << 8) / MID_VOL;
    d <<= 1;
    for (i = 0; i < 0x100; i++)
    {
        *p = s;
        s += d;
        p++;
    }

    // i = (sample & 0xff0000) >> 16
    // s2[i] = ((s32)(((s8)i)) << 16) * vol / MID_VOL
    s = 0;
    //d = (vol << 16) / MID_VOL;
    d <<= 8;
    for (i = 0; i < 0x80; i++)
    {
        *p = s;
        s += d;
        p++;
    }

    s = -(((int32_t)vol) << 16);
    for (i = 0; i < 0x80; i++)
    {
        *p = s;
        s += d;
        p++;
    }

    #if DEBUG == 1
    for (i = 0; i < 3; i++)
    {
        FILE *f;
        char fn[7];
        snprintf (fn, 7, "_amp.%i", (int) i);
        fn[6] = '\0';
        f = fopen (fn, "w+b");
        if (f)
        {
            fwrite (&amptab[256*i], 256 * 4, 1, f);
            fclose (f);
        }
    }
    #endif  /* DEBUG == 1 */
}

#if USE_ASM_I386 != 1

// "mb" is mixing buffer

void __far pascal _amplify32 (int32_t __far *amptab, int32_t __far *mb, uint16_t count)
{
    union sample_32_t __far *out = (union sample_32_t __far *) mb;

    while (count)
    {
        out->s32 = amptab[out->u8[0]] + amptab[256+out->u8[1]] + amptab[512+out->u8[2]];
        out++;
        count--;
    }
}

void __far pascal _clip32_u8 (void __far *outbuf, const int32_t __far *mb, uint16_t count)
{
    const int32_t *src = mb;
    uint8_t *out = (uint8_t *) outbuf;

    while (count)
    {
        int32_t s = *src;
        if (s < CLIP_MIN)
            s = CLIP_MIN;
        else
            if (s > CLIP_MAX)
                s = CLIP_MAX;

        *out = (uint8_t)(((uint16_t)s & 0xff00) >> 8) ^ 0x80;

        src++;
        out++;
        count--;
    }
}

void __far pascal _clip32_s8 (void __far *outbuf, const int32_t __far *mb, uint16_t count)
{
    const int32_t *src = mb;
    int8_t *out = (int8_t *) outbuf;

    while (count)
    {
        int32_t s = *src;
        if (s < CLIP_MIN)
            s = CLIP_MIN;
        else
            if (s > CLIP_MAX)
                s = CLIP_MAX;

        *out = (int8_t)(((uint16_t)s & 0xff00) >> 8);

        src++;
        out++;
        count--;
    }
}

void __far pascal _clip32_u16 (void __far *outbuf, const int32_t __far *mb, uint16_t count)
{
    const int32_t *src = mb;
    uint16_t *out = (uint16_t *) outbuf;

    while (count)
    {
        int32_t s = *src;
        if (s < CLIP_MIN)
            s = CLIP_MIN;
        else
            if (s > CLIP_MAX)
                s = CLIP_MAX;

        *out = (uint16_t)s ^ 0x8000;

        src++;
        out++;
        count--;
    }
}

void __far pascal _clip32_s16 (void __far *outbuf, const int32_t __far *mb, uint16_t count)
{
    const int32_t *src = mb;
    int16_t *out = (int16_t *) outbuf;

    while (count)
    {
        int32_t s = *src;

        if (s < CLIP_MIN)
            s = CLIP_MIN;
        else
            if (s > CLIP_MAX)
                s = CLIP_MAX;

        *out = (int16_t)s;

        src++;
        out++;
        count--;
    }
}

void __far pascal _clip32_u8_lq (void __far *outbuf, const int32_t __far *mb, uint16_t count)
{
    const int32_t *src = mb;
    uint16_t *out = (uint16_t *) outbuf;

    while (count)
    {
        int32_t s = src[0];

        if (s < CLIP_MIN)
            s = CLIP_MIN;
        else
            if (s > CLIP_MAX)
                s = CLIP_MAX;

        *out = (((uint16_t)s & 0xff00) + (((uint16_t)s & 0xff00) > 8)) ^ 0x8080;

        src++;
        out++;
        count--;
    }
}

void __far pascal _clip32_s8_lq (void __far *outbuf, const int32_t __far *mb, uint16_t count)
{
    const int32_t *src = mb;
    uint16_t *out = (uint16_t *) outbuf;

    while (count)
    {
        int32_t s = src[0];

        if (s < CLIP_MIN)
            s = CLIP_MIN;
        else
            if (s > CLIP_MAX)
                s = CLIP_MAX;

        *out = ((uint16_t)s & 0xff00) + (((uint16_t)s & 0xff00) > 8);

        src++;
        out++;
        count--;
    }
}

void __far pascal _clip32_u16_lq (void __far *outbuf, const int32_t __far *mb, uint16_t count)
{
    const int32_t *src = mb;
    uint16_t *out = (uint16_t *) outbuf;

    while (count)
    {
        int32_t s = src[0];

        if (s < CLIP_MIN)
            s = CLIP_MIN;
        else
            if (s > CLIP_MAX)
                s = CLIP_MAX;

        s ^= 0x8000;
        out[0] = (uint16_t)s;
        out[1] = (uint16_t)s;

        src++;
        out += 2;
        count--;
    }
}

void __far pascal _clip32_s16_lq (void __far *outbuf, const int32_t __far *mb, uint16_t count)
{
    const int32_t *src = mb;
    int16_t *out = (int16_t *) outbuf;

    while (count)
    {
        int32_t s = src[0];

        if (s < CLIP_MIN)
            s = CLIP_MIN;
        else
            if (s > CLIP_MAX)
                s = CLIP_MAX;

        out[0] = (int16_t)s;
        out[1] = (int16_t)s;

        src++;
        out += 2;
        count--;
    }
}

void __far pascal _clip32_u8_lq_stereo (void __far *outbuf, const int32_t __far *mb, uint16_t count)
{
    const int32_t *src = mb;
    uint16_t *out = (uint16_t *) outbuf;

    while (count)
    {
        int32_t s[2];
        uint16_t v;

        s[0] = src[0];
        s[1] = src[1];

        if (s[0] < CLIP_MIN)
            s[0] = CLIP_MIN;
        else
            if (s[0] > CLIP_MAX)
                s[0] = CLIP_MAX;

        if (s[1] < CLIP_MIN)
            s[1] = CLIP_MIN;
        else
            if (s[1] > CLIP_MAX)
                s[1] = CLIP_MAX;

        v = ((((uint16_t)s[0] & 0xff00) >> 8) + ((uint16_t)s[1] & 0xff00)) ^ 0x8080;

        out[0] = v;
        out[1] = v;

        src += 2;
        out += 2;
        count--;
    }
}

void __far pascal _clip32_s8_lq_stereo (void __far *outbuf, const int32_t __far *mb, uint16_t count)
{
    const int32_t *src = mb;
    uint16_t *out = (uint16_t *) outbuf;

    while (count)
    {
        int32_t s[2];
        uint16_t v;

        s[0] = src[0];
        s[1] = src[1];

        if (s[0] < CLIP_MIN)
            s[0] = CLIP_MIN;
        else
            if (s[0] > CLIP_MAX)
                s[0] = CLIP_MAX;

        if (s[1] < CLIP_MIN)
            s[1] = CLIP_MIN;
        else
            if (s[1] > CLIP_MAX)
                s[1] = CLIP_MAX;

        v = (((uint16_t)s[0] & 0xff00) >> 8) + ((uint16_t)s[1] & 0xff00);

        out[0] = v;
        out[1] = v;

        src += 2;
        out += 2;
        count--;
    }
}

void __far pascal _clip32_u16_lq_stereo (void __far *outbuf, const int32_t __far *mb, uint16_t count)
{
    const int32_t *src = mb;
    uint32_t *out = (uint32_t *) outbuf;

    while (count)
    {
        int32_t s[2];
        union sample_32_t v;

        s[0] = src[0];
        s[1] = src[1];

        if (s[0] < CLIP_MIN)
            s[0] = CLIP_MIN;
        else
            if (s[0] > CLIP_MAX)
                s[0] = CLIP_MAX;

        if (s[1] < CLIP_MIN)
            s[1] = CLIP_MIN;
        else
            if (s[1] > CLIP_MAX)
                s[1] = CLIP_MAX;

        v.u16[0] = (uint16_t)s[0];
        v.u16[1] = (uint16_t)s[1];
        v.u32 ^= 0x80008000L;

        out[0] = v.u32;
        out[1] = v.u32;

        src += 2;
        out += 2;
        count--;
    }
}

void __far pascal _clip32_s16_lq_stereo (void __far *outbuf, const int32_t __far *mb, uint16_t count)
{
    const int32_t *src = mb;
    uint32_t *out = (uint32_t *)outbuf;

    while (count)
    {
        int32_t s[2];
        union sample_32_t v;

        s[0] = src[0];
        s[1] = src[1];

        if (s[0] < CLIP_MIN)
            s[0] = CLIP_MIN;
        else
            if (s[0] > CLIP_MAX)
                s[0] = CLIP_MAX;

        if (s[1] < CLIP_MIN)
            s[1] = CLIP_MIN;
        else
            if (s[1] > CLIP_MAX)
                s[1] = CLIP_MAX;

        v.s16[0] = (int16_t)s[0];
        v.s16[1] = (int16_t)s[1];
        v.u32 ^= 0x80008000L;

        out[0] = v.u32;
        out[1] = v.u32;

        src += 2;
        out += 2;
        count--;
    }
}

#endif  /* USE_ASM_I386 != 1 */
