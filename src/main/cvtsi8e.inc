/* cvtsi8e.inc - "convert_sign_8_EM" routine.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

bool convert_sign_8_EM (EMSHDL handle, uint32_t offset, uint32_t size)
{
    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("Requested: size=0x%lX, handle=0x%04hX, offset=0x%lX, pages=0x%04hX:0x%04hX-0x%04hX:0x%04hX",
            (uint32_t) size,
            (uint16_t) handle,
            (uint32_t) offset,
            (uint16_t) get_EM_page (offset),
            (uint16_t) get_EM_page_offset (offset),
            (uint16_t) get_EM_page (offset + size - 1),
            (uint16_t) get_EM_page_offset (offset + size - 1)
        );

    /* 'size' is counter */

    while (size)
    {
        uint16_t seg_offset = get_EM_page_offset (offset);
        uint16_t count;
        uint8_t __far *p;
        bool aligned;

        /* counter register in DOS segment limit */
        if ((uint32_t) seg_offset + size > (1UL<<16))
            count = (1UL<<16) - seg_offset;
        else
            count = size;

        /* trick: instead of 0 count use 1<<16 */

        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("Mapping: size=0x%lX, handle=0x%04hX, offset=0x%lX, pages=0x%04hX:0x%04hX-0x%04hX:0x%04hX",
                (uint32_t) count ? count : (1UL<<16),
                (uint16_t) handle,
                (uint32_t) offset,
                (uint16_t) get_EM_page (offset),
                (uint16_t) get_EM_page_offset (offset),
                (uint16_t) get_EM_page (offset + (count ? count : (1UL<<16)) - 1),
                (uint16_t) get_EM_page_offset (offset + (count ? count : (1UL<<16)) - 1)
            );

        if (!map_EM_data (handle, offset, count ? count : (1UL<<16)))
        {
            DEBUG_ERR ("Failed to map EM.");
            return false;
        }

        p = MK_FP (emsFrameSeg, seg_offset);

        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("Converting: count=0x%lX, address=0x%lX, pointer=0x%04hX:0x%04hX",
                (uint32_t) count ? count : (1UL<<16),
                (uint32_t) FARPTR_TO_LONG (p),
                (uint16_t) FP_SEG (p),
                (uint16_t) FP_OFF (p)
            );

        offset += count ? count : (1UL<<16);
        size -= count ? count : (1UL<<16);

        /* 2 bytes alignment */
        if (FP_OFF (p) & 1)
        {
            *p ^= 0x80;
            p++;
            count--;
            aligned = true;
        }
        else
            aligned = false;

        if (((!count) && !aligned) || (count & ~1))
            do
            {
                *((uint16_t __far *)p) ^= 0x8080;
                p += 2;
                count -= 2;
            } while (count & ~1);

        if (count & 1)
            *p ^= 0x80;
    }

    return true;
}
