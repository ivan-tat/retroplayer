/* pcmsmp.c -- PCM sample handling library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/string.h"
#include "debug.h"
#include "cc/dos.h"
#include "cc/stdio.h"
#include "dos/ems.h"
#include "main/musdefs.h"
#include "main/pcmsmp.h"

/*** PCM sample ***/

void pcmsmp_init (PCMSMP *self)
{
    if (self)
    {
        memset (self, 0, sizeof (PCMSMP));
        self->data.em.handle = EMSBADHDL;
    }
}

bool pcmsmp_needs_expansion (PCMSMP *self, uint16_t min_length)
{
    if (self)
    {
        uint32_t len;

        if ((self->flags & PCMSMPFL_LOOP_MASK) == PCMSMPFL_LOOP_NONE)
            len = self->length;
        else
            len = self->loop_end - self->loop_start;

        return len < min_length;
    }
    else
        return false;
}

void pcmsmp_calc_expansion (PCMSMP *self, PCMSMPEXP *se, uint16_t min_length)
{
    PCMSMPLOOP loop;
    PCMSMPEXP x;

    loop = (self->flags >> PCMSMPFL_LOOPSHIFT) & PCMSMPLOOP_MASK;

    if (loop == PCMSMPLOOP_NONE)
    {
        x.orig_start = 0;
        x.orig_len = self->length;
    }
    else
    {
        x.orig_start = self->loop_start;
        x.orig_len = self->loop_end - x.orig_start;
    }

    x.start = x.orig_start;
    x.len = x.orig_len;

    if (x.len < min_length)
    {
        switch (loop)
        {
        case PCMSMPLOOP_BACKWARD:
            /* Skip one forward part to repeat it's mirrored version */
            x.start += x.len;
            break;
        case PCMSMPLOOP_PINGPONG:
            /* We must repeat two parts (forward and backward) */
            x.len <<= 1;
            break;
        default:
            break;
        }

        /* Expand loop: len * count >= min_length */
        x.count = (min_length + x.len - 1) / x.len;
        x.len *= x.count;
    }
    else
        x.count = 0;

    *se = x;
}

bool pcmsmp_expand (PCMSMP *self, uint16_t min_length)
{
    PCMSMPEXP se;
    uint8_t bits;
    #pragma pack(push, 1);
    union
    {
        void __far *p;
        uint8_t __far *u8;
        uint16_t __far *u16;
    } data, target, backward;
    #pragma pack(pop);
    uint32_t o_start;
    uint16_t o_size;
    uint32_t n_start;
    uint16_t n_size;
    uint32_t copy_start;
    uint16_t copy_size, i;
    //#if DEBUG == 1
    void __far *data_start;
    //#endif  /* DEBUG == 1*/
    PCMSMPLOOP loop;

    pcmsmp_calc_expansion (self, &se, min_length);

    bits = ((self->flags & PCMSMPFL_16BITS) ? 16 : 8);

    //#if DEBUG == 1
    if (DEBUG_FILE_S3M_LOAD)
    {
        DEBUG_INFO_ ("Requested: bits=%hhu, orig_start=0x%lX, orig_len=0x%hX, start=0x%lX, len=0x%hX, count=%hu",
            (uint8_t) bits,
            (uint32_t) se.orig_start,
            (uint16_t) se.orig_len,
            (uint32_t) se.start,
            (uint16_t) se.len,
            (uint16_t) se.count
        );
    }
    //#endif  /* DEBUG == 1*/

    o_start = se.orig_start;
    o_size = se.orig_len;
    n_start = se.start;
    n_size = se.len;
    copy_size = o_size;

    if (bits == 16)
    {
        o_start <<= 1;
        o_size <<= 1;
        n_start <<= 1;
        n_size <<= 1;
        copy_size <<= 1;
    }

    //#if DEBUG == 1
    if (DEBUG_FILE_S3M_LOAD)
    {
        DEBUG_INFO_ ("data: old_size=0x%lX, new_size=0x%lX",
            (uint32_t) o_start + o_size,
            (uint32_t) n_start + n_size
        );
    }
    //#endif  /* DEBUG == 1*/

    if (self->flags & PCMSMPFL_EM)
    {
        copy_start = self->data.em.offset + o_start;
        if (!map_EM_data (self->data.em.handle,
            copy_start, n_start + n_size - o_start))
        {
            DEBUG_ERR_ ("Failed to map EM for %s.", "sample");
            return false;   /* Error */
        }
        data.p = MK_FP (emsFrameSeg, get_EM_page_offset (copy_start));
    }
    else
    {
        copy_start = FARPTR_TO_LONG (self->data.dos.ptr) + o_start;
        data.p = FARPTR_FROM_LONG (copy_start);
    }

    /* 'data' points to original loop start */

    //#if DEBUG == 1
    data_start = data.p;
    //#endif  /* DEBUG == 1*/

    target.p = (void __far *) (data.u8 + o_size);

    /* 'target' points to current sample's end */

    loop = (self->flags >> PCMSMPFL_LOOPSHIFT) & PCMSMPLOOP_MASK;
    switch (loop)
    {
    case PCMSMPLOOP_FORWARD:
        break;

    case PCMSMPLOOP_BACKWARD:
    case PCMSMPLOOP_PINGPONG:
        /* In any case store backward part of the original loop:
         * - for backward loop we have to repeat only backward part;
         * - for ping-pong loop we have to repeat both parts. */
        backward.p = target.p;  /* save value */
        data.p = target.p;      /* 'data' is current pointer */
        if (bits == 16)
        {
            for (i = se.orig_len; i; i--)
            {
                data.u16--;     /* go backward */
                *target.u16 = *data.u16;
                target.u16++;   /* go forward */
            }
        }
        else
        {
            for (i = se.orig_len; i; i--)
            {
                data.u8--;      /* go backward */
                *target.u8 = *data.u8;
                target.u8++;    /* go forward */
            }
        }

        /* 'data' points to forward loop start */

        if (loop == PCMSMPLOOP_BACKWARD)
            data.p = backward.p;    /* restore value for backward loop only */
        else
            copy_size *= 2;         /* we have to copy two parts for ping-pong loop */

        self->flags &= ~PCMSMPFL_LOOP_MASK;
        self->flags |= PCMSMPFL_LOOP_FORWARD;
        se.count--;
        break;

    case PCMSMPLOOP_NONE:
    default:
        memset (target.p, 0, n_size - o_size);
        se.count = 0;
        break;
    }

    /* Copy loop body if needed */
    for (; se.count; target.u8 += copy_size, se.count--)
        memcpy (target.p, data.p, copy_size);

    self->size = n_start + n_size;
    self->length = se.start + se.len;
    self->loop_start = se.start;
    self->loop_end = se.start + se.len;

    //#if DEBUG == 1
    if (DEBUG_FILE_S3M_LOAD)
    {
        DEBUG_dump_sample_info (self, -1);
        DEBUG_INFO ("Dump of expanded part:");
        DEBUG_dump_mem (data_start, n_size, "data: ");
    }
    //#endif  /* DEBUG == 1 */

    return true;
}

void pcmsmp_free (PCMSMP *self)
{
    if (self->flags & PCMSMPFL_EM)
    {
        if (self->flags & PCMSMPFL_OWNHDL)
            emsFree (self->data.em.handle);
    }
    else
    {
        void __far *data = self->data.dos.ptr;

        if (data)
            _dos_freemem (FP_SEG (data));
    }
}

/*** PCM samples list ***/

void _pcmsmpl_init_item (void *self, void *item)
{
    pcmsmp_init ((PCMSMP *) item);
}

void _pcmsmpl_free_item (void *self, void *item)
{
    pcmsmp_free ((PCMSMP *) item);
}

void pcmsmpl_init (PCMSMPLIST *self)
{
    if (self)
    {
        memset (self, 0, sizeof (PCMSMPLIST));
        dynarr_init (&self->list, self, sizeof (PCMSMP), _pcmsmpl_init_item, _pcmsmpl_free_item);
        self->handle = EMSBADHDL;
    }
}

void pcmsmpl_free (PCMSMPLIST *self)
{
    if (self)
    {
        dynarr_free (&self->list);

        if (self->flags & PCMSMPLFL_OWNHDL)
            emsFree (self->handle);

        pcmsmpl_init (self);    // clear
    }
}

#if DEBUG == 1

void DEBUG_dump_sample_info (PCMSMP *smp, int index)
{
    #define _BUF_SIZE 96
    char place[_BUF_SIZE];

    if (smp->flags & PCMSMPFL_EM)
    {
        uint32_t start = smp->data.em.offset;
        uint32_t end = start + smp->mem_size - 1;

        snprintf (place, _BUF_SIZE,
                "EM (handle=0x%04hX(%s), offset=0x%lX, pages=0x%04hX:0x%04hX-0x%04hX:0x%04hX)",
            (uint16_t) smp->data.em.handle,
            (char *) (smp->flags & PCMSMPFL_OWNHDL) ? "priv" : "glob",
            (uint32_t) start,
            (uint16_t) get_EM_page (start),
            (uint16_t) get_EM_page_offset (start),
            (uint16_t) get_EM_page (end),
            (uint16_t) get_EM_page_offset (end)
        );
    }
    else
    {
        void __far *data = smp->data.dos.ptr;

        snprintf (place, _BUF_SIZE, "DOS (address=0x%lX, pointer=0x%04hX:0x%04hX)",
            (uint32_t) FARPTR_TO_LONG (data),
            (uint16_t) FP_SEG (data),
            (uint16_t) FP_OFF (data)
        );
    }

    DEBUG_INFO_ ("index=%d, address=0x%lX, pointer=0x%04hX:0x%04hX, flags=0x%hhX, available=%hhu, volume=%hhu, title='%s'",
        index,
        (uint32_t) FARPTR_TO_LONG (smp),
        (uint16_t) FP_SEG (smp),
        (uint16_t) FP_OFF (smp),
        (uint8_t) smp->flags,
        (uint8_t) ((smp->flags & PCMSMPFL_AVAIL) ? 1 : 0),
        (uint8_t) smp->volume,
        (char *) smp->title
    );

    DEBUG_INFO_ ("place=%s.",
        place
    );

    DEBUG_INFO_ ("format: size=0x%lX, mem_size=0x%lX, bits=%hhu, rate=%hu, loop=%hhu:0x%lX-0x%lX.",
        (uint32_t) smp->size,
        (uint32_t) smp->mem_size,
        (uint8_t) ((smp->flags & PCMSMPFL_16BITS) ? 16 : 8),
        (uint16_t) smp->rate,
        (uint8_t) ((smp->flags >> PCMSMPFL_LOOPSHIFT) & PCMSMPLOOP_MASK),
        (uint32_t) smp->loop_start,
        (uint32_t) smp->loop_end
    );

    DEBUG_INFO ("Raw object dump:");
    DEBUG_dump_mem (smp, sizeof (PCMSMP), "object: ");
    #undef _BUF_SIZE
}

#endif  /* DEBUG == 1 */
