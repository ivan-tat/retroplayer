/* fillvars.c -- variables for functions to fill DMA buffer.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/dos.h"
#include "cc/stdio.h"
#include "cc/string.h"
#include "debug.h"
#include "common.h"
#include "hw/dma.h"
#include "hw/sb/sbctl.h"
#include "main/fillvars.h"

void snddmabuf_init (SNDDMABUF *self)
{
    if (self)
    {
        self->flags = 0;
        self->buf = NULL;
        clear_sample_format(&(self->format));
        self->frameSize = 0;
        self->framesCount = 0;
        self->frameLast = 0;
        self->frameActive = 0;
    }
}

bool snddmabuf_alloc (SNDDMABUF *self, uint32_t dmaSize)
{
    DEBUG_BEGIN ();

    if (self)
    {
        if (!self->buf)
            self->buf = _new(DMABUF);

        if (!self->buf)
        {
            DEBUG_ERR ("Failed to initialize DMA buffer object.");
            return false;
        }

        dmaBuf_init(self->buf);

        if (dmaBuf_alloc(self->buf, dmaSize))
        {
            DEBUG_SUCCESS ();
            return true;
        }
        else
        {
            DEBUG_ERR ("Failed to allocate DMA buffer.");
            return false;
        }
    }
    else
    {
        DEBUG_ERR ("Self is NULL.");
        return false;
    }
}

static uint16_t __near _snddmabuf_get_frame_offset(SNDDMABUF *self, uint8_t index)
{
    return index < self->framesCount ? index * self->frameSize : 0;
}

uint16_t snddmabuf_get_frame_offset (SNDDMABUF *self, uint8_t index)
{
    if (self)
        return _snddmabuf_get_frame_offset(self, index);
    else
        return 0;
}

void *snddmabuf_get_frame (SNDDMABUF *self, uint8_t index)
{
    if (self && self->buf)
    {
        void __far *data = self->buf->data;
        if (data)
            return MK_FP(FP_SEG(data), FP_OFF(data) + _snddmabuf_get_frame_offset(self, index));
    }

    return NULL;
}

uint16_t snddmabuf_get_offset_from_count (SNDDMABUF *self, uint16_t count)
{
    uint16_t bufOff;

    if (self)
    {
        bufOff = count;

        if (get_sample_format_bits(&(self->format)) == 16)
            bufOff <<= 1;

        if (get_sample_format_channels(&(self->format)) == 2)
            bufOff <<= 1;

        if (self->flags & SNDDMABUFFL_LQ)
            bufOff <<= 1;
    }
    else
        bufOff = 0;

    return bufOff;
}

uint16_t snddmabuf_get_count_from_offset (SNDDMABUF *self, uint16_t bufOff)
{
    uint16_t count;

    if (self)
    {
        count = bufOff;

        if (get_sample_format_bits(&(self->format)) == 16)
            count >>= 1;

        if (get_sample_format_channels(&(self->format)) == 2)
            count >>= 1;

        if (self->flags & SNDDMABUFFL_LQ)
            count >>= 1;
    }
    else
        count = 0;

    return count;
}

void snddmabuf_free (SNDDMABUF *self)
{
    if (self)
        if (self->buf)
        {
            dmaBuf_free(self->buf);
            _delete(self->buf);
        }
}

#if DEBUG == 1

void _DEBUG_dump_snddmabuf_info (const char *file, int line, const char *func, const char *name, SNDDMABUF *p)
{
    _DEBUG_dump_ptr (file, line, func, name, p);
    if (p)
    {
        _DEBUG_dump_u16 (file, line, func, "(SNDDMABUF)->flags", &p->flags);
        _DEBUG_dump_dmabuf_info (file, line, func, "(SNDDMABUF)->(DMABUF)buf", p->buf);
        _DEBUG_dump_hwsmpfmt_info (file, line, func, "(SNDDMABUF)->(HWSMPFMT)format", &p->format);
        _DEBUG_dump_u16 (file, line, func, "(SNDDMABUF)->frameSize", &p->frameSize);
        _DEBUG_dump_u8 (file, line, func, "(SNDDMABUF)->framesCount", &p->framesCount);
        _DEBUG_dump_s8 (file, line, func, "(SNDDMABUF)->frameLast", &p->frameLast);
        _DEBUG_dump_u8 (file, line, func, "(SNDDMABUF)->frameActive", &p->frameActive);
    }
}

#endif  /* DEBUG == 1 */
