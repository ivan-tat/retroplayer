(* posttab.pas -- Pascal declarations for posttab.c.

   This file is for linking compiled object files with Pascal linker.
   It will be deleted in future when we rewrite the project in C.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. *)

unit posttab;

interface

(*$I defines.pas*)

var
    amptab: array [1..256*3] of LongInt;

procedure amptab_set_volume;
procedure _amplify32;
procedure _clip32_s8;
procedure _clip32_u8;
procedure _clip32_s16;
procedure _clip32_u16;
procedure _clip32_s8_lq;
procedure _clip32_u8_lq;
procedure _clip32_s16_lq;
procedure _clip32_u16_lq;
procedure _clip32_s8_lq_stereo;
procedure _clip32_u8_lq_stereo;
procedure _clip32_s16_lq_stereo;
procedure _clip32_u16_lq_stereo;

implementation

uses
(*$ifdef DEBUG*)
    watcom,
    stdio;
(*$else*)
    watcom;
(*$endif*)  (* DEBUG *)

(*$ifdef USE_ASM_I386*)
(*$l asm/amp.obj*)
(*$l asm/clip.obj*)
(*$endif*)  (* USE_ASM_I386 *)

(*$l posttab.obj*)

procedure amptab_set_volume; external;
procedure _amplify32; external;
procedure _clip32_s8; external;
procedure _clip32_u8; external;
procedure _clip32_s16; external;
procedure _clip32_u16; external;
procedure _clip32_s8_lq; external;
procedure _clip32_u8_lq; external;
procedure _clip32_s16_lq; external;
procedure _clip32_u16_lq; external;
procedure _clip32_s8_lq_stereo; external;
procedure _clip32_u8_lq_stereo; external;
procedure _clip32_s16_lq_stereo; external;
procedure _clip32_u16_lq_stereo; external;

end.
