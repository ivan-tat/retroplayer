/* musmod.h -- declarations for musmod.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _MUSMOD_H_INCLUDED
#define _MUSMOD_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/string.h"
#include "main/pcmsmp.h"
#include "main/musins.h"
#include "main/muspat.h"

/* Limits */

#define MUSMOD_TITLE_LEN    28  // including trailing zero
#define MUSMOD_FORMAT_LEN   32  // including trailing zero
#define MUSMOD_CHANNELS_MAX 32
#define MUSMOD_ORDER_LEN    100 // 0..99 entries

#define MUSMOD_GLOBAL_VOLUME_MAX 64
#define MUSMOD_GLOBAL_VOLUME_BITS 6
#define MUSMOD_MASTER_VOLUME_MAX 128
#define MUSMOD_MASTER_VOLUME_BITS 7

/* Channel panning */

typedef uint8_t musmod_channel_pan_t;
typedef musmod_channel_pan_t MUSMODCHNPAN;

#define MUSMODCHNPANFL_PAN_MASK 0x7f
#define MUSMODCHNPANFL_ENABLED  0x80

#define MUSMODCHNPAN_LEFT   0
#define MUSMODCHNPAN_CENTER 32
#define MUSMODCHNPAN_RIGHT  64
#define MUSMODCHNPAN_MAX    64
#define MUSMODCHNPAN_SHIFT  6

/* Channel */

#pragma pack(push, 1);
typedef struct musmod_channel_t
{
    MUSMODCHNPAN pan;
};
#pragma pack(pop);
typedef struct musmod_channel_t MUSMODCHN;

/* Flags */

typedef uint16_t musmod_flags_t;
typedef musmod_flags_t MUSMODFLAGS;

#define MUSMODFL_LOADED         (1 << 0)
#define MUSMODFL_STEREO         (1 << 1)
#define MUSMODFL_AMIGA_LIMITS   (1 << 2)

/* Structure */

typedef struct music_module_t
{
    MUSMODFLAGS flags;
    char        title[MUSMOD_TITLE_LEN];
    char        format[MUSMOD_FORMAT_LEN];
    MUSMODCHN   channels[MUSMOD_CHANNELS_MAX];
    PCMSMPLIST  samples;
    MUSINSLIST  instruments;
    MUSPATLIST  patterns;
    MUSPATORDER order;
    uint8_t     channels_count;
    /*
    uint16_t    order_start;
    */
    uint8_t     global_volume;
    uint8_t     master_volume;
    uint8_t     tempo;
    uint8_t     speed;
};
typedef struct music_module_t MUSMOD;

/* Methods */

void musmod_init (MUSMOD *self);
void musmod_free (MUSMOD *self);

#endif  /* !_MUSMOD_H_INCLUDED */
