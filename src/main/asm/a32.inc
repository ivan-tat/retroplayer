; a32.inc - "_amplify32" routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

LOOP_TABLE _amplify32, 4

AMP32@PROC _amplify32
AMP32@ENTER _amplify32
    push    ax
    xor     edi, edi
    lds     si, [_dBuf]
    les     di, [_dAmpTab]
    sub     si, bx                      ; input data is 1 channel @ 32 bits
    sub     si, bx
; word at ss:[sp] = jump target
; cx = loops count
; ebx: bits 16-31 are cleared
; ds:si = input/output buffer
; es:edi = lookup table
    retn                                ; jump

_amplify32@loop macro num
_amplify32@loop_&num:
    mov     bl, ds:[si][num*4+0]
    xor     bh, bh
    mov     eax, es:[edi+ebx*4]
    mov     bl, ds:[si][num*4+1]
    inc     bh
    add     eax, es:[edi+ebx*4]
    mov     bl, ds:[si][num*4+2]
    inc     bh
    add     eax, es:[edi+ebx*4]
    mov     ds:[si][num*4], eax
endm

align 16

index = 0
rept _amplify32@LENGTH
    _amplify32@loop %index
    index = index + 1
endm

    add     si, _amplify32@LENGTH*4
    dec     cx
    jnz     _amplify32@loop_0
AMP32@LEAVE
AMP32@ENDP _amplify32
