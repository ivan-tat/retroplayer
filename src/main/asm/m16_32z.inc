; m16_32z.inc - "_mix16_32s2" routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

LOOP_TABLE _mix16_32s2, 3

MIX16_32@PROC _mix16_32s2
MIX16_32@ENTER _mix16_32s2
    push    bp                          ; save 'bp'
    push    ax
    mov     ax, cx                      ; 'ax' = count
    lfs     si, [_dInBuf]
    les     di, [_dOutBuf]
    sub     si, bx                      ; input data is 1 channel @ 16 bits
    shl     bx, 2                       ; 'bx' = n * 8
    sub     di, bx                      ; output buffer is 2 channels @ 32 bits
    xor     ebx, ebx
    xor     ecx, ecx
    mov     ds, [_wVolTabSeg]
    mov     cx, [_wVol]
    mov     bh, cl
    mov     edx, 8000h
    mov     bp, ax
; word at ss:[sp] = jump target
; bp = loops count
; ebx = (volume_left & 0x3f) << 8
; ecx = (volume_right & 0x3f) << 8
; edx = 0x8000
; fs:si = input data
; es:di = output buffer
; ds:0 = volume table
    retn                                ; jump

_mix16_32s2@loop macro num
_mix16_32s2@loop_&num:
    mov     bl, fs:[si][num*2+1]
    movsx   eax, word ptr ds:[ebx+ebx]
    mov     cl, bl
    mov     bl, fs:[si][num*2]
    add     ax, ds:[edx+ebx*2]
    add     es:[di][num*8], eax
    movsx   eax, word ptr ds:[ecx+ecx]
    mov     cl, bl
    add     ax, ds:[edx+ecx*2]
    add     es:[di][num*8+4], eax
endm

align 16

index = 0
rept _mix16_32s2@LENGTH
    _mix16_32s2@loop %index
    index = index + 1
endm

    add     si, _mix16_32s2@LENGTH*2
    add     di, _mix16_32s2@LENGTH*8
    dec     bp
    jnz     _mix16_32s2@loop_0

    pop     bp                          ; restore 'bp'
MIX16_32@EXIT
MIX16_32@ENDP _mix16_32s2
