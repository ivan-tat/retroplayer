; mixer.asm - mixer routines.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

.386

include "main/mixer.def"
include "main/asm/common.def"
include "main/asm/common.mac"

MIXER_TEXT segment para public use16 'CODE'
MIXER_TEXT ends

CODE_SEGMENT macro
MIXER_TEXT segment
assume cs:MIXER_TEXT, ds:DGROUP, ss:DGROUP
endm

CODE_ENDS macro
MIXER_TEXT ends
endm

include "main/asm/m16_32.mac"
include "main/asm/m16_32.inc"
include "main/asm/m16_32s.inc"
include "main/asm/m16_32z.inc"

end
