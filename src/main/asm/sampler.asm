; sampler.asm - sampler routines.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

.386

include "main/mixer.def"
include "main/asm/common.def"
include "main/asm/common.mac"

SAMPLER_TEXT segment para public use16 'CODE'
SAMPLER_TEXT ends

CODE_SEGMENT macro
SAMPLER_TEXT segment
assume cs:SAMPLER_TEXT, ds:DGROUP, ss:DGROUP
endm

CODE_ENDS macro
SAMPLER_TEXT ends
endm

include "main/asm/pn16.mac"
include "main/asm/p8n16.inc"
include "main/asm/p16n16.inc"
include "main/asm/pl16.mac"
include "main/asm/p8l16.inc"
include "main/asm/p16l16.inc"
include "main/asm/pmn32.mac"
include "main/asm/pm8n32.inc"
include "main/asm/pm16n32.inc"
include "main/asm/pm8n32s.inc"
include "main/asm/pm16n32s.inc"
include "main/asm/pm8n32z.inc"
include "main/asm/pm16n32z.inc"

end
