; pm16n32.inc - "_playmix16_nearest_32" routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

LOOP_TABLE _playmix16_nearest_32, 4

PLAYMIX_NEAREST_32@PROC _playmix16_nearest_32
local _count: word
local _step_frac: word
local _step: word
PLAYMIX_NEAREST_32@ENTER _playmix16_nearest_32
    push    ax
    mov     [_count], cx
    les     di, [_dOutBuf]
    shl     bx, 1                       ; 'bx' = n * 4
    sub     di, bx                      ; output is 1 channel @ 32 bits
    lgs     bx, [_dParam]
    xor     esi, esi
    lds     si, gs:[bx][play_sample_param_t.dData]
    mov     cx, gs:[bx][play_sample_param_t.wPosFrac]
    shr     si, 1
    add     si, gs:[bx][play_sample_param_t.wPos]
    mov     ax, gs:[bx][play_sample_param_t.wStepFrac]
    mov     dx, gs:[bx][play_sample_param_t.wStep]
    mov     [_step_frac], ax
    mov     [_step], dx
    mov     edx, 8000h
    xor     ebx, ebx
    mov     bh, byte ptr [_wVol]
    mov     fs, [_wVolTabSeg]
; word at ss:[sp] = jump target
; word at ss:[_count] = loops count
; word at ss:[_step_frac] = step.frac
; word at ss:[_step] = step.int
; ebx = (volume & 0x3f) << 8
; cx = pos.frac
; edx = 0x8000
; esi = pos.int & 0x7fff
; ds = input data segment
; ds:esi*2 = input data
; es:di = output buffer
; fs:0 = volume table
    retn                                ; jump

_playmix16_nearest_32@loop macro num
_playmix16_nearest_32@loop_&num:
    mov     bl, ds:[esi+esi][1]
    movsx   eax, word ptr fs:[ebx+ebx]
    mov     bl, ds:[esi+esi]
    add     ax, fs:[edx+ebx*2]
    add     cx, [_step_frac]
    adc     si, [_step]
    add     es:[di][num*4], eax
endm

align 16

index = 0
rept _playmix16_nearest_32@LENGTH
    _playmix16_nearest_32@loop %index
    index = index + 1
endm

    add     di, _playmix16_nearest_32@LENGTH*4
    dec     [_count]
    jnz     _playmix16_nearest_32@loop_0

    shl     si, 1
    mov     bx, word ptr [_dParam]
    sub     si, word ptr gs:[bx][play_sample_param_t.dData]
    shr     si, 1
PLAYMIX_NEAREST_32@EXIT
PLAYMIX_NEAREST_32@ENDP _playmix16_nearest_32
