; p16n16.inc - "_play16_nearest_16" routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

LOOP_TABLE _play16_nearest_16, 4

PLAY_NEAREST_16@PROC _play16_nearest_16
PLAY_NEAREST_16@ENTER _play16_nearest_16
    push    bp                          ; save 'bp'
    push    ax
    mov     ax, cx                      ; 'ax' = count
    mov     dx, bx                      ; 'dx' = n * 2
    xor     esi, esi
    lfs     bx, [_dParam]
    lds     si, fs:[bx][play_sample_param_t.dData]
    shr     si, 1                       ; 'esi' is in 16 bits units
    mov     cx, fs:[bx][play_sample_param_t.wPosFrac]
    add     si, fs:[bx][play_sample_param_t.wPos]
    les     di, fs:[bx][play_sample_param_t.dBuf]
    sub     di, dx                      ; output is 1 channel @ 16 bits
    mov     dx, fs:[bx][play_sample_param_t.wStep]
    mov     bx, fs:[bx][play_sample_param_t.wStepFrac]
    mov     bp, ax                      ; 'bp' = count
; word at ss:[sp] = jump target
; word at ss:[sp+2] = saved 'bp'
; bp = loops count
; bx = step.frac
; cx = pos.frac
; dx = step.int
; esi = pos.int & 0x7fff
; ds = input data segment
; ds:esi*2 = input data
; es:di = output buffer
    retn                                ; jump

_play16_nearest_16@loop macro num
_play16_nearest_16@loop_&num:
    mov     ax, ds:[esi+esi]
    add     cx, bx
    adc     si, dx
    mov     es:[di+num*2], ax
endm

align 16

index = 0
rept _play16_nearest_16@LENGTH
    _play16_nearest_16@loop %index
    index = index + 1
endm
    add     di, _play16_nearest_16@LENGTH*2
    dec     bp
    jnz     _play16_nearest_16@loop_0

    pop     bp                          ; restore 'bp'
    shl     si, 1
    mov     bx, word ptr [_dParam]
    sub     si, word ptr fs:[bx][play_sample_param_t.dData]
    shr     si, 1
PLAY_NEAREST_16@EXIT
PLAY_NEAREST_16@ENDP _play16_nearest_16
