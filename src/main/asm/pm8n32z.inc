; pm8n32z.inc - "_playmix8_nearest_32s2" routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

LOOP_TABLE _playmix8_nearest_32s2, 4

PLAYMIX_NEAREST_32@PROC _playmix8_nearest_32s2
local _count: word
local _step_frac: word
local _step: word
PLAYMIX_NEAREST_32@ENTER _playmix8_nearest_32s2
    push    ax
    mov     [_count], cx
    les     di, [_dOutBuf]
    shl     bx, 2                       ; 'bx' = n * 8
    sub     di, bx                      ; output is 2 channels @ 32 bits
    lgs     bx, [_dParam]
    lfs     si, gs:[bx][play_sample_param_t.dData]
    mov     cx, gs:[bx][play_sample_param_t.wPosFrac]
    add     si, gs:[bx][play_sample_param_t.wPos]
    mov     ax, gs:[bx][play_sample_param_t.wStepFrac]
    mov     dx, gs:[bx][play_sample_param_t.wStep]
    mov     [_step_frac], ax
    mov     [_step], dx
    xor     ebx, ebx
    movzx   edx, [_wVol]
    mov     bh, dl
    mov     ds, [_wVolTabSeg]
; word at ss:[sp] = jump target
; word at ss:[_count] = loops count
; word at ss:[_step_frac] = step.frac
; word at ss:[_step] = step.int
; ebx = (volume_left & 0x3f) << 8
; edx = (volume_right & 0x3f) << 8
; cx = pos.frac
; si = pos.int
; fs = input data segment
; fs:si = input data
; es:di = output buffer
; ds:0 = volume table
    retn                                ; jump

_playmix8_nearest_32s2@loop macro num
_playmix8_nearest_32s2@loop_&num:
    mov     bl, fs:[si]
    mov     dl, bl
    movsx   eax, word ptr ds:[ebx+ebx]
    add     es:[di][num*8], eax
    movsx   eax, word ptr ds:[edx+edx]
    add     cx, [_step_frac]
    adc     si, [_step]
    add     es:[di][num*8+4], eax
endm

align 16

index = 0
rept _playmix8_nearest_32s2@LENGTH
    _playmix8_nearest_32s2@loop %index
    index = index + 1
endm

    add     di, _playmix8_nearest_32s2@LENGTH*8
    dec     [_count]
    jnz     _playmix8_nearest_32s2@loop_0

    mov     bx, word ptr [_dParam]
    sub     si, word ptr gs:[bx][play_sample_param_t.dData]
PLAYMIX_NEAREST_32@EXIT
PLAYMIX_NEAREST_32@ENDP _playmix8_nearest_32s2
