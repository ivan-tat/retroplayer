; pm8n32.inc - "_playmix8_nearest_32" routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

LOOP_TABLE _playmix8_nearest_32, 4

PLAYMIX_NEAREST_32@PROC _playmix8_nearest_32
local _count: word
local _step_frac: word
PLAYMIX_NEAREST_32@ENTER _playmix8_nearest_32
    push    ax
    mov     [_count], cx
    les     di, [_dOutBuf]
    shl     bx, 1                       ; 'bx' = n * 4
    sub     di, bx                      ; output is 1 channel @ 32 bits
    lgs     bx, [_dParam]
    lds     si, gs:[bx][play_sample_param_t.dData]
    mov     cx, gs:[bx][play_sample_param_t.wPosFrac]
    add     si, gs:[bx][play_sample_param_t.wPos]
    mov     ax, gs:[bx][play_sample_param_t.wStepFrac]
    mov     dx, gs:[bx][play_sample_param_t.wStep]
    mov     [_step_frac], ax
    xor     ebx, ebx
    mov     bh, byte ptr [_wVol]
    mov     fs, [_wVolTabSeg]
; word at ss:[sp] = jump target
; word at ss:[_count] = loops count
; word at ss:[_step_frac] = step.frac
; ebx = (volume & 0x3f) << 8
; cx = pos.frac
; dx = step.int
; si = pos.int
; ds = input data segment
; ds:si = input data
; es:di = output buffer
; fs:0 = volume table
    retn                                ; jump

_playmix8_nearest_32@loop macro num
_playmix8_nearest_32@loop_&num:
    mov     bl, ds:[si]
    add     cx, [_step_frac]
    adc     si, dx
    movsx   eax, word ptr fs:[ebx+ebx]
    add     es:[di][num*4], eax
endm

align 16

index = 0
rept _playmix8_nearest_32@LENGTH
    _playmix8_nearest_32@loop %index
    index = index + 1
endm

    add     di, _playmix8_nearest_32@LENGTH*4
    dec     [_count]
    jnz     _playmix8_nearest_32@loop_0

    mov     bx, word ptr [_dParam]
    sub     si, word ptr gs:[bx][play_sample_param_t.dData]
PLAYMIX_NEAREST_32@LEAVE
PLAYMIX_NEAREST_32@ENDP _playmix8_nearest_32
