; p8n16.inc - "_play8_nearest_16" routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

LOOP_TABLE _play8_nearest_16, 4

PLAY_NEAREST_16@PROC _play8_nearest_16
PLAY_NEAREST_16@ENTER _play8_nearest_16
    push    bp                          ; save 'bp'
    push    ax
    mov     ax, cx                      ; 'ax' = count
    mov     dx, bx                      ; 'dx' = n * 2
    lfs     bx, [_dParam]
    lds     si, fs:[bx][play_sample_param_t.dData]
    mov     cx, fs:[bx][play_sample_param_t.wPosFrac]
    add     si, fs:[bx][play_sample_param_t.wPos]
    les     di, fs:[bx][play_sample_param_t.dBuf]
    sub     di, dx                      ; output is 1 channel @ 16 bits
    mov     dx, fs:[bx][play_sample_param_t.wStep]
    mov     bx, fs:[bx][play_sample_param_t.wStepFrac]
    mov     bp, ax                      ; 'bp' = count
    xor     al, al
; word at ss:[sp] = jump target
; word at ss:[sp+2] = saved 'bp'
; bp = loops count
; al = 0
; bx = step.frac
; cx = pos.frac
; dx = step.int
; si = pos.int
; ds = input data segment
; ds:si = input data
; es:di = output buffer
    retn                                ; jump

_play8_nearest_16@loop macro num
_play8_nearest_16@loop_&num:
    mov     ah, ds:[si]
    add     cx, bx
    adc     si, dx
    mov     es:[di+num*2], ax
endm

align 16

index = 0
rept _play8_nearest_16@LENGTH
    _play8_nearest_16@loop %index
    index = index + 1
endm

    add     di, _play8_nearest_16@LENGTH*2
    dec     bp
    jnz     _play8_nearest_16@loop_0

    pop     bp                          ; restore 'bp'
    mov     bx, word ptr [_dParam]
    sub     si, word ptr fs:[bx][play_sample_param_t.dData]
PLAY_NEAREST_16@LEAVE
PLAY_NEAREST_16@ENDP _play8_nearest_16
