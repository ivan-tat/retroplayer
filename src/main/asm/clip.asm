; clip.asm - clip routines.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

.386

include "main/asm/common.def"
include "main/asm/common.mac"

CLIP_TEXT segment para public use16 'CODE'
CLIP_TEXT ends

CODE_SEGMENT macro
CLIP_TEXT segment
assume cs:CLIP_TEXT, ds:DGROUP, ss:DGROUP
endm

CODE_ENDS macro
CLIP_TEXT ends
endm

include "main/asm/c32.mac"
include "main/asm/c32s8.inc"
include "main/asm/c32u8.inc"
include "main/asm/c32s16.inc"
include "main/asm/c32u16.inc"
include "main/asm/c32s8l.inc"
include "main/asm/c32u8l.inc"
include "main/asm/c32s16l.inc"
include "main/asm/c32u16l.inc"
include "main/asm/c32s8ls.inc"
include "main/asm/c32u8ls.inc"
include "main/asm/c32s16ls.inc"
include "main/asm/c32u16ls.inc"

end
