; amp.asm - amplify routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

.386

include "main/asm/common.def"
include "main/asm/common.mac"

AMP_TEXT segment para public use16 'CODE'
AMP_TEXT ends

CODE_SEGMENT macro
AMP_TEXT segment
assume cs:AMP_TEXT, ds:DGROUP, ss:DGROUP
endm

CODE_ENDS macro
AMP_TEXT ends
endm

include "main/asm/a32.mac"
include "main/asm/a32.inc"

end
