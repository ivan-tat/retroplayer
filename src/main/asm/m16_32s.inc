; m16_32s.inc - "_mix16_32s" routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

LOOP_TABLE _mix16_32s, 4

MIX16_32@PROC _mix16_32s
MIX16_32@ENTER _mix16_32s
    push    ax
    lds     si, [_dInBuf]
    les     di, [_dOutBuf]
    sub     si, bx                      ; input data is 1 channel @ 16 bits
    shl     bx, 2                       ; 'bx' = n * 8
    sub     di, bx                      ; output buffer is 2 channels @ 32 bits
    xor     ebx, ebx
    mov     fs, [_wVolTabSeg]
    mov     bh, byte ptr [_wVol]
    mov     edx, 8000h
; word at ss:[sp] = jump target
; cx = loops count
; ebx = (volume & 0x3f) << 8
; edx = 0x8000
; ds:si = input data
; es:di = output buffer
; fs:0 = volume table
    retn                                ; jump

_mix16_32s@loop macro num
_mix16_32s@loop_&num:
    mov     bl, ds:[si][num*2+1]
    movsx   eax, word ptr fs:[ebx+ebx]
    mov     bl, ds:[si][num*2]
    add     ax, fs:[edx+ebx*2]
    add     es:[di][num*8], eax
endm

align 16

index = 0
rept _mix16_32s@LENGTH
    _mix16_32s@loop %index
    index = index + 1
endm

    add     si, _mix16_32s@LENGTH*2
    add     di, _mix16_32s@LENGTH*8
    dec     cx
    jnz     _mix16_32s@loop_0
MIX16_32@EXIT
MIX16_32@ENDP _mix16_32s
