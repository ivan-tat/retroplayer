; c32u8.inc - "_clip32_u8" routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

LOOP_TABLE _clip32_u8, 3

CLIP32@PROC _clip32_u8
CLIP32@ENTER _clip32_u8
    push    ax
    lds     si, [_dInBuf]
    les     di, [_dOutBuf]
    sub     si, bx                      ; input data is 1 channel @ 32 bits
    sub     si, bx
    shr     bx, 1                       ; 'bx' = n
    sub     di, bx                      ; output buffer is 1 channel @ 8 bits
    mov     edx, -8000h
    mov     ebx, 7fffh
; word at ss:[sp] = jump target
; cx = loops count
; edx = -0x8000
; ebx = 0x7fff
; ds:si = input data
; es:di = output buffer
    retn                                ; jump

_clip32_u8@loop macro num
_clip32_u8@loop_&num:
    mov     eax, ds:[si][num*4]
    cmp     eax, edx
    jge     short _clip32_u8@check_top_&num
    mov     ax, dx
    jmp     short _clip32_u8@save_&num
_clip32_u8@check_top_&num:
    cmp     eax, ebx
    jle     short _clip32_u8@save_&num
    mov     ax, bx
_clip32_u8@save_&num:
    add     ax, dx                      ; convert to unsigned
    mov     es:[di][num], ah
endm

align 16

index = 0
rept _clip32_u8@LENGTH
    _clip32_u8@loop %index
    index = index + 1
endm

    add     si, _clip32_u8@LENGTH*4
    add     di, _clip32_u8@LENGTH
    dec     cx
    jnz     _clip32_u8@loop_0
CLIP32@EXIT
CLIP32@ENDP _clip32_u8
