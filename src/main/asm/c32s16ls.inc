; c32s16ls.inc - "_clip32_s16_lq_stereo" routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

LOOP_TABLE _clip32_s16_lq_stereo, 2

CLIP32@PROC _clip32_s16_lq_stereo
CLIP32@ENTER _clip32_s16_lq_stereo
    push    bp                          ; save 'bp'
    push    ax
    les     si, [_dInBuf]
    lds     di, [_dOutBuf]
    shl     bx, 2                       ; 'bx' = n * 8
    sub     si, bx                      ; input data is 2 channels @ 32 bits
    sub     di, bx                      ; output buffer is 2 channels @ 16 bits (doubled)
    mov     edx, -8000h
    mov     ebx, 7fffh
    mov     bp, cx
; word at ss:[sp] = jump target
; word at ss:[sp][2] = saved 'bp'
; bp = loops count
; edx = -0x8000
; ebx = 0x7fff
; es:si = input data
; ds:di = output buffer
    retn                                ; jump

_clip32_s16_lq_stereo@loop macro num
_clip32_s16_lq_stereo@loop_&num:
    mov     eax, es:[si][num*8]         ; eax = sample_left
    cmp     eax, edx
    jge     short _clip32_s16_lq_stereo@check_top_&num
    mov     ax, dx
    jmp     short _clip32_s16_lq_stereo@next_&num
_clip32_s16_lq_stereo@check_top_&num:
    cmp     eax, ebx
    jle     short _clip32_s16_lq_stereo@next_&num
    mov     ax, bx
_clip32_s16_lq_stereo@next_&num:
    mov     cx, ax                      ; cx = clipped_left
    mov     eax, es:[si][num*8+4]       ; eax = sample_right
    cmp     eax, edx
    jge     short _clip32_s16_lq_stereo@check_top2_&num
    mov     ax, dx
    jmp     short _clip32_s16_lq_stereo@save_&num
_clip32_s16_lq_stereo@check_top2_&num:
    cmp     eax, ebx
    jle     short _clip32_s16_lq_stereo@save_&num
    mov     ax, bx
_clip32_s16_lq_stereo@save_&num:
    mov     ds:[di][num*8+0], cx
    mov     ds:[di][num*8+2], ax
    mov     ds:[di][num*8+4], cx
    mov     ds:[di][num*8+6], ax
endm

align 16

index = 0
rept _clip32_s16_lq_stereo@LENGTH
    _clip32_s16_lq_stereo@loop %index
    index = index + 1
endm

    add     si, _clip32_s16_lq_stereo@LENGTH*8
    add     di, _clip32_s16_lq_stereo@LENGTH*8
    dec     bp
    jnz     _clip32_s16_lq_stereo@loop_0
    pop     bp
CLIP32@EXIT
CLIP32@ENDP _clip32_s16_lq_stereo
