; p8l16.inc - "_play8_linear_16" routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

LOOP_TABLE _play8_linear_16, 4

PLAY_LINEAR_16@PROC _play8_linear_16
local _count: word
local _step_frac: word
PLAY_LINEAR_16@ENTER _play8_linear_16
    push    ax
    mov     [_count], cx
    mov     dx, bx                      ; 'dx' = n * 2
    mov     fs, [_wInterpTabSeg]
    lgs     bx, [_dParam]
    lds     si, gs:[bx][play_sample_param_t.dData]
    mov     cx, gs:[bx][play_sample_param_t.wPosFrac]
    add     si, gs:[bx][play_sample_param_t.wPos]
    les     di, gs:[bx][play_sample_param_t.dBuf]
    sub     di, dx                      ; output is 1 channel @ 16 bits
    movzx   eax, word ptr gs:[bx][play_sample_param_t.wStepFrac] ; Watcom bug: use "word ptr"!
    mov     [_step_frac], ax
    mov     dx, gs:[bx][play_sample_param_t.wStep]
; word at ss:[sp] = jump target
; word at ss:[bp][_count] = loops count
; word at ss:[bp][_step_frac] = step.frac
; eax: bits 31-16 are cleared
; ebx: bits 31-16 are cleared
; dx = step.int
; cx = pos.frac
; si = pos.int
; ds = input data segment
; ds:si = input data
; fs:0 = interpolation table
; es:di = output buffer
    retn                                ; jump

_play8_linear_16@loop macro num
_play8_linear_16@loop_&num:
    mov     ah, ch
    shr     ah, 4                       ; ah = pos.frac/4096 (0->15)
    mov     al, ds:[si]                 ; al = sample[pos]
    mov     bh, ah                      ; bh = pos.frac/4096 (0->15)
    mov     ax, fs:[eax+eax]            ; ax = sample[pos]*(16-pos.frac/4096)*16
    mov     bl, ds:[si][1]              ; bl = sample[pos+1]
    add     ax, fs:[ebx+ebx][16384]     ; ax = sample[pos]*(16-pos.frac/4096)*16
                                        ;    + sample[pos+1]*(pos.frac/4096)/16
    add     cx, [_step_frac]
    adc     si, dx
    mov     es:[di][num*2], ax
endm

align 16

index = 0
rept _play8_linear_16@LENGTH
    _play8_linear_16@loop %index
    index = index + 1
endm

    add     di, _play8_linear_16@LENGTH*2
    dec     [_count]
    jnz     _play8_linear_16@loop_0

    mov     bx, word ptr [_dParam]
    sub     si, word ptr gs:[bx][play_sample_param_t.dData]
PLAY_LINEAR_16@LEAVE
PLAY_LINEAR_16@ENDP _play8_linear_16
