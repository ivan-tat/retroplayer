; pm16n32z.inc - "_playmix16_nearest_32s2" routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

LOOP_TABLE _playmix16_nearest_32s2, 3

PLAYMIX_NEAREST_32@PROC _playmix16_nearest_32s2
local _count: word
local _step_frac: word
local _step: word
PLAYMIX_NEAREST_32@ENTER _playmix16_nearest_32s2
    push    ax
    mov     [_count], cx
    les     di, [_dOutBuf]
    shl     bx, 2                       ; 'bx' = n * 8
    sub     di, bx                      ; output is 2 channels @ 32 bits
    lgs     bx, [_dParam]
    xor     esi, esi
    lfs     si, gs:[bx][play_sample_param_t.dData]
    mov     cx, gs:[bx][play_sample_param_t.wPosFrac]
    shr     si, 1
    add     si, gs:[bx][play_sample_param_t.wPos]
    mov     ax, gs:[bx][play_sample_param_t.wStepFrac]
    mov     dx, gs:[bx][play_sample_param_t.wStep]
    mov     [_step_frac], ax
    mov     [_step], dx
    xor     ebx, ebx
    movzx   edx, [_wVol]
    mov     bh, dl
    mov     ds, [_wVolTabSeg]
; word at ss:[sp] = jump target
; word at ss:[_count] = loops count
; word at ss:[_step_frac] = step.frac
; word at ss:[_step] = step.int
; ebx = (volume_left & 0x3f) << 8
; edx = (volume_right & 0x3f) << 8
; cx = pos.frac
; esi = pos.int & 0x7fff
; fs = input data segment
; fs:esi*2 = input data
; es:di = output buffer
; ds:0 = volume table
    retn                                ; jump

_playmix16_nearest_32s2@loop macro num
_playmix16_nearest_32s2@loop_&num:
    mov     bl, fs:[esi+esi][1]
    mov     dl, bl
    movsx   eax, word ptr ds:[ebx+ebx]
    mov     bl, fs:[esi+esi]
    add     ax, ds:[8000h][ebx+ebx]
    add     es:[di][num*8], eax
    movsx   eax, word ptr ds:[edx+edx]
    mov     dl, bl
    add     ax, ds:[8000h][edx+edx]
    add     cx, [_step_frac]
    adc     si, [_step]
    add     es:[di][num*8+4], eax
endm

align 16

index = 0
rept _playmix16_nearest_32s2@LENGTH
    _playmix16_nearest_32s2@loop %index
    index = index + 1
endm

    add     di, _playmix16_nearest_32s2@LENGTH*8
    dec     [_count]
    jnz     _playmix16_nearest_32s2@loop_0

    shl     si, 1
    mov     bx, word ptr [_dParam]
    sub     si, word ptr gs:[bx][play_sample_param_t.dData]
    shr     si, 1
PLAYMIX_NEAREST_32@EXIT
PLAYMIX_NEAREST_32@ENDP _playmix16_nearest_32s2
