; c32s16l.inc - "_clip32_s16_lq" routine.
;
; This is free and unencumbered software released into the public domain.
; For more information, please refer to <http://unlicense.org>.

LOOP_TABLE _clip32_s16_lq, 3

CLIP32@PROC _clip32_s16_lq
CLIP32@ENTER _clip32_s16_lq
    push    ax
    les     si, [_dInBuf]
    lds     di, [_dOutBuf]
    shl     bx, 1                       ; 'bx' = n * 4
    sub     si, bx                      ; input data is 1 channel @ 32 bits
    sub     di, bx                      ; output buffer is 1 channel @ 16 bits (doubled)
    mov     edx, -8000h
    mov     ebx, 7fffh
; word at ss:[sp] = jump target
; cx = loops count
; edx = -0x8000
; ebx = 0x7fff
; es:si = input data
; ds:di = output buffer
    retn                                ; jump

_clip32_s16_lq@loop macro num
_clip32_s16_lq@loop_&num:
    mov     eax, es:[si][num*4]
    cmp     eax, edx
    jge     short _clip32_s16_lq@check_top_&num
    mov     ax, dx
    jmp     short _clip32_s16_lq@save_&num
_clip32_s16_lq@check_top_&num:
    cmp     eax, ebx
    jle     short _clip32_s16_lq@save_&num
    mov     ax, bx
_clip32_s16_lq@save_&num:
    mov     ds:[di][num*4+0], ax
    mov     ds:[di][num*4+2], ax
endm

align 16

index = 0
rept _clip32_s16_lq@LENGTH
    _clip32_s16_lq@loop %index
    index = index + 1
endm

    add     si, _clip32_s16_lq@LENGTH*4
    add     di, _clip32_s16_lq@LENGTH*4
    dec     cx
    jnz     _clip32_s16_lq@loop_0
CLIP32@EXIT
CLIP32@ENDP _clip32_s16_lq
