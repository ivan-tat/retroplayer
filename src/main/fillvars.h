/* fillvars.h -- declarations for fillvars.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _FILLVARS_H_INCLUDED
#define _FILLVARS_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "hw/dma.h"
#include "hw/sndctl_t.h"

/* DMA buffer for sound */

typedef uint16_t sound_DMA_buffer_flags_t;
typedef sound_DMA_buffer_flags_t SNDDMABUFFLAGS;

#define SNDDMABUFFL_LOCKED (1<<0)
#define SNDDMABUFFL_SLOW   (1<<1)
#define SNDDMABUFFL_LQ     (1<<2)

#define DMA_BUF_SIZE_MAX (8<<10)

#pragma pack(push, 1);
typedef struct sound_DMA_buffer_t
{
    SNDDMABUFFLAGS flags;
    DMABUF  *buf;
    HWSMPFMT format;
    uint16_t frameSize;
    uint8_t  framesCount;
    int8_t   frameLast;
    uint8_t  frameActive;
};
#pragma pack(pop);
typedef struct sound_DMA_buffer_t SNDDMABUF;

/* player */

void     snddmabuf_init (SNDDMABUF *self);
bool     snddmabuf_alloc (SNDDMABUF *self, uint32_t dmaSize);
uint16_t snddmabuf_get_frame_offset (SNDDMABUF *self, uint8_t index);
void    *snddmabuf_get_frame (SNDDMABUF *self, uint8_t index);
uint16_t snddmabuf_get_offset_from_count (SNDDMABUF *self, uint16_t count);
uint16_t snddmabuf_get_count_from_offset (SNDDMABUF *self, uint16_t bufOff);
void     snddmabuf_free (SNDDMABUF *self);

#if DEBUG == 1
void _DEBUG_dump_snddmabuf_info (const char *file, int line, const char *func, const char *name, SNDDMABUF *p);
# define DEBUG_dump_snddmabuf_info(name, p) _DEBUG_dump_snddmabuf_info (__FILE__, __LINE__, __func__, name, p)
#else
# define DEBUG_dump_snddmabuf_info(name, p)
#endif  /* DEBUG != 1 */

#endif  /* !_FILLVARS_H_INCLUDED */
