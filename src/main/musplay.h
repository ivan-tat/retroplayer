/* musplay.h -- declarations for musplay.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _S3MPLAY_H_INCLUDED
#define _S3MPLAY_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "debug.h"
#include "main/mixchn.h"
#include "main/mixer.h"
#include "main/fillvars.h"

/*** Sound ***/

typedef uint8_t sound_device_type_t;
typedef sound_device_type_t SNDDEVTYPE;

#define SNDDEVTYPE_NONE 0
#define SNDDEVTYPE_SB   1

typedef uint8_t sound_device_setup_method_t;
typedef sound_device_setup_method_t SNDDEVSETMET;

#define SNDDEVSETMET_MANUAL 0
#define SNDDEVSETMET_DETECT 1
#define SNDDEVSETMET_ENV    2
#define SNDDEVSETMET_INPUT  3

/*** Player ***/

extern const char *PLAYER_VERSION;

typedef void MUSPLAYER;

// General initialization

MUSPLAYER   *player_new (void);
bool         player_init (MUSPLAYER *self);
bool         player_is_error (MUSPLAYER *self);
const char  *player_get_error (MUSPLAYER *self);
void         player_set_EM_usage (MUSPLAYER *self, bool value);
bool         player_is_EM_in_use (MUSPLAYER *self);

// Output device

bool         player_init_device (MUSPLAYER *self, SNDDEVTYPE type, SNDDEVSETMET method);
char        *player_device_get_name (MUSPLAYER *self);
void         player_device_dump_conf (MUSPLAYER *self);
SNDDMABUF   *player_get_sound_buffer (MUSPLAYER *self);
void         player_set_sound_buffer_fps (MUSPLAYER *self, uint8_t value);
bool         player_set_mode (MUSPLAYER *self, bool f_16bits, bool f_stereo, uint16_t rate, bool LQ);
uint16_t     player_get_output_rate (MUSPLAYER *self);
uint8_t      player_get_output_channels (MUSPLAYER *self);
uint8_t      player_get_output_bits (MUSPLAYER *self);
bool         player_get_output_lq (MUSPLAYER *self);
bool         player_play_start (MUSPLAYER *self);
void         player_play_pause (MUSPLAYER *self);
void         player_play_continue (MUSPLAYER *self);
void         player_play_stop (MUSPLAYER *self);
uint16_t     player_get_buffer_pos (MUSPLAYER *self);
void         player_free_device (MUSPLAYER *self);

// Mixer

bool         player_init_mixer (MUSPLAYER *self);
MIXER       *player_get_mixer (MUSPLAYER *self);
void         player_free_mixer (MUSPLAYER *self);

// Song

bool         player_load_s3m (MUSPLAYER *self, char *name, MUSMOD **_track);
bool         player_set_active_track (MUSPLAYER *self, MUSMOD *track);
PLAYSTATE   *player_get_play_state (MUSPLAYER *self);
uint8_t      player_get_master_volume (MUSPLAYER *self);
void         player_set_master_volume (MUSPLAYER *self, uint8_t value);
void         player_free_module (MUSPLAYER *self, MUSMOD *track);
void         player_free_modules (MUSPLAYER *self);

// General finalization

void         player_free (MUSPLAYER *self);
void         player_delete (MUSPLAYER **self);

/*** Initialization ***/

void init_musplay (void);
void done_musplay (void);

#endif  /* !_S3MPLAY_H_INCLUDED */
