/* muspat.c -- musical pattern handling library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/string.h"
#include "cc/stdio.h"
#include "cc/dos.h"
#include "debug.h"
#include "dynarray.h"
#include "dos/ems.h"
#include "main/muspat.h"

/*** Music pattern channel's event ***/

void muspatchnevent_clear (MUSPATCHNEVENT *self)
{
    self->flags = 0;
    self->data.instrument  = CHN_INS_NONE;
    self->data.note        = CHN_NOTE_NONE;
    self->data.note_volume = CHN_NOTEVOL_NONE;
    self->data.command     = CHN_CMD_NONE;
    self->data.parameter   = 0;
}

/*** Music pattern row's event ***/

void muspatrowevent_clear (MUSPATROWEVENT *self)
{
    self->channel = 0;
    self->event.flags = 0;
    self->event.data.instrument  = CHN_INS_NONE;
    self->event.data.note        = CHN_NOTE_NONE;
    self->event.data.note_volume = CHN_NOTEVOL_NONE;
    self->event.data.command     = CHN_CMD_NONE;
    self->event.data.parameter   = 0;
}

/*** Music pattern ***/

#define _muspat_get_packed_data_start(o) ((o)->rows * sizeof (uint16_t))

void muspat_init (MUSPAT *self)
{
    if (self)
    {
        memset(self, 0, sizeof(MUSPAT));
        self->data.em.handle = EMSBADHDL;
    }
}

uint16_t muspat_get_row_start (MUSPAT *self, uint16_t row, uint16_t channel)
{
    return (self->channels * row + channel) * sizeof (MUSPATCHNEVDATA);
}

/* assumes EM data is mapped before call */
void muspat_set_packed_row_start (MUSPAT *self, uint16_t row, uint16_t offset)
{
    if (row)
    {
        uint16_t *offsets;

        if (self->flags & MUSPATFL_EM)
            offsets = MK_FP (emsFrameSeg, get_EM_page_offset (self->data.em.offset));
        else
            offsets = self->data.dos.ptr;

        offsets [row - 1] = offset - _muspat_get_packed_data_start (self);
    }
}

/* assumes EM data is mapped before call */
uint16_t muspat_get_packed_row_start (MUSPAT *self, uint16_t row)
{
    uint16_t start;

    start = _muspat_get_packed_data_start (self);
    if (row)
    {
        uint16_t *offsets;

        if (self->flags & MUSPATFL_EM)
            offsets = MK_FP (emsFrameSeg, get_EM_page_offset (self->data.em.offset));
        else
            offsets = self->data.dos.ptr;

        start += offsets [row - 1];
    }

    return start;
}

/* assumes EM data is mapped before call */
uint16_t muspat_get_packed_size (MUSPAT *self)
{
    uint16_t start;
    uint16_t rows;

    start = _muspat_get_packed_data_start (self);
    rows = self->rows;
    if (rows)
    {
        uint16_t *offsets;

        if (self->flags & MUSPATFL_EM)
            offsets = MK_FP (emsFrameSeg, get_EM_page_offset (self->data.em.offset));
        else
            offsets = self->data.dos.ptr;

        start += offsets [rows - 1];
    }

    return start;
}

void muspat_free (MUSPAT *self)
{
    if (self)
    {
        if (self->flags & MUSPATFL_EM)
        {
            if (self->flags & MUSPATFL_OWNHDL)
                emsFree (self->data.em.handle);
        }
        else
        {
            void __far *p = self->data.dos.ptr;
            if (p)
                _dos_freemem(FP_SEG(p));
        }
    }
}

/*** Music pattern reader ***/

typedef unsigned char music_pattern_row_event_flags_t;
typedef music_pattern_row_event_flags_t MUSPATROWEVFLAGS;

#define MUSPATROWEVFL_CHN               (1 << 0)
#define MUSPATROWEVFL_CHNEVENT_SHIFT    1

void _muspatio_seek (MUSPATIO *self, unsigned int row, unsigned char channel);
void _muspatio_seek_packed (MUSPATIO *self, unsigned int row, unsigned char channel);
void _muspatio_read (MUSPATIO *self, MUSPATROWEVENT *event);
void _muspatio_read_packed (MUSPATIO *self, MUSPATROWEVENT *event);
bool _muspatio_is_end_of_row (MUSPATIO *self);
bool _muspatio_is_end_of_row_packed (MUSPATIO *self);
void _muspatio_end_row (MUSPATIO *self);
void _muspatio_end_row_packed_write (MUSPATIO *self);
void _muspatio_write (MUSPATIO *self, MUSPATROWEVENT *event);
void _muspatio_write_packed (MUSPATIO *self, MUSPATROWEVENT *event);
void _muspatio_close (MUSPATIO *self);

static void __near muspatio_open_read (MUSPATIO *self)
{
    self->m_seek            = & _muspatio_seek;
    self->m_read_write      = & _muspatio_read;
    self->m_is_end_of_row   = & _muspatio_is_end_of_row;
    self->m_end_row         = & _muspatio_end_row;
    self->m_close           = & _muspatio_close;
    //~ self->row_start         = 0;
    //~ self->row_end           = 0;
    //~ self->offset            = 0;
    //~ self->channel           = 0;
}

static void __near muspatio_open_write (MUSPATIO *self)
{
    self->m_seek            = & _muspatio_seek;
    self->m_read_write      = & _muspatio_write;
    self->m_is_end_of_row   = & _muspatio_is_end_of_row;
    self->m_end_row         = & _muspatio_end_row;
    self->m_close           = & _muspatio_close;
    //~ self->row_start         = 0;
    //~ self->row_end           = 0;
    //~ self->offset            = 0;
    //~ self->channel           = 0;
}

static void __near muspatio_open_packed_read (MUSPATIO *self)
{
    self->m_seek            = & _muspatio_seek_packed;
    self->m_read_write      = & _muspatio_read_packed;
    self->m_is_end_of_row   = & _muspatio_is_end_of_row_packed;
    self->m_end_row         = & _muspatio_end_row;
    self->m_close           = & _muspatio_close;
    self->row_start         = _muspat_get_packed_data_start (self->pattern);
    self->row_end           = muspat_get_packed_row_start (self->pattern, 1);
    self->offset            = self->row_start;
    //~ self->channel           = 0;
}

static void __near muspatio_open_packed_write (MUSPATIO *self)
{
    self->m_seek            = & _muspatio_seek_packed;
    self->m_read_write      = & _muspatio_write_packed;
    self->m_is_end_of_row   = & _muspatio_is_end_of_row_packed;
    self->m_end_row         = & _muspatio_end_row_packed_write;
    self->m_close           = & _muspatio_close;
    self->row_start         = _muspat_get_packed_data_start (self->pattern);
    //~ self->row_end           = 0;    // undefined
    self->offset            = self->row_start;
    //~ self->channel           = 0;
}

static void __near muspatio_clear (MUSPATIO *self)
{
    memset (self, 0, sizeof (MUSPATIO));
}

bool muspatio_open (MUSPATIO *self, MUSPAT *pattern, MUSPATIOMODE mode)
{
    if (self && pattern)
    {
        muspatio_clear (self);

        self->pattern = pattern;

        if (pattern->flags & MUSPATFL_EM)
        {
            if (!map_EM_data (pattern->data.em.handle, pattern->data.em.offset,
                    pattern->size))
            {
                self->error = "EM map failed";
                return false;
            }
            self->data = MK_FP (emsFrameSeg, get_EM_page_offset (pattern->data.em.offset));
        }
        else
        {
            self->data = pattern->data.dos.ptr;
            if (!self->data)
            {
                self->error = "No data";
                return false;
            }
        }

        self->mode = mode;

        if (pattern->flags & MUSPATFL_PACKED)
        {
            switch (mode)
            {
            case MUSPATIOMD_READ:
                muspatio_open_packed_read (self);
                return true;
            case MUSPATIOMD_WRITE:
                muspatio_open_packed_write (self);
                return true;
            default:
                self->error = "Bad mode";
                return false;
            }
        }
        else
        {
            switch (mode)
            {
            case MUSPATIOMD_READ:
                muspatio_open_read (self);
                return true;
            case MUSPATIOMD_WRITE:
                muspatio_open_write (self);
                return true;
            default:
                self->error = "Bad mode";
                return false;
            }
        }
    }

    if (self)
        self->error = "Bad arguments";

    return false;
}

void _muspatio_seek (MUSPATIO *self, unsigned int row, unsigned char channel)
{
    MUSPAT *pattern;

    self->row = row;
    pattern = self->pattern;

    self->row_start = muspat_get_row_start (pattern, row, 0);
    self->row_end = muspat_get_row_start (pattern, row + 1, 0);
    self->offset = muspat_get_row_start (pattern, row, channel);
    self->channel = channel;
}

void _muspatio_seek_packed (MUSPATIO *self, unsigned int row, unsigned char channel)
{
    MUSPAT *pattern;

    self->row = row;
    pattern = self->pattern;

    self->row_start = muspat_get_packed_row_start (pattern, row);
    self->row_end = muspat_get_packed_row_start (pattern, row + 1);
    self->offset = self->row_start;
    self->channel = 0;
}

void _muspatio_read (MUSPATIO *self, MUSPATROWEVENT *event)
{
    unsigned char *data;
    MUSCHNEVFLAGS flags;

    data = self->data + self->offset;

    event->channel = self->channel;
    flags = 0;

    event->event.data.instrument = data [0];
    if (event->event.data.instrument != CHN_INS_NONE)
        flags |= MUSPATCHNEVFL_INS;

    event->event.data.note = data [1];
    if (event->event.data.note != CHN_NOTE_NONE)
        flags |= MUSPATCHNEVFL_NOTE;

    event->event.data.note_volume = data [2];
    if (event->event.data.note_volume != CHN_NOTEVOL_NONE)
        flags |= MUSPATCHNEVFL_VOL;

    event->event.data.command = data [3];
    event->event.data.parameter = data [4];
    if (event->event.data.command != CHN_CMD_NONE)
        flags |= MUSPATCHNEVFL_CMD;

    event->event.flags = flags;
    self->offset += 5;
    self->channel++;
}

void _muspatio_read_packed (MUSPATIO *self, MUSPATROWEVENT *event)
{
    unsigned char *data;
    MUSPATROWEVFLAGS row_flags;
    unsigned char channel;

    data = self->data + self->offset;

    row_flags = data [0];
    data++;

    if (row_flags & MUSPATROWEVFL_CHN)
    {
        channel = data [0];
        data++;
    }
    else
        channel = self->channel;

    event->channel = channel;
    event->event.flags = (row_flags >> MUSPATROWEVFL_CHNEVENT_SHIFT) & MUSPATCHNEVFL_ALL;

    if (row_flags & (MUSPATCHNEVFL_INS << MUSPATROWEVFL_CHNEVENT_SHIFT))
    {
        event->event.data.instrument = data [0];
        data++;
    }
    else
        event->event.data.instrument = CHN_INS_NONE;

    if (row_flags & (MUSPATCHNEVFL_NOTE << MUSPATROWEVFL_CHNEVENT_SHIFT))
    {
        event->event.data.note = data [0];
        data++;
    }
    else
        event->event.data.note = CHN_NOTE_NONE;

    if (row_flags & (MUSPATCHNEVFL_VOL << MUSPATROWEVFL_CHNEVENT_SHIFT))
    {
        event->event.data.note_volume = data [0];
        data++;
    }
    else
        event->event.data.note_volume = CHN_NOTEVOL_NONE;

    if (row_flags & (MUSPATCHNEVFL_CMD << MUSPATROWEVFL_CHNEVENT_SHIFT))
    {
        event->event.data.command = data [0];
        event->event.data.parameter = data [1];
        data += 2;
    }
    else
    {
        event->event.data.command = CHN_CMD_NONE;
        event->event.data.parameter = 0;
    }

    self->offset = FP_OFF (data) - FP_OFF (self->data);
    self->channel = channel + 1;
}

void _muspatio_write (MUSPATIO *self, MUSPATROWEVENT *event)
{
    if (event->channel < self->pattern->channels)
    {
        unsigned char *data;

        if (!(event->event.flags & MUSPATCHNEVFL_INS))
            event->event.data.instrument = CHN_INS_NONE;

        if (!(event->event.flags & MUSPATCHNEVFL_NOTE))
            event->event.data.note = CHN_NOTE_NONE;

        if (!(event->event.flags & MUSPATCHNEVFL_VOL))
            event->event.data.note_volume = CHN_NOTEVOL_NONE;

        if (!(event->event.flags & MUSPATCHNEVFL_CMD))
        {
            event->event.data.command = CHN_CMD_NONE;
            event->event.data.parameter = 0;
        }

        muspatio_seek (self, self->row, event->channel);

        data = self->data + self->offset;
        data [0] = event->event.data.instrument;
        data [1] = event->event.data.note;
        data [2] = event->event.data.note_volume;
        data [3] = event->event.data.command;
        data [4] = event->event.data.parameter;
        self->offset += 5;
        self->channel++;
    }
}

void _muspatio_write_packed (MUSPATIO *self, MUSPATROWEVENT *event)
{
    unsigned char *data;
    MUSPATROWEVFLAGS row_flags;
    unsigned char channel;

    data = self->data + self->offset;

    row_flags = (event->event.flags & MUSPATCHNEVFL_ALL) << MUSPATROWEVFL_CHNEVENT_SHIFT;

    channel = event->channel;
    if (channel != self->channel)
        row_flags |= MUSPATROWEVFL_CHN;

    data [0] = row_flags;
    data++;

    if (row_flags & MUSPATROWEVFL_CHN)
    {
        data [0] = channel;
        data++;
    }

    if (row_flags & (MUSPATCHNEVFL_INS << MUSPATROWEVFL_CHNEVENT_SHIFT))
    {
        data [0] = event->event.data.instrument;
        data++;
    }

    if (row_flags & (MUSPATCHNEVFL_NOTE << MUSPATROWEVFL_CHNEVENT_SHIFT))
    {
        data [0] = event->event.data.note;
        data++;
    }

    if (row_flags & (MUSPATCHNEVFL_VOL << MUSPATROWEVFL_CHNEVENT_SHIFT))
    {
        data [0] = event->event.data.note_volume;
        data++;
    }

    if (row_flags & (MUSPATCHNEVFL_CMD << MUSPATROWEVFL_CHNEVENT_SHIFT))
    {
        data [0] = event->event.data.command;
        data [1] = event->event.data.parameter;
        data += 2;
    }

    self->offset = FP_OFF (data) - FP_OFF (self->data);
    self->channel = channel + 1;
}

bool _muspatio_is_end_of_row (MUSPATIO *self)
{
    return (self->channel >= self->pattern->channels) || (self->offset >= self->row_end);
}

bool _muspatio_is_end_of_row_packed (MUSPATIO *self)
{
    return self->offset >= self->row_end;
}

void _muspatio_end_row (MUSPATIO *self)
{
    muspatio_seek (self, self->row + 1, 0);
}

void _muspatio_end_row_packed_write (MUSPATIO *self)
{
    if (self->row < self->pattern->rows)
    {
        self->row++;

        muspat_set_packed_row_start (self->pattern, self->row, self->offset);

        self->row_start = self->offset;
        self->row_end = 0;
        self->channel = 0;
    }
}

void _muspatio_close (MUSPATIO *self)
{
    muspatio_clear (self);
}

/*** Music patterns list ***/

void _muspatl_init_item (void *self, void *item)
{
    muspat_init((MUSPAT *)item);
}

void _muspatl_free_item (void *self, void *item)
{
    muspat_free((MUSPAT *)item);
}

void muspatl_init (MUSPATLIST *self)
{
    if (self)
    {
        dynarr_init (&self->list, self, sizeof (MUSPAT), _muspatl_init_item, _muspatl_free_item);
        self->handle = EMSBADHDL;
    }
}

void muspatl_free (MUSPATLIST *self)
{
    if (self)
    {
        dynarr_free (&self->list);

        if (self->flags & MUSPATLFL_OWNHDL)
            emsFree (self->handle);
    }
}

/*** Music patterns order ***/

void _muspatorder_init_item (void *self, void *item)
{
    *(MUSPATORDENT *) item = MUSPATORDENT_END;
}

void _muspatorder_free_item (void *self, void *item)
{
}

void muspatorder_init (MUSPATORDER *self)
{
    if (self)
        dynarr_init (&self->list, self, sizeof (MUSPATORDENT), _muspatorder_init_item, _muspatorder_free_item);
}

int muspatorder_find_next_pattern (MUSPATORDER *self, int first, int last, int pos, int step, bool skipend)
{
    bool found;

    found = false;
    while ((!found) && (first <= pos) && (pos <= last))
    {
        MUSPATORDENT *entry = muspatorder_get (self, pos);

        switch (*entry)
        {
        case MUSPATORDENT_SKIP:
            pos += step;
            break;
        case MUSPATORDENT_END:
            if (skipend)
                pos += step;
            else
                pos = -1;
            break;
        default:
            found = true;
            break;
        }
    }

    return found ? pos : -1;
}

int muspatorder_find_last (MUSPATORDER *self, bool skipend)
{
    int i;

    if (self)
    {
        int last = muspatorder_get_count (self) - 1;
        bool found = false;

        if (! skipend)
        {
            /* Search for first MUSPATORDENT_END mark (as usual) */
            i = 0;
            while ((!found) && (i < last))
            {
                MUSPATORDENT *entry = muspatorder_get (self, i);
                if (*entry == MUSPATORDENT_END)
                    found = true;
                else
                    i++;
            }
            i--;
        }
        else
        {
            /* It is not important, we can also do simply order_last = order_length - 1 */
            i = last;
            while ((!found) && (i > 0))
            {
                MUSPATORDENT *entry = muspatorder_get (self, i);
                if ((*entry != MUSPATORDENT_SKIP)
                &&  (*entry != MUSPATORDENT_END))
                    found = true;
                else
                    i--;
            }
        }
    }
    else
        i = 0;

    return i;
}

void muspatorder_free (MUSPATORDER *self)
{
    if (self)
        dynarr_free (&self->list);
}

/*** Debug ***/

#if DEBUG == 1

#include "cc/hexdigts.h"

static const char __halftones[16] = "cCdDefFgGaAb????";
static const char __octaves[16] = "0123456789??????";

void DEBUG_get_pattern_channel_event_str (char *s, MUSPATCHNEVENT *event)
{
    uint8_t v;

    if (event->flags & MUSPATCHNEVFL_NOTE)
    {
        v = event->data.note;
        switch (v)
        {
        case CHN_NOTE_NONE:
            s[0] = '.';
            s[1] = '.';
            break;
        case CHN_NOTE_OFF:
            s[0] = '=';
            s[1] = '=';
            break;
        default:
            if (v <= CHN_NOTE_MAX)
            {
                s[0] = __halftones[v & 0x0f];
                s[1] = __octaves[v >> 4];
            }
            else
            {
                s[0] = '?';
                s[1] = '?';
            }
            break;
        }
    }
    else
    {
        s[0] = '.';
        s[1] = '.';
    }

    s[2] = ' ';

    if (event->flags & MUSPATCHNEVFL_INS)
    {
        v = event->data.instrument;
        switch (v)
        {
        case CHN_INS_NONE:
            s[3] = '.';
            s[4] = '.';
            break;
        default:
            v = _get_instrument (v);
            v++;
            s[3] = HEXDIGITS[v >> 4];
            s[4] = HEXDIGITS[v & 0x0f];
            break;
        }
    }
    else
    {
        s[3] = '.';
        s[4] = '.';
    }

    s[5] = ' ';

    if (event->flags & MUSPATCHNEVFL_VOL)
    {
        v = event->data.note_volume;
        switch (v)
        {
        case CHN_NOTEVOL_NONE:
            s[6] = '.';
            s[7] = '.';
            break;
        default:
            if (v <= CHN_NOTEVOL_MAX)
            {
                s[6] = '0' + (v / 10);
                s[7] = '0' + (v % 10);
            }
            else
            {
                s[6] = '?';
                s[7] = '?';
            }
            break;
        }
    }
    else
    {
        s[6] = '.';
        s[7] = '.';
    }

    s[8] = ' ';

    if (event->flags & MUSPATCHNEVFL_CMD)
    {
        v = event->data.command;
        if (v == CHN_CMD_NONE)
        {
            s[9] = '.';
            s[10] = '.';
            s[11] = '.';
        }
        else
        {
            if (v <= CHN_CMD_MAX)
                s[9] = 'A' + v - 1;
            else
                s[9] = '?';

            snprintf (s + 10, 2, "%02X", event->data.parameter);
        }
    }
    else
    {
        s[9] = '.';
        s[10] = '.';
        s[11] = '.';
    }

    s[12] = 0;
}

void DEBUG_dump_pattern_info (MUSPAT *pattern, int index)
{
    #define _BUF_SIZE1 128
    #define _BUF_SIZE2 64
    uint16_t size;
    char s[_BUF_SIZE1], f[_BUF_SIZE2];

    if (pattern->flags & MUSPATFL_EM)
    {
        if (!map_EM_data (pattern->data.em.handle, pattern->data.em.offset,
                pattern->size))
        {
            DEBUG_ERR_ ("Failed to map EM for %s.", "pattern");
            return;
        }
    }

    if (pattern->flags & MUSPATFL_PACKED)
    {
        size = muspat_get_packed_size (pattern);
        snprintf (f, _BUF_SIZE2,
            "packed (size=0x%hX, data_start=0x%04hX)",
            (uint16_t) size,
            (uint16_t) _muspat_get_packed_data_start (pattern)
        );
    }
    else
    {
        uint16_t row_size = (uint16_t) pattern->channels * sizeof (MUSPATCHNEVDATA);
        size = row_size * pattern->rows;
        snprintf (f, _BUF_SIZE2,
            "raw (size=0x%hX, channels=%hhu, row_size=0x%hX)",
            (uint16_t) size,
            (uint8_t) pattern->channels,
            (uint16_t) row_size
        );
    }

    if (pattern->flags & MUSPATFL_EM)
    {
        uint32_t start = pattern->data.em.offset;
        uint32_t end = start + size - 1;

        snprintf (s, _BUF_SIZE1,
            "EM (handle=0x%04hX, offset=0x%lX, pages=0x%04hX:0x%04hX-0x%04hX:0x%04hX)",
            (uint16_t) pattern->data.em.handle,
            (uint32_t) start,
            (uint16_t) get_EM_page (start),
            (uint16_t) get_EM_page_offset (start),
            (uint16_t) get_EM_page (end),
            (uint16_t) get_EM_page_offset (end)
        );
    }
    else
    {
        void __far *data = pattern->data.dos.ptr;
        snprintf (s, _BUF_SIZE1,
            "DOS (address=0x%lX, pointer=0x%04hX:0x%04hX)",
            (uint32_t) FARPTR_TO_LONG (data),
            (uint16_t) FP_SEG (data),
            (uint16_t) FP_OFF (data)
        );
    }

    DEBUG_INFO_ ("index=%d, address=0x%lX, pointer=0x%04hX:0x%04hX, flags=0x%02hhX, rows=%hu, mem_size=0x%hX.",
        index,
        (uint32_t) FARPTR_TO_LONG (pattern),
        (uint16_t) FP_SEG (pattern),
        (uint16_t) FP_OFF (pattern),
        (uint8_t) pattern->flags,
        (uint16_t) pattern->rows,
        (uint16_t) pattern->size,
        s,
        f
    );

    DEBUG_INFO_ ("place=%s.",
        s
    );

    DEBUG_INFO_ ("format=%s.",
        f
    );

    DEBUG_INFO ("Raw object dump:");
    DEBUG_dump_mem (pattern, sizeof (MUSPAT), "object: ");
    #undef _BUF_SIZE1
    #undef _BUF_SIZE2
}

// "s" must hold atleast 64 bytes or (num_channels * 13) bytes
bool DEBUG_dump_pattern (MUSPAT *pattern, char *s, uint8_t num_channels)
{
    MUSPATIO f;
    MUSPATROWEVENT e, empty;
    unsigned int size;
    unsigned int rows, row;

    DEBUG_INFO_ ("Requested: address=0x%lX, pointer=0x%04hX:0x%04hX, channels=%hhu",
        (uint32_t) FARPTR_TO_LONG (pattern),
        (uint16_t) FP_SEG (pattern),
        (uint16_t) FP_OFF (pattern),
        (uint8_t) num_channels
    );

    if ((!pattern) || (!muspatio_open (&f, pattern, MUSPATIOMD_READ)))
    {
        DEBUG_ERR_ ("Failed to read pattern (%s)", f.error);
        return false;
    }

    if (pattern->flags & MUSPATFL_PACKED)
        size = muspat_get_packed_size (pattern);
    else
        size = pattern->size;

    DEBUG_INFO ("Raw data dump:");
    DEBUG_dump_mem (f.data, size, "data: ");

    DEBUG_INFO ("Human-readable data dump:");

    rows = pattern->rows;
    for (row = 0; row < rows; row++)
    {
        bool row_read;
        unsigned char c;

        muspatio_seek (&f, row, 0);

        /* Linear reading of pattern's events while increasing channel number */
        muspatrowevent_clear (&empty);
        c = 0;
        row_read = !muspatio_is_end_of_row (&f);
        while (c < num_channels)
        {
            // walk through from current channel (c) to the end
            bool row_ev_ok = false;
            unsigned char next_c = num_channels;

            if (row_read)
            {
                muspatio_read (&f, &e);
                if (e.channel < num_channels)
                {
                    // walk through from current channel (c) to current event's channel
                    row_ev_ok = true;
                    next_c = e.channel;
                    row_read = !muspatio_is_end_of_row (&f);
                }
            }

            /* walk through from channel (c) to (next_c)  */
            while (c < next_c)
            {
                empty.channel = c;
                DEBUG_get_pattern_channel_event_str (s + empty.channel * 13, & (empty.event));
                s[empty.channel * 13 + 12] = '|';
                c++;
            }

            if (row_ev_ok)
            {
                DEBUG_get_pattern_channel_event_str (s + e.channel * 13, & (e.event));
                s[e.channel * 13 + 12] = '|';
                c++;
            }
        }

        s[(num_channels - 1) * 13 + 12] = 0;
        _DEBUG_LOG (DBGLOG_MSG, NULL, 0, NULL, "%03hhu: |%s|", row, s);
    }

    muspatio_close (&f);
    return true;
}

#endif  /* DEBUG == 1 */
