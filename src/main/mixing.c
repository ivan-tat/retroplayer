/* mixing.c -- mixing routines.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/string.h"
#include "dos/ems.h"
#include "main/musdefs.h"
#include "main/musmod.h"
#include "main/musmodps.h"
#include "main/mixer.h"
#include "main/interp.h"
#include "main/voltab.h"
#include "main/effvars.h"
#include "main/effects.h"
#include "main/readnote.h"
#include "main/mixing.h"

static void __near song_new_tick (PLAYSTATE *ps)
{
    ps->tick_spc_counter = ps->tick_spc;

    if (ps->tick <= 1)
    {
        if (ps->patdelay_count)
        {
            ps->patdelay_count--;
            if (ps->patdelay_count)
                ps->row--;
        }
        readnewnotes (ps);
    }
    else
    {
        ps->tick--;
    }
}

/* count > 0 */
static void __near next_sample_pos (SMPPOS *next, SMPPOS *pos, int32_t step, uint16_t count)
{
    #pragma pack(push, 1);
    union
    {
        SMPPOS sp;
        int16_t i16[4];
        uint16_t u16[4];
        int64_t i64;
    } x;
    union
    {
        int16_t i16[2];
        uint16_t u16[2];
        int32_t i32;
    } s;
    #pragma pack(pop);

    x.sp = *pos;
    if (x.i16[2] < 0)
        x.i16[3] = -1;
    else
        x.i16[3] = 0;

    s.i32 = step;

    if (count <= 4)
    {
        do
        {
            x.i64 += s.i32;
            count--;
        } while (count);
    }
    else
    {
        if (s.u16[0])
            x.i64 += ((uint32_t) s.u16[0]) * count;
        if (s.i16[1])
            x.sp.pos += ((int32_t) s.i16[1]) * count;
    }

    *next = x.sp;
}

static void __near sample_dist (SMPPOS *dist, SMPPOS *start, SMPPOS *end)
{
    #pragma pack(push, 1);
    union
    {
        SMPPOS sp;
        int16_t i16[4];
        int64_t i64;
    } x;
    #pragma pack(pop);

    x.sp = *end;
    x.i16[3] = (x.i16[2] < 0) ? -1 : 0;
    x.sp.pos -= start->pos;
    if (x.i16[2] < 0)
        x.i16[3] = -1;
    x.i64 -= start->frac;

    *dist = x.sp;
}

static uint16_t __near sample_steps (SMPPOS *dist, int32_t step)
{
    #pragma pack(push, 1);
    union
    {
        SMPPOS sp;
        int16_t i16[4];
        int64_t i64;
    } x;
    #pragma pack(pop);

    x.sp = *dist;
    x.i16[3] = (x.sp.pos < 0) ? -1 : 0;
    if (x.i64 < 0)
        x.i64 = -x.i64;
    /* x.i64 >= 0 */
    if (step < 0)
        step = -step;
    /* step >= 0 */

    if (x.i64 && step)
    {
        x.i64 /= step;
        /* counter register and DOS segment limit */
        if (x.i64 > 0xfffe)
            return 0xfffe;
        else
            return x.i64;
    }
    else
        return 0;
}

/* sample end or loop bounds */
static bool __near limit_sample_pos (SMPPOS *end, MIXCHN *chn)
{
    /* bottom limit: start position */
    /* top limit: last position before the end */

    if (chn->dSmpStep > 0)
    {
        /* position top limit */
        if (end->pos >= chn->dSmpLoopEnd)
        {
            end->frac = 0xffff;
            end->pos = chn->dSmpLoopEnd - 1;
            return true;
        }
    }
    else
    {
        /* position bottom limit */
        if (end->pos < chn->dSmpLoopStart)
        {
            end->frac = 0;
            end->pos = chn->dSmpLoopStart;
            return true;
        }
    }

    return false;
}

static uint32_t near extra_sample_len (MIXER *mixer)
{
    switch (mixer->quality)
    {
    case MIXQ_LINEAR:
        return EXTRA_LEN_FOR_LINEAR_INTERP;

    default:
        return 0;
    }
}

/* DOS segment bounds, 16 bits sample data must be aligned to 2 bytes */
static bool __near limit_sample_pos_dos (SMPPOS *end, MIXCHN *chn, MIXER *mixer)
{
    bool forward = chn->dSmpStep > 0;
    PCMSMP *smp = chn->sample;
    uint32_t pos_start = chn->rSmpPos.pos;
    int32_t pos_end = end->pos;
    char shift = (smp->flags & PCMSMPFL_16BITS) ? 1 : 0;
    int32_t bound;  /* bottom or top */

    /* adjust top limit for backward loop */

    if (!forward)
        pos_start += extra_sample_len (mixer);

    /* bottom limit: first byte at the start */
    /* top limit: last byte before the end */

    if (smp->flags & PCMSMPFL_EM)
    {
        uint32_t base = smp->data.em.offset;

        bound = ((base + (pos_start << shift)) & ~(EM_PAGE_SIZE - 1)) - base;

        if (!forward)
            bound += EM_PAGE_SIZE;
    }
    else
    {
        uint32_t base = FARPTR_TO_LONG (smp->data.dos.ptr);

        bound = ((base + (pos_start << shift)) & ~15UL) - base;

        if (!forward)
            bound += 16;
    }

    /* switch to position */
    bound = (uint32_t) bound >> shift;

    if (forward)
    {
        bound += (1UL<<16) >> shift;
        bound -= extra_sample_len (mixer);

        if (pos_end >= bound)
        {
            end->frac = 0xffff;
            end->pos = bound - 1;
            return true;
        }
    }
    else
    {
        bound -= (1UL<<16) >> shift;

        if (bound < 0)
            bound = 0;

        if (pos_end < bound)
        {
            end->frac = 0;
            end->pos = bound;
            return true;
        }
    }

    return false;
}

static bool __near set_play_param (struct play_sample_param_t *param, MIXCHN *chn, MIXER *mixer, SMPPOS *end)
{
    bool forward = chn->dSmpStep > 0;
    PCMSMP *smp = chn->sample;
    uint8_t shift;
    uint32_t rel_start; /* relative to 'base' */
    uint32_t size;
    uint32_t base;
    uint32_t start;

    if (forward)
    {
        rel_start = chn->rSmpPos.pos;
        size = end->pos - chn->rSmpPos.pos + 1;
    }
    else
    {
        rel_start = end->pos;
        size = chn->rSmpPos.pos - end->pos + 1;
    }

    if (smp->flags & PCMSMPFL_EM)
        base = smp->data.em.offset;
    else
    {
        base = FARPTR_TO_LONG (smp->data.dos.ptr);
        if (!base)
            return false;   /* Error */
    }

    param->wPosFrac = chn->rSmpPos.frac;
    param->wPos = chn->rSmpPos.pos - rel_start;
    param->dStep = chn->dSmpStep;

    shift = (smp->flags & PCMSMPFL_16BITS) ? 1 : 0;

    start = base + (rel_start << shift);

    if (smp->flags & PCMSMPFL_EM)
    {
        if (map_EM_data (smp->data.em.handle, start,
            (size + extra_sample_len (mixer)) << shift))
            param->dData = MK_FP (emsFrameSeg, get_EM_page_offset (start));
        else
            return false;   /* Error */
    }
    else
        param->dData = FARPTR_FROM_LONG (start);

    return true;
}

static void __near check_sample_pos_normal (MIXCHN *chn)
{
    if (chn->rSmpPos.pos >= chn->dSmpLoopEnd)
    {
        chn->rSmpPos.frac = 0;
        chn->rSmpPos.pos = 0;
        chn->flags &= ~MIXCHNFL_PLAYING;
    }
}

static void __near check_sample_pos_forward (MIXCHN *chn)
{
    if (chn->rSmpPos.pos >= chn->dSmpLoopEnd)
        chn->rSmpPos.pos = chn->dSmpLoopStart
            + ((chn->rSmpPos.pos - chn->dSmpLoopEnd)
                % (chn->dSmpLoopEnd - chn->dSmpLoopStart));
}

static void __near check_sample_pos_backward (MIXCHN *chn)
{
    uint32_t loop_len, loop_offset;

    if (chn->dSmpStep > 0)
    {
        /* Normal play or forward part of backward loop */
        if (chn->rSmpPos.pos >= chn->dSmpLoopEnd)
        {
            /* Switch to backward loop */
            loop_len = chn->dSmpLoopEnd - chn->dSmpLoopStart;
            loop_offset = (chn->rSmpPos.pos - chn->dSmpLoopEnd) % loop_len;

            /* +1.0 */
            chn->rSmpPos.frac = 0x10000UL - chn->rSmpPos.frac;
            /* -1.0 */
            chn->rSmpPos.pos = chn->dSmpLoopEnd - loop_offset - 1;
            /* Switch direction */
            chn->dSmpStep = -chn->dSmpStep;
        }
    }
    else
    {
        /* In backward loop */
        if (chn->rSmpPos.pos < chn->dSmpLoopStart)
        {
            /* Restart loop */
            loop_len = chn->dSmpLoopEnd - chn->dSmpLoopStart;
            loop_offset = (chn->dSmpLoopStart - chn->rSmpPos.pos) % loop_len;

            chn->rSmpPos.pos = chn->dSmpLoopEnd - loop_offset;
        }
    }
}

static void __near check_sample_pos_pingpong (MIXCHN *chn)
{
    uint32_t loop_len, loops_count, loop_offset;

    if (chn->dSmpStep > 0)
    {
        /* Normal play or forward part of ping-pong loop */

        if (chn->rSmpPos.pos >= chn->dSmpLoopEnd)
        {
            /* Retrieve loop type */

            loop_len = chn->dSmpLoopEnd - chn->dSmpLoopStart;
            loops_count = (chn->rSmpPos.pos - chn->dSmpLoopEnd) / loop_len;
            loop_offset = (chn->rSmpPos.pos - chn->dSmpLoopEnd) % loop_len;

            if (loops_count & 1)
            {
                /* Odd: forward */
                chn->rSmpPos.pos = chn->dSmpLoopStart + loop_offset;
            }
            else
            {
                /* Even: backward */
                /* +1.0 */
                chn->rSmpPos.frac = 0x10000UL - chn->rSmpPos.frac;
                /* -1.0 */
                chn->rSmpPos.pos = chn->dSmpLoopEnd - loop_offset - 1;
                /* Switch direction */
                chn->dSmpStep = -chn->dSmpStep;
            }
        }
    }
    else
    {
        /* Backward part of ping-pong loop */

        if (chn->rSmpPos.pos < chn->dSmpLoopStart)
        {
            /* Retrieve loop type */

            loop_len = chn->dSmpLoopEnd - chn->dSmpLoopStart;
            loops_count = (chn->dSmpLoopStart - chn->rSmpPos.pos) / loop_len;
            loop_offset = (chn->dSmpLoopStart - chn->rSmpPos.pos) % loop_len;

            if (loops_count & 1)
            {
                /* Odd: forward */
                /* +1.0 */
                chn->rSmpPos.frac = 0x10000UL - chn->rSmpPos.frac;
                /* -1.0 */
                chn->rSmpPos.pos = chn->dSmpLoopEnd - loop_offset - 1;
                 /* Switch direction */
                chn->dSmpStep = -chn->dSmpStep;
            }
            else
            {
                /* Even: backward */
                chn->rSmpPos.pos = chn->dSmpLoopStart + loop_offset;
            }
        }
    }
}

static void __near check_sample_pos (MIXCHN *chn)
{
    switch ((chn->sample->flags >> PCMSMPFL_LOOPSHIFT) & PCMSMPLOOP_MASK)
    {
    case PCMSMPLOOP_NONE:
        check_sample_pos_normal (chn);
        break;

    case PCMSMPLOOP_FORWARD:
        check_sample_pos_forward (chn);
        break;

    case PCMSMPLOOP_BACKWARD:
        check_sample_pos_backward (chn);
        break;

    case PCMSMPLOOP_PINGPONG:
        check_sample_pos_pingpong (chn);
        break;

    default:
        break;
    }
}

static void __near play8_nearest (
    struct play_sample_param_t *param,
    uint16_t count
)
{
    _play8_nearest_16 (param, count);
}

static void __near play16_nearest (
    struct play_sample_param_t *param,
    uint16_t count
)
{
    _play16_nearest_16 (param, count);
}

static void __near play8_linear (
    struct play_sample_param_t *param,
    uint16_t count
)
{
    _play8_linear_16 (FP_SEG (*interptab), param, count);
}

static void __near play16_linear (
    struct play_sample_param_t *param,
    uint16_t count
)
{
    _play16_linear_16 (FP_SEG (*interptab), param, count);
}

static void __near playmix8_nearest (
    int32_t __far *outbuf,
    struct play_sample_param_t *param,
    MIXVOL vol,
    uint16_t count
)
{
    _playmix8_nearest_32 (outbuf, param, FP_SEG (*volumetableptr),
        vol.b[0] - 1, count);
}

static void __near playmix16_nearest (
    int32_t __far *outbuf,
    struct play_sample_param_t *param,
    MIXVOL vol,
    uint16_t count
)
{
    _playmix16_nearest_32 (outbuf, param, FP_SEG (*volumetableptr),
        vol.b[0] - 1, count);
}

static void __near playmix8_nearest_s (
    int32_t __far *outbuf,
    struct play_sample_param_t *param,
    MIXVOL vol,
    uint16_t count
)
{
    if (vol.b[0] && vol.b[1])
        _playmix8_nearest_32s2 (outbuf, param,
            FP_SEG (*volumetableptr), vol.w - 0x101, count);
    else
        _playmix8_nearest_32s (outbuf + (vol.b[0] ? 0 : 1), param,
            FP_SEG (*volumetableptr), vol.b[0] + vol.b[1] - 1, count);
}

static void __near playmix16_nearest_s (
    int32_t __far *outbuf,
    struct play_sample_param_t *param,
    MIXVOL vol,
    uint16_t count
)
{
    if (vol.b[0] && vol.b[1])
        _playmix16_nearest_32s2 (outbuf, param,
            FP_SEG (*volumetableptr), vol.w - 0x101, count);
    else
        _playmix16_nearest_32s (outbuf + (vol.b[0] ? 0 : 1), param,
            FP_SEG (*volumetableptr), vol.b[0] + vol.b[1] - 1, count);
}

static void __near mix (
    int32_t __far *outbuf,
    int16_t __far *inbuf,
    MIXVOL vol,
    uint16_t count
)
{
    _mix16_32 (outbuf, inbuf, FP_SEG (*volumetableptr), vol.b[0] - 1, count);
}

static void __near mix_s (
    int32_t __far *outbuf,
    int16_t __far *inbuf,
    MIXVOL vol,
    uint16_t count
)
{
    if (vol.b[0] && vol.b[1])
        _mix16_32s2 (outbuf, inbuf, FP_SEG (*volumetableptr),
            vol.w - 0x101, count);
    else
        _mix16_32s (outbuf + (vol.b[0] ? 0 : 1), inbuf,
            FP_SEG (*volumetableptr), vol.b[0] + vol.b[1] - 1, count);
}

static void __near song_play_channel (PLAYSTATE *ps, MIXCHN *chn, MIXER *mixer, uint16_t count, uint16_t bufOff)
{
    MIXBUF *mb = mixer->mixbuf;
    int32_t __far *ob = (int32_t __far *) ((uint8_t __far *) mb->buf + bufOff);
    struct play_sample_param_t param;
    MUSINS *ins;
    PCMSMP *smp;
    uint8_t NV, IV, SV, GV;

    /* first check for correct position inside sample */

    smp = chn->sample;
    check_sample_pos (chn);

    if (!(chn->flags & MIXCHNFL_PLAYING))
        return;

    ins = chn->instrument;
    NV = chn->eff_volume;               /* = 0..CHN_NOTEVOL_MAX (CHN_NOTEVOL_BITS bits) */
    IV = ins->volume;                   /* = 0..MUSINS_VOLUME_MAX (MUSINS_VOLUME_BITS bits) */
    SV = smp->volume;                   /* = 0..PCMSMP_VOLUME_MAX  (PCMSMP_VOLUME_BITS bits) */
    GV = ps->global_volume;             /* = 0..MUSMOD_GLOBAL_VOLUME_MAX (MUSMOD_GLOBAL_VOLUME_BITS bits) */
    if (NV && IV && SV && GV)
    {
        /* output volume = 0..MIXCHNVOL_MAX (MIXCHNVOL_BITS bits) */
        chn->bSmpVol = ((uint32_t) NV * IV * SV * GV)
            >> (CHN_NOTEVOL_BITS
                + MUSINS_VOLUME_BITS
                + PCMSMP_VOLUME_BITS
                + MUSMOD_GLOBAL_VOLUME_BITS
                - MIXCHNVOL_BITS);
        if (chn->bSmpVol > MIXCHNVOL_MAX)
            chn->bSmpVol = MIXCHNVOL_MAX;
    }
    else
        chn->bSmpVol = 0;

    if ((chn->flags & MIXCHNFL_MIXING) && chn->bSmpVol)
    {
        MIXVOL mixvol;
        void __near (*play_proc) (struct play_sample_param_t *, uint16_t);
        void __near (*playmix_proc) (int32_t __far *, struct play_sample_param_t *, MIXVOL, uint16_t);
        void __near (*mix_proc) (int32_t __far *, int16_t __far *, MIXVOL, uint16_t);
        char ob_shift;
        int16_t __far *sb;
        uint16_t mix_count;

        if (mb->channels == 2)
        {
            switch (chn->pan)
            {
            case 0:
                mixvol.b[0] = chn->bSmpVol;
                mixvol.b[1] = 0;
                break;
            case MIXCHNPAN_MAX:
                mixvol.b[0] = 0;
                mixvol.b[1] = chn->bSmpVol;
                break;
            default:
                mixvol.b[1] = ((uint16_t) chn->bSmpVol * chn->pan) >> MIXCHNPAN_BITS;
                mixvol.b[0] = chn->bSmpVol - mixvol.b[1];
                break;
            }
            ob_shift = 1;
        }
        else
        {
            mixvol.w = chn->bSmpVol;
            ob_shift = 0;
        }

        chn->rSmpMixVol = mixvol;

        switch (mixer->quality)
        {
        case MIXQ_NEAREST:
            if (smp->flags & PCMSMPFL_16BITS)
                play_proc = &play16_nearest;
            else
                play_proc = &play8_nearest;
            if (mb->channels == 2)
                mix_proc = &mix_s;
            else
                mix_proc = &mix;
            playmix_proc = NULL;
            break;

        case MIXQ_LINEAR:
            if (smp->flags & PCMSMPFL_16BITS)
                play_proc = &play16_linear;
            else
                play_proc = &play8_linear;
            if (mb->channels == 2)
                mix_proc = &mix_s;
            else
                mix_proc = &mix;
            playmix_proc = NULL;
            break;

        case MIXQ_FASTEST:
        default:
            if (mb->channels == 2)
            {
                if (smp->flags & PCMSMPFL_16BITS)
                    playmix_proc = &playmix16_nearest_s;
                else
                    playmix_proc = &playmix8_nearest_s;
            }
            else
            {
                if (smp->flags & PCMSMPFL_16BITS)
                    playmix_proc = &playmix16_nearest;
                else
                    playmix_proc = &playmix8_nearest;
            }
            play_proc = NULL;
            mix_proc = NULL;
            break;
        }

        if (mixer->quality != MIXQ_FASTEST)
        {
            sb = mixer->smpbuf->buf;
            param.dBuf = sb;
        }

        /* values per single channel mixed (or have to mix) into mixbuf */
        mix_count = 0;

        do
        {
            bool recalc = false;    /* recalculate sample position */
            SMPPOS lp;              /* last position */
            uint16_t part_count;    /* partial count (limited by DOS segment) */
            bool data_ok;           /* data available (and optionally mapped) */

            if (count > 1)
            {
                next_sample_pos (&lp, &chn->rSmpPos, chn->dSmpStep, count - 1);

                if (limit_sample_pos (&lp, chn))
                    recalc = true;

                if (limit_sample_pos_dos (&lp, chn, mixer))
                    recalc = true;
            }
            else
                lp = chn->rSmpPos;

            if (recalc)
            {
                SMPPOS d;   /* distance */

                sample_dist (&d, &chn->rSmpPos, &lp);
                part_count = sample_steps (&d, chn->dSmpStep) + 1;
                if (part_count > 1)
                    next_sample_pos (&lp, &chn->rSmpPos, chn->dSmpStep, part_count - 1);
                else
                    lp = chn->rSmpPos;
            }
            else
                part_count = count;

            data_ok = set_play_param (&param, chn, mixer, &lp);

            switch (mixer->quality)
            {
            case MIXQ_FASTEST:
                if (data_ok)
                    playmix_proc (ob, &param, mixvol, part_count);
                ob += part_count << ob_shift;
                break;

            default:
                if (data_ok)
                    play_proc (&param, part_count);
                else
                    fill_16 (param.dBuf, 0, part_count);
                param.dBuf += part_count;
                break;
            }

            /* one step forward - get next position */
            next_sample_pos (&chn->rSmpPos, &lp, chn->dSmpStep, 1);

            check_sample_pos (chn);

            mix_count += part_count;
            count -= part_count;
        } while ((chn->flags & MIXCHNFL_PLAYING) && count);

        if (mixer->quality != MIXQ_FASTEST)
            mix_proc (ob, sb, mixvol, mix_count);
    }
    else
    {
        /* Silent play */
        next_sample_pos (&chn->rSmpPos, &chn->rSmpPos, chn->dSmpStep, count);
        check_sample_pos (chn);
    }
}

void song_play (PLAYSTATE *ps, MIXER *mixer, uint16_t len)
{
    MIXCHNLIST *channels = ps->channels;
    MIXBUF *mb = mixer->mixbuf;
    uint16_t bufSize = mixbuf_get_offset_from_count (mb, len);
    uint16_t bufOff = 0;

    while ((ps->flags & PLAYSTATEFL_PLAYING) && (bufOff < bufSize))
    {
        /* samples per channel to calculate */
        uint16_t count = mixbuf_get_count_from_offset (mb, bufSize - bufOff);
        bool callEffects;
        uint8_t i;

        if (!ps->tick_spc_counter)
        {
            song_new_tick (ps);
            if (!(ps->flags & PLAYSTATEFL_PLAYING))
                break;
            callEffects = ps->tick != ps->speed;
        }
        else
            callEffects = false;

        if (count > ps->tick_spc_counter)
            count = ps->tick_spc_counter;

        if (!count)
            break;

        /* do effects */
        if (callEffects)
            for (i = 0; i < mixchnl_get_count (channels); i++)
            {
                MIXCHN *chn = mixchnl_get (channels, i);

                if (chn->flags & MIXCHNFL_ENABLED)
                    chn_effTick (ps, chn);
            }

        for (i = 0; i < mixchnl_get_count (channels); i++)
        {
            MIXCHN *chn = mixchnl_get (channels, i);

            if ((chn->flags & MIXCHNFL_ENABLED)
            &&  (chn->flags & MIXCHNFL_PLAYING)
            &&  (chn->type == MIXCHNTYPE_PCM))
                    song_play_channel (ps, chn, mixer, count, bufOff);
        }

        ps->tick_spc_counter -= count;
        bufOff += mixbuf_get_offset_from_count (mb, count);
    }
}
