/* muspat.h -- declarations for muspat.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _MUSPAT_H_INCLUDED
#define _MUSPAT_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "dos/ems.h"
#include "dynarray.h"
#include "main/musdefs.h"

/*** Music pattern channel's event ***/

#pragma pack(push, 1);
typedef struct music_pattern_channel_event_data_t
{
    unsigned char instrument;
    unsigned char note;
    unsigned char note_volume;
    unsigned char command;
    unsigned char parameter;
};
#pragma pack(pop);
typedef struct music_pattern_channel_event_data_t MUSPATCHNEVDATA;

typedef uint8_t music_pattern_channel_event_flags_t;
typedef music_pattern_channel_event_flags_t MUSCHNEVFLAGS;

#define MUSPATCHNEVFL_INS  (1 << 0)
#define MUSPATCHNEVFL_NOTE (1 << 1)
#define MUSPATCHNEVFL_VOL  (1 << 2)
#define MUSPATCHNEVFL_CMD  (1 << 3)
#define MUSPATCHNEVFL_ALL  (MUSPATCHNEVFL_INS | MUSPATCHNEVFL_NOTE | MUSPATCHNEVFL_VOL | MUSPATCHNEVFL_CMD)

#pragma pack(push, 1);
typedef struct music_pattern_channel_event_t
{
    MUSCHNEVFLAGS flags;
    MUSPATCHNEVDATA data;
};
#pragma pack(pop);
typedef struct music_pattern_channel_event_t MUSPATCHNEVENT;

void muspatchnevent_clear (MUSPATCHNEVENT *self);

/*** Music pattern row's event ***/

typedef struct music_pattern_row_event_t
{
    unsigned char channel;
    MUSPATCHNEVENT event;
};
typedef struct music_pattern_row_event_t MUSPATROWEVENT;

void muspatrowevent_clear (MUSPATROWEVENT *self);

/*** Music pattern ***/

typedef uint8_t music_pattern_flags_t;
typedef music_pattern_flags_t MUSPATFLAGS;

#define MUSPATFL_EM     (1 << 0)    /* data is in EM */
#define MUSPATFL_OWNHDL (1 << 1)    /* has own EM handle, needs to be freed when done */
#define MUSPATFL_PACKED (1 << 2)    /* data is packed */

#pragma pack(push, 1);
typedef struct music_pattern_t
{
    MUSPATFLAGS flags;
    uint8_t channels;
    uint16_t rows;
    uint16_t size;
    union
    {
        struct
        {
            void *ptr;
        } dos;
        struct
        {
            uint32_t offset;
            EMSHDL   handle;
        } em;
    } data;
};
#pragma pack(pop);
typedef struct music_pattern_t MUSPAT;

void     muspat_init (MUSPAT *self);
uint16_t muspat_get_row_start (MUSPAT *self, uint16_t row, uint16_t channel);
void     muspat_set_packed_row_start (MUSPAT *self, uint16_t row, uint16_t offset); /* assumes EM data is mapped before call */
uint16_t muspat_get_packed_row_start (MUSPAT *self, uint16_t row);                  /* assumes EM data is mapped before call */
uint16_t muspat_get_packed_size (MUSPAT *self);                                     /* assumes EM data is mapped before call */
void     muspat_free (MUSPAT *self);

/*** Music pattern IO ***/

typedef unsigned char music_pattern_io_mode_t;
typedef music_pattern_io_mode_t MUSPATIOMODE;

#define MUSPATIOMD_READ     0
#define MUSPATIOMD_WRITE    1

typedef struct music_pattern_io_t;
typedef struct music_pattern_io_t MUSPATIO;

typedef void muspatio_seek_t (MUSPATIO *self, unsigned int row, unsigned char channel);
typedef void muspatio_read_write_t (MUSPATIO *self, MUSPATROWEVENT *event);
typedef bool muspatio_is_end_of_row_t (MUSPATIO *self);
typedef void muspatio_end_row_t (MUSPATIO *self);
typedef void muspatio_close_t (MUSPATIO *self);

typedef struct music_pattern_io_t
{
    muspatio_seek_t *m_seek;
    muspatio_read_write_t *m_read_write;
    muspatio_is_end_of_row_t *m_is_end_of_row;
    muspatio_end_row_t *m_end_row;
    muspatio_close_t *m_close;
    char *error;
    MUSPAT *pattern;
    MUSPATIOMODE mode;
    unsigned char *data;
    unsigned int row;
    unsigned int row_start;
    unsigned int row_end;
    unsigned int offset;
    unsigned char channel;
};
typedef struct music_pattern_io_t MUSPATIO;

bool       muspatio_open (MUSPATIO *self, MUSPAT *pattern, MUSPATIOMODE mode);  /* maps EM data */
#define    muspatio_seek(self, row, channel)    (self)->m_seek ((self), (row), (channel))
#define    muspatio_read(self, event)           (self)->m_read_write ((self), (event))
#define    muspatio_write(self, event)          (self)->m_read_write ((self), (event))
#define    muspatio_is_end_of_row(self)         (self)->m_is_end_of_row (self)
#define    muspatio_end_row(self)               (self)->m_end_row (self)
#define    muspatio_close(self)                 (self)->m_close (self)

/*** Patterns list ***/

typedef uint16_t music_patterns_list_flags_t;
typedef music_patterns_list_flags_t MUSPATLFLAGS;

#define MUSPATLFL_EM     (1 << 0)   /* data is in EM */
#define MUSPATLFL_OWNHDL (1 << 1)   /* has own EM handle, needs to be freed when done */

typedef struct music_patterns_list_t
{
    MUSPATLFLAGS flags;
    DYNARR list;
    EMSHDL handle;
};
typedef struct music_patterns_list_t MUSPATLIST;

/* Methods */

void    muspatl_init (MUSPATLIST *self);
#define muspatl_set_count(o, v) dynarr_set_size (&(o)->list, v)
#define muspatl_get_count(o)    dynarr_get_size (&(o)->list)
#define muspatl_set(o, i, v)    dynarr_set_item (&(o)->list, i, v)
#define muspatl_get(o, i)       dynarr_get_item (&(o)->list, i)
void    muspatl_free (MUSPATLIST *self);

/*** Patterns order ***/

typedef uint8_t music_patterns_order_entry_t;
typedef music_patterns_order_entry_t MUSPATORDENT;

#define MUSPATORDENT_SKIP   0xfe
#define MUSPATORDENT_END    0xff

typedef struct music_patterns_order_t
{
    DYNARR list;
};
typedef struct music_patterns_order_t MUSPATORDER;

void    muspatorder_init (MUSPATORDER *self);
#define muspatorder_set_count(o, v) dynarr_set_size (&(o)->list, v)
#define muspatorder_get_count(o)    dynarr_get_size (&(o)->list)
#define muspatorder_set(o, i, v)    dynarr_set_item (&(o)->list, i, v)
#define muspatorder_get(o, i)       (MUSPATORDENT *) dynarr_get_item (&(o)->list, i)
int     muspatorder_find_next_pattern (MUSPATORDER *self, int first, int last, int pos, int step, bool skipend);
int     muspatorder_find_last (MUSPATORDER *self, bool skipend);
void    muspatorder_free (MUSPATORDER *self);

/*** Debug ***/

#if DEBUG == 1

// Format is "no in vl eff" (12 characters + zero)
void DEBUG_get_pattern_channel_event_str (char *s, MUSPATCHNEVENT *event);

void DEBUG_dump_pattern_info (MUSPAT *pattern, int index);

// "s" must hold atleast 64 bytes or (num_channels * 13) bytes
bool DEBUG_dump_pattern (MUSPAT *self, char *s, uint8_t num_channels);

#else  /* DEBUG != 1 */

#define DEBUG_get_pattern_channel_event_str(s, event)
#define DEBUG_dump_pattern_info(pattern, index)
#define DEBUG_dump_pattern(self, s, num_channels)

#endif  /* DEBUG != 1 */

#endif  /* !_MUSPAT_H_INCLUDED */
