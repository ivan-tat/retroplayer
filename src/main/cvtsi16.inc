/* cvtsi16.inc - "convert_sign_16" routine.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

void convert_sign_16 (void __far *data, uint32_t size)
{
    uint32_t offset = FARPTR_TO_LONG (data);

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("Requested: size=0x%lX, address=0x%lX, pointer=0x%04hX:0x%04hX",
            (uint32_t) size,
            (uint32_t) offset,
            (uint16_t) FP_SEG (data),
            (uint16_t) FP_OFF (data)
        );

    size /= 2;  /* 'size' is counter */

    while (size)
    {
        uint16_t __far *p = FARPTR_FROM_LONG (offset);
        uint16_t count;

        /* counter register in DOS segment limit */
        if ((uint32_t) FP_OFF (p) + size * 2 > (1UL<<16))
            count = ((1UL<<16) - FP_OFF (p)) / 2;
        else
            count = size;

        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("Converting: count=0x%hX, address=0x%lX, pointer=0x%04hX:0x%04hX",
                (uint16_t) count,
                (uint32_t) offset,
                (uint16_t) FP_SEG (p),
                (uint16_t) FP_OFF (p)
            );

        offset += (uint32_t) count * 2;
        size -= count;

        do
        {
            *p ^= 0x8000;
            p++;
            count--;
        } while (count);
    }
}
