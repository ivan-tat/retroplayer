/* musmodps.c -- music module play state handling methods.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/string.h"
#include "common.h"
#include "dynarray.h"
#include "debug.h"
#include "main/musdefs.h"
#include "main/musmod.h"
#include "main/mixchn.h"
#include "main/musmodps.h"

void playstate_init (PLAYSTATE *self)
{
    if (self)
        memset (self, 0, sizeof (PLAYSTATE));
    else
        DEBUG_ERR ("self is NULL!");
}

bool playstate_alloc_channels (PLAYSTATE *self)
{
    if (self)
    {
        MUSMOD *track = self->track;

        if (track && (track->flags & MUSMODFL_LOADED))
        {
            MIXCHNLIST *channels = self->channels;
            uint8_t num_channels;

            if (!channels)
            {
                channels = _new (MIXCHNLIST);
                if (!channels)
                {
                    DEBUG_ERR_ ("Failed to allocate memory for %s.", "mixing channels object");
                    return false;
                }
                mixchnl_init (channels);
                self->channels = channels;
            }

            num_channels = track->channels_count;

            if (mixchnl_get_count (channels) != num_channels)
                if (!mixchnl_set_count (channels, track->channels_count))
                {
                    DEBUG_ERR_ ("Failed to allocate memory for %s.", "mixing channels");
                    return false;
                }

            return true;
        }
        else
        {
            DEBUG_ERR ("Track is not set or loaded.");
            return false;
        }
    }
    else
    {
        DEBUG_ERR ("self is NULL!");
        return false;
    }
}

void playstate_reset_channels (PLAYSTATE *self)
{
    if (self)
    {
        MUSMOD *track = self->track;

        if (track && (track->flags & MUSMODFL_LOADED))
        {
            MIXCHNLIST *channels = self->channels;

            if (channels)
            {
                uint8_t num_channels = track->channels_count;
                uint8_t num_channels2 = mixchnl_get_count (channels);
                uint8_t i;

                if (num_channels > num_channels2)
                    num_channels = num_channels2;

                for (i = 0; i < num_channels; i++)
                {
                    MIXCHN *chn = mixchnl_get (channels, i);
                    MIXCHNPAN pan = track->channels[i].pan & MUSMODCHNPANFL_PAN_MASK;
                    MIXCHNTYPE type;
                    MIXCHNFLAGS flags;

                    if (track->channels[i].pan & MUSMODCHNPANFL_ENABLED)
                    {
                        type = MIXCHNTYPE_PCM;
                        flags = MIXCHNFL_ENABLED | MIXCHNFL_MIXING;
                    }
                    else
                    {
                        type = MIXCHNTYPE_NONE;
                        flags = 0;
                    }

                    chn->type = type;
                    chn->pan = pan;
                    chn->flags = flags;
                    if (chn->type != MIXCHNTYPE_NONE)
                        mixchn_reset_wave_tables (chn);
                }
            }
            else
                DEBUG_ERR ("No mixing channels.");
        }
        else
            DEBUG_ERR ("Track is not set or loaded.");
    }
    else
        DEBUG_ERR ("self is NULL!");
}

void playstate_free_channels (PLAYSTATE *self)
{
    if (self)
    {
        if (self->channels)
        {
            mixchnl_free (self->channels);
            _delete (self->channels);
        }
    }
    else
        DEBUG_ERR ("self is NULL!");
}

void playstate_set_speed (PLAYSTATE *self, uint8_t value)
{
    if (self)
    {
        if (value > 0)
            self->speed = value;
    }
    else
        DEBUG_ERR ("self is NULL!");
}

void playstate_set_tempo (PLAYSTATE *self, uint8_t value)
{
    if (self)
    {
        if (value >= 32)
            self->tempo = value;
        else
            value = self->tempo;

        if (value)
            self->tick_spc = (long)self->rate * 5 / (int)(value * 2);
    }
    else
        DEBUG_ERR ("self is NULL!");
}

void playstate_setup_patterns_order (PLAYSTATE *self)
{
    if (self)
    {
        MUSMOD *track = self->track;
        int i;

        if (track && (track->flags & MUSMODFL_LOADED))
            i = muspatorder_find_last (&track->order, self->flags & PLAYSTATEFL_SKIPENDMARK);
        else
            i = 0;

        self->order_last = i;
    }
    else
        DEBUG_ERR ("self is NULL!");
}

int playstate_find_next_pattern (PLAYSTATE *self, int index, int step)
{
    if (self)
    {
        MUSMOD *track = self->track;

        if (track && (track->flags & MUSMODFL_LOADED))
        {
            MUSPATORDER *order = &track->order;
            int start = self->order_start;
            int last = self->order_last;
            int pos = index;
            bool skipend = self->flags & PLAYSTATEFL_SKIPENDMARK;

            // Check bounds

            if ((step < 0) && (pos <= start))
                // Rewind
                return muspatorder_find_next_pattern (order, start, last, start, 1, skipend);

            if ((step > 0) && (pos >= self->order_last))
            {
                if (self->flags & PLAYSTATEFL_SONGLOOP)
                    // Rewind
                    return muspatorder_find_next_pattern (order, start, last, start, 1, skipend);
                else
                    // Stop
                    return -1;
            }

            pos = muspatorder_find_next_pattern (order, start, last, pos + step, step, skipend);

            if (pos < 0)
            {
                if ((step < 0) || (self->flags & PLAYSTATEFL_SONGLOOP))
                    // Rewind
                    return muspatorder_find_next_pattern (order, start, last, start, 1, skipend);
            }

            return pos;
        }
        else
        {
            DEBUG_ERR ("Track is not set or loaded.");
            return -1;
        }
    }
    else
    {
        DEBUG_ERR ("self is NULL!");
        return -1;
    }
}

void playstate_set_pos (PLAYSTATE *self, uint8_t start_order, uint8_t start_row, bool keep)
{
    if (self)
    {
        MUSMOD *track = self->track;

        if (track && (track->flags & MUSMODFL_LOADED))
        {
            MUSPATORDER *order = &track->order;
            MUSPATORDENT *order_entry = muspatorder_get (order, start_order);

            self->order = start_order;      // next order to read from
            self->pattern = *order_entry;   // next pattern to read from
            self->row = start_row;          // next row to read from
            self->tick = 1;                 // last tick (go to next row)
            self->tick_spc_counter = 0;     // immediately next tick

            if (!keep)
            {
                // reset pattern effects:
                self->patdelay_count = 0;
                self->flags &= ~PLAYSTATEFL_PATLOOP;
                self->patloop_count = 0;
                self->patloop_start_row = 0;
            }
        }
        else
            DEBUG_ERR ("Track is not set or loaded.");
    }
    else
        DEBUG_ERR ("self is NULL!");
}

void playstate_set_initial_state (PLAYSTATE *self)
{
    if (self)
    {
        MUSMOD *track = self->track;

        if (track && (track->flags & MUSMODFL_LOADED))
        {
            playstate_set_tempo (self, track->tempo);   // first priority (is output mixer-dependant)
            playstate_set_speed (self, track->speed);   // second priority (is song's internal value)
            self->global_volume = track->global_volume; // is song's internal value
            self->master_volume = track->master_volume; // is song's output
        }
        else
            DEBUG_ERR ("Track is not set or loaded.");
    }
    else
        DEBUG_ERR ("self is NULL!");
}

void playstate_free (PLAYSTATE *self)
{
    if (self)
        playstate_free_channels (self);
    else
        DEBUG_ERR ("self is NULL!");
}
