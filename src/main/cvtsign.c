/* cvtsign.c - routines to convert sign of integers.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "debug.h"
#include "cc/i86.h"
#include "dos/ems.h"
#include "main/cvtsign.h"

#include "main/cvtsi8.inc"
#include "main/cvtsi16.inc"
#include "main/cvtsi8e.inc"
#include "main/cvtsi16e.inc"
