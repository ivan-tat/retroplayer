/* mixer.h -- declarations for mixer.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _MIXER_H_INCLUDED
#define _MIXER_H_INCLUDED 1

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

#define EXTRA_LEN_FOR_LINEAR_INTERP 1

/* Sample playing */

#define PLAYSMPFL_16BITS 0x01
#define PLAYSMPFL_LOOP   0x02

#pragma pack(push, 1);
typedef struct play_sample_param_t
{
    void __far *dData;
    uint16_t wPosFrac;
    uint16_t wPos;
    int32_t  dStep;
    uint16_t wLen;
    uint16_t wLoopStart;
    uint16_t wLoopEnd;
    uint16_t wFlags;
    int16_t __far *dBuf;

};
#pragma pack(pop);

typedef void __far __pascal play_nearest_proc_t (
    struct play_sample_param_t __far *param,
    uint16_t count
);

/* Type: play_nearest_proc_t */
extern void __far __pascal _play8_nearest_16 (
    struct play_sample_param_t __far *param,
    uint16_t count
);

/* Type: play_nearest_proc_t */
extern void __far __pascal _play16_nearest_16 (
    struct play_sample_param_t __far *param,
    uint16_t count
);

typedef void __far __pascal play_linear_proc_t (
    uint16_t interptab,
    struct play_sample_param_t __far *param,
    uint16_t count
);

/* Type: play_linear_proc_t */
extern void __far __pascal _play8_linear_16 (
    uint16_t interptab,
    struct play_sample_param_t __far *param,
    uint16_t count
);

/* Type: play_linear_proc_t */
extern void __far __pascal _play16_linear_16 (
    uint16_t interptab,
    struct play_sample_param_t __far *param,
    uint16_t count
);

typedef void __far __pascal playmix_32_proc_t (
    int32_t __far *outbuf,
    struct play_sample_param_t __far *param,
    uint16_t voltab,
    uint16_t vol,
    uint16_t count
);

/* Type: playmix_32_proc_t */
extern void __far __pascal _playmix8_nearest_32 (
    int32_t __far *outbuf,
    struct play_sample_param_t __far *param,
    uint16_t voltab,
    uint16_t vol,
    uint16_t count
);

/* Type: playmix_32_proc_t */
extern void __far __pascal _playmix16_nearest_32 (
    int32_t __far *outbuf,
    struct play_sample_param_t __far *param,
    uint16_t voltab,
    uint16_t vol,
    uint16_t count
);

/* Type: playmix_32_proc_t */
extern void __far __pascal _playmix8_nearest_32s (
    int32_t __far *outbuf,
    struct play_sample_param_t __far *param,
    uint16_t voltab,
    uint16_t vol,
    uint16_t count
);

/* Type: playmix_32_proc_t */
extern void __far __pascal _playmix16_nearest_32s (
    int32_t __far *outbuf,
    struct play_sample_param_t __far *param,
    uint16_t voltab,
    uint16_t vol,
    uint16_t count
);

/* Type: playmix_32_proc_t */
extern void __far __pascal _playmix8_nearest_32s2 (
    int32_t __far *outbuf,
    struct play_sample_param_t __far *param,
    uint16_t voltab,
    uint16_t vol,
    uint16_t count
);

/* Type: playmix_32_proc_t */
extern void __far __pascal _playmix16_nearest_32s2 (
    int32_t __far *outbuf,
    struct play_sample_param_t __far *param,
    uint16_t voltab,
    uint16_t vol,
    uint16_t count
);

typedef void __far __pascal mix16_32_proc_t (
    int32_t __far *outbuf,
    int16_t __far *inbuf,
    uint16_t voltab,
    uint16_t vol,
    uint16_t count
);

/* Type: mix16_32_proc_t */
extern void __far __pascal _mix16_32 (
    int32_t __far *outbuf,
    int16_t __far *inbuf,
    uint16_t voltab,
    uint16_t vol,
    uint16_t count
);

/* Type: mix16_32_proc_t */
extern void __far __pascal _mix16_32s (
    int32_t __far *outbuf,
    int16_t __far *inbuf,
    uint16_t voltab,
    uint16_t vol,
    uint16_t count
);

/* Type: mix16_32_proc_t */
extern void __far __pascal _mix16_32s2 (
    int32_t __far *outbuf,
    int16_t __far *inbuf,
    uint16_t voltab,
    uint16_t vol,
    uint16_t count
);

/* Filling */

void fill_8(void *dest, uint8_t value, uint16_t count);
void fill_16(void *dest, uint16_t value, uint16_t count);
void fill_32(void *dest, uint32_t value, uint16_t count);

/* Playing */

extern uint16_t ST3Periods[12];

#define getNotePeriod(note) ((ST3Periods[(note) & 0x0f] << 4) >> ((note) >> 4))

uint32_t _calc_sample_step(uint16_t period, uint16_t rate);

/* Sample buffer for at least one frame */

#pragma pack(push, 1);
typedef struct sample_buffer_t
{
    int16_t *buf;
    uint16_t len;
};
#pragma pack(pop);
typedef struct sample_buffer_t SMPBUF;

void     smpbuf_init(SMPBUF *self);
bool     smpbuf_alloc(SMPBUF *self, uint16_t len);
void     smpbuf_free(SMPBUF *self);

#if DEBUG == 1
void _DEBUG_dump_smpbuf_info (const char *file, int line, const char *func, const char *name, SMPBUF *p);
# define DEBUG_dump_smpbuf_info(name, p) _DEBUG_dump_smpbuf_info (__FILE__, __LINE__, __func__, name, p)
#else
# define DEBUG_dump_smpbuf_info(name, p)
#endif  /* DEBUG != 1 */

/* Mixing buffer for at least one frame.
   Size depends on: sample rate, channels count, tempo, FPS (almost as DMA frame) */

#pragma pack(push, 1);
typedef struct mixing_buffer_t
{
    int32_t *buf;
    uint16_t len;
    uint8_t channels;
    uint16_t samples_per_channel;
};
#pragma pack(pop);
typedef struct mixing_buffer_t MIXBUF;

void     mixbuf_init(MIXBUF *self);
bool     mixbuf_alloc(MIXBUF *self, uint16_t len);
uint16_t mixbuf_get_offset_from_count(MIXBUF *self, uint16_t value);
uint16_t mixbuf_get_count_from_offset(MIXBUF *self, uint16_t value);
void     mixbuf_free(MIXBUF *self);

#if DEBUG == 1
void _DEBUG_dump_mixbuf_info (const char *file, int line, const char *func, const char *name, const MIXBUF *p);
# define DEBUG_dump_mixbuf_info(name, p) _DEBUG_dump_mixbuf_info (__FILE__, __LINE__, __func__, name, p)
#else
# define DEBUG_dump_mixbuf_info(name, p)
#endif  /* DEBUG != 1 */

/*** Mixer ***/

typedef uint8_t mixer_flags_t;
typedef mixer_flags_t MIXERFLAGS;

#define MIXERFL_OWN_SMPBUF  (1 << 0)    // has own sample buffer, free it when done
#define MIXERFL_OWN_MIXBUF  (1 << 1)    // has own mixing buffer, free it when done

typedef uint8_t mixer_quality_t;
typedef mixer_quality_t MIXERQUALITY;

#define MIXQ_NEAREST    0
#define MIXQ_LINEAR     1
#define MIXQ_FASTEST    2
#define MIXQ_MAX        2

typedef uint8_t mixer_buffers_mask_t;
typedef mixer_buffers_mask_t MIXERBUFMASK;

#define MIXERBUFMASK_SMPBUF   (1 << 0)
#define MIXERBUFMASK_MIXBUF   (1 << 1)

#pragma pack(push, 1);
typedef struct mixer_t
{
    MIXERFLAGS flags;
    MIXERQUALITY quality;
    uint8_t num_channels;
    uint16_t num_spc;   // samples per channel
    SMPBUF *smpbuf;
    MIXBUF *mixbuf;
};
#pragma pack(pop);
typedef struct mixer_t MIXER;

void    mixer_init (MIXER *self);
bool    mixer_alloc_buffers (MIXER *self, MIXERBUFMASK mask);
void    mixer_free_buffers (MIXER *self, MIXERBUFMASK mask);
void    mixer_free (MIXER *self);

#if DEBUG == 1
void _DEBUG_dump_mixer_info (const char *file, int line, const char *func, const char *name, const MIXER *p);
# define DEBUG_dump_mixer_info(name, p) _DEBUG_dump_mixer_info (__FILE__, __LINE__, __func__, name, p)
#else
# define DEBUG_dump_mixer_info(name, p)
#endif  /* DEBUG != 1 */

#endif /* !_MIXER_H_INCLUDED */
