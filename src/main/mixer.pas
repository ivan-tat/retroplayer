(* mixer.pas -- Pascal declarations for mixer.c.

   This file is for linking compiled object files with Pascal linker.
   It will be deleted in future when we rewrite the project in C.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. *)

unit mixer;

interface

(*$I defines.pas*)

(*$ifdef DEFINE_LOCAL_DATA*)

const
    ST3Periods: array [0..11] of Word = (
    (*$i main\nperiods.inc*)
    );

(*$endif*)  (* DEFINE_LOCAL_DATA *)

procedure _play8_nearest_16;
procedure _play16_nearest_16;

procedure _play8_linear_16;
procedure _play16_linear_16;

procedure _playmix8_nearest_32;
procedure _playmix16_nearest_32;
procedure _playmix8_nearest_32s;
procedure _playmix16_nearest_32s;
procedure _playmix8_nearest_32s2;
procedure _playmix16_nearest_32s2;

procedure _mix16_32;
procedure _mix16_32s;
procedure _mix16_32s2;

procedure fill_8;
procedure fill_16;
procedure fill_32;

procedure _calc_sample_step;

procedure smpbuf_init;
procedure smpbuf_alloc;
procedure smpbuf_free;

(*$ifdef DEBUG*)
procedure _DEBUG_dump_smpbuf_info;
(*$endif*)  (* DEBUG *)

procedure mixbuf_init;
procedure mixbuf_alloc;
procedure mixbuf_get_offset_from_count;
procedure mixbuf_get_count_from_offset;
procedure mixbuf_free;

(*$ifdef DEBUG*)
procedure _DEBUG_dump_mixbuf_info;
(*$endif*)  (* DEBUG *)

procedure mixer_init;
procedure mixer_alloc_buffers;
procedure mixer_free_buffers;
procedure mixer_free;

(*$ifdef DEBUG*)
procedure _DEBUG_dump_mixer_info;
(*$endif*)  (* DEBUG *)

implementation

uses
    watcom,
    i86,
    string_,
    dos_,
    common,
    debug;

(*$l asm\sampler.obj*)

procedure _play8_nearest_16; external;
procedure _play16_nearest_16; external;

procedure _play8_linear_16; external;
procedure _play16_linear_16; external;

procedure _playmix8_nearest_32; external;
procedure _playmix16_nearest_32; external;
procedure _playmix8_nearest_32s; external;
procedure _playmix16_nearest_32s; external;
procedure _playmix8_nearest_32s2; external;
procedure _playmix16_nearest_32s2; external;

(*$l asm\mixer.obj*)

procedure _mix16_32; external;
procedure _mix16_32s; external;
procedure _mix16_32s2; external;

(*$l mixer.obj*)

procedure fill_8; external;
procedure fill_16; external;
procedure fill_32; external;

procedure _calc_sample_step; external;

procedure smpbuf_init; external;
procedure smpbuf_alloc; external;
procedure smpbuf_free; external;

(*$ifdef DEBUG*)
procedure _DEBUG_dump_smpbuf_info; external;
(*$endif*)  (* DEBUG *)

procedure mixbuf_init; external;
procedure mixbuf_alloc; external;
procedure mixbuf_get_offset_from_count; external;
procedure mixbuf_get_count_from_offset; external;
procedure mixbuf_free; external;

(*$ifdef DEBUG*)
procedure _DEBUG_dump_mixbuf_info; external;
(*$endif*)  (* DEBUG *)

procedure mixer_init; external;
procedure mixer_alloc_buffers; external;
procedure mixer_free_buffers; external;
procedure mixer_free; external;

(*$ifdef DEBUG*)
procedure _DEBUG_dump_mixer_info; external;
(*$endif*)  (* DEBUG *)

end.
