/* musins.c -- musical instrument handling library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/string.h"
#include "cc/stdio.h"
#include "debug.h"
#include "cc/dos.h"
#include "dynarray.h"
#include "dos/ems.h"
#include "main/pcmsmp.h"
#include "main/musins.h"

/*** Musical instrument ***/

void musins_init (MUSINS *self)
{
    if (self)
    {
        memset (self, 0, sizeof (MUSINS));
        //self->type = MUSINST_EMPTY;
    }
}

void musins_free (MUSINS *self)
{
    if (self)
    {
        if (self->type == MUSINST_PCM)
        {
            PCMSMP *smp = musins_get_sample (self);

            pcmsmp_free (smp);
            pcmsmp_init (smp);  // clear
        }
        musins_init (self); // clear
    }
}

/*** Musical instruments list ***/

void _musinsl_init_item (void *self, void *item)
{
    musins_init ((MUSINS *) item);
}

void _musinsl_free_item (void *self, void *item)
{
    musins_free ((MUSINS *) item);
}

void musinsl_init (MUSINSLIST *self)
{
    if (self)
        dynarr_init (&self->list, self, sizeof (MUSINS), _musinsl_init_item, _musinsl_free_item);
}

void musinsl_free (MUSINSLIST *self)
{
    if (self)
        dynarr_free (&self->list);
}

#if DEBUG == 1

void DEBUG_dump_instrument_info (MUSINS *self, uint8_t index, PCMSMPLIST *samples)
{
    #define _BUF_SIZE 8
    char *type;
    char s[_BUF_SIZE];
    int link;

    switch (self->type)
    {
    case MUSINST_EMPTY:
        type = "none";
        link = -1;
        break;
    case MUSINST_PCM:
        type = "sample";
        link = pcmsmpl_indexof (samples, _musins_get_sample (self));
        break;
    default:
        type = "unknown";
        link = -1;
        break;
    };
    snprintf (s, _BUF_SIZE, "%d", (int) link);
    DEBUG_INFO_ ("index=%hhu, type=%s (index=%s), volume=%hhu, title='%s'",
        (uint8_t) index,
        (char *) type,
        (char *) s,
        (uint8_t) self->note_volume,
        (char *) self->title
    );
    #undef _BUF_SIZE
}

#endif  /* DEBUG == 1 */
