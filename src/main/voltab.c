/* voltab.c -- functions to handle volume table.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#include "cc/string.h"
#include "cc/i86.h"
#include "cc/dos.h"
#if DEBUG == 1
# include "cc/stdio.h"
#endif  /* DEBUG == 1 */
#include "main/mixer.h"
#include "main/voltab.h"

#if DEFINE_LOCAL_DATA == 1

voltab_t *volumetableptr;

#endif  /* DEFINE_LOCAL_DATA == 1 */

void voltab_init(void)
{
    volumetableptr = NULL;
}

bool voltab_alloc(void)
{
    uint16_t seg;

    if (!_dos_allocmem(_dos_para(sizeof(voltab_t)), &seg))
    {
        volumetableptr = MK_FP(seg, 0);
        //memset(volumetableptr, 0, sizeof(voltab_t));
        fill_16 (volumetableptr, 0, sizeof (voltab_t) / 2);

        return true;
    }

    return false;
}

void voltab_calc(void)
{
    int16_t i, j, *p = (int16_t *)volumetableptr;

    for (i = 1; i <= 64; i++)
        for (j = 0; j < 0x100; j++)
        {
            // voltab[0][i-1][j]: sample MSB (signed)
            // voltab[1][i-1][j]: sample LSB (unsigned)
            p[0*64*256] = (i * (int8_t) j) << 2;
            p[1*64*256] = (i * j) >> 6;
            p++;
        }

    #if DEBUG == 1
    for (i = 0; i < 2; i++)
    {
        FILE *f;
        char *s = "_vol.x";
        s[5] = '0' + i;
        f = fopen (s, "w+b");
        if (f)
        {
            fwrite ((char *) volumetableptr + i * sizeof (voltab_t) / 2, sizeof (voltab_t) / 2, 1, f);
            fclose (f);
        }
    }
    #endif  /* DEBUG == 1 */
}

void voltab_free(void)
{
    if (volumetableptr)
    {
        _dos_freemem(FP_SEG(volumetableptr));
        volumetableptr = NULL;
    }
}
