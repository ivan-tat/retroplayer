/* effects.c -- effects handling.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/string.h"
#include "cc/stdio.h"
#include "cc/stdlib.h"
#include "dos/ems.h"
#include "main/musdefs.h"
#include "main/muspat.h"
#include "main/effvars.h"
#include "main/effects.h"
#include "main/musmodps.h"
#include "main/mixer.h"

#define get_i8_value(off, pos) *(int8_t *)MK_FP(FP_SEG(&wavetab), off + pos)

/*** Effects ***/

// Effect's method
typedef bool __near emInit_t  (PLAYSTATE *ps, ROWSTATE *rs, MIXCHN *chn, CHNSTATE *cs, uint8_t param);
typedef void __near emHandle_t(PLAYSTATE *ps, MIXCHN *chn, CHNSTATE *cs);
typedef void __near emTick_t  (PLAYSTATE *ps, MIXCHN *chn);
typedef bool __near emCont_t  (MIXCHN *chn, CHNSTATE *cs);
typedef void __near emStop_t  (PLAYSTATE *ps, MIXCHN *chn);

// Effect's methods table
typedef struct effMethodsTable_t
{
    emInit_t   *init;
    emHandle_t *handle;
    emTick_t   *tick;
    emCont_t   *cont;
    emStop_t   *stop;
};
typedef struct effMethodsTable_t EFFMT;

// Effect's method name (effm)
#define NAME_INIT(name)   effm_##name##_init
#define NAME_HANDLE(name) effm_##name##_handle
#define NAME_TICK(name)   effm_##name##_tick
#define NAME_CONT(name)   effm_##name##_cont
#define NAME_STOP(name)   effm_##name##_stop

// Effect's method definition
#define METHOD_INIT(name)   static bool __near NAME_INIT(name)  (PLAYSTATE *ps, ROWSTATE *rs, MIXCHN *chn, CHNSTATE *cs, uint8_t param)
#define METHOD_HANDLE(name) static void __near NAME_HANDLE(name)(PLAYSTATE *ps, MIXCHN *chn, CHNSTATE *cs)
#define METHOD_TICK(name)   static void __near NAME_TICK(name)  (PLAYSTATE *ps, MIXCHN *chn)
#define METHOD_CONT(name)   static bool __near NAME_CONT(name)  (MIXCHN *chn, CHNSTATE *cs)
#define METHOD_STOP(name)   static void __near NAME_STOP(name)  (PLAYSTATE *ps, MIXCHN *chn)

#define DEFINE_METHOD_INIT(name)   METHOD_INIT (name)
#define DEFINE_METHOD_HANDLE(name) METHOD_HANDLE (name)
#define DEFINE_METHOD_TICK(name)   METHOD_TICK (name)
#define DEFINE_METHOD_CONT(name)   METHOD_CONT (name)
#define DEFINE_METHOD_STOP(name)   METHOD_STOP (name)

// Effect's description (effd)
#define EFFECT(name) effd_##name
#define DEFINE_EFFECT(name, init, handle, tick, cont, stop)                  \
static const EFFMT __near EFFECT (name) =                                    \
{                                                                            \
   NAME_INIT  (init),                                                        \
   NAME_HANDLE(handle),                                                      \
   NAME_TICK  (tick),                                                        \
   NAME_CONT  (cont),                                                        \
   NAME_STOP  (stop),                                                        \
}

// Effects descriptions list (effdl)
#define EFFECTS_LIST(name) effdl_##name
#define DEFINE_EFFECTS_LIST(name) static const EFFMT __near *EFFECTS_LIST (name)[]

// Sub-effects description (effsd)
#define SUB_EFFECT(name) effsd_##name
#define DEFINE_SUB_EFFECT(name, init, handle, tick, cont, stop)              \
static const EFFMT __near SUB_EFFECT (name) =                                \
{                                                                            \
    NAME_INIT  (init),                                                       \
    NAME_HANDLE(handle),                                                     \
    NAME_TICK  (tick),                                                       \
    NAME_CONT  (cont),                                                       \
    NAME_STOP  (stop)                                                        \
}

// Sub-effects descriptions list (effsdl)
#define SUB_EFFECTS_LIST(name) effsdl_##name
#define DEFINE_SUB_EFFECTS_LIST(name) static const EFFMT __near *SUB_EFFECTS_LIST (name)[]

// Sub-effects names list (effsnl)
#define SUB_EFFECTS_NAME_LIST(name) effsnl_##name
#define DEFINE_SUB_EFFECTS_NAME_LIST(name) static const char __near *SUB_EFFECTS_NAME_LIST (name)[]

/*** No effect ***/

// effect continue checks

DEFINE_METHOD_CONT(allow);
DEFINE_METHOD_CONT(deny);

// empty effect "none"

DEFINE_METHOD_INIT   (none);
DEFINE_METHOD_HANDLE (none);
DEFINE_METHOD_TICK   (none);
DEFINE_METHOD_STOP   (none);
DEFINE_EFFECT (none, none, none, none, deny, none);

// empty effect with name 'n/a' "none_na"

DEFINE_METHOD_INIT   (none);
DEFINE_METHOD_HANDLE (none);
DEFINE_METHOD_TICK   (none);
DEFINE_METHOD_STOP   (none);
DEFINE_EFFECT (none_na, none, none, none, deny, none);

// empty sub-effect "none"

DEFINE_SUB_EFFECT(none, none, none, none, deny, none);

/****** Global effects ******/

/*** Set tempo "setTempo" ***/
/* Scream Tracker 3 command: T */

DEFINE_METHOD_INIT (setTempo);
DEFINE_EFFECT (setTempo, setTempo, none, none, deny, none);

/*** Set speed "setSpeed" ***/
/* Scream Tracker 3 command: A */

DEFINE_METHOD_INIT (setSpeed);
DEFINE_EFFECT (setSpeed, setSpeed, none, none, deny, none);

/*** Jump to order "jumpToOrder" ***/
/* Scream Tracker 3 command: B */

DEFINE_METHOD_INIT (jumpToOrder);
DEFINE_EFFECT (jumpToOrder, jumpToOrder, none, none, deny, none);

/*** Pattern break "patBreak" ***/
/* Scream Tracker 3 command: C */

DEFINE_METHOD_INIT (patBreak);
DEFINE_EFFECT (patBreak, patBreak, none, none, deny, none);

/*** Set global volume "setGVol" ***/
/* Scream Tracker 3 command: V */

DEFINE_METHOD_INIT (setGVol);
DEFINE_EFFECT (setGVol, setGVol, none, none, deny, none);

// TODO: move here pattern delay

/****** Channel effects ******/

/*** Volume slide "volSlide" ***/

/* Scream Tracker 3 command: D */

// sub-effects

DEFINE_METHOD_TICK(volSlide_down);
DEFINE_SUB_EFFECT(volSlide_down, none, none, volSlide_down, allow, none);

DEFINE_METHOD_TICK(volSlide_up);
DEFINE_SUB_EFFECT(volSlide_up, none, none, volSlide_up, allow, none);

DEFINE_METHOD_HANDLE(volSlide_fineDown);
DEFINE_SUB_EFFECT(volSlide_fineDown, none, volSlide_fineDown, none, allow, none);

DEFINE_METHOD_HANDLE(volSlide_fineUp);
DEFINE_SUB_EFFECT(volSlide_fineUp, none, volSlide_fineUp, none, allow, none);

// sub-effects list

DEFINE_SUB_EFFECTS_LIST(volSlide) =
{
    &SUB_EFFECT(volSlide_down),
    &SUB_EFFECT(volSlide_up),
    &SUB_EFFECT(volSlide_fineDown),
    &SUB_EFFECT(volSlide_fineUp)
};

// router

DEFINE_METHOD_INIT   (volSlide);
DEFINE_METHOD_HANDLE (volSlide);
DEFINE_METHOD_TICK   (volSlide);
DEFINE_EFFECT (volSlide, volSlide, volSlide, volSlide, allow, none);

/*** Pitch slide down "pitchDown" ***/
/* Scream Tracker 3 command: E */

// sub-effects

DEFINE_METHOD_TICK(pitchDown_normal);
DEFINE_SUB_EFFECT(pitchDown_normal, none, none, pitchDown_normal, allow, none);

DEFINE_METHOD_HANDLE(pitchDown_fine);
DEFINE_SUB_EFFECT(pitchDown_fine, none, pitchDown_fine, none, allow, none);

DEFINE_METHOD_HANDLE(pitchDown_extra);
DEFINE_SUB_EFFECT(pitchDown_extra, none, pitchDown_extra, none, allow, none);

// sub-effects list

DEFINE_SUB_EFFECTS_LIST(pitchDown) =
{
    &SUB_EFFECT(pitchDown_normal),
    &SUB_EFFECT(pitchDown_fine),
    &SUB_EFFECT(pitchDown_extra)
};

// router

DEFINE_METHOD_INIT   (pitchDown);
DEFINE_METHOD_HANDLE (pitchDown);
DEFINE_METHOD_TICK   (pitchDown);

DEFINE_EFFECT (pitchDown, pitchDown, pitchDown, pitchDown, allow, none);

/*** Pitch slide up "pitchUp" ***/
/* Scream Tracker 3 command: F */

// sub-effects

DEFINE_METHOD_TICK(pitchUp_normal);
DEFINE_SUB_EFFECT(pitchUp_normal, none, none, pitchUp_normal, allow, none);

DEFINE_METHOD_HANDLE(pitchUp_fine);
DEFINE_SUB_EFFECT(pitchUp_fine, none, pitchUp_fine, none, allow, none);

DEFINE_METHOD_HANDLE(pitchUp_extra);
DEFINE_SUB_EFFECT(pitchUp_extra, none, pitchUp_extra, none, allow, none);

// sub-effects list

DEFINE_SUB_EFFECTS_LIST(pitchUp) =
{
    &SUB_EFFECT(pitchUp_normal),
    &SUB_EFFECT(pitchUp_fine),
    &SUB_EFFECT(pitchUp_extra)
};

// router

DEFINE_METHOD_INIT   (pitchUp);
DEFINE_METHOD_HANDLE (pitchUp);
DEFINE_METHOD_TICK   (pitchUp);
DEFINE_EFFECT (pitchUp, pitchUp, pitchUp, pitchUp, allow, none);

/*** Portamento to note "porta" ***/
/* Scream Tracker 3 command: G */

DEFINE_METHOD_INIT   (porta);
DEFINE_METHOD_HANDLE (porta);
DEFINE_METHOD_TICK   (porta);
DEFINE_EFFECT (porta, porta, porta, porta, allow, none);

/*** Portamento to note + Volume slide "porta_vol" ***/
/* Scream Tracker 3 command: L (G + D) */

DEFINE_METHOD_INIT   (porta_vol);
DEFINE_METHOD_HANDLE (porta_vol);
DEFINE_METHOD_TICK   (porta_vol);
DEFINE_EFFECT (porta_vol, porta_vol, porta_vol, porta_vol, allow, none);

/*** Vibrato (normal) "vibNorm" ***/
/* Scream Tracker 3 command: H */

DEFINE_METHOD_INIT   (vibNorm);
DEFINE_METHOD_HANDLE (vibNorm);
DEFINE_METHOD_TICK   (vibNorm);
DEFINE_METHOD_CONT   (vibNorm);
DEFINE_METHOD_STOP   (vibNorm);
DEFINE_EFFECT (vibNorm, vibNorm, vibNorm, vibNorm, vibNorm, vibNorm);

/*** Vibrato (fine) "vibFine" ***/
/* Scream Tracker 3 command: U */

DEFINE_METHOD_INIT (vibFine);
DEFINE_METHOD_TICK (vibFine);
DEFINE_EFFECT (vibFine, vibFine, vibNorm, vibFine, vibNorm, vibNorm);

/*** Vibrato (normal) + Volume slide "vibNorm_vol" ***/
/* Scream Tracker 3 command: K (H + D) */

DEFINE_METHOD_INIT   (vibNorm_vol);
DEFINE_METHOD_HANDLE (vibNorm_vol);
DEFINE_METHOD_TICK   (vibNorm_vol);
DEFINE_EFFECT (vibNorm_vol, vibNorm_vol, vibNorm_vol, vibNorm_vol, vibNorm, vibNorm);

/*** Tremor "tremor" ***/
/* Scream Tracker 3 command: I */

DEFINE_METHOD_INIT (tremor);
DEFINE_METHOD_HANDLE (tremor);
DEFINE_METHOD_TICK (tremor);
DEFINE_EFFECT (tremor, tremor, tremor, tremor, allow, none);

/*** Arpeggio "arpeggio" ***/
/* Scream Tracker 3 command: J */

DEFINE_METHOD_INIT   (arpeggio);
DEFINE_METHOD_HANDLE (arpeggio);
DEFINE_METHOD_TICK   (arpeggio);
DEFINE_METHOD_STOP   (arpeggio);
DEFINE_EFFECT (arpeggio, arpeggio, arpeggio, arpeggio, allow, arpeggio);

/*** Set sample offset "sampleOffset" ***/
/* Scream Tracker 3 command: O */

DEFINE_METHOD_HANDLE (sampleOffset);
DEFINE_EFFECT (sampleOffset, none, sampleOffset, none, deny, none);

/*** Note retrigger + Volume slide "retrig" ***/
/* Scream Tracker 3 command: Q */

DEFINE_METHOD_INIT (retrig);
DEFINE_METHOD_TICK (retrig);
DEFINE_EFFECT (retrig, retrig, none, retrig, allow, none);

// sub-effects

DEFINE_METHOD_INIT(retrig_none);
DEFINE_SUB_EFFECT(retrig_none, retrig_none, none, none, allow, none);

DEFINE_METHOD_INIT(retrig_slideDown);
DEFINE_METHOD_TICK(retrig_slideDown);
DEFINE_SUB_EFFECT(retrig_slideDown, retrig_slideDown, none, retrig_slideDown, allow, none);

DEFINE_METHOD_INIT(retrig_use2div3);
DEFINE_METHOD_TICK(retrig_use2div3);
DEFINE_SUB_EFFECT(retrig_use2div3, retrig_use2div3, none, retrig_use2div3, allow, none);

DEFINE_METHOD_INIT(retrig_use1div2);
DEFINE_METHOD_TICK(retrig_use1div2);
DEFINE_SUB_EFFECT(retrig_use1div2, retrig_use1div2, none, retrig_use1div2, allow, none);

DEFINE_METHOD_INIT(retrig_slideUp);
DEFINE_METHOD_TICK(retrig_slideUp);
DEFINE_SUB_EFFECT(retrig_slideUp, retrig_slideUp, none, retrig_slideUp, allow, none);

DEFINE_METHOD_INIT(retrig_use3div2);
DEFINE_METHOD_TICK(retrig_use3div2);
DEFINE_SUB_EFFECT(retrig_use3div2, retrig_use3div2, none, retrig_use3div2, allow, none);

DEFINE_METHOD_INIT(retrig_use2div1);
DEFINE_METHOD_TICK(retrig_use2div1);
DEFINE_SUB_EFFECT(retrig_use2div1, retrig_use2div1, none, retrig_use2div1, allow, none);

// sub-effects list

DEFINE_SUB_EFFECTS_LIST(retrig) =
{
    &SUB_EFFECT(retrig_none),
    &SUB_EFFECT(retrig_slideDown),
    &SUB_EFFECT(retrig_use2div3),
    &SUB_EFFECT(retrig_use1div2),
    &SUB_EFFECT(retrig_slideUp),
    &SUB_EFFECT(retrig_use3div2),
    &SUB_EFFECT(retrig_use2div1)
};

static const uint8_t eff_retrig_route[16] =
{
    EFFIDX_RETRIG_VOLSLIDE_NONE,
    EFFIDX_RETRIG_VOLSLIDE_DOWN,
    EFFIDX_RETRIG_VOLSLIDE_DOWN,
    EFFIDX_RETRIG_VOLSLIDE_DOWN,
    EFFIDX_RETRIG_VOLSLIDE_DOWN,
    EFFIDX_RETRIG_VOLSLIDE_DOWN,
    EFFIDX_RETRIG_VOLSLIDE_USE2DIV3,
    EFFIDX_RETRIG_VOLSLIDE_USE1DIV2,
    EFFIDX_RETRIG_VOLSLIDE_NONE,
    EFFIDX_RETRIG_VOLSLIDE_UP,
    EFFIDX_RETRIG_VOLSLIDE_UP,
    EFFIDX_RETRIG_VOLSLIDE_UP,
    EFFIDX_RETRIG_VOLSLIDE_UP,
    EFFIDX_RETRIG_VOLSLIDE_UP,
    EFFIDX_RETRIG_VOLSLIDE_USE3DIV2,
    EFFIDX_RETRIG_VOLSLIDE_USE2DIV1
};

/*** Tremolo "tremolo" ***/
/* Scream Tracker 3 command: R */

DEFINE_METHOD_INIT   (tremolo);
DEFINE_METHOD_HANDLE (tremolo);
DEFINE_METHOD_TICK   (tremolo);
DEFINE_EFFECT (tremolo, tremolo, tremolo, tremolo, allow, none);

/****** Special effects "special" ******/
/* Scream Tracker 3 command: S */

// sub-effects

/*** Fine tune "special_fineTune" ***/
/* Scream Tracker 3 command: S3 */

DEFINE_METHOD_HANDLE(special_fineTune);
DEFINE_SUB_EFFECT(special_fineTune, none, special_fineTune, none, deny, none);

/*** Set vibrato waveform "special_setVibWave" ***/

DEFINE_METHOD_INIT(special_setVibWave);
DEFINE_SUB_EFFECT(special_setVibWave, special_setVibWave, none, none, deny, none);

/*** Set tremolo waveform "special_setTremWave" ***/

DEFINE_METHOD_INIT(special_setTremWave);
DEFINE_SUB_EFFECT(special_setTremWave, special_setTremWave, none, none, deny, none);

/*** Set pan position "special_setPanPos" ***/

DEFINE_METHOD_INIT(special_setPanPos);
DEFINE_SUB_EFFECT(special_setPanPos, special_setPanPos, none, none, deny, none);

/*** Pattern loop "special_patLoop" ***/

DEFINE_METHOD_INIT(special_patLoop);
DEFINE_SUB_EFFECT(special_patLoop, special_patLoop, none, none, deny, none);

/*** Note cut "special_noteCut" ***/
/* Scream Tracker 3 command: SC */

DEFINE_METHOD_TICK(special_noteCut);
DEFINE_SUB_EFFECT(special_noteCut, none, none, special_noteCut, deny, none);

/*** Note delay "special_noteDelay" ***/
/* Scream Tracker 3 command: SD */

DEFINE_METHOD_INIT(special_noteDelay);
DEFINE_METHOD_TICK(special_noteDelay);
DEFINE_SUB_EFFECT(special_noteDelay, special_noteDelay, none, special_noteDelay, deny, none);

/*** Pattern delay "special_patDelay" ***/
/* Scream Tracker 3 command: SE */

DEFINE_METHOD_INIT  (special_patDelay);
DEFINE_METHOD_HANDLE(special_patDelay);
DEFINE_SUB_EFFECT(special_patDelay, special_patDelay, special_patDelay, none, deny, none);

// sub-effects list

DEFINE_SUB_EFFECTS_LIST(special) =
{
    &SUB_EFFECT(none),
    &SUB_EFFECT(special_fineTune),
    &SUB_EFFECT(special_setVibWave),
    &SUB_EFFECT(special_setTremWave),
    &SUB_EFFECT(special_setPanPos),
    &SUB_EFFECT(special_patLoop),
    &SUB_EFFECT(special_noteCut),
    &SUB_EFFECT(special_noteDelay),
    &SUB_EFFECT(special_patDelay)
};

static const uint8_t eff_special_route[16] =
{
    EFFIDX_SPECIAL_NONE,    /* 0 - n/a */
    EFFIDX_SPECIAL_NONE,    /* 1 - Set filter */
    EFFIDX_SPECIAL_NONE,    /* 2 - Set glissando */
    EFFIDX_SPECIAL_FINETUNE,
    EFFIDX_SPECIAL_VIBWAVE,
    EFFIDX_SPECIAL_TREMWAVE,
    EFFIDX_SPECIAL_NONE,    /* 6 - n/a */
    EFFIDX_SPECIAL_NONE,    /* 7 - n/a */
    EFFIDX_SPECIAL_PANPOS,
    EFFIDX_SPECIAL_NONE,    /* 9 - n/a */
    EFFIDX_SPECIAL_NONE,    /* A - Stereo control */
    EFFIDX_SPECIAL_PATLOOP,
    EFFIDX_SPECIAL_NOTECUT,
    EFFIDX_SPECIAL_NOTEDELAY,
    EFFIDX_SPECIAL_PATDELAY,
    EFFIDX_SPECIAL_NONE     /* SF - Function repeat */
};

// router

DEFINE_METHOD_INIT (special);
DEFINE_METHOD_TICK (special);
DEFINE_EFFECT (special, special, none, special, deny, none);

/*** Main effects table ***/

DEFINE_EFFECTS_LIST(main) =
{
    &EFFECT(none),
    &EFFECT(setSpeed),      // A
    &EFFECT(jumpToOrder),   // B
    &EFFECT(patBreak),      // C
    &EFFECT(volSlide),      // D
    &EFFECT(pitchDown),     // E
    &EFFECT(pitchUp),       // F
    &EFFECT(porta),         // G
    &EFFECT(vibNorm),       // H
    &EFFECT(tremor),        // I
    &EFFECT(arpeggio),      // J
    &EFFECT(vibNorm_vol),   // K: (H) + (D)
    &EFFECT(porta_vol),     // L: (G) + (D)
    &EFFECT(none_na),       // M
    &EFFECT(none_na),       // N
    &EFFECT(sampleOffset),  // O
    &EFFECT(none_na),       // P
    &EFFECT(retrig),        // Q
    &EFFECT(tremolo),       // R
    &EFFECT(special),       // S
    &EFFECT(setTempo),      // T
    &EFFECT(vibFine),       // U
    &EFFECT(setGVol),       // V
    // W
    // X
    // Y
    // Z
};

/*** Effects ***/

/*** Continue checks ***/

METHOD_CONT(allow)
{
    return true;
}

METHOD_CONT(deny)
{
    return false;
}

/*** No effect ***/

METHOD_INIT(none)
{
    if (param)
        chn->bParameter = param;
    else
        param = chn->bParameter;
    return true;
}

METHOD_HANDLE(none)
{
    return;
}

METHOD_TICK(none)
{
    return;
}

METHOD_STOP(none)
{
    return;
}

//============================================================================
// setSpeed
//============================================================================

METHOD_INIT(setSpeed)
{
    if (param)
        chn->bParameter = param;
    playstate_set_speed (ps, param);
    return true;
}

//============================================================================
// setTempo
//============================================================================

METHOD_INIT(setTempo)
{
    if (param)
        chn->bParameter = param;
    playstate_set_tempo (ps, param);
    return true;
}

//============================================================================
// jumpToOrder
//============================================================================

METHOD_INIT(jumpToOrder)
{
    if (param)
        chn->bParameter = param;
    rs->flags |= ROWSTATEFL_JUMP_TO_ORDER;
    rs->jump_pos = param;
    return true;
}

//============================================================================
// patBreak
//============================================================================

METHOD_INIT(patBreak)
{
    chn->bParameter = param;
    rs->flags |= ROWSTATEFL_PATTERN_BREAK;
    rs->break_pos = param;  // TODO: check row is available
    return true;
}

//============================================================================
// setGVol
//============================================================================

METHOD_INIT(setGVol)
{
    chn->bParameter = param;
    rs->flags |= ROWSTATEFL_GLOBAL_VOLUME;
    rs->global_volume = param > MUSMOD_GLOBAL_VOLUME_MAX ? MUSMOD_GLOBAL_VOLUME_MAX : param;
    return true;
}

//============================================================================
// volSlide
//============================================================================

METHOD_INIT(volSlide)
{
    if (param)
        chn->bParameter = param;
    else
        param = chn->bParameter;
    if ((param & 0xf0) == 0xf0)
    {
        if (param == 0xf0)
            // x0
            chn->bCommand2 = EFFIDX_VOLSLIDE_UP;
        else
            // Fx
            chn->bCommand2 = EFFIDX_VOLSLIDE_FINE_DOWN;
    }
    else
    {
        if ((param & 0x0f) == 0x0f)
        {
            if (param == 0x0f)
                // 0x
                chn->bCommand2 = EFFIDX_VOLSLIDE_DOWN;
            else
                // xF
                chn->bCommand2 = EFFIDX_VOLSLIDE_FINE_UP;
        }
        else
        {
            if (param & 0x0f)
                // 0x
                chn->bCommand2 = EFFIDX_VOLSLIDE_DOWN;
            else
                // x0
                chn->bCommand2 = EFFIDX_VOLSLIDE_UP;
        }
    }
    return true;
}

METHOD_HANDLE(volSlide)
{
    if (chn->bCommand2 <= EFFIDX_VOLSLIDE_MAX)
        SUB_EFFECTS_LIST(volSlide)[chn->bCommand2]->handle (ps, chn, cs);
}

METHOD_TICK(volSlide)
{
    if (chn->bCommand2 <= EFFIDX_VOLSLIDE_MAX)
        SUB_EFFECTS_LIST(volSlide)[chn->bCommand2]->tick (ps, chn);
}

METHOD_TICK(volSlide_down)
{
    chn->eff_volume = chn->note_volume = range_0_64 (chn->note_volume - (chn->bParameter & 0x0f));
}

METHOD_TICK(volSlide_up)
{
    chn->eff_volume = chn->note_volume = range_0_64 (chn->note_volume + (chn->bParameter >> 4));
}

METHOD_HANDLE(volSlide_fineDown)
{
    chn->eff_volume = chn->note_volume = range_0_64 (chn->note_volume - (chn->bParameter & 0x0f));
}

METHOD_HANDLE(volSlide_fineUp)
{
    chn->eff_volume = chn->note_volume = range_0_64 (chn->note_volume + (chn->bParameter >> 4));
}

//============================================================================
// pitchDown
//============================================================================

METHOD_INIT(pitchDown)
{
    if (param)
        chn->bParameter = param;
    else
        param = chn->bParameter;
    if (param < 0xe0)
        // xx
        chn->bCommand2 = EFFIDX_PITCHDOWN_NORMAL;
    else
        if (param < 0xf0)
            // Ex
            chn->bCommand2 = EFFIDX_PITCHDOWN_EXTRA;
        else
            // Fx
            chn->bCommand2 = EFFIDX_PITCHDOWN_FINE;
    return true;
}

METHOD_HANDLE(pitchDown)
{
    if (chn->bCommand2 <= EFFIDX_PITCHDOWN_MAX)
        SUB_EFFECTS_LIST(pitchDown)[chn->bCommand2]->handle (ps, chn, cs);
}

METHOD_TICK(pitchDown)
{
    if (chn->bCommand2 <= EFFIDX_PITCHDOWN_MAX)
        SUB_EFFECTS_LIST(pitchDown)[chn->bCommand2]->tick (ps, chn);
}

METHOD_TICK(pitchDown_normal)
{
    mixchn_setup_sample_period (chn,
        chn->wSmpPeriod + (chn->bParameter << 2),
        ps->rate);
}

METHOD_HANDLE(pitchDown_fine)
{
    mixchn_setup_sample_period (chn,
        chn->wSmpPeriod + ((chn->bParameter & 0x0f) << 2),
        ps->rate);
}

METHOD_HANDLE(pitchDown_extra)
{
    mixchn_setup_sample_period (chn,
        chn->wSmpPeriod + (chn->bParameter & 0x0f),
        ps->rate);
}

//============================================================================
// pitchUp
//============================================================================

METHOD_INIT(pitchUp)
{
    if (param)
        chn->bParameter = param;
    else
        param = chn->bParameter;
    if (param < 0xe0)
        // xx
        chn->bCommand2 = EFFIDX_PITCHUP_NORMAL;
    else
        if (param < 0xf0)
            // Ex
            chn->bCommand2 = EFFIDX_PITCHUP_EXTRA;
        else
            // Fx
            chn->bCommand2 = EFFIDX_PITCHUP_FINE;
    return true;
}

METHOD_HANDLE(pitchUp)
{
    if (chn->bCommand2 <= EFFIDX_PITCHUP_MAX)
        SUB_EFFECTS_LIST(pitchUp)[chn->bCommand2]->handle (ps, chn, cs);
}

METHOD_TICK(pitchUp)
{
    if (chn->bCommand2 <= EFFIDX_PITCHUP_MAX)
        SUB_EFFECTS_LIST(pitchUp)[chn->bCommand2]->tick (ps, chn);
}

METHOD_TICK(pitchUp_normal)
{
    mixchn_setup_sample_period (chn,
        chn->wSmpPeriod - (chn->bParameter << 2),
        ps->rate);
}

METHOD_HANDLE(pitchUp_fine)
{
    mixchn_setup_sample_period (chn,
        chn->wSmpPeriod - ((chn->bParameter & 0x0f) << 2),
        ps->rate);
}

METHOD_HANDLE(pitchUp_extra)
{
    mixchn_setup_sample_period (chn,
        chn->wSmpPeriod - (chn->bParameter & 0x0f),
        ps->rate);
}

//============================================================================
// porta
//============================================================================

static void __near eff_porta_start (MIXCHN *chn, CHNSTATE *cs)
{
    cs->flags |= CHNSTATEFL_PORTAMENTO;
    if (((cs->cur_note) != CHN_NOTE_OFF)
    &&  ((cs->cur_note) != CHN_NOTE_NONE))
    {
        /* now save some values (we want to slide from) */
        cs->porta_sample_step_old   = chn->dSmpStep;
        cs->porta_sample_period_old = chn->wSmpPeriod;
    }
}

static void __near eff_porta_stop (MIXCHN *chn, CHNSTATE *cs)
{
    cs->flags &= ~CHNSTATEFL_PORTAMENTO;
    chn->bCommand = EFFIDX_NONE;
}

METHOD_INIT(porta)
{
    if (param)
    {
        chn->bParameter = param;
        chn->bPortParam = param;
    }

    if (chn->flags & MIXCHNFL_PLAYING)
        eff_porta_start (chn, cs);
    else
        eff_porta_stop (chn, cs);

    return true;
}

METHOD_HANDLE(porta)
{
    if (((cs->cur_note) != CHN_NOTE_OFF)
    &&  ((cs->cur_note) != CHN_NOTE_NONE))
    {
        chn->wSmpPeriodDest = chn->wSmpPeriod;
        chn->wSmpPeriod = cs->porta_sample_period_old;
        chn->dSmpStep = cs->porta_sample_step_old;
    }
}

METHOD_TICK(porta)
{
    int32_t period = chn->wSmpPeriod;
    unsigned int slide = chn->bPortParam << 2;   // <- use amiga slide = para*4
    if (period > chn->wSmpPeriodDest)
    {
        period -= slide;
        if (period < chn->wSmpPeriodDest)
            period = chn->wSmpPeriodDest;
    }
    else
    {
        period += slide;
        if (period > chn->wSmpPeriodDest)
            period = chn->wSmpPeriodDest;
    }
    mixchn_setup_sample_period (chn, period, ps->rate);
}

//============================================================================
// porta_vol
//============================================================================

METHOD_INIT(porta_vol)
{
    bool state;
    state = EFFECT(porta).init (ps, rs, chn, cs, 0);
    if (cs->flags & CHNSTATEFL_PORTAMENTO)
        state |= EFFECT(volSlide).init (ps, rs, chn, cs, param);
    return state;
}

METHOD_HANDLE(porta_vol)
{
    EFFECT(volSlide).handle (ps, chn, cs);
    EFFECT(porta).handle (ps, chn, cs);
}

METHOD_TICK(porta_vol)
{
    EFFECT(volSlide).tick (ps, chn);
    EFFECT(porta).tick (ps, chn);
}

//============================================================================
// vibNorm
//============================================================================

static void near eff_vibrato (PLAYSTATE *ps, MIXCHN *chn)
{
    int d;

    switch (chn->bVibType)
    {
    case 0:
        d = sinuswave[chn->bVibPos & 63] >> 2;
        break;
    case 1:
        d = 32 - (chn->bVibPos & 63);
        break;
    case 2:
        d = 32 - (chn->bVibPos & 32);
        break;
    case 3:
        d = 32 - (rand () & 63);
    default:
        break;
    }

    chn->bVibPos -= chn->bVibSpeed;

    mixchn_setup_sample_period (chn,
        chn->wSmpPeriodOld - ((d * chn->bVibDepth) >> 4),
        ps->rate);
}

METHOD_INIT(vibNorm)
{
    if (param)
        chn->bParameter = param;

    if (!(chn->bEffFlags & EFFFLAG_CONTINUE))
        chn->bVibPos = 0;

    if (param & 0x0f)
        chn->bVibDepth = (param & 0x0f) << 2;
    else
        param |= chn->bVibParam & 0x0f;

    if (param >> 4)
        chn->bVibSpeed = param >> 4;
    else
        param |= chn->bVibParam & 0xf0;

    chn->bVibParam = param;
    return true;
}

METHOD_HANDLE(vibNorm)
{
    if (!(chn->bEffFlags & EFFFLAG_CONTINUE))
        chn->wSmpPeriodOld = chn->wSmpPeriod;
}

METHOD_TICK(vibNorm)
{
    if (chn->flags & MIXCHNFL_PLAYING)
        eff_vibrato (ps, chn);
}

METHOD_CONT(vibNorm)
{
    return (((cs->cur_note) == CHN_NOTE_OFF)
    ||      ((cs->cur_note) == CHN_NOTE_NONE));
}

METHOD_STOP(vibNorm)
{
    unsigned int period;
    period = chn->wSmpPeriodOld;
    chn->wSmpPeriod = period;
    if (period)
        chn->dSmpStep = _calc_sample_step (period, ps->rate);
}

//============================================================================
// vibFine
//============================================================================

METHOD_INIT(vibFine)
{
    if (param)
        chn->bParameter = param;

    if (!(chn->bEffFlags & EFFFLAG_CONTINUE))
        chn->bVibPos = 0;

    if (param & 0x0f)
        chn->bVibDepth = param & 0x0f;
    else
        param |= chn->bVibParam & 0x0f;

    if (param >> 4)
        chn->bVibSpeed = param >> 4;
    else
        param |= chn->bVibParam & 0xf0;

    chn->bVibParam = param;
    return true;
}

METHOD_TICK(vibFine)
{
    if (chn->flags & MIXCHNFL_PLAYING)
        eff_vibrato (ps, chn);
}

//============================================================================
// vibNorm_vol
//============================================================================

METHOD_INIT(vibNorm_vol)
{
    EFFECT(volSlide).init (ps, rs, chn, cs, param);
    EFFECT(vibNorm).init (ps, rs, chn, cs, 0);
    return true;
}

METHOD_HANDLE(vibNorm_vol)
{
    EFFECT(volSlide).handle (ps, chn, cs);
    EFFECT(vibNorm).handle (ps, chn, cs);
}

METHOD_TICK(vibNorm_vol)
{
    EFFECT(volSlide).tick (ps, chn);
    EFFECT(vibNorm).tick (ps, chn);
}

//============================================================================
// tremor
//============================================================================

static void near eff_tremor (PLAYSTATE *ps, MIXCHN *chn)
{
    if (chn->bTremorOnCounter)
        chn->bTremorOnCounter--;

    if (!chn->bTremorOnCounter)
    {
        if (chn->bTremorOffCounter)
        {
            chn->bTremorOffCounter--;
            chn->eff_volume = 0;
        }
        else
        {
            chn->bTremorOnCounter = chn->bTremorOn;
            chn->bTremorOffCounter = chn->bTremorOff;
        }
    }
}

METHOD_INIT(tremor)
{
    if (param)
        chn->bParameter = param;
    else
        param = chn->bParameter;

    if (param & 0x0f)
        chn->bTremorOff = param & 0x0f;
    else
        chn->bTremorOff = 1;

    if (param >> 4)
        chn->bTremorOn = param >> 4;
    else
        chn->bTremorOn = 1;

    if (!(chn->bEffFlags & EFFFLAG_CONTINUE))
    {
        chn->bTremorOnCounter = chn->bTremorOn;
        chn->bTremorOffCounter = chn->bTremorOff;
    }

    return true;
}

METHOD_HANDLE(tremor)
{
    eff_tremor (ps, chn);
}

METHOD_TICK(tremor)
{
    eff_tremor (ps, chn);
}

//============================================================================
// arpeggio
//============================================================================

METHOD_INIT(arpeggio)
{
    if (param)
    {
        cs->flags |= CHNSTATEFL_ARPEGGIO;
        chn->bParameter = param;
    }
    else
        cs->flags &= ~CHNSTATEFL_ARPEGGIO;
    return true;
}

METHOD_HANDLE(arpeggio)
{
    uint8_t param, note;
    PCMSMP *smp;

    if (!(cs->flags & CHNSTATEFL_ARPEGGIO))
    {
        if (chn->bEffFlags & EFFFLAG_CONTINUE)
            return;
        /* start arpeggio: */
        chn->bArpPos = 0;
    }

    param = chn->bParameter;

    note = _unpack_note (chn->note) + (param >> 4);
    chn->bArpNotes[0] = _pack_note (note > NOTE_MAX ? NOTE_MAX : note);

    note = _unpack_note (chn->note) + (param & 0x0f);
    chn->bArpNotes[1] = _pack_note (note > NOTE_MAX ? NOTE_MAX : note);

    smp = chn->sample;
    if (smp && (smp->flags & PCMSMPFL_AVAIL))
    {
        uint32_t rate = smp->rate;
        if (rate)
        {
            uint16_t mixrate = ps->rate;
            chn->dArpSmpSteps[0] = chn_calcNoteStep(chn, rate, chn->note, mixrate);
            chn->dArpSmpSteps[1] = chn_calcNoteStep(chn, rate, chn->bArpNotes[0], mixrate);
            chn->dArpSmpSteps[2] = chn_calcNoteStep(chn, rate, chn->bArpNotes[1], mixrate);
        }
    }
}

METHOD_TICK(arpeggio)
{
    unsigned int pos = chn->bArpPos + 1;
    if (pos >= 3)
        pos = 0;
    chn->bArpPos = pos;
    chn->dSmpStep = chn->dArpSmpSteps[pos];
}

METHOD_STOP(arpeggio)
{
    chn->dSmpStep = chn->dArpSmpSteps[0];
}

//============================================================================
// sampleOffset
//============================================================================

METHOD_HANDLE(sampleOffset)
{
    uint16_t param;
    param = chn->bParameter;
    chn->dSmpStart = param << 8;
    if (((cs->cur_note) != CHN_NOTE_OFF)
    &&  ((cs->cur_note) != CHN_NOTE_NONE))
    {
        chn->rSmpPos.frac = 0;
        chn->rSmpPos.pos = chn->dSmpStart;
    }
}

//============================================================================
// retrig
//============================================================================

METHOD_INIT(retrig)
{
    if (!param)
        param = chn->bParameter;
    else
    {
        uint8_t ticks;

        chn->bParameter = param;
        ticks = param & 0x0f;
        if (ticks)
            chn->bRetrigTicks = ticks - 1;
        else
        {
            chn->bCommand = EFFIDX_NONE;
            return true;
        }
    }
    return SUB_EFFECTS_LIST(retrig)[eff_retrig_route[param >> 4]]->init (ps, rs, chn, cs, param);
}

METHOD_TICK(retrig)
{
    unsigned ticks;

    if (chn->bRetrigTicks)
    {
        if (! --chn->bRetrigTicks)
            return;
    }
    chn->rSmpPos.frac = 0;
    chn->rSmpPos.pos = 0;
    ticks = chn->bParameter & 0x0f;
    if (ticks)
    {
        chn->bRetrigTicks = ticks;
        if (chn->bCommand2 <= 6)
            SUB_EFFECTS_LIST(retrig)[chn->bCommand2]->tick (ps, chn);
    }
}

METHOD_INIT(retrig_none)
{
    chn->bCommand2 = EFFIDX_RETRIG_VOLSLIDE_NONE;
    return true;
}

METHOD_INIT(retrig_slideDown)
{
    chn->bCommand2 = EFFIDX_RETRIG_VOLSLIDE_DOWN;
    return true;
}

METHOD_INIT(retrig_use2div3)
{
    chn->bCommand2 = EFFIDX_RETRIG_VOLSLIDE_USE2DIV3;
    return true;
}

METHOD_INIT(retrig_use1div2)
{
    chn->bCommand2 = EFFIDX_RETRIG_VOLSLIDE_USE1DIV2;
    return true;
}

METHOD_INIT(retrig_slideUp)
{
    chn->bCommand2 = EFFIDX_RETRIG_VOLSLIDE_UP;
    return true;
}

METHOD_INIT(retrig_use3div2)
{
    chn->bCommand2 = EFFIDX_RETRIG_VOLSLIDE_USE3DIV2;
    return true;
}

METHOD_INIT(retrig_use2div1)
{
    chn->bCommand2 = EFFIDX_RETRIG_VOLSLIDE_USE2DIV1;
    return true;
}

METHOD_TICK(retrig_slideDown)
{
    chn->eff_volume = chn->note_volume = range_0_64 (chn->note_volume - ((int) 1 << (chn->bParameter >> 4)));
}

METHOD_TICK(retrig_use2div3)
{
    chn->eff_volume = chn->note_volume = ((int) chn->note_volume * 2) / 3;
}

METHOD_TICK(retrig_use1div2)
{
    chn->eff_volume = chn->note_volume = chn->note_volume >> 1;
}

METHOD_TICK(retrig_slideUp)
{
    chn->eff_volume = chn->note_volume = range_0_64 (chn->note_volume + ((int) 1 << (chn->bParameter >> 4)));
}

METHOD_TICK(retrig_use3div2)
{
    chn->eff_volume = chn->note_volume = range_0_64 (((int) chn->note_volume * 3) >> 1);
}

METHOD_TICK(retrig_use2div1)
{
    chn->eff_volume = chn->note_volume = range_0_64 ((int) chn->note_volume << 1);
}

//============================================================================
// tremolo
//============================================================================

static void near eff_tremolo (PLAYSTATE *ps, MIXCHN *chn)
{
    int d;

    switch (chn->bTrmType)
    {
    case 0:
        d = sinuswave[chn->bTrmPos & 63] >> 2;
        break;
    case 1:
        d = 32 - (chn->bTrmPos & 63);
        break;
    case 2:
        d = 32 - (chn->bTrmPos & 32);
        break;
    case 3:
        d = 32 - (rand () & 63);
    default:
        break;
    }

    chn->bTrmPos += chn->bTrmSpeed;

    chn->eff_volume = range_0_64 (chn->eff_volume + ((d * chn->bTrmDepth) >> 4));
}

METHOD_INIT(tremolo)
{
    if (param)
        chn->bParameter = param;

    if (!(chn->bEffFlags & EFFFLAG_CONTINUE))
        chn->bTrmPos = 0;

    if (param & 0x0f)
        chn->bTrmDepth = param & 0x0f;

    if (param >> 4)
        chn->bTrmSpeed = param >> 4;

    return true;
}

METHOD_HANDLE(tremolo)
{
/*
    if ((cs->cur_instrument != CHN_INS_NONE)
    ||  (cs->cur_note_volume != CHN_NOTEVOL_NONE)
    ||  (!(chn->bEffFlags & EFFFLAG_CONTINUE)))
        chn->bSmpVolOld = chn->eff_volume;
*/
    eff_tremolo (ps, chn);
}

METHOD_TICK(tremolo)
{
    eff_tremolo (ps, chn);
}

//============================================================================
// special
//============================================================================

METHOD_HANDLE(special_fineTune) // TODO: special_fineTune
{
    return;
}

METHOD_INIT(special_setVibWave)
{
    chn->bVibType = param & 3;
    return true;
}

METHOD_INIT(special_setTremWave)
{
    chn->bTrmType = param & 3;
    return true;
}

METHOD_INIT(special_setPanPos)
{
    chn->pan = ((uint16_t) param * MIXCHNPAN_MAX) / 15;
    return true;
}

METHOD_INIT(special_patLoop)
{
    if (!param)
        ps->patloop_start_row = ps->row;
    else
    {
        if (!(ps->flags & PLAYSTATEFL_PATLOOP))
        {
            ps->flags |= PLAYSTATEFL_PATLOOP;
            param++;
            ps->patloop_count = param;
        }
        rs->flags |= ROWSTATEFL_PATTERN_LOOP;
    }

    return true;
}

METHOD_TICK(special_noteCut)
{
    if (! --chn->bDelayTicks)
        chn->flags &= ~MIXCHNFL_PLAYING;
}

METHOD_INIT(special_noteDelay)
{
    chn->bDelayTicks = param;
    if (!(rs->flags & ROWSTATEFL_PATTERN_DELAY))
    {
        /* new note, instrument, volume for later use */
        chn->bSavIns = cs->cur_instrument;
        chn->bSavNote = cs->cur_note;
        chn->bSavVol = cs->cur_note_volume;
    }
    return false;   /* setup note, instrument, volume later */
}

METHOD_TICK(special_noteDelay)
{
    if (! --chn->bDelayTicks)
    {
        unsigned insNum = chn->bSavIns;
        unsigned note;

        if (insNum)
            chn_setupInstrument(chn, ps->track, insNum);
        note = chn->bSavNote;
        if (note != CHN_NOTE_NONE)
        {
            if (note == CHN_NOTE_OFF)
                chn->flags &= ~MIXCHNFL_PLAYING;
            else
            {
                chn_setupNote (chn, note, ps->rate, 0);
                chn->flags |= MIXCHNFL_PLAYING;
            }
        }
        if (chn->bSavVol != CHN_NOTEVOL_NONE)
            chn->eff_volume = chn->note_volume = chn->bSavVol;
        chn->bCommand = EFFIDX_NONE;
    }
}

METHOD_INIT(special_patDelay)
{
    if (!(rs->flags & ROWSTATEFL_PATTERN_DELAY))
    {
        ps->patdelay_count = param + 1;
        cs->patdelay_saved_parameter = chn->bParameter;
    }
    return true;
}

METHOD_HANDLE(special_patDelay)
{
    chn->bCommand = cs->patdelay_saved_command;
    chn->bCommand2 = 0;
    chn->bParameter = cs->patdelay_saved_parameter;
}

METHOD_INIT(special)
{
    if (param)
        chn->bParameter = param;
    else
        param = chn->bParameter;
    chn->bCommand2 = eff_special_route[param >> 4];
    return SUB_EFFECTS_LIST(special)[chn->bCommand2]->init (ps, rs, chn, cs, param & 0x0f);
}

METHOD_TICK(special)
{
    if (chn->bCommand2 <= EFFIDX_SPECIAL_MAX)
        SUB_EFFECTS_LIST(special)[chn->bCommand2]->tick (ps, chn);
}

//============================================================================

bool chn_effInit (PLAYSTATE *ps, ROWSTATE *rs, MIXCHN *chn, CHNSTATE *cs, uint8_t param)
{
    if (chn->bCommand <= MAXEFF)
        return EFFECTS_LIST(main)[chn->bCommand]->init (ps, rs, chn, cs, param);
    else
        return false;
}

void chn_effHandle (PLAYSTATE *ps, MIXCHN *chn, CHNSTATE *cs)
{
    chn->eff_volume = chn->note_volume;
    if (chn->bCommand <= MAXEFF)
        EFFECTS_LIST(main)[chn->bCommand]->handle (ps, chn, cs);
}

void chn_effTick (PLAYSTATE *ps, MIXCHN *chn)
{
    chn->eff_volume = chn->note_volume;
    if (chn->bCommand <= MAXEFF)
        EFFECTS_LIST(main)[chn->bCommand]->tick (ps, chn);
}

bool chn_effCanContinue (MIXCHN *chn, CHNSTATE *cs)
{
    if (chn->bCommand <= MAXEFF)
        return EFFECTS_LIST(main)[chn->bCommand]->cont (chn, cs);
    else
        return false;
}

void chn_effStop (PLAYSTATE *ps, MIXCHN *chn)
{
    if (chn->bCommand <= MAXEFF)
        EFFECTS_LIST(main)[chn->bCommand]->stop (ps, chn);
}
