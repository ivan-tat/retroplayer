/* cvtsi8.inc - "convert_sign_8" routine.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

void convert_sign_8 (void __far *data, uint32_t size)
{
    uint32_t offset = FARPTR_TO_LONG (data);

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("Requested: size=0x%lX, address=0x%lX, pointer=0x%04hX:0x%04hX",
            (uint32_t) size,
            (uint32_t) offset,
            (uint16_t) FP_SEG (data),
            (uint16_t) FP_OFF (data)
        );

    /* 'size' is counter */

    while (size)
    {
        uint8_t __far *p = FARPTR_FROM_LONG (offset);
        uint16_t count;
        bool aligned;

        /* counter register in DOS segment limit */
        if ((uint32_t) FP_OFF (p) + size > (1UL<<16))
            count = (1UL<<16) - FP_OFF (p);
        else
            count = size;

        /* trick: instead of 0 count use 1<<16 */

        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("Converting: count=0x%lX, address=0x%lX, pointer=0x%04hX:0x%04hX",
                (uint32_t) count ? count : (1UL<<16),
                (uint32_t) offset,
                (uint16_t) FP_SEG (p),
                (uint16_t) FP_OFF (p)
            );

        offset += count ? count : (1UL<<16);
        size -= count ? count : (1UL<<16);

        /* 2 bytes alignment */
        if (FP_OFF (p) & 1)
        {
            *p ^= 0x80;
            p++;
            count--;
            aligned = true;
        }
        else
            aligned = false;

        if (((!count) && !aligned) || (count & ~1))
            do
            {
                *((uint16_t __far *)p) ^= 0x8080;
                p += 2;
                count -= 2;
            } while (count & ~1);

        if (count & 1)
            *p ^= 0x80;
    }
}
