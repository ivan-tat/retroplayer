(* musmod.pas -- Pascal declarations for musmod.c.

   This file is for linking compiled object files with Pascal linker.
   It will be deleted in future when we rewrite the project in C.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. *)

unit musmod;

interface

(*$I defines.pas*)

procedure musmod_init;
procedure musmod_free;

implementation

uses
    common,
    string_,
    pcmsmp,
    musins,
    muspat;

(*$l musmod.obj*)

procedure musmod_init; external;
procedure musmod_free; external;

end.
