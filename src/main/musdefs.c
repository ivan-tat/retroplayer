/* musdefs.c -- common routines for music library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include "defines.h"
#include "main/musdefs.h"

int range_0_64 (int value)
{
    return (value < 0) ? 0 : (value > 64) ? 64: value;
}
