/* pcmsmp.h -- declarations for pcmsmp.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _PCMSMP_H_INCLUDED
#define _PCMSMP_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/dos.h"
#include "dos/ems.h"
#include "dynarray.h"
#include "main/musdefs.h"

/*** PCM sample ***/

#define PCMSMP_VOLUME_MAX 64
#define PCMSMP_VOLUME_BITS 6

/* Loop type (2 bits) */

typedef uint8_t pcm_sample_loop_t;
typedef pcm_sample_loop_t PCMSMPLOOP;

#define PCMSMPLOOP_NONE     0
#define PCMSMPLOOP_FORWARD  1
#define PCMSMPLOOP_BACKWARD 2
#define PCMSMPLOOP_PINGPONG 3
#define PCMSMPLOOP_MASK     3

/* Flags */

typedef uint8_t pcm_sample_flags_t;
typedef pcm_sample_flags_t PCMSMPFLAGS;

#define PCMSMPFL_AVAIL    (1 << 0)  /* data is allocated */
#define PCMSMPFL_EM       (1 << 1)  /* data is in EM */
#define PCMSMPFL_OWNHDL   (1 << 2)  /* has own EM handle, needs to be freed when done */
#define PCMSMPFL_16BITS   (1 << 3)  /* sample format is 16 bits if set */
#define PCMSMPFL_LOOPSHIFT 4
#define PCMSMPFL_LOOP_NONE     (PCMSMPLOOP_NONE     << PCMSMPFL_LOOPSHIFT)
#define PCMSMPFL_LOOP_FORWARD  (PCMSMPLOOP_FORWARD  << PCMSMPFL_LOOPSHIFT)
#define PCMSMPFL_LOOP_BACKWARD (PCMSMPLOOP_BACKWARD << PCMSMPFL_LOOPSHIFT)
#define PCMSMPFL_LOOP_PINGPONG (PCMSMPLOOP_PINGPONG << PCMSMPFL_LOOPSHIFT)
#define PCMSMPFL_LOOP_MASK     (PCMSMPLOOP_MASK     << PCMSMPFL_LOOPSHIFT)

/* Structure */

#define PCMSMP_TITLE_LEN 16

#pragma pack(push, 1);
typedef struct pcm_sample_t
{
    PCMSMPFLAGS flags;
    uint8_t  volume;
    uint32_t size;
    uint32_t mem_size;
    union
    {
        struct
        {
            void __far *ptr;
        } dos;
        struct
        {
            uint32_t offset;
            EMSHDL   handle;
        } em;
    } data;
    uint16_t rate;
    uint32_t length;
    uint32_t loop_start;
    uint32_t loop_end;
    char     title[PCMSMP_TITLE_LEN];
};  /* 46 bytes */
#pragma pack(pop);

typedef struct pcm_sample_t PCMSMP;

typedef struct pcm_sample_expansion_t
{
    uint32_t orig_start;    /* original loop start */
    uint16_t orig_len;      /* original loop length */
    uint32_t start;         /* new loop start */
    uint16_t len;           /* new loop length */
    uint16_t count;         /* if no loop: count = 0 */
        /* for forward and backward loops: count = len / orig_len */
        /* for ping-pong loop: count = len / (orig_len * 2) */
} PCMSMPEXP;

/* Methods */

#define PCMSMP_EXTRA_DATA_SIZE 0

void pcmsmp_init (PCMSMP *self);
bool pcmsmp_needs_expansion (PCMSMP *self, uint16_t min_length);
void pcmsmp_calc_expansion (PCMSMP *self, PCMSMPEXP *se, uint16_t min_length);
bool pcmsmp_expand (PCMSMP *self, uint16_t min_length);
void pcmsmp_free (PCMSMP *self);

/*** PCM samples list ***/

/* Flags */

typedef uint16_t pcm_samples_list_flags_t;
typedef pcm_samples_list_flags_t PCMSMPLFLAGS;

#define PCMSMPLFL_EM     (1<<0) /* data is in EM */
#define PCMSMPLFL_OWNHDL (1<<1) /* has own EM handle, needs to be freed when done */

/* Structure */

#pragma pack(push, 1);
typedef struct pcm_samples_list_t
{
    PCMSMPLFLAGS flags;
    DYNARR list;
    EMSHDL handle;
};
#pragma pack(pop);

typedef struct pcm_samples_list_t PCMSMPLIST;

/* Methods */

void    pcmsmpl_init (PCMSMPLIST *self);
#define pcmsmpl_set_count(o, v) dynarr_set_size (&(o)->list, v)
#define pcmsmpl_get_count(o)    dynarr_get_size (&(o)->list)
#define pcmsmpl_set(o, i, v)    dynarr_set_item (&(o)->list, i, v)
#define pcmsmpl_get(o, i)       dynarr_get_item (&(o)->list, i)
#define pcmsmpl_indexof(o, v)   dynarr_indexof (&(o)->list, v)
void    pcmsmpl_free (PCMSMPLIST *self);

/*** Debug ***/

#if DEBUG == 1

void DEBUG_dump_sample_info (PCMSMP *smp, int index);

#else   /* DEBUG != 1 */

#define DEBUG_dump_sample_info(smp, index)

#endif  /* DEBUG != 1 */

#endif  /* !_PCMSMP_H_INCLUDED */
