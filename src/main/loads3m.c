/* loads3m.c -- library for loading Scream Tracker 3.x music modules.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/stdio.h"
#include "cc/string.h"
#include "debug.h"
#include "cc/dos.h"
#include "dos/ems.h"
#include "common.h"
#include "main/musdefs.h"
#include "main/pcmsmp.h"
#include "main/musins.h"
#include "main/muspat.h"
#include "main/musmod.h"
#include "main/effects.h"
#include "main/musplay.h"
#include "main/cvtsign.h"
#include "main/loads3m.h"

/* Limits */
#define _S3M_MAX_INSTRUMENTS 99
#define _S3M_MAX_PATTERNS 100
#define _S3M_MAX_CHANNELS 32
#define _S3M_PATTERN_ROWS 64

#define _calc_max_file_pattern_size(rows, channels)     ((rows) * ((channels) * 5 + 1))
#define _calc_max_raw_pattern_size(rows, channels)      ((rows) * (channels) * sizeof (MUSPATCHNEVDATA))
#define _calc_max_packed_pattern_size(rows, channels)   ((rows) * ((channels) * sizeof (MUSPATCHNEVENT) + sizeof (uint16_t)))

#define _S3M_IN_PATTERN_SIZE_MAX    _calc_max_file_pattern_size (_S3M_PATTERN_ROWS, _S3M_MAX_CHANNELS)
#define _S3M_OUT_PATTERN_SIZE_MAX   _calc_max_packed_pattern_size (_S3M_PATTERN_ROWS, _S3M_MAX_CHANNELS)

/* Macros */
#define mk_u16(a, b)        ((((a) & 0xffU) << 0) | (((b) & 0xffU) << 8))
#define mk_u32(a, b, c, d)  ((((a) & 0xffUL) << 0) | (((b) & 0xffUL) << 8) | (((c) & 0xffUL) << 16) | (((d) & 0xffUL) << 24))

/*** File header ***/

#define _S3M_TITLE_LEN 28

/* Format */
#define _S3M_FILE_TYPE      mk_u16 (0x1a, 16)
#define _S3M_FILE_FORMAT_1  1
#define _S3M_FILE_FORMAT_2  2
#define _S3M_FILE_MAGIC     mk_u32 ('S', 'C', 'R', 'M')

/* Header flags */
#define _S3M_FLAG_ST2_VIBRATO   0x01    // not supported
#define _S3M_FLAG_ST2_TEMPO     0x02    // not supported
#define _S3M_FLAG_AMIGA_SLIDES  0x04    // not supported
#define _S3M_FLAG_VOL_0_OPTI    0x08    // not supported
#define _S3M_FLAG_AMIGA_LIMITS  0x10
#define _S3M_FLAG_SB_FILTER     0x20    // not supported
#define _S3M_FLAG_CUSTOM        0x80    // not supported

/* Master volume bits */
#define _S3M_MVOL_MASK      0x7f
#define _S3M_MVOL_SHIFT     0
#define _S3M_MVOL_STEREO    0x80

/* Tracker version bits */
#define _S3M_TRACKER_VER_MINOR_MASK     0x00ff
#define _S3M_TRACKER_VER_MINOR_SHIFT    0
#define _S3M_TRACKER_VER_MAJOR_MASK     0x0f00
#define _S3M_TRACKER_VER_MAJOR_SHIFT    8
#define _S3M_TRACKER_TYPE_MASK          0xf000
#define _S3M_TRACKER_TYPE_SHIFT         12

/* Tracker type */
#define _S3M_TRACKER_ST3        1
#define _S3M_TRACKER_IMPULSE    3
#define _S3M_TRACKER_SCHISM     4

/* Tracker version */
#define _S3M_TRACKER_VER_MAJOR_ST3  3

/* Channel bits */
#define _S3M_CHN_TYPE_MASK      0x1f
#define _S3M_CHN_DISABLED       0x80

/* Channel type */
#define _S3M_CHN_OFF            0
#define _S3M_CHN_LEFT           1
#define _S3M_CHN_RIGHT          2
#define _S3M_CHN_ADLIB_MELODY   3
#define _S3M_CHN_ADLIB_DRUMS    4

#pragma pack(push, 1);
typedef struct _s3m_header_t
{
    char name[_S3M_TITLE_LEN];
    uint16_t type;
    uint8_t unused1[2];
    uint16_t ordnum;
    uint16_t insnum;
    uint16_t patnum;
    uint16_t flags;
    uint16_t tracker;
    uint16_t format;
    uint32_t magic;
    uint8_t gvolume;    // global volume
    uint8_t speed;
    uint8_t tempo;
    uint8_t mvolume;    // master volume
    uint8_t unused2;
    uint8_t defpan;     // default pan flag
    uint8_t unused3[10];
    uint8_t channelset[_S3M_MAX_CHANNELS];
};
#pragma pack(pop);
typedef struct _s3m_header_t _S3M_HEADER;

/*** Packed pattern lead byte flags ***/

typedef uint8_t _s3m_event_flags_t;
typedef _s3m_event_flags_t _S3M_EVENTFLAGS;

#define _S3M_EVENTFL_CHNMASK  0x1f
#define _S3M_EVENTFL_NOTE_INS 0x20
#define _S3M_EVENTFL_VOL      0x40
#define _S3M_EVENTFL_CMD_PARM 0x80

/*** PCM sample ***/

typedef uint8_t _s3m_sample_flags_t;
typedef _s3m_sample_flags_t _S3M_SMPFLAGS;

#define _S3M_SMPFL_LOOP   0x01
#define _S3M_SMPFL_STEREO 0x02
#define _S3M_SMPFL_16BITS 0x04

#pragma pack(push, 1);
typedef struct _s3m_sample_t
{
    uint8_t filepos_hi;
    uint16_t filepos;
    uint32_t length;
    uint32_t loopbeg;
    uint32_t loopend;
    uint8_t note_volume;
    uint8_t unused1;
    uint8_t packinfo;
    _S3M_SMPFLAGS flags;
    uint32_t rate;
    uint8_t unused2[12];
};
#pragma pack(pop);
typedef struct _s3m_sample_t _S3M_SMP;

/*** AdLib instrument ***/

#pragma pack(push, 1);
typedef struct _s3m_adlib_instrument_t
{
    uint8_t unused1[3];
    uint8_t data[12];
    uint8_t note_volume;
    uint8_t unused2[3];
    uint32_t rate;
    uint8_t unused3[12];
};
#pragma pack(pop);
typedef struct _s3m_adlib_instrument_t _S3M_ADL;

/*** Instrument ***/

typedef uint8_t _s3m_instrument_type_t;
typedef _s3m_instrument_type_t _S3M_INSTYPE;

#define _S3M_INST_EMPTY  0x00
#define _S3M_INST_PCM    0x01
#define _S3M_INST_AMEL   0x02
#define _S3M_INST_ABD    0x03
#define _S3M_INST_ASNARE 0x04
#define _S3M_INST_ATOM   0x05
#define _S3M_INST_ACYM   0x06
#define _S3M_INST_AHIHAT 0x07

#define _S3M_INS_FILENAME_LEN 12
#define _S3M_INS_TITLE_LEN 28

#pragma pack(push, 1);
typedef struct _s3m_instrument_t
{
    _S3M_INSTYPE type;
    char dosname[_S3M_INS_FILENAME_LEN];
    union
    {
        _S3M_SMP sample;
        _S3M_ADL adlib;
    } data;
    char title[_S3M_INS_TITLE_LEN];
    uint32_t magic;
};
#pragma pack(pop);
typedef struct _s3m_instrument_t _S3M_INS;

/*** Patterns order entry ***/

typedef uint8_t _s3m_patterns_order_entry_t;
typedef _s3m_patterns_order_entry_t _S3M_PATORDENT;

#define _S3M_PATORDENT_SKIP 0xfe
#define _S3M_PATORDENT_END  0xff

/*** Loader ***/

#pragma pack(push, 1);
typedef struct _s3m_instrument_info_t
{
    _S3M_INS header;
    int8_t smp_num;
};
#pragma pack(pop);
typedef struct _s3m_instrument_info_t _S3M_INSINFO;

#define _S3M_ERROR_LEN 128

typedef uint8_t _s3m_loader_flags_t;
typedef _s3m_loader_flags_t _S3M_LOADERFLAGS;

#define _S3M_LOADERFL_USE_EM        (1 << 0)
#define _S3M_LOADERFL_SIGNED_DATA   (1 << 1)

struct _em_data_t {
    uint16_t pages;
    uint32_t size;
    uint32_t offset;
};

static void __near _em_data_init (struct _em_data_t *self, uint16_t pages)
{
    self->pages = pages;
    self->size = (uint32_t) pages * EM_PAGE_SIZE;
    self->offset = 0;
}

typedef struct _s3m_loader_t
{
    _S3M_LOADERFLAGS flags;
    char err[_S3M_ERROR_LEN];
    MUSMOD *track;
    FILE *f;
    char *in_buf;
    char *pat_buf;
    uint8_t   ins_count;
    uint8_t   smp_count;
    uint8_t   pat_count;
    uint8_t   ord_length;
    uint16_t *ins_offsets;
    _S3M_INSINFO *ins_info;
    uint16_t *pat_offsets;
    struct _em_data_t pat_EM;
    struct _em_data_t smp_EM;
};
typedef struct _s3m_loader_t _S3M_LOADER;

#define _get_orders_file_pos(o)         sizeof (_S3M_HEADER)
#define _get_instruments_file_pos(o)    _get_orders_file_pos (o) + _Self->ord_length * sizeof (_S3M_PATORDENT)
#define _get_patterns_file_pos(o)       _get_instruments_file_pos (o) + _Self->ins_count * sizeof (uint16_t)
#define _get_channels_file_pos(o)       _get_patterns_file_pos (o) + _Self->pat_count * sizeof (uint16_t)

/**********************************************************************/

static bool __near read_sample_data (void __far *dest, PCMSMP *smp, uint32_t offset, uint16_t size)
{
    if (smp->flags & PCMSMPFL_EM)
    {
        offset += smp->data.em.offset;
        if (map_EM_data (smp->data.em.handle, offset, size))
        {
            memcpy (dest, MK_FP (emsFrameSeg, get_EM_page_offset (offset)), size);
            return true;
        }
    }
    else
    {
        void __far *p = smp->data.dos.ptr;
        if (p)
        {
            offset += FARPTR_TO_LONG (p);
            memcpy (dest, FARPTR_FROM_LONG (offset), size);
            return true;
        }
    }
    return false;
}

static bool __near write_sample_data (PCMSMP *smp, uint32_t offset, void __far *src, uint16_t size)
{
    if (smp->flags & PCMSMPFL_EM)
    {
        offset += smp->data.em.offset;
        if (map_EM_data (smp->data.em.handle, offset, size))
        {
            memcpy (MK_FP (emsFrameSeg, get_EM_page_offset (offset)), src, size);
            return true;
        }
    }
    else
    {
        void __far *p = smp->data.dos.ptr;
        if (p)
        {
            offset += FARPTR_TO_LONG (p);
            memcpy (FARPTR_FROM_LONG (offset), src, size);
            return true;
        }
    }
    return false;
}

static bool __near adjust_sample_interp (PCMSMP *smp, uint16_t count)
{
    #define BUF_SIZE EXTRA_LEN_FOR_LINEAR_INTERP
    uint32_t start;
    #pragma pack(push, 1);
    union
    {
        uint8_t u8[BUF_SIZE];
        uint16_t u16[BUF_SIZE];
    } buf;
    #pragma pack(pop);
    uint16_t size, i;

    if ((!count) || (count > EXTRA_LEN_FOR_LINEAR_INTERP))
    {
        DEBUG_INFO ("Internal failure.");
        return false;
    }

    size = count;
    if (smp->flags & PCMSMPFL_16BITS)
        size *= 2;

    /* Read sample data into buffer */

    switch ((smp->flags >> PCMSMPFL_LOOPSHIFT) & PCMSMPLOOP_MASK)
    {
    case PCMSMPLOOP_NONE:
        /* No need, clear buffer */
        if (smp->flags & PCMSMPFL_16BITS)
            memset (&buf, 0, count * 2);
        else
            memset (&buf, 0, count);
        break;

    case PCMSMPLOOP_FORWARD:
        start = smp->loop_start;
        if (smp->flags & PCMSMPFL_16BITS)
            start *= 2;
        if (!read_sample_data (&buf, smp, start, size))
            return false;
        break;

    case PCMSMPLOOP_BACKWARD:
    case PCMSMPLOOP_PINGPONG:
        start = smp->loop_end - count;
        if (smp->flags & PCMSMPFL_16BITS)
            start *= 2;
        if (!read_sample_data (&buf, smp, start, size))
            return false;
        /* Reverse the order of values */
        if (smp->flags & PCMSMPFL_16BITS)
            for (i = 0; i < count / 2; i++)
            {
                uint16_t t = buf.u16[i];
                buf.u16[i] = buf.u16[count-i-1];
                buf.u16[count-i-1] = t;
            }
        else
            for (i = 0; i < count / 2; i++)
            {
                uint8_t t = buf.u8[i];
                buf.u8[i] = buf.u8[count-i-1];
                buf.u8[count-i-1] = t;
            }
        break;

    default:
        return false;
    }

    /* Write sample data */

    return write_sample_data (smp, smp->size, &buf, size);
    #undef BUF_SIZE
}

/**********************************************************************/

/*** Initialization ***/

LOADER_S3M *load_s3m_new (void)
{
    return _new (_S3M_LOADER);
}

void load_s3m_init (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;

    if (_Self)
        memset (_Self, 0, sizeof (_S3M_LOADER));
}

/*** Errors ***/

static void __near load_s3m_clear_error (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;

    memset (_Self->err, 0, _S3M_ERROR_LEN);
}

#if DEBUG == 1

static void __near load_s3m_set_error (LOADER_S3M *self, const char *method, int line, const char *format, ...)
{
    va_list ap;
    _S3M_LOADER *_Self = self;

    va_start(ap, format);
    vsnprintf (_Self->err, _S3M_ERROR_LEN, format, ap);
    _DEBUG_LOG (DBGLOG_ERR, __FILE__, line, method, "%s", _Self->err);
    va_end (ap);
}

#define ERROR(o, f, ...) load_s3m_set_error (o, __func__, __LINE__, f, __VA_ARGS__)

#else   /* DEBUG != 1 */

static void __near load_s3m_set_error (LOADER_S3M *self, const char *format, ...)
{
    va_list ap;
    _S3M_LOADER *_Self = self;

    va_start(ap, format);
    vsnprintf (_Self->err, _S3M_ERROR_LEN, format, ap);
    va_end (ap);
}

#define ERROR(o, f, ...) load_s3m_set_error (o, f, __VA_ARGS__)

#endif  /* DEBUG != 1 */

const char *load_s3m_get_error (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;

    if (_Self)
    {
        if (_Self->err[0] == 0)
            return NULL;

        return _Self->err;
    }
    else
        return NULL;
}

/*** File IO ***/

static bool __near
load_s3m_seek (LOADER_S3M *self, uint32_t pos)
{
    _S3M_LOADER *_Self = self;

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("Requested: pos=0x%lX",
            (uint32_t) pos
        );

    if (fsetpos (_Self->f, pos))
    {
        ERROR (self, "Failed to read %s.", "file");
        return false;
    }
    return true;
}

static bool __near
load_s3m_load_data (LOADER_S3M *self, void __far *data, uint32_t size, bool dump)
{
    _S3M_LOADER *_Self = self;
    uint32_t offset;
    uint16_t cur_size;

    offset = FARPTR_TO_LONG (data);

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("Requested: size=0x%lX, address=0x%lX, pointer=0x%04hX:0x%04hX",
            (uint32_t) size,
            (uint32_t) offset,
            (uint16_t) FP_SEG (data),
            (uint16_t) FP_OFF (data)
        );

    while (size)
    {
        data = FARPTR_FROM_LONG (offset);

        /* counter register limit */
        if (size > (1UL<<16) - 1)
            cur_size = (1UL<<16) - 1;
        else
            cur_size = size;

        /* DOS segment limit */
        if ((uint32_t) cur_size + FP_OFF (data) > (1UL<<16))
            cur_size = (1UL<<16) - FP_OFF (data);

        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("Reading: size=0x%hX, address=0x%lX, pointer=0x%04hX:0x%04hX, next_size=0x%lX",
                (uint16_t) cur_size,
                (uint32_t) offset,
                (uint16_t) FP_SEG (data),
                (uint16_t) FP_OFF (data),
                (uint32_t) size - cur_size
            );

        if (!fread (data, cur_size, 1, _Self->f))
        {
            ERROR (self,  "Failed to read %s.", "file");
            return false;
        }

        if (DEBUG_FILE_S3M_LOAD && dump)
            DEBUG_dump_mem (data, cur_size, "file: ");

        offset += cur_size;
        size -= cur_size;
    }

    return true;
}

static bool __near
load_s3m_load_data_EM (LOADER_S3M *self, EMSHDL handle, uint32_t offset,
    uint32_t size, bool dump)
{
    _S3M_LOADER *_Self = self;
    uint16_t seg_offset;
    uint16_t cur_size;
    bool map;

    seg_offset = get_EM_page_offset (offset);

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("Requested: size=0x%lX, handle=0x%04hX, offset=0x%lX, pages=0x%04hX:0x%04hX-0x%04hX:0x%04hX",
            (uint32_t) size,
            (uint16_t) handle,
            (uint32_t) offset,
            (uint16_t) get_EM_page (offset),
            (uint16_t) seg_offset,
            (uint16_t) get_EM_page (offset + size - 1),
            (uint16_t) get_EM_page_offset (offset + size - 1)
        );

    cur_size = 0;

    while (size)
    {
        void __far *data;
        /* DOS segment limit */
        if ((uint32_t) seg_offset + cur_size < (1UL<<16))
        {
            seg_offset += cur_size;
            map = cur_size == 0;
        }
        else
        {
            seg_offset = get_EM_page_offset (offset);
            map = true;
        }

        if (map)
            if (!map_EM_data (handle, offset, size))
            {
                ERROR (_Self, "%s", "Failed to map EM.");
                return false;
            }

        data = MK_FP (emsFrameSeg, seg_offset);

        /* counter register limit */
        if (size > (1UL<<16) - 1)
            cur_size = (1UL<<16) - 1;
        else
            cur_size = size;

        /* DOS segment limit */
        if ((uint32_t) cur_size + seg_offset > (1UL<<16))
            cur_size = (1UL<<16) - seg_offset;

        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("Reading: size=0x%lX, address=0x%lX, pointer=0x%04hX:0x%04hX, next_size=0x%lX",
                (uint32_t) cur_size,
                (uint32_t) FARPTR_TO_LONG (data),
                (uint16_t) FP_SEG (data),
                (uint16_t) FP_OFF (data),
                (uint32_t) size - cur_size
            );

        if (!fread (data, cur_size, 1, _Self->f))
        {
            ERROR (_Self, "Failed to read %s.", "file");
            return false;
        }

        if (DEBUG_FILE_S3M_LOAD && dump)
            DEBUG_dump_mem (data, cur_size, "file: ");

        offset += cur_size;
        size -= cur_size;
    }

    return true;
}

/*** Header ***/

static uint8_t __near getchtyp (uint8_t b)
{
    if (b <= 7)
        return _S3M_CHN_LEFT;

    if (b <= 15)
        return _S3M_CHN_RIGHT;

    if (b <= 23)
        return _S3M_CHN_ADLIB_MELODY;

    if (b <= 31)
        return _S3M_CHN_ADLIB_DRUMS;

    return _S3M_CHN_OFF;
}

static bool __near load_s3m_read_header (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;
    _S3M_HEADER header;
    MUSMOD *track;
    bool usepan;
    uint8_t defpan[_S3M_MAX_CHANNELS], maxused, i;

    track = _Self->track;

    if (!load_s3m_load_data (self, &header, sizeof (_S3M_HEADER), DEBUG_FILE_S3M_LOAD))
        return false;

    if ((header.type != _S3M_FILE_TYPE)
    || (header.magic != _S3M_FILE_MAGIC)
    || ((header.format != _S3M_FILE_FORMAT_1) && (header.format != _S3M_FILE_FORMAT_2)))
    {
        DEBUG_INFO_ ("type=0x%04hX (%s)",
            (uint16_t) header.type,
            (header.type != _S3M_FILE_TYPE)
                ? "wrong" : "ok"
        );
        DEBUG_INFO_ ("magic=0x%08lX (%s)",
            (uint32_t) header.magic,
            (header.magic != _S3M_FILE_MAGIC)
                ? "wrong" : "ok"
        );
        DEBUG_INFO_ ("format=0x%04hX (%s)",
            (uint16_t) header.format,
            ((header.format != _S3M_FILE_FORMAT_1) && (header.format != _S3M_FILE_FORMAT_2))
                ? "wrong" : "ok"
        );
        ERROR (self, "Unknown %s.", "file format");
        return false;
    }

    switch ((header.tracker & _S3M_TRACKER_TYPE_MASK) >> _S3M_TRACKER_TYPE_SHIFT)
    {
    case _S3M_TRACKER_ST3:
        if ((header.tracker & _S3M_TRACKER_VER_MAJOR_MASK) != (_S3M_TRACKER_VER_MAJOR_ST3 << _S3M_TRACKER_VER_MAJOR_SHIFT))
        {
            DEBUG_INFO_ ("tracker=0x%04hX (%s)",
                (uint16_t) header.tracker,
                ((header.tracker & _S3M_TRACKER_VER_MAJOR_MASK) != (_S3M_TRACKER_VER_MAJOR_ST3 << _S3M_TRACKER_VER_MAJOR_SHIFT))
                    ? "wrong" : "ok"
            );
            ERROR (self, "Unknown %s.", "tracker version");
            return false;
        }
        snprintf (
            track->format,
            MUSMOD_FORMAT_LEN,
            "Scream Tracker %hhx.%02hhx module",
            (uint8_t) ((header.tracker & _S3M_TRACKER_VER_MAJOR_MASK) >> _S3M_TRACKER_VER_MAJOR_SHIFT),
            (uint8_t) ((header.tracker & _S3M_TRACKER_VER_MINOR_MASK) >> _S3M_TRACKER_VER_MINOR_SHIFT)
        );
        break;
    case _S3M_TRACKER_IMPULSE:
        if ((header.tracker & (_S3M_TRACKER_VER_MAJOR_MASK | _S3M_TRACKER_VER_MINOR_MASK))
        <=  ((2 << _S3M_TRACKER_VER_MAJOR_SHIFT) | (0x14 << _S3M_TRACKER_VER_MINOR_SHIFT)))
            snprintf (
                track->format,
                MUSMOD_FORMAT_LEN,
                "Impulse Tracker %hhu.%02hhx module",
                (uint8_t) ((header.tracker & _S3M_TRACKER_VER_MAJOR_MASK) >> _S3M_TRACKER_VER_MAJOR_SHIFT),
                (uint8_t) ((header.tracker & _S3M_TRACKER_VER_MINOR_MASK) >> _S3M_TRACKER_VER_MINOR_SHIFT)
            );
        else
            snprintf (
                track->format,
                MUSMOD_FORMAT_LEN,
                "Impulse Tracker 2.14p%hu module",
                (uint16_t) (header.tracker & (_S3M_TRACKER_VER_MAJOR_MASK | _S3M_TRACKER_VER_MINOR_MASK)) -
                ((2 << _S3M_TRACKER_VER_MAJOR_SHIFT) | (0x14 << _S3M_TRACKER_VER_MINOR_SHIFT))
            );
        break;
    case _S3M_TRACKER_SCHISM:
        snprintf (
            track->format,
            MUSMOD_FORMAT_LEN,
            "%s",
            "Schism Tracker module"
        );
        break;
    default:
        DEBUG_INFO_ ("tracker=0x%04hX, format=0x%04hX",
            (uint16_t) header.tracker,
            (uint16_t) header.format
        );
        ERROR (self, "Unknown %s.", "tracker version");
        return false;
    }

    strncpy (track->title, header.name,
        _S3M_TITLE_LEN > MUSMOD_TITLE_LEN ? MUSMOD_TITLE_LEN : _S3M_TITLE_LEN);
    track->title[MUSMOD_TITLE_LEN - 1] = 0;

    _Self->ins_count = header.insnum;
    _Self->pat_count = header.patnum;
    _Self->ord_length = header.ordnum;
    if (header.mvolume & _S3M_MVOL_STEREO)
        track->flags |= MUSMODFL_STEREO;
    else
        track->flags &= ~MUSMODFL_STEREO;
    if (header.flags & _S3M_FLAG_AMIGA_LIMITS)
        track->flags |= MUSMODFL_AMIGA_LIMITS;
    else
        track->flags &= ~MUSMODFL_AMIGA_LIMITS;
    track->global_volume = header.gvolume;
    track->master_volume = (header.mvolume & _S3M_MVOL_MASK) >> _S3M_MVOL_SHIFT;
    track->tempo = header.tempo;
    track->speed = header.speed;
    if (header.format == _S3M_FILE_FORMAT_1)
        _Self->flags |= _S3M_LOADERFL_SIGNED_DATA;

    if (header.defpan == 252)
    {
        if (!load_s3m_seek (self, _get_channels_file_pos (_Self)))
            return false;
        if (!load_s3m_load_data (self, defpan, _S3M_MAX_CHANNELS, true))
            return false;
        usepan = true;
    }
    else
        usepan = false;

    maxused = 0;
    for (i = 0; i < _S3M_MAX_CHANNELS; i++)
        if ((header.channelset[i] & _S3M_CHN_DISABLED) == 0)
        {
            MUSMODCHNPAN pan;

            switch (getchtyp (header.channelset[i] & _S3M_CHN_TYPE_MASK))
            {
            case _S3M_CHN_OFF:
                pan = MUSMODCHNPAN_CENTER;
                break;
            case _S3M_CHN_LEFT:
                if (usepan && (defpan[i] & 32))
                    pan = (uint16_t) MUSMODCHNPAN_MAX * (defpan[i] & 15) / 15;
                else
                    pan = (header.mvolume & _S3M_MVOL_STEREO) ? MUSMODCHNPAN_LEFT : MUSMODCHNPAN_CENTER;
                pan |= MUSMODCHNPANFL_ENABLED;
                maxused = i + 1;
                break;
            case _S3M_CHN_RIGHT:
                if (usepan && (defpan[i] & 32))
                    pan = (uint16_t) MUSMODCHNPAN_MAX * (defpan[i] & 15) / 15;
                else
                    pan = (header.mvolume & _S3M_MVOL_STEREO) ? MUSMODCHNPAN_RIGHT : MUSMODCHNPAN_CENTER;
                pan |= MUSMODCHNPANFL_ENABLED;
                maxused = i + 1;
                break;
            case _S3M_CHN_ADLIB_MELODY:
                pan = MUSMODCHNPAN_CENTER;
                break;
            case _S3M_CHN_ADLIB_DRUMS:
                pan = MUSMODCHNPAN_CENTER;
                break;
            default:
                pan = MUSMODCHNPAN_CENTER;
                break;
            }
            track->channels[i].pan = pan;
        }

    if (!maxused)
    {
        ERROR (self, "%s", "All channels are disabled.");
        return false;
    }

    track->channels_count = maxused;

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("channels=%hhu",
            (uint8_t) track->channels_count
        );

    return true;
}

/*** Samples ***/

/* Minimal sample length or sample loop length (in samples) to enlarge too
 * short samples or loops to reduce CPU load while mixing */
#define MIN_SAMPLE_LEN 128

#define SAMPLE_SIZE_MAX (1UL<<30)

static uint32_t __near _calc_sample_size (PCMSMP *smp)
{
    uint32_t size = smp->length;

    if (smp->flags & PCMSMPFL_16BITS)
        size <<= 1;

    return size;
}

static uint32_t __near _calc_sample_mem_size (PCMSMP *smp)
{
    uint32_t size;

    if (pcmsmp_needs_expansion (smp, MIN_SAMPLE_LEN))
    {
        PCMSMPEXP se;

        pcmsmp_calc_expansion (smp, &se, MIN_SAMPLE_LEN);

        size = se.start + se.len;

        if (smp->flags & PCMSMPFL_16BITS)
            size <<= 1;
    }
    else
        size = smp->size;

    if (smp->flags & PCMSMPFL_16BITS)
        size += EXTRA_LEN_FOR_LINEAR_INTERP * 2;
    else
        size += EXTRA_LEN_FOR_LINEAR_INTERP;

    return size + PCMSMP_EXTRA_DATA_SIZE;
}

static bool __near load_s3m_allocate_sample_data (LOADER_S3M *self, PCMSMP *smp, uint8_t index)
{
    uint32_t size;
    void __far *data;

    size = smp->mem_size;

    data = __new (size);
    if (!data)
    {
        ERROR (self, "Failed to allocate memory for %s.", "sample data");
        return false;
    }

    smp->flags &= ~PCMSMPFL_EM;
    smp->data.dos.ptr = data;

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("index=%hhu, size=0x%lX, address=0x%lX, pointer=0x%04hX:0x%04hX.",
            (uint8_t) index,
            (uint32_t) size,
            (uint32_t) FARPTR_TO_LONG (data),
            (uint16_t) FP_SEG (data),
            (uint16_t) FP_OFF (data)
        );

    return true;
}

static void __near load_s3m_fake_allocate_sample_data_EM (LOADER_S3M *self, PCMSMP *smp)
{
    _S3M_LOADER *_Self = self;
    uint32_t offset, size;

    offset = _Self->smp_EM.offset;
    size = smp->mem_size;

    if (smp->flags & PCMSMPFL_16BITS)
        offset = (offset + 1) & ~1UL;   // align offset to 2 bytes

    // calculate next EM offset
    _Self->smp_EM.offset = offset + size;
}

static bool __near load_s3m_allocate_sample_data_EM (LOADER_S3M *self, PCMSMP *smp, uint8_t index)
{
    _S3M_LOADER *_Self = self;
    MUSMOD *track;
    PCMSMPLIST *samples;
    uint32_t offset, size;

    track = _Self->track;
    samples = &track->samples;

    offset = _Self->smp_EM.offset;
    size = smp->mem_size;

    if (smp->flags & PCMSMPFL_16BITS)
        offset = (offset + 1) & ~1UL;   // align offset to 2 bytes

    if (offset + size - 1 < _Self->smp_EM.size)
    {
        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("index=%hhu, size=0x%lX, handle=0x%04hX, offset=0x%lX, pages=0x%04hX:0x%04hX-0x%04hX:0x%04hX",
                (uint8_t) index,
                (uint32_t) size,
                (uint16_t) samples->handle,
                (uint32_t) offset,
                (uint16_t) get_EM_page (offset),
                (uint16_t) get_EM_page_offset (offset),
                (uint16_t) get_EM_page (offset + size - 1),
                (uint16_t) get_EM_page_offset (offset + size - 1)
            );
        smp->flags |= PCMSMPFL_EM;
        smp->flags &= ~PCMSMPFL_OWNHDL;
        smp->data.em.handle = samples->handle;
        smp->data.em.offset = offset;
        // calculate next EM offset
        _Self->smp_EM.offset = offset + size;
        return true;
    }
    else
        return false;
}

static void __near load_s3m_alloc_EM_samples_pages (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;
    MUSMOD *track;
    PCMSMPLIST *samples;
    MUSINSLIST *instruments;
    uint16_t free_pages, req_pages;
    int16_t i;
    PCMSMP *smp;
    EMSHDL handle;

    track = _Self->track;
    samples = &track->samples;
    instruments = &track->instruments;

    _em_data_init (&_Self->smp_EM, 0);

    for (i = 0; i < _Self->ins_count; i++)
    {
        MUSINS *ins;
        ins = musinsl_get (instruments, i);
        if (ins->type == MUSINST_PCM)
        {
            smp = musins_get_sample (ins);
            load_s3m_fake_allocate_sample_data_EM (self, smp);
        }
    }

    _Self->smp_EM.pages = get_EM_page (_Self->smp_EM.offset + EM_PAGE_SIZE - 1);
    free_pages = emsGetFreePagesCount ();

    if (_Self->smp_EM.pages > free_pages)
        req_pages = free_pages;
    else
        req_pages = _Self->smp_EM.pages;

    if (DEBUG_FILE_S3M_LOAD)
    {
        DEBUG_INFO ("Memory info:");
        DEBUG_INFO_ ("Samples size:       0x%08lX bytes (%hu samples)",
            (uint32_t) _Self->smp_EM.offset,
            (uint16_t) _Self->smp_count
        );
        DEBUG_INFO_ ("Samples size in EM: 0x%08lX bytes (%hu pages)",
            (uint32_t) _Self->smp_EM.pages * EM_PAGE_SIZE,
            (uint16_t) _Self->smp_EM.pages
        );
        DEBUG_INFO_ ("Free EM size:       0x%08lX bytes (%hu pages)",
            (uint32_t) free_pages * EM_PAGE_SIZE,
            (uint16_t) free_pages
        );
        DEBUG_INFO_ ("Requested EM size:  0x%08lX bytes (%hu pages)",
            (uint32_t) req_pages * EM_PAGE_SIZE,
            (uint16_t) req_pages
        );
    }

    _em_data_init (&_Self->smp_EM, 0);

    if (!req_pages)
    {
        samples->flags &= ~PCMSMPLFL_EM;
        DEBUG_WARN_ ("Requested no EM for %s.", "samples");
        return;
    }

    handle = emsAlloc (req_pages);
    if (emsEC != E_EMS_SUCCESS)
    {
        samples->flags &= ~PCMSMPLFL_EM;
        ERROR (self, "Failed to allocate EM for %s.", "samples");
        return;
    }

    samples->flags |= PCMSMPLFL_EM;
    samples->handle = handle;
    emsSetHandleName (samples->handle, "smplist");
    _em_data_init (&_Self->smp_EM, req_pages);

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("0x%08lX bytes EM (%hu pages) allocated for %s.",
            (uint32_t) req_pages * EM_PAGE_SIZE,
            (uint16_t) req_pages,
            "samples"
        );
}

static bool __near load_s3m_load_sample (LOADER_S3M *self, uint8_t index)
{
    _S3M_LOADER *_Self = self;
    _S3M_INSINFO *info;
    MUSMOD *track;
    PCMSMPLIST *samples;
    PCMSMP *smp;
    uint32_t load_size, mem_size;

    info = &_Self->ins_info[index];
    if (!load_s3m_seek (self, ((info->header.data.sample.filepos +
        ((uint32_t)info->header.data.sample.filepos_hi << 16)) << 4)))
        return false;

    track = _Self->track;
    samples = &track->samples;
    smp = pcmsmpl_get (samples, info->smp_num);
    load_size = smp->size;
    mem_size = smp->mem_size;
    if (mem_size > SAMPLE_SIZE_MAX)
    {
        ERROR (self, "%s", "Sample is too large.");
        return false;
    }

    if (!((samples->flags & PCMSMPLFL_EM)
          && load_s3m_allocate_sample_data_EM (self, smp, index)))
    {
        if (!load_s3m_allocate_sample_data (self, smp, index))
        {
            ERROR (self, "Failed to allocate memory for %s.", "sample data");
            return false;
        }
    }

    if (smp->flags & PCMSMPFL_EM)
    {
        if (!load_s3m_load_data_EM (self, smp->data.em.handle,
            smp->data.em.offset, load_size, DEBUG_FILE_DUMP_SAMPLE_DATA))
        {
            ERROR (self, "Failed to load %s into %s.", "sample data", "EM");
            return false;
        }
        if (smp->flags & PCMSMPFL_16BITS)
            convert_sign_16_EM (smp->data.em.handle,
                smp->data.em.offset, load_size);
        else
            convert_sign_8_EM (smp->data.em.handle,
                smp->data.em.offset, load_size);
    }
    else
    {
        if (!load_s3m_load_data (self, smp->data.dos.ptr, load_size, DEBUG_FILE_DUMP_SAMPLE_DATA))
        {
            ERROR (self, "Failed to load %s into %s.", "sample data", "DM");
            return false;
        }
        if (smp->flags & PCMSMPFL_16BITS)
            convert_sign_16 (smp->data.dos.ptr, load_size);
        else
            convert_sign_8 (smp->data.dos.ptr, load_size);
    }

    /* Expand short sample or loop */
    if (pcmsmp_needs_expansion (smp, MIN_SAMPLE_LEN))
    {
        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("needs_expansion=%s", "yes");
        pcmsmp_expand (smp, MIN_SAMPLE_LEN);
    }
    else
    {
        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("needs_expansion=%s", "no");
    }

    adjust_sample_interp (smp, EXTRA_LEN_FOR_LINEAR_INTERP);

    smp->flags |= PCMSMPFL_AVAIL;

    return true;
}

static bool __near load_s3m_load_samples (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;
    int count, i;

    count = _Self->ins_count;
    for (i = 0; i < count; i++)
        if (_Self->ins_info[i].smp_num >= 0)
            if (!load_s3m_load_sample (_Self, i))
                return false;

    return true;
}

static void __near load_s3m_free_unused_samples_pages (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;
    MUSMOD *track;
    PCMSMPLIST *samples;

    track = _Self->track;
    samples = &track->samples;
    if (samples->flags & PCMSMPLFL_EM)
    {
        uint16_t pages = get_EM_page (_Self->smp_EM.offset + EM_PAGE_SIZE - 1);
        uint16_t pages_free = _Self->smp_EM.pages - pages;

        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("EM pages: %u used, %u free", pages, pages_free);

        // try to free unused pages
        if (pages_free)
        {
            EMSHDL handle = samples->handle;

            if (pages)
            {
                if (!emsResize (handle, pages))
                    DEBUG_WARN_ ("Failed to free unused EM pages for %s.", "samples");
            }
            else
            {
                if (emsFree (handle))
                {
                    samples->flags &= ~PCMSMPLFL_EM;
                    samples->handle = EMSBADHDL;
                }
                else
                    DEBUG_WARN_ ("Failed to free unused EM pages for %s.", "samples");
            }
        }
    }
}

/*** Instruments ***/

static bool __near load_s3m_load_ins_offsets (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;
    uint16_t size;

    size = _Self->ins_count * sizeof (uint16_t);

    _Self->ins_offsets = __new (size);
    if (!_Self->ins_offsets)
    {
        ERROR (self, "Failed to allocate memory for %s.", "instruments offsets");
        return false;
    }

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("Reading %s...", "instruments offsets");

    if (!load_s3m_seek (self, _get_instruments_file_pos (_Self)))
        return false;

    if (!load_s3m_load_data (self, _Self->ins_offsets, size, DEBUG_FILE_S3M_LOAD))
        return false;

    return true;
}

static bool __near load_s3m_load_ins_headers (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;
    uint8_t i;

    _Self->ins_info = __new (sizeof (_S3M_INSINFO) * _Self->ins_count);
    if (!_Self->ins_info)
    {
        ERROR (self, "Failed to allocate memory for %s.", "instruments headers");
        return false;
    }

    _Self->smp_count = 0;
    for (i = 0; i < _Self->ins_count; i++)
    {
        _S3M_INSINFO *info;

        if (!load_s3m_seek (self, _Self->ins_offsets[i] * 16))
            return false;

        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("Reading %s...", "instrument header");

        info = &_Self->ins_info[i];
        if (!load_s3m_load_data (self, & (info->header), sizeof (_S3M_INS), DEBUG_FILE_S3M_LOAD))
            return false;

        switch (info->header.type)
        {
        case _S3M_INST_EMPTY:
            info->smp_num = -1;
            break;
        case _S3M_INST_PCM:
            if (info->header.data.sample.packinfo)
            {
                ERROR (self, "%s is not supported.", "Packed sample");
                return false;
            }
            if (info->header.data.sample.flags & _S3M_SMPFL_STEREO)
            {
                ERROR (self, "%s is not supported.", "Stereo sample");
                return false;
            }
            if (info->header.data.sample.length)
            {
                info->smp_num = _Self->smp_count;
                _Self->smp_count++;
            }
            else
            {
                info->header.type = _S3M_INST_EMPTY;
                info->smp_num = -1;
                DEBUG_WARN_ (self, "Empty %s.", "sample");
            }
            break;
        case _S3M_INST_AMEL:
        case _S3M_INST_ABD:
        case _S3M_INST_ASNARE:
        case _S3M_INST_ATOM:
        case _S3M_INST_ACYM:
        case _S3M_INST_AHIHAT:
            ERROR (self, "%s is not supported.", "Adlib instrument");
            return false;
        default:
            ERROR (self, "%s", "Unknown instrument type.");
            return false;
        }

        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("index=%hd, sample=%hd", i, info->smp_num);
    }

    return true;
}

static bool __near load_s3m_load_instrument (LOADER_S3M *self, uint8_t index)
{
    _S3M_LOADER *_Self = self;
    _S3M_INSINFO *info;
    _S3M_INS *_ins;
    MUSMOD *track;
    PCMSMPLIST *samples;
    MUSINSLIST *instruments;
    MUSINS *ins;
    char ins_title[MUSINS_TITLE_LEN + 1];       /* including terminating zero */

    info = &_Self->ins_info[index];
    _ins = & (info->header);

    track = _Self->track;
    samples = &track->samples;
    instruments = &track->instruments;
    ins = musinsl_get (instruments, index);
    musins_init (ins);

    if (_ins->type == _S3M_INST_PCM)
    {
        PCMSMP *smp;
        char smp_title[_S3M_INS_FILENAME_LEN + 1];  /* including terminating zero */

        smp = pcmsmpl_get (samples, info->smp_num);
        smp->flags |= PCMSMPFL_AVAIL;
        if (_ins->data.sample.flags & _S3M_SMPFL_16BITS)
            smp->flags |= PCMSMPFL_16BITS;
        else
            smp->flags &= ~PCMSMPFL_16BITS;
        smp->flags &= ~PCMSMPFL_LOOP_MASK;
        if (_ins->data.sample.flags & _S3M_SMPFL_LOOP)
        {
            smp->flags |= PCMSMPFL_LOOP_FORWARD;
            smp->loop_start = _ins->data.sample.loopbeg;
            smp->loop_end = _ins->data.sample.loopend;
            smp->length = _ins->data.sample.loopend;
        }
        else
        {
            smp->flags |= PCMSMPFL_LOOP_NONE;
            smp->length = _ins->data.sample.length;
        }

        smp->size = _calc_sample_size (smp);
        smp->mem_size = _calc_sample_mem_size (smp);

        smp->rate = _ins->data.sample.rate;
        smp->volume = PCMSMP_VOLUME_MAX;
        memcpy (smp_title, _ins->dosname, _S3M_INS_FILENAME_LEN);
        smp_title[_S3M_INS_FILENAME_LEN] = 0;
        strncpy (smp->title, smp_title, PCMSMP_TITLE_LEN);
        ins->type = MUSINST_PCM;
        ins->volume = MUSINS_VOLUME_MAX;
        ins->note_volume = _ins->data.sample.note_volume;
        musins_set_sample (ins, smp);
    }
    else
    {
        ins->type = MUSINST_EMPTY;
    }

    memcpy (ins_title, _ins->title, _S3M_INS_TITLE_LEN);
    ins_title[_S3M_INS_TITLE_LEN] = 0;
    strncpy (ins->title, ins_title, MUSINS_TITLE_LEN);
    ins->title[MUSINS_TITLE_LEN - 1] = 0;

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_dump_instrument_info (ins, index, samples);

    return true;
}

static bool __near load_s3m_load_instruments (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;
    int count, i;

    count = _Self->ins_count;
    for (i = 0; i < count; i++)
        if (!load_s3m_load_instrument (_Self, i))
            return false;

    return true;
}

/*** Patterns ***/

static bool __near load_s3m_load_pat_offsets (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;
    uint16_t size;

    size = sizeof (uint16_t) * _Self->pat_count;

    _Self->pat_offsets = __new (size);
    if (!_Self->pat_offsets)
    {
        ERROR (self, "Failed to allocate memory for %s.", "pattern offsets");
        return false;
    }

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("Reading %s...", "patterns offsets");

    if (!load_s3m_seek (self, _get_patterns_file_pos (_Self)))
        return false;

    if (!load_s3m_load_data (self, _Self->pat_offsets, size, DEBUG_FILE_S3M_LOAD))
        return false;

    return true;
}

static bool __near load_s3m_allocate_pattern_data (LOADER_S3M *self, MUSPAT *pat, uint8_t index, uint32_t size)
{
    void __far *data;

    data = __new (size);
    if (!data)
    {
        ERROR (self, "Failed to allocate memory for %s.", "pattern data");
        return false;
    }

    pat->flags &= ~MUSPATFL_EM;
    pat->data.dos.ptr = data;

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("index=%hhu, size=0x%lX, address=0x%lX, pointer=0x%04hX:0x%04hX.",
            (uint8_t) index,
            (uint32_t) size,
            (uint32_t) FARPTR_TO_LONG (data),
            (uint16_t) FP_SEG (data),
            (uint16_t) FP_OFF (data)
        );

    return true;
}

static void __near load_s3m_fake_allocate_pattern_data_EM (LOADER_S3M *self, uint32_t size)
{
    _S3M_LOADER *_Self = self;
    uint32_t offset;

    offset = (_Self->pat_EM.offset + 1) & ~1UL; // align offset to 2 bytes

    // calculate next EM offset
    _Self->pat_EM.offset = offset + size;
}

static bool __near load_s3m_allocate_pattern_data_EM (LOADER_S3M *self, MUSPAT *pat, uint8_t index, uint32_t size)
{
    _S3M_LOADER *_Self = self;
    MUSMOD *track;
    MUSPATLIST *patterns;
    uint32_t offset;

    track = _Self->track;
    patterns = &track->patterns;

    offset = (_Self->pat_EM.offset + 1) & ~1UL;   // align offset to 2 bytes

    if (offset + size - 1 < _Self->pat_EM.size)
    {
        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("index=%hhu, size=0x%lX, handle=0x%04hX, offset=0x%lX, pages=0x%04hX:0x%04hX-0x%04hX:0x%04hX",
                (uint8_t) index,
                (uint32_t) size,
                (uint16_t) patterns->handle,
                (uint32_t) offset,
                (uint16_t) get_EM_page (offset),
                (uint16_t) get_EM_page_offset (offset),
                (uint16_t) get_EM_page (offset + size - 1),
                (uint16_t) get_EM_page_offset (offset + size - 1)
            );
        pat->flags |= MUSPATFL_EM;
        pat->flags &= ~MUSPATFL_OWNHDL;
        pat->data.em.handle = patterns->handle;
        pat->data.em.offset = offset;
        // calculate next EM offset
        _Self->pat_EM.offset = offset + size;
        return true;
    }
    else
        return false;
}

static void __near load_s3m_alloc_EM_patterns_pages (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;
    MUSMOD *track;
    MUSPATLIST *patterns;
    uint16_t num_patterns, i;
    uint16_t patsize;
    uint16_t free_pages, req_pages;
    EMSHDL handle;

    track = _Self->track;
    patterns = &track->patterns;
    num_patterns = muspatl_get_count (patterns);
    patsize = _calc_max_raw_pattern_size (_S3M_PATTERN_ROWS, track->channels_count);

    _em_data_init (&_Self->pat_EM, 0);

    for (i = 0; i < num_patterns; i++)
        load_s3m_fake_allocate_pattern_data_EM (self, patsize);

    _Self->pat_EM.pages = get_EM_page (_Self->pat_EM.offset + EM_PAGE_SIZE - 1);
    free_pages = emsGetFreePagesCount ();

    if (_Self->pat_EM.pages > free_pages)
        req_pages = free_pages;
    else
        req_pages = _Self->pat_EM.pages;

    if (DEBUG_FILE_S3M_LOAD)
    {
        DEBUG_INFO ("Memory info:");
        DEBUG_INFO_ ("Patterns size:       0x%08lX bytes (%hu patterns, each 0x%hX bytes size)",
            (uint32_t) _Self->pat_EM.offset,
            (uint16_t) num_patterns,
            (uint16_t) patsize
        );
        DEBUG_INFO_ ("Patterns size in EM: 0x%08lX bytes (%lu patterns, %hu pages)",
            (uint32_t) _Self->pat_EM.pages * EM_PAGE_SIZE,
            (uint32_t) _Self->pat_EM.pages * EM_PAGE_SIZE / patsize,
            (uint16_t) _Self->pat_EM.pages
        );
        DEBUG_INFO_ ("Free EM:             0x%08lX bytes (%lu patterns, %hu pages)",
            (uint32_t) free_pages * EM_PAGE_SIZE,
            (uint32_t) free_pages * EM_PAGE_SIZE / patsize,
            (uint16_t) free_pages
        );
        DEBUG_INFO_ ("Requested EM:        0x%08lX bytes (%lu patterns, %hu pages)",
            (uint32_t) req_pages * EM_PAGE_SIZE,
            (uint32_t) req_pages * EM_PAGE_SIZE / patsize,
            (uint16_t) req_pages
        );
    }

    _em_data_init (&_Self->pat_EM, 0);

    if (!req_pages)
    {
        patterns->flags &= ~MUSPATLFL_EM;
        DEBUG_WARN_ ("Requested no EM for %s.", "patterns");
        return;
    }

    handle = emsAlloc (req_pages);
    if (emsEC != E_EMS_SUCCESS)
    {
        patterns->flags &= ~MUSPATLFL_EM;
        ERROR (self, "Failed to allocate EM for %s.", "patterns");
        return;
    }

    patterns->flags |= MUSPATLFL_EM | MUSPATLFL_OWNHDL;
    patterns->handle = handle;
    emsSetHandleName (patterns->handle, "patlist");
    _em_data_init (&_Self->pat_EM, req_pages);

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("0x%08lX bytes EM (%hu pages) allocated for %s.",
            (uint32_t) req_pages * EM_PAGE_SIZE,
            (uint16_t) req_pages,
            "patterns"
        );
}

/* Packed pattern event */

typedef struct _s3m_pattern_channel_event_data_t
{
    unsigned char instrument;
    unsigned char note;
    unsigned char note_volume;
    unsigned char command;
    unsigned char parameter;
};
typedef struct _s3m_pattern_channel_event_data_t _S3M_PATCHNEVDATA;

typedef unsigned char _s3m_pattern_channel_event_flags_t;
typedef _s3m_pattern_channel_event_flags_t _S3M_PATCHNEVFLAGS;

#define _S3M_PATEVFL_INS    (1 << 0)
#define _S3M_PATEVFL_NOTE   (1 << 1)
#define _S3M_PATEVFL_VOL    (1 << 2)
#define _S3M_PATEVFL_CMD    (1 << 3)
#define _S3M_PATEVFL_ROWEND (1 << 4)

typedef struct _s3m_pattern_channel_event_t
{
    _S3M_PATCHNEVFLAGS flags;
    _S3M_PATCHNEVDATA data;
};
typedef struct _s3m_pattern_channel_event_t _S3M_PATCHNEVENT;

typedef struct _s3m_pattern_row_event_t
{
    unsigned char channel;
    _S3M_PATCHNEVENT event;
};
typedef struct _s3m_pattern_row_event_t _S3M_PATROWEVENT;

/* Packed pattern I/O */

typedef struct _s3m_pattern_io_t
{
    unsigned char *data;
    unsigned int size;
    unsigned int offset;
    char *error;
};
typedef struct _s3m_pattern_io_t _S3M_PATIO;

static void __near _s3m_patio_open (_S3M_PATIO *self, char *data, unsigned int size)
{
    self->data = data;
    self->size = size;
    self->offset = 0;
    self->error = NULL;
}

static bool __near _s3m_patio_read (_S3M_PATIO *self, char *buf, unsigned int size)
{
    if (self->offset + size - 1 < self->size)
    {
        memcpy (buf, self->data + self->offset, size);
        self->offset += size;
        return true;
    }
    else
    {
        self->error = "Out of data";
        return false;
    }
}

static bool __near _s3m_patio_eof (_S3M_PATIO *self)
{
    return self->size <= self->offset;
}

static bool __near _s3m_patio_read_event (_S3M_PATIO *self, _S3M_PATROWEVENT *event)
{
    unsigned char flags, v[2];

    if (!_s3m_patio_read (self, &flags, 1))
        return false;

    event->event.data.instrument  = CHN_INS_NONE;
    event->event.data.note        = CHN_NOTE_NONE;
    event->event.data.note_volume = CHN_NOTEVOL_NONE;
    event->event.data.command     = CHN_CMD_NONE;
    event->event.data.parameter   = 0;

    if (flags)
    {
        event->channel = flags & _S3M_EVENTFL_CHNMASK;
        event->event.flags = 0;

        if (flags & _S3M_EVENTFL_NOTE_INS)
        {
            // note, instrument
            if (!_s3m_patio_read (self, v, 2))
                return false;

            switch (v[0])
            {
            case 0xff:
                break;
            case 0xfe:
                event->event.data.note = CHN_NOTE_OFF;
                event->event.flags |= _S3M_PATEVFL_NOTE;
                break;
            default:
                if (((v[0] & 0x0f) <= 11) && ((v[0] >> 4) <= 7))
                {
                    event->event.data.note = v[0];
                    event->event.flags |= _S3M_PATEVFL_NOTE;
                }
                break;
            }

            if (v[1] && (v[1] <= 99))
            {
                event->event.data.instrument = v[1];
                event->event.flags |= _S3M_PATEVFL_INS;
            }
        }

        if (flags & _S3M_EVENTFL_VOL)
        {
            // note volume
            if (!_s3m_patio_read (self, v, 1))
                return false;

            if (v[0] <= 64)
            {
                event->event.data.note_volume = v[0];
                event->event.flags |= _S3M_PATEVFL_VOL;
            }
        }

        if (flags & _S3M_EVENTFL_CMD_PARM)
        {
            // command, parameter
            if (!_s3m_patio_read (self, v, 2))
                return false;

            if (v[0] && (v[0] <= 26))
            {
                event->event.data.command = v[0];
                event->event.data.parameter = v[1];
                event->event.flags |= _S3M_PATEVFL_CMD;
            }
        }
        return true;
    }
    else
    {
        event->channel = 0;
        event->event.flags = _S3M_PATEVFL_ROWEND;
        return true;
    }
}

void __near _s3m_patio_close (_S3M_PATIO *self)
{
}

void __near _clear_events (MUSPATCHNEVENT *events)
{
    unsigned char i;

    for (i = 0; i < _S3M_MAX_CHANNELS; i++)
    {
        muspatchnevent_clear (events);
        events++;
    }
}

static void __near convert_event (MUSPATROWEVENT *out, _S3M_PATROWEVENT *in)
{
    muspatrowevent_clear (out);
    out->channel = in->channel;

    if (in->event.flags)
    {
        if (in->event.flags & _S3M_PATEVFL_INS)
        {
            out->event.data.instrument = in->event.data.instrument;
            out->event.flags |= MUSPATCHNEVFL_INS;
        }

        if (in->event.flags & _S3M_PATEVFL_NOTE)
        {
            out->event.data.note = in->event.data.note;
            out->event.flags |= MUSPATCHNEVFL_NOTE;
        }

        if (in->event.flags & _S3M_PATEVFL_VOL)
        {
            out->event.data.note_volume = in->event.data.note_volume;
            out->event.flags |= MUSPATCHNEVFL_VOL;
        }

        if ((in->event.flags & _S3M_PATEVFL_CMD) && in->event.data.command)
        {
            unsigned char param = in->event.data.parameter;

            switch (in->event.data.command)
            {
            case 'A'-'A'+1: // Axx: Set speed to "xx" (1->255)
                if (param)
                {
                    out->event.data.command = EFFIDX_A_SET_SPEED;
                    out->event.data.parameter = param;
                    out->event.flags |= MUSPATCHNEVFL_CMD;
                }
                break;
            case 'B'-'A'+1: // Bxx: Jump to order "xx" (0->254)
                if (param <= 254)
                {
                    out->event.data.command = EFFIDX_B_JUMP;
                    out->event.data.parameter = param;
                    out->event.flags |= MUSPATCHNEVFL_CMD;
                    break;
                }
            case 'C'-'A'+1: // Cxx: Break pattern to row "xx" (BCD, 0->63)
                if (((param >> 4) <= 6) && ((param & 0x0f) <= 9))
                {
                    param = (param & 0x0f) + (param >> 4) * 10;
                    if (param <= 63)
                    {
                        out->event.data.command = EFFIDX_C_PATTERN_BREAK;
                        out->event.data.parameter = param;
                        out->event.flags |= MUSPATCHNEVFL_CMD;
                    }
                }
                break;
            case 'D'-'A'+1:
                // Dx0: Volume slide up by "x"
                // D0y: Volume slide down by "y"
                // DxF: Fine volume slide up by "x" (x!=0,0xF)
                // DFy: Fine volume slide down by "y" (y!=0,0xF)
                out->event.data.command = EFFIDX_D_VOLUME_SLIDE;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'E'-'A'+1:
                // Exx: Slide down by "xx" (0->0xDF)
                // EFx: Fine slide down by "x"
                // EEx: Extra fine slide down by "x"
                out->event.data.command = EFFIDX_E_PITCH_DOWN;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'F'-'A'+1:
                // Fxx: Slide up by "xx" (0->0xDF)
                // FFx: Fine slide up by "x"
                // FEx: Extra fine slide up by "x"
                out->event.data.command = EFFIDX_F_PITCH_UP;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'G'-'A'+1: // Gxx: Tone portamento with speed "xx"
                out->event.data.command = EFFIDX_G_PORTAMENTO;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'H'-'A'+1: // Hxy: Vibrato with speed "x" and depth "y"
                out->event.data.command = EFFIDX_H_VIBRATO;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'I'-'A'+1: // Ixy: Tremor with ontime "x" and offtime "y"
                out->event.data.command = EFFIDX_I_TREMOR;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'J'-'A'+1: // Jxy: Arpeggio (adds halftones "x", "y", 0)
                out->event.data.command = EFFIDX_J_ARPEGGIO;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'K'-'A'+1: // Kxy: Dual command H00 & Dxy
                out->event.data.command = EFFIDX_K_VIB_VOLSLIDE;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'L'-'A'+1: // Lxy: Dual command G00 & Dxy
                out->event.data.command = EFFIDX_L_PORTA_VOLSLIDE;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'O'-'A'+1: // Oxx: Set sample offset to "xx"*256
                out->event.data.command = EFFIDX_O_SAMPLE_OFFSET;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'Q'-'A'+1: // Qxy: Retrig note with type "x" and speed "y"
                out->event.data.command = EFFIDX_Q_RETRIG_VOLSLIDE;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'R'-'A'+1: // Rxy: Tremolo with speed "x" and depth "y"
                out->event.data.command = EFFIDX_R_TREMOLO;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'S'-'A'+1: // Sxy: Special command "x" with parameter "y"
                out->event.data.command = EFFIDX_S_SPECIAL;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'T'-'A'+1: // Txx: Set tempo "xx" (32->255)
                if (param >= 32)
                {
                    out->event.data.command = EFFIDX_T_SET_TEMPO;
                    out->event.data.parameter = param;
                    out->event.flags |= MUSPATCHNEVFL_CMD;
                }
                break;
            case 'U'-'A'+1: // Uxy: Fine vibrato with speed "x" and depth "y"
                out->event.data.command = EFFIDX_U_FINE_VIBRATO;
                out->event.data.parameter = param;
                out->event.flags |= MUSPATCHNEVFL_CMD;
                break;
            case 'V'-'A'+1: // Vxx: Set global volume (0->64)
                if (param <= 64)
                {
                    out->event.data.command = EFFIDX_V_SET_GVOLUME;
                    out->event.data.parameter = param;
                    out->event.flags |= MUSPATCHNEVFL_CMD;
                }
                break;
            default:
                break;
            }
        }
    }
}

static bool __near load_s3m_convert_pattern (LOADER_S3M *self, uint8_t *src, uint16_t src_len, MUSPAT *pattern, bool pack)
{
    _S3M_LOADER *_Self = self;
    _S3M_PATIO fi;
    void __far *data;
    _S3M_PATROWEVENT ei;
    MUSPATIO f;
    MUSPATROWEVENT e;
    MUSPATCHNEVENT events[_S3M_MAX_CHANNELS];
    uint16_t maxrow, r;
    uint8_t maxchn, c;
    uint16_t offset;
    #if DEBUG_FILE_DUMP_PATTERN_DATA == 1
    char s[13], *e_str;
    #endif  /* DEBUG_FILE_DUMP_PATTERN_DATA == 1 */

    if (pack)
        pattern->flags |= MUSPATFL_PACKED;
    else
        pattern->flags &= ~MUSPATFL_PACKED;

    if (!muspatio_open (&f, pattern, MUSPATIOMD_WRITE))
    {
        ERROR (self, "Failed to open pattern for writing (%s).", f.error);
        return false;
    }

    src = _Self->in_buf;

    _s3m_patio_open (&fi, src, src_len);

    if (pattern->flags & MUSPATFL_EM)
        data = MK_FP (emsFrameSeg, get_EM_page_offset (pattern->data.em.offset));
    else
        data = pattern->data.dos.ptr;

    memset (data, 0xaa, pattern->size);

    maxrow = pattern->rows;
    maxchn = pattern->channels;

    if (!(pattern->flags & MUSPATFL_PACKED))
    {
        /* clear pattern */

        muspatrowevent_clear (&e);
        for (r = 0; r < maxrow; r++)
        {
            muspatio_seek (&f, r, 0);
            for (c = 0; c < maxchn; c++)
            {
                e.channel = c;
                muspatio_write (&f, &e);
            }
        }
        muspatio_seek (&f, 0, 0);
    }

    _clear_events (events);

    r = 0;
    while (r < maxrow)
    {
        offset = fi.offset;
        if (_s3m_patio_read_event (&fi, &ei))
        {
            if (ei.event.flags & _S3M_PATEVFL_ROWEND)
            {
                if (DEBUG_FILE_DUMP_PATTERN_DATA)
                    _DEBUG_LOG (DBGLOG_MSG, NULL, 0, NULL,
                        "row=%02hhu, offset=0x%04hX, size=%hu, type=end",
                        (uint8_t) r,
                        (uint16_t) offset,
                        (uint16_t) fi.offset - offset
                    );
                for (c = 0; c < maxchn; c++)
                    if (events [c].flags)
                    {
                        e.channel = c;
                        memcpy (& (e.event), & (events [c]), sizeof (MUSPATCHNEVENT));
                        muspatio_write (&f, &e);
                    }
                muspatio_end_row (&f);
                r++;

                if (r < maxrow)
                    _clear_events (events);
            }
            else
            {
                convert_event (&e, &ei);

                if (e.event.flags)
                    memcpy (& (events [e.channel]), & (e.event), sizeof (MUSPATCHNEVENT));

                #if DEBUG_FILE_DUMP_PATTERN_DATA == 1
                if (e.event.flags)
                {
                    DEBUG_get_pattern_channel_event_str (s, & (e.event));
                    e_str = s;
                }
                else
                    e_str = "empty";

                _DEBUG_LOG (DBGLOG_MSG, NULL, 0, NULL,
                    "row=%02hhu, offset=0x%04hX, size=%hu, type=event <%02hhu:%s>",
                    (uint8_t) r,
                    (uint16_t) offset,
                    (uint16_t) fi.offset - offset,
                    (uint8_t) e.channel,
                    e_str
                );
                #endif  /* DEBUG_FILE_DUMP_PATTERN_DATA == 1 */
            }
        }
        else
        {
            ERROR (self, "Failed to read pattern (%s).", fi.error);
            muspatio_close (&f);
            _s3m_patio_close (&fi);
            return false;
        }
    }

    muspatio_close (&f);
    _s3m_patio_close (&fi);

    return true;
}

static bool __near load_s3m_load_pattern (LOADER_S3M *self, uint8_t index)
{
    #define _BUF_SIZE (_S3M_MAX_CHANNELS * 13)
    _S3M_LOADER *_Self = self;
    MUSMOD *track;
    MUSPATLIST *patterns;
    uint32_t pos;
    uint16_t in_size, mem_size;
    MUSPAT pat_static, *pat;
    void *data;

    pos = _Self->pat_offsets[index] * 16;

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("index=%hhu, file_offset=0x%lX",
            (uint8_t) index,
            (uint32_t) pos
        );

    if (!pos)
    {
        /*
        pat = muspatl_get (patterns, index);
        muspat_init (pat);  // is not necessary because it was already initialized by DYNARR's set_size().
        */
        return true;
    }

    if (!load_s3m_seek (self, pos))
        return false;

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("Reading %s...", "pattern size");

    if (!load_s3m_load_data (self, &in_size, 2, DEBUG_FILE_S3M_LOAD))
        return false;

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("block_size=0x%hX",
            (uint16_t) in_size
        );

    if ((in_size <= 2) || (in_size > _S3M_IN_PATTERN_SIZE_MAX))
    {
        ERROR (self, "Bad %s.", "pattern size");
        return false;
    }

    in_size -= 2;

    if (DEBUG_FILE_S3M_LOAD)
    {
        DEBUG_INFO_ ("load_size=0x%hX",
            (uint16_t) in_size
        );
        DEBUG_INFO_ ("Reading %s...", "pattern data");
    }

    if (!load_s3m_load_data (self, _Self->in_buf, in_size, DEBUG_FILE_DUMP_PATTERN_DATA))
        return false;

    // target is pat_buf
    track = _Self->track;
    patterns = &track->patterns;
    pat = &pat_static;
    muspat_init (pat);
    pat->flags &= ~MUSPATFL_EM;
    pat->channels = track->channels_count;
    pat->rows = _S3M_PATTERN_ROWS;
    pat->size = _S3M_OUT_PATTERN_SIZE_MAX;
    pat->data.dos.ptr = _Self->pat_buf;

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("Converting %s...", "pattern data");

    if (!load_s3m_convert_pattern (self, _Self->in_buf, in_size, pat, true))
        return false;

    mem_size = muspat_get_packed_size (pat);    // get final size
    pat->size = mem_size;   // reduce (in most cases) memory usage

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("Done: packed_size=0x%hX",
            (uint16_t) mem_size
        );

    if (!((patterns->flags & MUSPATLFL_EM)
          && load_s3m_allocate_pattern_data_EM (self, pat, index, mem_size)))
    {
        if (!load_s3m_allocate_pattern_data (self, pat, index, mem_size))
        {
            ERROR (self, "Failed to allocate memory for %s.", "pattern data");
            return false;
        }
    }

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("Copying %s from buffer...", "pattern data");

    if (pat->flags & MUSPATFL_EM)
    {
        if (!map_EM_data (pat->data.em.handle, pat->data.em.offset,
                pat->size))
        {
            ERROR (self, "Failed to map EM for %s.", "pattern data");
            return false;
        }
        data = MK_FP (emsFrameSeg, get_EM_page_offset (pat->data.em.offset));
    }
    else
        data = pat->data.dos.ptr;

    memcpy (data, _Self->pat_buf, mem_size);    // save data

    muspatl_set (patterns, index, pat);     // save final pattern
    pat = muspatl_get (patterns, index);    // update pointer

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_dump_pattern_info (pat, index);

    if (DEBUG_FILE_DUMP_PATTERN_DATA)
    {
        char s[_BUF_SIZE];
        DEBUG_dump_pattern (pat, s, track->channels_count);
    }

    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_SUCCESS ();

    return true;
    #undef _BUF_SIZE
}

static bool __near load_s3m_load_patterns (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;
    MUSMOD *track;
    MUSPATLIST *patterns;
    int count, i;

    track = _Self->track;
    patterns = &track->patterns;
    count = muspatl_get_count (patterns);
    for (i = 0; i < count; i++)
        if (!load_s3m_load_pattern (_Self, i))
            return false;

    return true;
}

static void __near load_s3m_free_unused_patterns_pages (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;
    MUSMOD *track;
    MUSPATLIST *patterns;

    track = _Self->track;
    patterns = &track->patterns;
    if (patterns->flags & MUSPATLFL_EM)
    {
        uint16_t pages = get_EM_page (_Self->pat_EM.offset + EM_PAGE_SIZE - 1);
        uint16_t pages_free = _Self->pat_EM.pages - pages;

        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("EM pages: %u used, %u free", pages, pages_free);

        // try to free unused pages
        if (pages_free)
        {
            EMSHDL handle = patterns->handle;

            if (pages)
            {
                if (!emsResize (handle, pages))
                    DEBUG_WARN_ ("Failed to free unused EM pages for %s.", "patterns");
            }
            else
            {
                if (emsFree (handle))
                {
                    patterns->flags &= ~MUSPATLFL_EM;
                    patterns->handle = EMSBADHDL;
                }
                else
                    DEBUG_WARN_ ("Failed to free unused EM pages for %s.", "patterns");
            }
        }
    }
}

/*** Patterns order ***/

static bool __near load_s3m_load_patterns_order (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;
    _S3M_PATORDENT *buf;
    MUSMOD *track;
    MUSPATORDER *order;
    MUSPATORDENT index;
    uint16_t count, size, i;
    bool found;

    track = _Self->track;
    order = &track->order;
    count = _Self->ord_length;
    size = count * sizeof (_S3M_PATORDENT);

    buf = __new (size);
    if (!buf)
    {
        ERROR (self, "Failed to allocate memory for %s.", "patterns order");
        return false;
    }

    if (!load_s3m_seek (self, _get_orders_file_pos (_Self)))
        return false;

    if (!load_s3m_load_data (self, buf, size, DEBUG_FILE_S3M_LOAD))
    {
        ERROR (self, "Failed to read %s.", "patterns order");
        _delete (buf);
        return false;
    }

    for (i = 0; i < count; i++)
    {
        switch (buf[i])
        {
        case _S3M_PATORDENT_SKIP:
            index = MUSPATORDENT_SKIP;
            break;
        case _S3M_PATORDENT_END:
            index = MUSPATORDENT_END;
            break;
        default:
            if (buf[i] < _S3M_MAX_PATTERNS)
                index = buf[i];
            else
                index = MUSPATORDENT_END;
            break;
        }
        muspatorder_set (order, i, &index);
    }

    _delete (buf);

    // check order if there's one 'real' (playable) entry ...
    found = false;
    i = 0;
    while ((i < count) && (!found))
    {
        MUSPATORDENT *p = muspatorder_get (order, i);
        found = *p < _S3M_MAX_PATTERNS;
        i++;
    }

    if (!found)
    {
        ERROR (self, "%s", "Playable entry not found.");
        return false;
    }

    return true;
}

/**********************************************************************/

MUSMOD *load_s3m_load (LOADER_S3M *self, const char *name, bool use_EM)
{
    _S3M_LOADER *_Self = self;
    MUSMOD *track;
    PCMSMPLIST *samples;
    MUSINSLIST *instruments;
    MUSPATLIST *patterns;
    MUSPATORDER *order;

    if ((!_Self) || (!name))
    {
        ERROR (self, "%s", "Bad arguments.");
        return NULL;
    }

    if (use_EM && emsInstalled && emsGetFreePagesCount ())
        _Self->flags |= _S3M_LOADERFL_USE_EM;
    else
        _Self->flags &= ~_S3M_LOADERFL_USE_EM;

    track = _new (MUSMOD);
    if (!track)
    {
        ERROR (self, "Failed to allocate memory for %s.", "music module");
        return NULL;
    }
    musmod_init (track);
    _Self->track = track;
    samples = &track->samples;
    instruments = &track->instruments;
    patterns = &track->patterns;
    order = &track->order;

    load_s3m_clear_error (self);
    _Self->f = fopen (name, "rb");
    if (!_Self->f)
    {
        ERROR (self, "Failed to open file '%s'.", name);
        return NULL;
    }

    if (!load_s3m_read_header (_Self))
        return NULL;

    if (!musinsl_set_count (instruments, _Self->ins_count))
    {
        ERROR (self, "Failed to allocate memory for %s.", "instruments list");
        return NULL;
    }

    if (!muspatl_set_count (patterns, _Self->pat_count))
    {
        ERROR (self, "Failed to allocate memory for %s.", "patterns list");
        return NULL;
    }

    if (!muspatorder_set_count (order, _Self->ord_length))
    {
        ERROR (self, "Failed to allocate memory for %s.", "patterns order");
        return NULL;
    }

    if (!load_s3m_load_patterns_order (_Self))
        return NULL;

    if (!load_s3m_load_ins_offsets (_Self))
        return NULL;

    if (!load_s3m_load_pat_offsets (_Self))
        return NULL;

    _Self->in_buf = __new (_S3M_IN_PATTERN_SIZE_MAX);
    if (!_Self->in_buf)
    {
        ERROR (self, "Failed to allocate memory for %s.", "input pattern buffer");
        return NULL;
    }

    _Self->pat_buf = __new (_S3M_OUT_PATTERN_SIZE_MAX);
    if (!_Self->pat_buf)
    {
        ERROR (self, "Failed to allocate memory for %s.", "output pattern buffer");
        return NULL;
    }

    if (_Self->flags & _S3M_LOADERFL_USE_EM)
        load_s3m_alloc_EM_patterns_pages (_Self);

    if (!load_s3m_load_patterns (_Self))
        return NULL;

    if (_Self->flags & _S3M_LOADERFL_USE_EM)
        load_s3m_free_unused_patterns_pages (_Self);

    if (!load_s3m_load_ins_headers (_Self))
        return NULL;

    if (!pcmsmpl_set_count (samples, _Self->smp_count))
    {
        ERROR (self, "Failed to allocate memory for %s.", "samples list");
        return NULL;
    }

    if (!load_s3m_load_instruments (_Self))
        return NULL;

    if (_Self->flags & _S3M_LOADERFL_USE_EM)
        load_s3m_alloc_EM_samples_pages (_Self);

    if (!load_s3m_load_samples (_Self))
        return NULL;

    if (_Self->flags & _S3M_LOADERFL_USE_EM)
        load_s3m_free_unused_samples_pages (_Self);

    track->flags |= MUSMODFL_LOADED;

    _Self->track = NULL;
    return track;
}

/*** Free ***/

void load_s3m_free (LOADER_S3M *self)
{
    _S3M_LOADER *_Self = self;

    if (_Self)
    {
        _delete (_Self->in_buf);
        _delete (_Self->pat_buf);
        _delete (_Self->ins_offsets);
        _delete (_Self->ins_info);
        _delete (_Self->pat_offsets);
        if (_Self->f)
        {
            fclose (_Self->f);
            _Self->f = NULL;
        }
        if (_Self->track)
        {
            musmod_free (_Self->track);
            _delete (_Self->track);
        }
    }
}

void load_s3m_delete (LOADER_S3M **self)
{
    _delete (self);
}
