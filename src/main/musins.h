/* musins.h -- declarations for musins.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _MUSINS_H_INCLUDED
#define _MUSINS_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "dos/ems.h"
#include "dynarray.h"
#include "main/pcmsmp.h"

/*** Musical instrument ***/

typedef uint8_t music_instrument_type_t;
typedef music_instrument_type_t MUSINSTYPE;

#define MUSINST_EMPTY 0
#define MUSINST_PCM   1
#define MUSINST_ADLIB 2

#define MUSINS_VOLUME_MAX 128
#define MUSINS_VOLUME_BITS 7

#define MUSINS_TITLE_LEN 28

#pragma pack(push, 1);
typedef struct instrument_t
{
    MUSINSTYPE  type;
    uint8_t     volume;
    uint8_t     note_volume;
    void       *link;
    uint8_t     title[MUSINS_TITLE_LEN];
};
#pragma pack(pop);
typedef struct instrument_t MUSINS;

#define _musins_get_link(o)         (o)->link
#define _musins_get_sample(o)       (PCMSMP *) _musins_get_link (o)
#define _musins_set_sample(o, v)    _musins_get_link (o) = (void *) (v)

void       musins_init (MUSINS *self);
#define    musins_set_sample(o, v)  _musins_set_sample (o, v)
#define    musins_get_sample(o)     _musins_get_sample (o)
void       musins_free (MUSINS *self);

/*** Musical instruments list ***/

typedef struct music_instruments_list_t
{
    DYNARR list;
};
typedef struct music_instruments_list_t MUSINSLIST;

void    musinsl_init (MUSINSLIST *self);
#define musinsl_set_count(o, v) dynarr_set_size (&(o)->list, v)
#define musinsl_get_count(o)    dynarr_get_size (&(o)->list)
#define musinsl_set(o, i, v)    dynarr_set_item (&(o)->list, i, v)
#define musinsl_get(o, i)       dynarr_get_item (&(o)->list, i)
void    musinsl_free (MUSINSLIST *self);

/*** Debug ***/

#if DEBUG == 1

void DEBUG_dump_instrument_info (MUSINS *self, uint8_t index, PCMSMPLIST *samples);

#else   /* DEBUG != 1 */

#define DEBUG_dump_instrument_info(self, index, samples)

#endif  /* DEBUG != 1 */

#endif  /* !_MUSINS_H_INCLUDED */
