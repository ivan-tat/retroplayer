/* posttab.h -- declarations for posttab.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _POSTTAB_H_INCLUDED
#define _POSTTAB_H_INCLUDED 1

#include <stdint.h>
#include "defines.h"

extern int32_t amptab[256*3];

void amptab_set_volume (int32_t __far *amptab, uint8_t volume);

// "mb" is mixing buffer

extern void __far pascal _amplify32 (int32_t __far *amptab, int32_t __far *mb, uint16_t count);

#define CLIP32_TEMPLATE(name) extern void __far pascal name (void __far *outbuf, const int32_t __far *mb, uint16_t count)

CLIP32_TEMPLATE (_clip32_s8);
CLIP32_TEMPLATE (_clip32_u8);
CLIP32_TEMPLATE (_clip32_s16);
CLIP32_TEMPLATE (_clip32_u16);
CLIP32_TEMPLATE (_clip32_s8_lq);
CLIP32_TEMPLATE (_clip32_u8_lq);
CLIP32_TEMPLATE (_clip32_s16_lq);
CLIP32_TEMPLATE (_clip32_u16_lq);
CLIP32_TEMPLATE (_clip32_s8_lq_stereo);
CLIP32_TEMPLATE (_clip32_u8_lq_stereo);
CLIP32_TEMPLATE (_clip32_s16_lq_stereo);
CLIP32_TEMPLATE (_clip32_u16_lq_stereo);

#endif  /* !_POSTTAB_H_INCLUDED */
