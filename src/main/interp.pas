(* interp.pas -- Pascal declarations for "interp.c".

   This file is for linking compiled object files with Pascal linker.
   It will be deleted in future when we rewrite the project in C.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. *)

unit interp;

interface

(*$I defines.pas*)

(*$ifdef DEFINE_LOCAL_DATA*)

var
    interptab: Pointer;

(*$endif*)  (* DEFINE_LOCAL_DATA *)

procedure interptab_init;
procedure interptab_alloc;
procedure interptab_calc;
procedure interptab_free;

implementation

uses
    string_,
(*$ifdef DEBUG*)
    stdio,
(*$endif*)  (* DEBUG *)
    dos_,
    mixer;

(*$l interp.obj*)
procedure interptab_init; external;
procedure interptab_alloc; external;
procedure interptab_calc; external;
procedure interptab_free; external;

end.
