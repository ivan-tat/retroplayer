/* filldma.h -- declarations for filldma.pas.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _FILLDMA_H_INCLUDED
#define _FILLDMA_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/dos.h"
#include "main/musmod.h"
#include "main/mixchn.h"
#include "main/musmodps.h"
#include "main/mixer.h"
#include "main/fillvars.h"

#if DEBUG_WRITE_OUTPUT_STREAM == 1

// write sound output streams to files (mixing buffer and DMA buffer)
#include "cc/stdio.h"
extern FILE *_debug_stream[2];

void DEBUG_open_output_streams (void);
void DEBUG_close_output_streams (void);

#endif  /* DEBUG_WRITE_OUTPUT_STREAM == 1 */

void fill_DMAbuffer (PLAYSTATE *ps, MIXER *mixer, SNDDMABUF *outbuf);

#endif  /* !_FILLDMA_H_INCLUDED */
