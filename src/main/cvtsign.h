/* cvtsign.h - declarations for "cvtsign.c".

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _CVTSIGN_H_INCLUDED
#define _CVTSIGN_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "dos/ems.h"

void convert_sign_8 (void __far *data, uint32_t size);
void convert_sign_16 (void __far *data, uint32_t size);
bool convert_sign_8_EM (EMSHDL handle, uint32_t offset, uint32_t size);
bool convert_sign_16_EM (EMSHDL handle, uint32_t offset, uint32_t size);

#endif  /* !_CVTSIGN_H_INCLUDED */
