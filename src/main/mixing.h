/* mixing.h -- declarations for mixing.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _MIXING_H_INCLUDED
#define _MIXING_H_INCLUDED 1

#include <stdint.h>
#include "defines.h"
#include "main/musmodps.h"
#include "main/mixer.h"

void song_play (PLAYSTATE *ps, MIXER *mixer, uint16_t len);

#endif  /* !_MIXING_H_INCLUDED */
