/* cvtsi16e.inc - "convert_sign_16_EM" routine.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

bool convert_sign_16_EM (EMSHDL handle, uint32_t offset, uint32_t size)
{
    if (DEBUG_FILE_S3M_LOAD)
        DEBUG_INFO_ ("Requested: size=0x%lX, handle=0x%04hX, offset=0x%lX, pages=0x%04hX:0x%04hX-0x%04hX:0x%04hX",
            (uint32_t) size,
            (uint16_t) handle,
            (uint32_t) offset,
            (uint16_t) get_EM_page (offset),
            (uint16_t) get_EM_page_offset (offset),
            (uint16_t) get_EM_page (offset + size - 1),
            (uint16_t) get_EM_page_offset (offset + size - 1)
        );

    size /= 2;  /* 'size' is counter */

    while (size)
    {
        uint16_t seg_offset = get_EM_page_offset (offset);
        uint16_t count;
        uint16_t __far *p;

        /* counter register in DOS segment limit */
        if ((uint32_t) seg_offset + size * 2 > (1UL<<16))
            count = ((1UL<<16) - seg_offset) / 2;
        else
            count = size;

        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("Mapping: size=0x%lX, handle=0x%04hX, offset=0x%lX, pages=0x%04hX:0x%04hX-0x%04hX:0x%04hX",
                (uint32_t) count * 2,
                (uint16_t) handle,
                (uint32_t) offset,
                (uint16_t) get_EM_page (offset),
                (uint16_t) get_EM_page_offset (offset),
                (uint16_t) get_EM_page (offset + (uint32_t) count * 2 - 1),
                (uint16_t) get_EM_page_offset (offset + (uint32_t) count * 2 - 1)
            );

        if (!map_EM_data (handle, offset, (uint32_t) count * 2))
        {
            DEBUG_ERR ("Failed to map EM.");
            return false;
        }

        p = MK_FP (emsFrameSeg, seg_offset);

        if (DEBUG_FILE_S3M_LOAD)
            DEBUG_INFO_ ("Converting: count=0x%lX, address=0x%lX, pointer=0x%04hX:0x%04hX",
                (uint32_t) count,
                (uint32_t) FARPTR_TO_LONG (p),
                (uint16_t) FP_SEG (p),
                (uint16_t) FP_OFF (p)
            );

        offset += (uint32_t) count * 2;
        size -= count;

        do
        {
            *p ^= 0x8000;
            p++;
            count--;
        } while (count);
    }

    return true;
}
