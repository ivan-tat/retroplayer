/* musmodps.h -- declarations for musmodps.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _MUSMODPS_H_INCLUDED
#define _MUSMODPS_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "main/musdefs.h"
#include "main/musmod.h"
#include "main/mixchn.h"

/*** play state ***/

typedef uint8_t play_state_flags_t;
typedef play_state_flags_t PLAYSTATEFLAGS;

#define PLAYSTATEFL_PLAYING     (1 << 0)    // now playing (stopped otherwise)
#define PLAYSTATEFL_PATLOOP     (1 << 1)    // pattern loop
#define PLAYSTATEFL_SONGLOOP    (1 << 2)    // song loop
#define PLAYSTATEFL_SKIPENDMARK (1 << 3)    // skip "end" mark in patterns' order

#pragma pack(push, 1);
typedef struct play_state_t
{
    MUSMOD *track;
    MIXCHNLIST *channels;
    PLAYSTATEFLAGS flags;
    uint16_t rate;
    uint8_t  tempo;
    uint8_t  speed;
    uint8_t  global_volume;
    uint8_t  master_volume;
    uint16_t tick_spc;  // samples per channel (depends on rate and tempo)
    // position in song - you can change it while playing to jump arround
    uint8_t  order_start;   // start position
    uint8_t  order_last;    // last order to play
    uint8_t  order;
    uint8_t  pattern;
    uint8_t  row;
    uint8_t  tick;
    uint16_t tick_spc_counter; // samples per channel left to next tick
    // pattern loop
    uint8_t  patloop_count;
    uint8_t  patloop_start_row;
    // pattern delay
    uint8_t  patdelay_count;
};
#pragma pack(pop);
typedef struct play_state_t PLAYSTATE;

void       playstate_init (PLAYSTATE *self);
bool       playstate_alloc_channels (PLAYSTATE *self);
void       playstate_reset_channels (PLAYSTATE *self);
void       playstate_free_channels (PLAYSTATE *self);
void       playstate_set_speed (PLAYSTATE *self, uint8_t value);
void       playstate_set_tempo (PLAYSTATE *self, uint8_t value);
void       playstate_setup_patterns_order (PLAYSTATE *self);
int        playstate_find_next_pattern (PLAYSTATE *self, int index, int step);
void       playstate_set_pos (PLAYSTATE *self, uint8_t start_order, uint8_t start_row, bool keep);
void       playstate_set_initial_state (PLAYSTATE *self);
void       playstate_free (PLAYSTATE *self);

#endif  /* !_MUSMODPS_H_INCLUDED */
