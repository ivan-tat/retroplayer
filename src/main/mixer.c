/* mixer.c -- mixer functions.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "common.h"
#include "cc/i86.h"
#include "cc/string.h"
#include "cc/dos.h"
#include "debug.h"
#include "main/musdefs.h"
#include "main/mixer.h"

uint32_t _calc_sample_step(uint16_t period, uint16_t rate)
{
    long long int a = (long long int) MID_C_PERIOD * MID_C_RATE << 16;
    long int b = (long int)period * (long int)rate;
    b = a / b;
    return b;
}

#if DEFINE_LOCAL_DATA == 1

uint16_t ST3Periods[12] =
{
#include "main/nperiods.inc"
};

#endif  /* DEFINE_LOCAL_DATA == 1 */

/* Filling */

void fill_8(void *dest, uint8_t value, uint16_t count)
{
    memset(dest, value, count);
}

void fill_16(void *dest, uint16_t value, uint16_t count)
{
    uint16_t *p = dest;
    uint16_t n = count;

    while (n)
    {
        *p = value;
        p++;
        n--;
    }
}

void fill_32(void *dest, uint32_t value, uint16_t count)
{
    uint32_t *p = dest;
    uint16_t n = count;

    while (n)
    {
        *p = value;
        p++;
        n--;
    }
}

/* Sample buffer */

void smpbuf_init(SMPBUF *self)
{
    if (self)
    {
        self->buf = NULL;
        self->len = 0;
    }
}

bool smpbuf_alloc(SMPBUF *self, uint16_t len)
{
    if (self)
    {
        uint16_t seg;

        if (!_dos_allocmem(_dos_para(len * sizeof(int16_t)), &seg))
        {
            self->buf = MK_FP(seg, 0);
            self->len = len;
            return true;
        }
    }

    return false;
}

void smpbuf_free(SMPBUF *self)
{
    if (self)
        if (self->buf)
        {
            _dos_freemem(FP_SEG(self->buf));
            self->buf = NULL;
        }
}

#if DEBUG == 1

void _DEBUG_dump_smpbuf_info (const char *file, int line, const char *func, const char *name, SMPBUF *p)
{
    _DEBUG_dump_ptr (file, line, func, name, p);
    if (p)
    {
        _DEBUG_dump_ptr (file, line, func, "(SMPBUF)->buf", p->buf);
        _DEBUG_dump_u16 (file, line, func, "(SMPBUF)->len", &p->len);
    }
}

#endif  /* DEBUG == 1 */

/* Mixing buffer */

void mixbuf_init(MIXBUF *self)
{
    if (self)
    {
        self->buf = NULL;
        self->len = 0;
        self->channels = 0;
        self->samples_per_channel = 0;
    }
}

bool mixbuf_alloc(MIXBUF *self, uint16_t len)
{
    if (self)
    {
        uint16_t seg;

        if (!_dos_allocmem(_dos_para(len * sizeof(int32_t)), &seg))
        {
            self->buf = MK_FP(seg, 0);
            self->len = len;
            return true;
        }
    }

    return false;
}

uint16_t mixbuf_get_offset_from_count(MIXBUF *self, uint16_t value)
{
    if (self)
    {
        uint16_t result = value;

        if (self->channels == 2)
            result <<= 1;

        return result * sizeof(int32_t);
    }

    return 0;
}

uint16_t mixbuf_get_count_from_offset(MIXBUF *self, uint16_t value)
{
    if (self)
    {
        uint16_t result = value;

        if (self->channels == 2)
            result >>= 1;

        return result / sizeof(int32_t);
    }

    return 0;
}

void mixbuf_free(MIXBUF *self)
{
    if (self)
        if (self->buf)
        {
            _dos_freemem(FP_SEG(self->buf));
            self->buf = NULL;
        }
}

#if DEBUG == 1

void _DEBUG_dump_mixbuf_info (const char *file, int line, const char *func, const char *name, const MIXBUF *p)
{
    _DEBUG_dump_ptr (file, line, func, name, p);
    if (p)
    {
        _DEBUG_dump_ptr (file, line, func, "(MIXBUF)->buf", p->buf);
        _DEBUG_dump_u16 (file, line, func, "(MIXBUF)->len", &p->len);
        _DEBUG_dump_u8  (file, line, func, "(MIXBUF)->channels", &p->channels);
        _DEBUG_dump_u16 (file, line, func, "(MIXBUF)->samples_per_channel", &p->samples_per_channel);
    }
}

#endif  /* DEBUG == 1 */

/*** Mixer ***/

void mixer_init (MIXER *self)
{
    memset (self, 0, sizeof (MIXER));
}

static bool __near _mixer_alloc_smpbuf (MIXER *self)
{
    SMPBUF *sb = _new (SMPBUF);

    if (!sb)
        return false;

    self->smpbuf = sb;
    self->flags |= MIXERBUFMASK_SMPBUF;

    if (!smpbuf_alloc (sb, self->num_spc))
        return false;

    return true;
}

static void __near _mixer_free_smpbuf (MIXER *self)
{
    SMPBUF *sb = self->smpbuf;

    if (sb)
    {
        if (self->flags & MIXERBUFMASK_SMPBUF)
        {
            smpbuf_free (sb);
            _delete (sb);
            self->flags &= ~MIXERBUFMASK_SMPBUF;
        }
        self->smpbuf = NULL;
    }
}

static bool __near _mixer_alloc_mixbuf (MIXER *self)
{
    MIXBUF *mb = _new (MIXBUF);

    if (!mb)
        return false;

    self->mixbuf = mb;
    self->flags |= MIXERBUFMASK_MIXBUF;

    if (!mixbuf_alloc (mb, self->num_channels * self->num_spc))
        return false;

    return true;
}

static void __near _mixer_free_mixbuf (MIXER *self)
{
    MIXBUF *mb = self->mixbuf;

    if (mb)
    {
        if (self->flags & MIXERBUFMASK_MIXBUF)
        {
            mixbuf_free (mb);
            _delete (mb);
            self->flags &= ~MIXERBUFMASK_MIXBUF;
        }
        self->mixbuf = NULL;
    }
}

bool mixer_alloc_buffers (MIXER *self, MIXERBUFMASK mask)
{
    if (mask & MIXERBUFMASK_SMPBUF)
    {
        _mixer_free_smpbuf (self);
        if (!_mixer_alloc_smpbuf (self))
            return false;
    }

    if (mask & MIXERBUFMASK_MIXBUF)
    {
        _mixer_free_mixbuf (self);
        if (!_mixer_alloc_mixbuf (self))
            return false;
    }

    return true;
}

void mixer_free_buffers (MIXER *self, MIXERBUFMASK mask)
{
    if (mask & MIXERBUFMASK_SMPBUF)
        _mixer_free_smpbuf (self);

    if (mask & MIXERBUFMASK_MIXBUF)
        _mixer_free_mixbuf (self);
}

void mixer_free (MIXER *self)
{
    _mixer_free_smpbuf (self);
    _mixer_free_mixbuf (self);
}

#if DEBUG == 1

void _DEBUG_dump_mixer_info (const char *file, int line, const char *func, const char *name, const MIXER *p)
{
    _DEBUG_dump_ptr (file, line, func, name, p);
    if (p)
    {
        _DEBUG_dump_u8  (file, line, func, "(MIXER)->flags", &p->flags);
        _DEBUG_dump_u8  (file, line, func, "(MIXER)->quality", &p->quality);
        _DEBUG_dump_u8  (file, line, func, "(MIXER)->num_channels", &p->num_channels);
        _DEBUG_dump_u16 (file, line, func, "(MIXER)->num_spc", &p->num_spc);
        _DEBUG_dump_smpbuf_info (file, line, func, "(MIXER)->(SMPBUF)smpbuf", p->smpbuf);
        _DEBUG_dump_mixbuf_info (file, line, func, "(MIXER)->(MIXBUF)mixbuf", p->mixbuf);
    }
}

#endif  /* DEBUG == 1 */
