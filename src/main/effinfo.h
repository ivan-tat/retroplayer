/* effinfo.h -- declarations for "effinfo.c".

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _EFFINFO_H_INCLUDED
#define _EFFINFO_H_INCLUDED 1

#include "defines.h"
#include "main/mixchn.h"

void chn_effGetName (MIXCHN *chn, char *__s, unsigned __maxlen);

#endif  /* !_EFFINFO_H_INCLUDED */
