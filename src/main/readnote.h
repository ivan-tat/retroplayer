/* readnote.h -- declarations for readnote.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _READNOTE_H_INCLUDED
#define _READNOTE_H_INCLUDED 1

#include "defines.h"
#include "main/musmod.h"

void readnewnotes (PLAYSTATE *ps);

#endif  /* !_READNOTE_H_INCLUDED */
