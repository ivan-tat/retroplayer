/* mixchn.c -- mixing channel handling library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdint.h>
#include "defines.h"
#include "main/effvars.h"
#include "main/effects.h"
#include "main/musmod.h"
#include "main/mixer.h"
#include "main/mixchn.h"

void mixchn_init (MIXCHN *self)
{
    if (self)
        memset (self, 0, sizeof (MIXCHN));
}

void mixchn_set_sample_period_limits (MIXCHN *self, uint16_t rate, bool amiga)
{
    unsigned lo, hi;

    if (amiga)
    {
         lo = getNotePeriod((5 << 4) + 11); /* B-5 */
         hi = getNotePeriod((3 << 4) +  0); /* C-3 */
    }
    else
    {
         lo = getNotePeriod((7 << 4) + 11); /* B-7 */
         hi = getNotePeriod((0 << 4) +  0); /* C-0 */
    }
     self->wSmpPeriodLow  = (unsigned long)(MID_C_RATE * (unsigned long)(lo)) / rate;
     self->wSmpPeriodHigh = (unsigned long)(MID_C_RATE * (unsigned long)(hi)) / rate;
}

uint16_t mixchn_check_sample_period (MIXCHN *self, uint32_t value)
{
    if (value < self->wSmpPeriodLow)
        value = self->wSmpPeriodLow;
    else
        if (value > self->wSmpPeriodHigh)
            value = self->wSmpPeriodHigh;

    return value;
}

void mixchn_setup_sample_period (MIXCHN *self, uint32_t period, uint16_t mixrate)
{
    if (period)
    {
        period = mixchn_check_sample_period (self, period);
        self->wSmpPeriod = period;
        self->dSmpStep = _calc_sample_step (period, mixrate);
    }
    else
    {
        self->wSmpPeriod = 0;
        self->dSmpStep = 0;
    }
}

void mixchn_reset_wave_tables (MIXCHN *self)
{
    if (self)
    {
        self->bVibType = 0;
        self->bTrmType = 0;
    }
}

void mixchn_free (MIXCHN *self)
{
    return;
}

static bool near setup_sample (MIXCHN *chn, PCMSMP *smp, bool limits)
{
    if (smp && (smp->flags & PCMSMPFL_AVAIL) && smp->rate)
    {
        unsigned flags;

        chn->sample = smp;
        flags = 0;
        if (smp->flags & PCMSMPFL_16BITS)
            flags |= PLAYSMPFL_16BITS;
        if ((smp->flags & PCMSMPFL_LOOP_MASK) != PCMSMPFL_LOOP_NONE)
            flags |= PLAYSMPFL_LOOP;
        chn->bSmpFlags = flags;
        chn->dSmpLoopStart = smp->loop_start;
        if (flags & PLAYSMPFL_LOOP)
            chn->dSmpLoopEnd = smp->loop_end;
        else
            chn->dSmpLoopEnd = smp->length;
        chn->dSmpStart = 0;
        mixchn_set_sample_period_limits (chn, smp->rate, limits);
        return true;
    }
    else
        return false;
}

void chn_setupInstrument (MIXCHN *chn, MUSMOD *track, uint8_t insNum)
{
    MUSINSLIST *instruments = &track->instruments;
    MUSINS *ins = musinsl_get (instruments, insNum - 1);

    if ((ins->type == MUSINST_PCM)
    && (setup_sample (chn, musins_get_sample (ins), track->flags & MUSMODFL_AMIGA_LIMITS)))
    {
        chn->instrument_num = insNum;
        chn->note_volume = ins->note_volume;
        chn->instrument = ins;
    }
    else
        chn->instrument_num = 0;    // don't play it - it's wrong !
}

uint16_t chn_calcNotePeriod (MIXCHN *chn, uint32_t rate, uint8_t note)
{
    unsigned period = (unsigned long)(MID_C_RATE * (unsigned long)getNotePeriod(note)) / rate;

    return mixchn_check_sample_period(chn, period);
}

uint32_t chn_calcNoteStep (MIXCHN *chn, uint32_t rate, uint8_t note, uint16_t mixrate)
{
    unsigned period = chn_calcNotePeriod(chn, rate, note);

    if (period)
        return _calc_sample_step (period, mixrate);
    else
        return 0;
}

void chn_setupNote (MIXCHN *chn, uint8_t note, uint16_t mixrate, bool keep)
{
    chn->note = note;
    chn->wSmpPeriod = 0;    // clear it first - just to make sure we really set it
    if (chn->instrument_num)
    {
        MUSINS *ins = chn->instrument;
        if (ins->type == MUSINST_PCM)
        {
            PCMSMP *smp = musins_get_sample (ins);
            if (smp && (smp->flags & PCMSMPFL_AVAIL))
            {
                uint32_t rate = smp->rate;
                if (rate)
                {
                    mixchn_setup_sample_period(chn, chn_calcNotePeriod(chn, rate, note), mixrate);
                    if (! keep)
                    {
                        // restart instrument
                        chn->rSmpPos.frac = 0;
                        chn->rSmpPos.pos = chn->dSmpStart;
                        chn->flags |= MIXCHNFL_PLAYING;
                    }
                }
            }
        }
    }
}

/*** Mixing channels list ***/

void _mixchnl_init_item (void *self, void *item)
{
    mixchn_init ((MIXCHN *) item);
}

void _mixchnl_free_item (void *self, void *item)
{
    mixchn_free ((MIXCHN *) item);
}

void mixchnl_init (MIXCHNLIST *self)
{
    if (self)
    {
        memset (self, 0, sizeof (MIXCHNLIST));
        dynarr_init (&self->list, self, sizeof (MIXCHN), _mixchnl_init_item, _mixchnl_free_item);
    }
}

void mixchnl_free (MIXCHNLIST *self)
{
    if (self)
    {
        dynarr_free (&self->list);
        mixchnl_init (self);    // clear
    }
}
