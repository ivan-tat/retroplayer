(* pcmsmp.pas -- Pascal declarations for pcmsmp.c.

   This file is for linking compiled object files with Pascal linker.
   It will be deleted in future when we rewrite the project in C.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. *)

unit pcmsmp;

interface

(*$I defines.pas*)

procedure pcmsmp_init;
procedure pcmsmp_needs_expansion;
procedure pcmsmp_calc_expansion;
procedure pcmsmp_expand;
procedure pcmsmp_free;

procedure pcmsmpl_init;
procedure pcmsmpl_free;

(*$ifdef DEBUG*)

procedure DEBUG_dump_sample_info;

(*$endif*)  (* DEBUG *)

implementation

uses
    i86,
    string_,
    stdio,
    dos_,
    debug,
    ems,
    dynarray;

(*$l pcmsmp.obj*)

procedure pcmsmp_init; external;
procedure pcmsmp_needs_expansion; external;
procedure pcmsmp_calc_expansion; external;
procedure pcmsmp_expand; external;
procedure pcmsmp_free; external;

procedure pcmsmpl_init; external;
procedure pcmsmpl_free; external;

(*$ifdef DEBUG*)

procedure DEBUG_dump_sample_info; external;

(*$endif*)  (* DEBUG *)

end.
