/* voltab.h -- declarations for voltab.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _VOLTAB_H_INCLUDED
#define _VOLTAB_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

typedef int16_t voltab_t[2][64][256];

extern voltab_t *volumetableptr;

void voltab_init(void);
bool voltab_alloc(void);
void voltab_calc(void);
void voltab_free(void);

#endif  /* !_VOLTAB_H_INCLUDED */
