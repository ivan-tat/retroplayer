(* musdefs.pas -- Pascal declarations for musdefs.h.

   This file is for linking compiled object files with Pascal linker.
   It will be deleted in future when we rewrite the project in C.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. *)

unit musdefs;

interface

(*$I defines.pas*)

procedure range_0_64;

implementation

(*$L musdefs.obj*)

procedure range_0_64; external;

end.
