(* cvtsign.pas - Pascal declarations for "cvtsign.c".

   This file is for linking compiled object files with Pascal linker.
   It will be deleted in future when we rewrite the project in C.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. *)

unit cvtsign;

interface

(*$I defines.pas*)

procedure convert_sign_8;
procedure convert_sign_16;
procedure convert_sign_8_EM;
procedure convert_sign_16_EM;

implementation

uses
(*$ifdef DEBUG*)
    debug,
(*$endif*)  (* DEBUG *)
    i86,
    ems;

(*$l cvtsign.obj*)

procedure convert_sign_8; external;
procedure convert_sign_16; external;
procedure convert_sign_8_EM; external;
procedure convert_sign_16_EM; external;

end.
