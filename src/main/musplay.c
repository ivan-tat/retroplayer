/* musplay.c -- main library for playing music modules.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include "defines.h"
#include "cc/stdio.h"
#include "cc/stdlib.h"
#include "cc/string.h"
#include "cc/dos.h"
#include "cc/hw/cpu.h"
#include "common.h"
#include "hw/sb/sbctl.h"
#include "main/musins.h"
#include "main/muspat.h"
#include "main/musmod.h"
#include "main/loads3m.h"
#include "main/musmodps.h"
#include "main/mixchn.h"
#include "main/effects.h"
#include "main/mixer.h"
#include "main/interp.h"
#include "main/voltab.h"
#include "main/posttab.h"
#include "main/fillvars.h"
#include "main/filldma.h"
#include "main/musplay.h"

#if DEFINE_LOCAL_DATA == 1

const char *PLAYER_VERSION = "1.70.1";

#endif  /* DEFINE_LOCAL_DATA == 1 */

#define _PLAYER_ERROR_LEN 128

// Current index to generate unique EM handle name to save EM mapping while in ISR
static uint16_t _isr_index;

typedef struct _music_player_t _MUSPLAYER;

#pragma pack(push, 1);
typedef struct play_isr_param_t {
    _MUSPLAYER *player;
    bool busy;
    SNDDMABUF *dmabuf;
};
#pragma pack(pop);
typedef struct play_isr_param_t PLAYISRPARAM;

typedef uint8_t _music_player_flags_t;
typedef _music_player_flags_t _MUSPLAYERFLAGS;

#define _MUSPLAYERFL_USE_EM     (1 << 0)
#define _MUSPLAYERFL_DEVICE     (1 << 1)

#pragma pack(push, 1);
typedef struct _music_player_t
{
    char error[_PLAYER_ERROR_LEN];
    _MUSPLAYERFLAGS flags;
    SBDEV *device;
    SNDDMABUF *sndbuf;
    uint8_t sound_buffer_fps;
    bool     mode_set;
    uint8_t  mode_bits;
    bool     mode_signed;
    uint8_t  mode_channels;
    uint16_t mode_rate;
    bool     mode_lq;
    PLAYSTATE *play_state;
    SMPBUF *smpbuf;
    MIXBUF *mixbuf;
    MIXER *mixer;
    EMMAPSTATE EM_map_state;
    PLAYISRPARAM play_isr_param;
};
#pragma pack(pop);

/* Each element is a pointer */

typedef struct pointers_list_t
{
    DYNARR list;
};
typedef struct pointers_list_t PTRLIST;

#define _ptrlist_set_count(o, v) dynarr_set_size (&(o)->list, v)
#define _ptrlist_get_count(o)    dynarr_get_size (&(o)->list)
#define _ptrlist_set(o, i, v)    dynarr_set_item (&(o)->list, i, v)
#define _ptrlist_get(o, i)       dynarr_get_item (&(o)->list, i)

static void __near
        ptrlist_init (PTRLIST *self);
#define ptrlist_set_count(o, v) _ptrlist_set_count (o, v)
#define ptrlist_get_count(o)    _ptrlist_get_count (o)
#define ptrlist_set(o, i, v)    _ptrlist_set (o, i, v)
#define ptrlist_get(o, i)       _ptrlist_get (o, i)
static void __near
        ptrlist_free (PTRLIST *self);

void _ptrlist_init_item (void *self, void *item)
{
    *(void **) item = NULL;
}

void _ptrlist_free_item (void *self, void *item)
{
    *((void **) item) = NULL;
}

static void __near ptrlist_init (PTRLIST *self)
{
    if (self)
        dynarr_init (&self->list, self, sizeof (MUSMOD *), _ptrlist_init_item, _ptrlist_free_item);
}

static void __near ptrlist_free (PTRLIST *self)
{
    if (self)
        dynarr_free (&self->list);
}

/*** Musical modules list ***/

typedef PTRLIST MUSMODLIST;

#define _musmodlist_init(o)         ptrlist_init (o)
#define _musmodlist_set_count(o, v) ptrlist_set_count (o, v)
#define _musmodlist_get_count(o)    ptrlist_get_count (o)
#define _musmodlist_set(o, i, v)    ptrlist_set (o, i, v)
#define _musmodlist_get(o, i)       (MUSMOD **) ptrlist_get (o, i)
#define _musmodlist_free(o)         ptrlist_free (o)

static MUSMODLIST _musmodlist;

static void __near _init_modules (void)
{
    _musmodlist_init (&_musmodlist);
}

static bool __near _add_module (MUSMOD *track)
{
    int count = _musmodlist_get_count (&_musmodlist);

    if (!_musmodlist_set_count (&_musmodlist, count + 1))
        return false;

    _musmodlist_set (&_musmodlist, count, &track);
    return true;
}

static void __near _free_module (MUSMOD *track)
{
    musmod_free (track);
    _delete (track);
}

static void __near _free_modules (void)
{
    int i, count = _musmodlist_get_count (&_musmodlist);

    for (i = 0; i < count; i++)
    {
        MUSMOD **track = _musmodlist_get (&_musmodlist, i);

        if (track && *track)
        {
            _free_module (*track);
            _musmodlist_set (&_musmodlist, i, NULL);
        }
    }

    _musmodlist_free (&_musmodlist);
}

/*** Sound ***/

/* IRQ routines */

/* Type: SoundHWISRCallback_t */
static void ISR_play (void *param)
{
    PLAYISRPARAM *self;
    _MUSPLAYER *player;
    SNDDMABUF *dmabuf;
    self = (PLAYISRPARAM *) param;
    player = self->player;
    dmabuf = self->dmabuf;

    if (self->busy)
    {
        dmabuf->flags |= SNDDMABUFFL_SLOW;
        dmabuf->frameActive = (dmabuf->frameActive + 1) & (dmabuf->framesCount - 1);
    }
    else
    {
        bool err;

        self->busy = true;

        dmabuf->frameActive = (dmabuf->frameActive + 1) & (dmabuf->framesCount - 1);

        err = false;

        if (player->flags & _MUSPLAYERFL_USE_EM)
        {
            err = true;
            if (save_EM_map_state (&player->EM_map_state))
                err = false;
        }

        fill_DMAbuffer (player->play_state, player->mixer, dmabuf);

        if ((player->flags & _MUSPLAYERFL_USE_EM) && !err)
            restore_EM_map_state (&player->EM_map_state);

        self->busy = false;
    }
}

/*** Player ***/

/* Error handling */

static void __near _player_clear_error (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    _Self->error[0] = 0;
}

bool player_is_error (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    return _Self->error[0] != 0;
}

const char *player_get_error (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    if (_Self->error[0] != 0)
        return _Self->error;
    else
        return NULL;
}

#if DEBUG == 1

static void __near player_set_error (MUSPLAYER *self, const char *method, int line, const char *format, ...)
{
    va_list ap;
    _MUSPLAYER *_Self = self;

    va_start(ap, format);
    vsnprintf (_Self->error, _PLAYER_ERROR_LEN, format, ap);
    va_end (ap);
    _DEBUG_LOG (DBGLOG_ERR, __FILE__, line, method, "%s", _Self->error);
}

#define ERROR(f, ...) player_set_error (self, __func__, __LINE__, f, __VA_ARGS__)

#else   /* DEBUG != 1 */

static void __near player_set_error (MUSPLAYER *self, const char *format, ...)
{
    va_list ap;
    _MUSPLAYER *_Self = self;

    va_start(ap, format);
    vsnprintf (_Self->error, _PLAYER_ERROR_LEN, format, ap);
    va_end (ap);
}

#define ERROR(f, ...) player_set_error (self, f, __VA_ARGS__)

#endif  /* DEBUG != 1 */

/* Initialization */

MUSPLAYER *player_new (void)
{
    return _new (_MUSPLAYER);
}

void player_clear (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    if (_Self)
    {
        memset (_Self, 0, sizeof (_MUSPLAYER));

        /* Device */
        //_Self->device = NULL;
        //_Self->sndbuf = NULL;

        /* Player */
        _Self->sound_buffer_fps = 70;
        //_Self->mode_set = false;
        //_Self->mode_bits = 0;
        //_Self->mode_signed = false;
        //_Self->mode_channels = 0;
        //_Self->mode_rate = 0;
        //_Self->mode_lq = false;

        /* Mixer */
        //_Self->smpbuf = NULL;
        //_Self->mixbuf = NULL;
        //_Self->mixer = NULL;

        /* ISR */
        clear_EM_map_state (&_Self->EM_map_state);

        /* Active track */
        //_Self->play_state = NULL;
    }
}

bool player_init (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    DEBUG_BEGIN ();

    if (_Self)
    {
        player_clear (_Self);

        if (emsInstalled)
            _Self->flags |= _MUSPLAYERFL_USE_EM;
        else
            _Self->flags &= ~_MUSPLAYERFL_USE_EM;

        _Self->sndbuf = _new (SNDDMABUF);
        if (!_Self->sndbuf)
        {
            ERROR ("Failed to allocate %s.", "sound buffer object");
            return false;
        }
        snddmabuf_init (_Self->sndbuf);

        if (!_Self->sndbuf->buf)
            if (!snddmabuf_alloc (_Self->sndbuf, DMA_BUF_SIZE_MAX))
            {
                ERROR ("Failed to initialize %s.", "DMA buffer");
                return false;
            }

        // Music modules
        _init_modules ();

        // ISR
        if (_Self->flags & _MUSPLAYERFL_USE_EM)
        {
            EMSNAME name;

            snprintf (name, sizeof (EMSNAME), "map%04hX", (uint16_t) _isr_index);

            if (!init_EM_map_state (&_Self->EM_map_state, name))
            {
                ERROR ("Failed to allocate %s.", "EM handle for mapping");
                return false;
            }

            _isr_index++;
        }

        interptab_init ();

        if (!interptab)
        {
            if (!interptab_alloc ())
            {
                ERROR ("Failed to initialize %s.", "interpolation table");
                return false;
            }
            interptab_calc ();
        }

        voltab_init ();

        if (!volumetableptr)
        {
            if (!voltab_alloc ())
            {
                ERROR ("Failed to initialize %s.", "volume table");
                return false;
            }
            voltab_calc ();
        }

        DEBUG_SUCCESS ();
        return true;
    }
    else
    {
        DEBUG_ERR ("self is NULL!");
        return false;
    }
}

void player_set_EM_usage (MUSPLAYER *self, bool value)
{
    _MUSPLAYER *_Self = self;

    if (value && emsInstalled)
        _Self->flags |= _MUSPLAYERFL_USE_EM;
    else
        _Self->flags &= ~_MUSPLAYERFL_USE_EM;
}

bool player_is_EM_in_use (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    return _Self->flags & _MUSPLAYERFL_USE_EM;
}

/* Output device */

bool player_init_device (MUSPLAYER *self, SNDDEVTYPE type, SNDDEVSETMET method)
{
    _MUSPLAYER *_Self = self;
    bool result;

    DEBUG_BEGIN ();

    switch (type)
    {
    case SNDDEVTYPE_SB:
        _Self->device = sb_new();
        if (!_Self->device)
        {
            ERROR ("Failed to create %s.", "sound device object");
            return false;
        }
        sb_init (_Self->device);
        break;
    default:
        ERROR ("Unknown %s.", "device type");
        return false;
    }

    switch (method)
    {
    case SNDDEVSETMET_MANUAL:
        result = true;
        break;
    case SNDDEVSETMET_DETECT:
        result = sb_conf_detect (_Self->device);
        break;
    case SNDDEVSETMET_ENV:
        result = sb_conf_env (_Self->device);
        break;
    case SNDDEVSETMET_INPUT:
        result = sb_conf_input (_Self->device);
        break;
    default:
        ERROR ("Unknown %s.", "method");
        sb_free (_Self->device);
        return false;
    }

    if (result)
    {
        DEBUG_SUCCESS ();
        _Self->flags |= _MUSPLAYERFL_DEVICE;
        return true;
    }
    else
    {
        ERROR ("%s", "No sound device.");
        sb_free (_Self->device);
        return false;
    }
}

char *player_device_get_name (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    if (_Self->flags & _MUSPLAYERFL_DEVICE)
        return sb_get_name (_Self->device);
    else
        return NULL;
}

void player_device_dump_conf (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    if (_Self->flags & _MUSPLAYERFL_DEVICE)
        sb_conf_dump (_Self->device);
}

SNDDMABUF *player_get_sound_buffer (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    return _Self->sndbuf;
}

void player_set_sound_buffer_fps (MUSPLAYER *self, uint8_t value)
{
    _MUSPLAYER *_Self = self;

    _Self->sound_buffer_fps = value;
}

bool player_set_mode (MUSPLAYER *self, bool f_16bits, bool f_stereo, uint16_t rate, bool LQ)
{
    _MUSPLAYER *_Self = self;

    DEBUG_BEGIN ();

    if (f_16bits)
    {
        _Self->mode_bits = 16;
        _Self->mode_signed = true;
    }
    else
    {
        _Self->mode_bits = 8;
        _Self->mode_signed = false;
    }

    if (f_stereo)
        _Self->mode_channels = 2;
    else
        _Self->mode_channels = 1;

    _Self->mode_rate = rate;
    _Self->mode_lq = LQ;
    _Self->mode_set = true;

    DEBUG_SUCCESS ();
    return true;
}

uint16_t player_get_output_rate (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    return _Self->mode_rate;
}

uint8_t player_get_output_channels (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    return _Self->mode_channels;
}

uint8_t player_get_output_bits (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    return _Self->mode_bits;
}

bool player_get_output_lq (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    return _Self->mode_lq;
}

static bool __near _player_setup_outbuf(MUSPLAYER *self, SNDDMABUF *outbuf, uint16_t spc)
{
    _MUSPLAYER *_Self = self;

    DEBUG_BEGIN ();

    if (_Self->mode_set)
    {
        uint16_t size;
        uint16_t i, count;

        outbuf->flags = 0;

        if (_Self->mode_lq)
            outbuf->flags |= SNDDMABUFFL_LQ;

        set_sample_format(&(outbuf->format),
            _Self->mode_bits, _Self->mode_signed, _Self->mode_channels);

        size = spc;
        if (_Self->mode_bits == 16)
            size *= 2;

        if (_Self->mode_channels == 2)
            size *= 2;

        outbuf->frameSize = size;

        i = outbuf->buf->size / size;
        count = 1;
        while (count < i)
            count *= 2;

        if (_Self->mode_lq)
            count /= 2;

        outbuf->framesCount = count / 2;

        DEBUG_SUCCESS ();
        return true;
    }
    else
    {
        ERROR ("No %s was set.", "play mode");
        return false;
    }
}

bool player_play_start (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;
    PLAYSTATE *ps;
    SNDDMABUF *outbuf;
    uint16_t frame_size;

    DEBUG_BEGIN ();

    if (!(_Self->flags & _MUSPLAYERFL_DEVICE))
    {
        ERROR ("No %s was set.", "sound device");
        return false;
    }

    if (!_Self->mode_set)
    {
        ERROR ("No %s was set.", "play mode");
        return false;
    }

    if (!_Self->mixer)
    {
        ERROR ("No %s was set.", "mixer");
        return false;
    }

    ps = _Self->play_state;
    if (!ps)
    {
        ERROR ("No %s was set.", "active track");
        return false;
    }

    ps->flags &= ~PLAYSTATEFL_PLAYING;  // stop for setup

    // 1. Setup output mode

    sb_set_transfer_mode (_Self->device, _Self->mode_rate, _Self->mode_channels, _Self->mode_bits, _Self->mode_signed);
    _Self->mode_rate     = sb_mode_get_rate (_Self->device);
    _Self->mode_channels = sb_mode_get_channels (_Self->device);
    _Self->mode_bits     = sb_mode_get_bits (_Self->device);
    _Self->mode_signed   = sb_mode_is_signed (_Self->device);

    ps->rate = _Self->mode_lq ? _Self->mode_rate / 2 : _Self->mode_rate;

    // Setup playing state
    playstate_setup_patterns_order (ps);
    playstate_set_initial_state (ps);   // depends on rate, master volume affects mixer tables
    playstate_set_pos (ps, ps->order_start, 0, false);
    playstate_reset_channels (ps);

    // 2. Setup mixer mode

    _Self->mixbuf->channels = _Self->mode_channels;
    _Self->mixbuf->samples_per_channel =
        ((1000000L / (uint16_t)(1000000L / ps->rate)) / _Self->sound_buffer_fps) + 1;

    // 3. Setup output buffer

    outbuf = _Self->sndbuf;

    if (!_player_setup_outbuf (self, outbuf, _Self->mixbuf->samples_per_channel))
    {
        DEBUG_ERR_ ("Failed to setup %s.", "output buffer");
        return false;
    }

    frame_size = outbuf->frameSize;
    if (_Self->mode_lq)
        frame_size *= 2;

    _Self->play_isr_param.busy = false;
    _Self->play_isr_param.player = _Self;
    _Self->play_isr_param.dmabuf = _Self->sndbuf;
    sb_set_transfer_buffer (_Self->device, outbuf->buf->data, frame_size, outbuf->framesCount, true, &ISR_play, &_Self->play_isr_param);

    // 4. Setup mixer tables

    amptab_set_volume (amptab, ps->master_volume);

    ps->flags |= PLAYSTATEFL_PLAYING;   // resume playing

    // 5. Prefill output buffer

    outbuf->frameLast = -1;
    outbuf->frameActive = outbuf->framesCount - 1;

    DEBUG_dump_snddmabuf_info ("(SNDDMABUF)outbuf", outbuf);
    DEBUG_dump_mixer_info ("(MIXER)mixer", _Self->mixer);

    DEBUG_INFO_ ("fill_DMAbuffer %s...", "start");

    fill_DMAbuffer (ps, _Self->mixer, outbuf);

    DEBUG_INFO_ ("fill_DMAbuffer %s...", "done");

    // 6. Start sound

    if (!sb_transfer_start (_Self->device))
    {
        ERROR ("%s", "Failed to start transfer.");
        return false;
    }

    DEBUG_SUCCESS ();
    return true;
}

void player_play_pause (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    if (_Self->device)
        sb_transfer_pause (_Self->device);
}

void player_play_continue (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    if (_Self->device)
        sb_transfer_continue (_Self->device);
}

void player_play_stop (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    if (_Self->device)
        sb_transfer_stop (_Self->device);
}

uint16_t player_get_buffer_pos (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    if (_Self->device)
        return sb_get_buffer_pos (_Self->device);
    else
        return 0;
}

void player_free_device (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    DEBUG_BEGIN ();

    if (_Self->device)
    {
        sb_free (_Self->device);
        sb_delete (&_Self->device);
    }

    _Self->flags &= ~_MUSPLAYERFL_DEVICE;

    DEBUG_END ();
}

/* Mixer */

bool player_init_mixer (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    DEBUG_BEGIN ();

    if (!isCPU_i386 ())
    {
        ERROR ("%s is not supported.", "CPU");
        return false;
    }

    if (!_Self->mixer)
    {
        _Self->mixer = _new (MIXER);
        if (!_Self->mixer)
        {
            ERROR ("Failed to allocate %s.", "sound mixer object");
            return false;
        }
        mixer_init (_Self->mixer);
    }

    if (!_Self->smpbuf)
    {
        _Self->smpbuf = _new (SMPBUF);
        if (!_Self->smpbuf)
        {
            ERROR ("Failed to allocate %s.", "sample buffer object");
            return false;
        }
        smpbuf_init (_Self->smpbuf);
    }
    _Self->mixer->smpbuf = _Self->smpbuf;

    if (!_Self->mixbuf)
    {
        _Self->mixbuf = _new (MIXBUF);
        if (!_Self->mixbuf)
        {
            ERROR ("Failed to allocate %s.", "mixing buffer object");
            return false;
        }
        mixbuf_init (_Self->mixbuf);
    }
    _Self->mixer->mixbuf = _Self->mixbuf;

    // allocate internal buffers

    if (_Self->sndbuf)
    {
        uint16_t len = _Self->sndbuf->buf->size / 2; // half DMA transfer buffer size (but in samples)
        SMPBUF *sb;
        MIXBUF *mb;

        sb = _Self->mixer->smpbuf;
        if (!sb->buf)
            if (!smpbuf_alloc (sb, len))
            {
                ERROR ("Failed to initialize %s.", "sample buffer");
                return false;
            }

        mb = _Self->mixer->mixbuf;
        if (!mb->buf)
            if (!mixbuf_alloc (mb, len))
            {
                ERROR ("Failed to initialize %s.", "mixing buffer");
                return false;
            }
    }
    else
    {
        ERROR ("No %s was set.", "sound buffer");
        return false;
    }

    DEBUG_SUCCESS ();
    return true;
}

MIXER *player_get_mixer (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    return _Self->mixer;
}

void player_free_mixer (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    DEBUG_BEGIN ();

    if (_Self->mixer)
    {
        mixer_free (_Self->mixer);
        _delete (_Self->mixer);
    }

    if (_Self->smpbuf)
    {
        smpbuf_free (_Self->smpbuf);
        _delete (_Self->smpbuf);
    }

    if (_Self->mixbuf)
    {
        mixbuf_free (_Self->mixbuf);
        _delete (_Self->mixbuf);
    }

    DEBUG_END ();
}

/* Song */

bool player_load_s3m (MUSPLAYER *self, char *name, MUSMOD **_track)
{
    _MUSPLAYER *_Self = self;
    LOADER_S3M *p = load_s3m_new();
    MUSMOD *track;

    if (!p)
    {
        ERROR ("Failed to initialize %s.", "S3M loader");
        return false;
    }
    load_s3m_init(p);

    track = load_s3m_load (p, name, _Self->flags & _MUSPLAYERFL_USE_EM);
    if ((!track) || (!(track->flags & MUSMODFL_LOADED)))
    {
        ERROR ("Failed to load S3M file (%s).", load_s3m_get_error (p));
        // free partially loaded track
        if (track)
            _free_module (track);
        load_s3m_free(p);
        load_s3m_delete(&p);
        return false;
    }

    load_s3m_free(p);
    load_s3m_delete(&p);

    if (!_add_module (track))
    {
        ERROR ("%s", "Failed to register loaded music module.");
        _free_module (track);
        return false;
    }

    *_track = track;

    DEBUG_SUCCESS ();
    return true;
}

bool player_set_active_track (MUSPLAYER *self, MUSMOD *track)
{
    _MUSPLAYER *_Self = self;
    PLAYSTATE *ps = _Self->play_state;

    if (!ps)
    {
        ps = _new (PLAYSTATE);
        if (!ps)
        {
            ERROR ("Failed to allocate %s.", "play state object");
            return false;
        }
        playstate_init (ps);
        _Self->play_state = ps;
    }

    ps->track = track;

    if (!playstate_alloc_channels (ps))
    {
        ERROR ("Failed to allocate %s.", "mixing channels");
        return false;
    }

    return true;
}

PLAYSTATE *player_get_play_state (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    return _Self->play_state;
}

uint8_t player_get_master_volume (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;
    PLAYSTATE *ps = _Self->play_state;

    if (ps)
        return ps->master_volume;
    else
    {
        ERROR ("No %s was set.", "active track");
        return 0;
    }
}

void player_set_master_volume (MUSPLAYER *self, uint8_t value)
{
    _MUSPLAYER *_Self = self;
    PLAYSTATE *ps = _Self->play_state;

    if (ps)
    {
        if (value > MUSMOD_MASTER_VOLUME_MAX)
            value = MUSMOD_MASTER_VOLUME_MAX;
        ps->master_volume = value;
        amptab_set_volume (amptab, value);
    }
    else
        ERROR ("No %s was set.", "active track");
}

void __near player_free_play_state (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    if (_Self->play_state)
    {
        playstate_free (_Self->play_state);
        _delete (_Self->play_state);
    }
}

void player_free_module (MUSPLAYER *self, MUSMOD *track)
{
    _MUSPLAYER *_Self = self;

    DEBUG_BEGIN ();
    if (track)
    {
        PLAYSTATE *ps = _Self->play_state;

        if (ps)
            if (ps->track == track)
                player_free_play_state (self);
        _free_module (track);
    }
    DEBUG_END ();
}

void player_free_modules (MUSPLAYER *self)
{
    DEBUG_BEGIN ();

    player_free_play_state (self);
    _free_modules ();

    DEBUG_END ();
}

/* Finalization */

void player_free (MUSPLAYER *self)
{
    _MUSPLAYER *_Self = self;

    DEBUG_BEGIN ();

    player_play_stop (self);
    player_free_modules (self);
    player_free_device (self);
    interptab_free ();
    voltab_free ();

    if (_Self)
    {
        if (_Self->sndbuf)
        {
            snddmabuf_free (_Self->sndbuf);
            _delete (_Self->sndbuf);
        }

        player_free_mixer (_Self);

        if (_Self->flags & _MUSPLAYERFL_USE_EM)
            free_EM_map_state (&_Self->EM_map_state);
    }

    DEBUG_END ();
}

void player_delete (MUSPLAYER **self)
{
    _delete (self);
}

/*** Initialization ***/

void init_musplay (void)
{
    DEBUG_BEGIN ();
#if DEBUG_WRITE_OUTPUT_STREAM == 1
    DEBUG_open_output_streams ();
#endif  /* DEBUG_WRITE_OUTPUT_STREAM == 1 */
    _isr_index = 0;
    cc_atexit ((void *)done_musplay);
    DEBUG_END ();
}

void done_musplay (void)
{
    DEBUG_BEGIN ();
#if DEBUG_WRITE_OUTPUT_STREAM == 1
    DEBUG_close_output_streams ();
#endif  /* DEBUG_WRITE_OUTPUT_STREAM == 1 */
    DEBUG_END ();
}
