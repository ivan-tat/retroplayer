/* This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/startup.h"
#include "cc/string.h"
#include "cc/i86.h"
#include "cc/io.h"
#include "cc/stdio.h"
#include "cc/stdlib.h"
#include "cc/unistd.h"
#include "cc/sysdbg.h"
#include "debug.h"
#include "tests/testheap.h"

#define CHECK_IO 1
#define NL CRLF

#if CHECK_IO == 1
# define IOCHK _cc_CheckInOutRes ();
#else   /* CHECK_IO != 1 */
# define IOCHK
#endif  /* CHECK_IO != 1 */

#define MAX_BLOCKS 4
struct
{
    FARPTR p;
    uint32_t size;
} blocks[MAX_BLOCKS];

void __noreturn _start_c (void)
{
    init_system ();
    if (!init_heap (0, 0xffff))
    {
        cc_TextWriteString (&cc_Output, "Failed to setup heap.", 0); IOCHK
        cc_TextWriteLn (&cc_Output); IOCHK
        _cc_Exit (1);
    }
    _cc_Exit (main ());
}

void __near dump_heap_info (void)
{
    char s[3][20];
    printf (
        "Heap: heap_ptr=%s, heap_free_list=%s, heap_end=%s" NL,
        SYSDEBUG_dump_heap_ptr (s[0], heap_ptr),
        SYSDEBUG_dump_heap_ptr (s[1], heap_free_list),
        SYSDEBUG_dump_heap_ptr (s[2], heap_end)
    );
}

void __near dump_free_list (void)
{
    char s[2][20];
    struct cc_heap_free_rec_t __far *cur;
    uint32_t free_size, cur_size, mem_avail;

    dump_heap_info ();

    if (IS_FARPTR_BELOW (heap_free_list, heap_org)
    ||  IS_FARPTR_ABOVE (heap_free_list, heap_end))
    {
        printf ("Free list: `%s' is outside the heap!", "heap_free_list");
        exit (1);
    }
    if (IS_FARPTR_BELOW (heap_ptr, heap_org)
    ||  IS_FARPTR_ABOVE (heap_ptr, heap_end))
    {
        printf ("Free list: `%s' is outside the heap!", "heap_ptr");
        exit (1);
    }

    cur = &cc_heap_free_rec;
    free_size = 0;
    while (true)
    {
        cur = cur->next;
        if (cur == heap_ptr)
            cur_size = FARPTR_DIST (heap_end, cur);
        else
            cur_size = cur->size;
        printf (
            "Free list: start=%s, size=%lu, end=%s" NL,
            SYSDEBUG_dump_heap_ptr (s[0], cur),
            (uint32_t) cur_size,
            SYSDEBUG_dump_heap_ptr (s[1], FARPTR_ADD_LONG (cur, cur_size - 1))
        );
        if (cur == heap_ptr)
            break;
        if (!cur->next)
        {
            cur_size = 0;
            printf ("Free list: ERROR: cur->next==0" NL);
            break;
        }
        if (cur->next == heap_end)
        {
            cur_size = 0;
            break;
        }
        if (IS_FARPTR_BELOW (cur->next, heap_org)
        ||  IS_FARPTR_ABOVE (cur->next, heap_end))
        {
            cur_size = 0;
            printf ("Free list: ERROR: cur->next is outside the heap" NL);
            break;
        }
        free_size += cur_size;
    }
    free_size += cur_size;
    printf (
        "Free list: free_size=%lu" NL,
        (uint32_t) free_size
    );
    mem_avail = memavail ();
    if (free_size == mem_avail)
        printf ("Free list: Heap is OK." NL);
    else
        printf (
            "Free list: Heap is BAD: memavail()=%lu, diff.=%lu" NL,
            (uint32_t) mem_avail,
            (uint32_t) free_size - mem_avail
        );
}

void __near init_blocks (void)
{
    int i;

    for (i = 0; i < MAX_BLOCKS; i++)
    {
        blocks[i].p = NULL;
        blocks[i].size = 0;
    }
}

void __near alloc_block (int i, uint32_t size)
{
    blocks[i].p = malloc (size);
    if (!blocks[i].p)
    {
        printf ("Failed to allocate memory!" NL);
        exit (1);
    }
    blocks[i].size = size;
}

void __near free_block (int i)
{
    char s[2][20];
    if (blocks[i].p)
    {
        printf (
            "Freeing user block: start=%s, size=%lu, end=%s" NL,
            SYSDEBUG_dump_heap_ptr (s[0], blocks[i].p),
            (uint32_t) blocks[i].size,
            SYSDEBUG_dump_heap_ptr (s[1], FARPTR_ADD_LONG (blocks[i].p, blocks[i].size - 1))
        );
        free (blocks[i].p);
        blocks[i].p = NULL;
        blocks[i].size = 0;
    }
}

void __near dump_blocks (void)
{
    char s[2][20];
    int i;
    uint32_t used_size, count;

    used_size = 0;
    count = 0;
    for (i = 0; i < MAX_BLOCKS; i++)
    {
        if (blocks[i].p)
        {
            printf (
                "User blocks: start=%s, size=%lu, end=%s" NL,
                SYSDEBUG_dump_heap_ptr (s[0], blocks[i].p),
                (uint32_t) blocks[i].size,
                SYSDEBUG_dump_heap_ptr (s[1], FARPTR_ADD_LONG (blocks[i].p, blocks[i].size - 1))
            );
            used_size += blocks[i].size;
            count++;
        }
    }
    printf (
        "User blocks: count=%lu, used_size=%lu" NL,
        (uint32_t) count,
        (uint32_t) used_size
    );
}

void __near dump_heap (void)
{
    dump_blocks ();
    dump_free_list ();
}

void __near test_alloc (uint32_t size)
{
    printf ("Testing allocation of %lu bytes blocks..." NL, (uint32_t) size);
    alloc_block (0, size);
    alloc_block (1, size);
    alloc_block (2, size);
    alloc_block (3, size);
    dump_heap ();
    free_block (0); dump_heap ();
    free_block (2); dump_heap ();
    free_block (1); dump_heap ();
    free_block (3); dump_heap ();
}

int main (void)
{
    printf ("memavail()=%lu" NL, (uint32_t) memavail ());
    printf ("maxavail()=%lu" NL, (uint32_t) maxavail ());
    dump_free_list ();
    init_blocks ();
    test_alloc (8 - sizeof (uint32_t));
    test_alloc (1024);
    printf ("Done." NL);
    return 0;
}
