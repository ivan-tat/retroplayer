/* strutils.h - declarations for "strutils.c".

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _STRUTILS_H_INCLUDED
#define _STRUTILS_H_INCLUDED 1

#include <stdint.h>
#include "defines.h"

void __far __pascal strpastoc (char __far *dest, char const __far *src, uint16_t maxlen);
void __far __pascal strctopas (char __far *dest, char const __far *src, uint16_t maxlen);

#endif  /* !_STRUTILS_H_INCLUDED */
