/* hwowner.c -- generic hardware resource owner library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/dos.h"
#include "cc/stdlib.h"
#include "cc/string.h"
#include "common.h"
#include "debug.h"
#include "hw/hwowner.h"

#pragma pack(push, 1);
typedef struct hwowner_t HWOWNERITEM;
typedef struct hwowner_t
{
    HWOWNERID id;
    const char *name;
};
#pragma pack(pop);

#define MAX_ITEMS 3

#pragma pack(push, 1);
typedef struct hwowner_entry_t HWOWNERENT;
typedef struct hwowner_entry_t
{
    HWOWNERENT *next;
    HWOWNERITEM *items[MAX_ITEMS];
};  // size of entry is 16 bytes
#pragma pack(pop);

#pragma pack(push, 1);
typedef struct hwowners_list_t HWOWNERSLIST;
typedef struct hwowners_list_t
{
    HWOWNERENT *root;
};
#pragma pack(pop);

#define OWNER ((HWOWNERITEM *)owner)

static HWOWNERID _hwowner_id;
static HWOWNERSLIST _hwowners_list;

/* Entry */

static void __near _entry_clear(HWOWNERENT *self)
{
    if (self)
        memset(self, 0, sizeof(HWOWNERENT));
}

static int __near _entry_find_item(HWOWNERENT *self, HWOWNERITEM *item)
{
    int i;

    for (i = 0; i < MAX_ITEMS; i++)
        if (self->items[i] == item)
            return i;

    return -1;
}

static int __near _entry_find_item_by_id(HWOWNERENT *self, HWOWNERID id)
{
    int i;

    for (i = 0; i < MAX_ITEMS; i++)
    {
        HWOWNERITEM *item = self->items[i];

        if (item && (item->id == id))
            return i;
    }

    return -1;
}

static bool __near _entry_is_empty(HWOWNERENT *self)
{
    int i;

    for (i = 0; i < MAX_ITEMS; i++)
        if (self->items[i])
            return false;

    return true;
}

/* List */

static void __near _list_clear(HWOWNERSLIST *self)
{
    if (self)
        self->root = NULL;
}

static HWOWNERENT *__near _list_find_item(HWOWNERSLIST *self, HWOWNERITEM *item, int *index)
{
    if (self)
    {
        HWOWNERENT *entry = self->root;

        while (entry)
        {
            int i = _entry_find_item (entry, item);

            if (i >= 0)
            {
                *index = i;
                return entry;
            }
            entry = entry->next;
        }
    }

    return NULL;
}

static HWOWNERENT *__near _list_find_item_by_id(HWOWNERSLIST *self, HWOWNERID id, int *index)
{
    if (self)
    {
        HWOWNERENT *entry = self->root;

        while (entry)
        {
            int i = _entry_find_item_by_id (entry, id);

            if (i >= 0)
            {
                *index = i;
                return entry;
            }
            entry = entry->next;
        }
    }

    return NULL;
}

static bool __near _list_add_item(HWOWNERSLIST *self, HWOWNERITEM *item)
{
    int i;

    if (self)
    {
        HWOWNERENT *entry = _list_find_item (self, NULL, &i);

        if (entry)
        {
            entry->items[i] = item;
            return true;
        }
        else
        {
            entry = _new(HWOWNERENT);
            if (entry)
            {
                _entry_clear(entry);
                entry->next = self->root;
                entry->items[0] = item;
                self->root = entry;
                return true;
            }
        }
    }

    return false;
}

static bool __near _list_remove_item(HWOWNERSLIST *self, HWOWNERITEM *item)
{
    if (self)
    {
        HWOWNERENT *prev = NULL;
        HWOWNERENT *entry = self->root;

        while (entry)
        {
            int i = _entry_find_item (entry, item);

            if (i >= 0)
            {
                entry->items[i] = NULL;
                if (_entry_is_empty(entry))
                {
                    if (prev)
                        prev->next = entry->next;
                    else
                        self->root = entry->next;

                    _delete(entry);
                }
                return true;
            }

            prev = entry;
            entry = entry->next;
        }
    }

    return false;
}

static void __near _list_free(HWOWNERSLIST *self)
{
    if (self)
        while (self->root)
        {
            HWOWNERENT *entry = self->root;
            int i;

            for (i = 0; i < MAX_ITEMS; i++)
            {
                HWOWNERITEM *item = entry->items[i];

                if (item)
                    _delete(item);
            }
            self->root = entry->next;
            _delete(entry);
        }
}

/* Entry */

void hwowner_init(HWOWNERITEM *self, const char *name)
{
    if (self)
    {
        self->id = ++_hwowner_id;
        self->name = name;
    }
}

HWOWNER *hwowner_register(const char *name)
{
    HWOWNERITEM *owner;

    owner = _new(HWOWNERITEM);
    if (owner)
    {
        hwowner_init(owner, name);
        if (_list_add_item(&_hwowners_list, owner))
        {
            DEBUG_INFO_ ("Registered HW owner \"%s\" with id=0x%04X.", name, owner->id);
            return owner;
        }
    }

    DEBUG_ERR ("Failed to register HW owner.");
    return NULL;
}

HWOWNERID hwowner_get_id(HWOWNER *owner)
{
    if (owner)
        return OWNER->id;
    else
        return 0;
}

const char *hwowner_get_name(HWOWNERID id)
{
    HWOWNERENT *entry;
    int i;

    entry = _list_find_item_by_id(&_hwowners_list, id, &i);
    if (entry)
        return entry->items[i]->name;
    else
        return NULL;
}

void hwowner_unregister(HWOWNER *owner)
{
    if (owner)
    {
        HWOWNERID id = OWNER->id;
        const char *name = OWNER->name;

        _list_remove_item(&_hwowners_list, owner);
        _delete(owner);
        DEBUG_INFO_ ("Unregistered HW owner \"%s\" with id=0x%04X.", name, id);
    }
}

/*** Initialization ***/

void init_hwowner (void)
{
    DEBUG_BEGIN ();
    _hwowner_id = 0;
    _list_clear(&_hwowners_list);
    cc_atexit ((void *)done_hwowner);
    DEBUG_END ();
}

void done_hwowner (void)
{
    DEBUG_BEGIN ();
    _list_free(&_hwowners_list);
    DEBUG_END ();
}
