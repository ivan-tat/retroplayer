/* hwowner.h -- declarations for hwowner.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _HWOWNER_H_INCLUDED
#define _HWOWNER_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

/* Hardware resource owner */

typedef uint16_t hw_owner_id_t;
typedef hw_owner_id_t HWOWNERID;
typedef void HWOWNER;

HWOWNER    *hwowner_register(const char *name);
HWOWNERID   hwowner_get_id(HWOWNER *owner);
const char *hwowner_get_name(HWOWNERID id);
void        hwowner_unregister(HWOWNER *owner);

/*** Initialization ***/

void init_hwowner (void);
void done_hwowner (void);

#endif  /* !_HWOWNER_H_INCLUDED */
