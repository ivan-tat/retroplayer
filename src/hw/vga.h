/* vga.h -- declarations for vga.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _VGA_H_INCLUDED
#define _VGA_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

extern uint16_t SegA000;

void vga_clear_page_320x200x8 (void *buf, char color);

extern void __far __pascal vga_line (void *buf, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint8_t color);
extern void __far __pascal vga_bar (void *buf, uint16_t o, uint16_t b, uint16_t l);

#ifdef __WATCOMC__
# pragma aux vga_line modify [ ax bx cx dx si di es ];
# pragma aux vga_bar modify [ ax bx cx dx si di es ];
#endif  /* __WATCOMC__ */

/*** Initialization ***/

void init_vga (void);
void done_vga (void);

#endif  /* !_VGA_H_INCLUDED */
