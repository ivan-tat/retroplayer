/* sndctl_t.h -- type declarations for sndctl.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _SNDCTL_T_H_INCLUDED
#define _SNDCTL_T_H_INCLUDED 1

#include <stdbool.h>
#include <stdint.h>
#include "defines.h"

/* Hardware sample format */

typedef uint8_t HWSMPFMTFLAGS;

#define HWSMPFMTFL_BITS_MASK 0x7f
#define HWSMPFMTFL_SIGNED    0x80

typedef struct hwSampleFormat_t {
    HWSMPFMTFLAGS flags;
    uint8_t channels;
};
typedef struct hwSampleFormat_t HWSMPFMT;

bool     set_sample_format (HWSMPFMT *p, uint8_t bits, bool sign, uint8_t channels);
uint8_t  get_sample_format_bits (HWSMPFMT *p);
bool     is_sample_format_signed (HWSMPFMT *p);
uint8_t  get_sample_format_channels (HWSMPFMT *p);
uint16_t get_sample_format_width (HWSMPFMT *p);
void     clear_sample_format (HWSMPFMT *p);

#if DEBUG == 1
void _DEBUG_dump_hwsmpfmt_info (const char *file, int line, const char *func, const char *name, HWSMPFMT *p);
# define DEBUG_dump_hwsmpfmt_info(name, p) _DEBUG_dump_hwsmpfmt_info (__FILE__, __LINE__, __func__, name, p)
#else
# define DEBUG_dump_hwsmpfmt_info(name, p)
#endif  /* DEBUG != 1 */

typedef void SoundHWISRCallback_t (void *param);

#endif  /* !_SNDCTL_T_H_INCLUDED */
