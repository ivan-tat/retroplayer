/* vga.c -- simple VGA library.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdint.h>
#include "defines.h"
#include "cc/stdlib.h"
#include "cc/string.h"
#include "debug.h"
#include "cc/hw/vbios.h"
#include "hw/vga.h"

#if DEFINE_LOCAL_DATA

uint16_t SegA000 = 0xa000;

#endif /* DEFINE_LOCAL_DATA */

void vga_clear_page_320x200x8 (void *buf, char color)
{
    memset (buf, color, 320*200);
}

/*** Initialization ***/

void init_vga (void)
{
    DEBUG_BEGIN ();
    cc_atexit ((void *)done_vga);
#if !DEFINE_LOCAL_DATA
    SegA000 = 0xa000;
#endif /* !DEFINE_LOCAL_DATA */
    DEBUG_END ();
}

void done_vga (void)
{
    DEBUG_BEGIN ();
    DEBUG_END ();
}
