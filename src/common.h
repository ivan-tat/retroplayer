/* common.h -- declarations for common.c.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#pragma once

#ifndef _COMMON_H_INCLUDED
#define _COMMON_H_INCLUDED

#include <stddef.h>
#include <stdint.h>
#include "defines.h"

void *__new (uint32_t size);
void  __delete(void **p);

void __copy_vmt (void **dst, void **src, unsigned n);

void *__far __pascal _new_ (uint32_t size);
void  __far __pascal _delete_ (void **p);

#define _new(t)     (t *)__new(sizeof(t))
#define _delete(p)  __delete((void **)&(p))

#define _copy_vmt(dst, src, type) __copy_vmt ((void **) & (dst->__vmt), (void **) & src, sizeof (type) / sizeof (void *))

#endif  /* !_COMMON_H_INCLUDED */
