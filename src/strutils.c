/* strutils.c - string utilities.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. */

#include <stdint.h>
#include "defines.h"
#include "cc/i86.h"
#include "cc/string.h"
#include "cc/pascal.h"
#include "strutils.h"

void __far __pascal strpastoc (char __far *dest, char const __far *src, uint16_t maxlen)
{
    if (maxlen)
    {
        uint16_t len = src[0];

        if (len > maxlen - 1)
            len = maxlen - 1;

        if (len)
            memcpy (dest, (const void *)(src + 1), len);

        if (maxlen - len)
            memset ((void *)(dest + len), 0, maxlen - len);
    }
}

void __far __pascal strctopas (char __far *dest, char const __far *src, uint16_t maxlen)
{
    if (maxlen)
    {
        char const *endptr = src;
        uint16_t len;

        if (maxlen > pascal_String_size)
            maxlen = pascal_String_size;

        while (endptr[0])
            endptr++;
        len = endptr - src;

        if (len > maxlen - 1)
            len = maxlen - 1;

        if (len)
            memcpy (dest + 1, (const void *)src, len);

        dest[0] = len;
        len++;

        if (maxlen - len)
            memset ((void *)(dest + len), 0, maxlen - len);
    }
}
