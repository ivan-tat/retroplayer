(* debug.pas -- Pascal declarations for "debug" library.

   This file is for linking compiled object files with Pascal linker.
   It will be deleted in future when we rewrite the project in C.

   This is free and unencumbered software released into the public domain.
   For more information, please refer to <http://unlicense.org>. *)

unit
    debug;

interface

(*$I defines.pas*)

procedure _DEBUG_LOG;
procedure _DEBUG_BEGIN;
procedure _DEBUG_END;
procedure _DEBUG_SUCCESS;
procedure _DEBUG_dump_bool;
procedure _DEBUG_dump_s8;
procedure _DEBUG_dump_u8;
procedure _DEBUG_dump_u16;
procedure _DEBUG_dump_u32;
procedure _DEBUG_dump_ptr;
procedure _DEBUG_dump_str;
procedure _DEBUG_dump_mem;

procedure init_debug;

implementation

uses
    i86,
    string_,
    commdbg,
    stdio,
    stdlib,
    conio;

(*$ifdef DEFINE_LOCAL_DATA*)

var
    debuglogfile: file;

(*$endif*)  (* DEFINE_LOCAL_DATA *)

(*$L debug.obj*)
procedure _DEBUG_LOG; external;
procedure _DEBUG_BEGIN; external;
procedure _DEBUG_END; external;
procedure _DEBUG_SUCCESS; external;
procedure _DEBUG_dump_bool; external;
procedure _DEBUG_dump_s8; external;
procedure _DEBUG_dump_u8; external;
procedure _DEBUG_dump_u16; external;
procedure _DEBUG_dump_u32; external;
procedure _DEBUG_dump_ptr; external;
procedure _DEBUG_dump_str; external;
procedure _DEBUG_dump_mem; external;

procedure init_debug; external;

end.
