#!/bin/bash -e
#
# This script launches test environment.
#
# This is free and unencumbered software released into the public domain.
# For more information, please refer to <http://unlicense.org>.
#
# Supported environments:
#   * GNU on Linux, FreeBSD etc.
#
# Debian packages used: coreutils, bash, dialog | whiptail.

DOSEMU_CONF=~/.dosemu/dosemu.conf

declare -A tmp_files
declare DIALOG
declare tmp
declare tool

msg_err_begin() {
    echo -en '\E[1;31m'
}

msg_err_end() {
    echo -en '\E[0m'
}

exit_on_errors() {
    local -i i
    local -i N
    N=${#errors[@]}
    if [[ $N -ne 0 ]]; then
        msg_err_begin >&2
        i=0
        while [[ $i -lt $N ]]; do
            echo "$BASH_SOURCE: Error ($((i+1))): ${errors[$i]}" >&2
            let i=i+1
        done
        echo -n "$BASH_SOURCE: " >&2
        if [[ $N -eq 1 ]]; then
            echo -n "$N error occured." >&2
        else
            echo -n "$N errors occured." >&2
        fi
        echo " Stopped." >&2
        msg_err_end >&2
        exit 1
    fi
}

# $1 = message (optional)
error_exit() {
    if [[ -n "$1" ]]; then
        msg_err_begin >&2
        echo "$1" >&2
        msg_err_end >&2
    fi
    exit 1
}

# $1 = temporary file name
new_tmp_file() {
    tmp_files[$1]=f
}

# $1 = temporary directory name
new_tmp_dir() {
    tmp_files[$tmp]=d
}

# $1 = temporary file or directory name
free_tmp() {
    unset tmp_files[$1]
}

# $1 = temporary file name
rm_tmp_file() {
    rm -f "$1"
    free_tmp "$1"
}

# $1 = temporary directory name
rm_tmp_dir() {
    rm -rf "$1"
    free_tmp "$1"
}

rm_all_tmps() {
    local OFS
    local t
    if [[ ${#tmp_files[*]} -gt 0 ]]; then
        OFS="$IFS"
        IFS=`echo -en "\t"`
        for t in ${!tmp_files[*]}; do
            case ${tmp_files[$t]} in
            f)
                rm_tmp_file "$t"
                ;;
            d)
                rm_tmp_dir "$t"
                ;;
            esac
        done
        IFS="$OFS"
    fi
}

OnError() {
    local err=$?
    msg_err_begin >&2
    echo "$BASH_SOURCE: Error on line $BASH_LINENO: Exit status $err. Stopped." >&2
    msg_err_end >&2
    exit $err
}

OnExit() {
    rm_all_tmps
}

trap OnError ERR
trap OnExit EXIT

DIALOG="`which dialog`" || true
if [[ x"$DIALOG" == x ]]; then
    DIALOG=`which whiptail` || true
fi
if [[ x"$DIALOG" == x ]]; then
    add_error 'Error: no "dialog" nor "whiptail" found.'
    exit 1
fi

tmp=`mktemp`
new_tmp_file "$tmp"
"$DIALOG" --notags --menu 'Select tool to launch' 12 40 3 \
    dosbox '[good] DOSBox' \
    dosemu '[bad] DOSEMU in a window' \
    dosemu_dumb '[bad] DOSEMU in a dumb mode' 2> "$tmp" || true
read tool < "$tmp" || true
rm_tmp_file "$tmp"

case "$tool" in
dosbox)
    OLDPWD="$PWD"
    cd scripts/dosbox
    ./test
    cd "$OLDPWD"
    ;;
dosemu)
    dosemu
    ;;
dosemu_dumb)
    dosemu -t
    ;;
*)
    echo 'Cancelled.' >&2
    ;;
esac

echo 'Done.' >&2
